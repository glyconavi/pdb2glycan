package org.glycoinfo.PDB2Glycan.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.HashMap;

import org.glycoinfo.PDB2Glycan.structure.Atom;
import org.glycoinfo.PDB2Glycan.structure.Bond;
import org.glycoinfo.PDB2Glycan.structure.Mol;


public class CCSuperposition {

	//低分子を重ね合わせることを目的としているクラス
	public static ArrayList<String> getThreeNearestAtomIds(String atomid,Mol mol,ArrayList<Atom> existing_atoms){
		HashSet<String> existing_string = new HashSet<>();
		for(Atom a:existing_atoms) {
			existing_string.add(a.getAtom_site_label_atom_id());
		}
		
		HashMap<String,Integer> atommap = new HashMap<>();
		int numatoms = mol.getAtoms().size();
		int targetid = -1;
		for(int ii = 0;ii < numatoms;ii++) {
			if(atommap.containsKey(mol.getAtoms().get(ii).getAtom_site_label_atom_id())) {
				throw new RuntimeException("Atom id duplication in mol."+mol.getAtoms().get(ii).getAtom_site_label_atom_id());
			}
			atommap.put(mol.getAtoms().get(ii).getAtom_site_label_atom_id(),ii);
			
			if(mol.getAtoms().get(ii).getAtom_site_label_atom_id().equals(atomid)) {
				targetid = ii;
			}
		}
		if(targetid < 0) {
			new RuntimeException("?? atom_id was not found in Mol.");
		}
		double[][] distance = new double[numatoms][numatoms];
		
		for(int ii = 0;ii < numatoms;ii++) {
			for(int jj = 0;jj < numatoms;jj++) {
				if(ii == jj) {
					distance[ii][jj] = 0.0;
				}
				distance[ii][jj] = Double.POSITIVE_INFINITY;
			}	
		}
		
		for(Bond b:mol.getBonds()) {
			Atom a1 = b.getFromAtom();
			Atom a2 = b.getToAtom();
			double ddist = a1.distance(a2);
			
			distance[atommap.get(a1.getAtom_site_label_atom_id())][atommap.get(a2.getAtom_site_label_atom_id())] = ddist;
			distance[atommap.get(a2.getAtom_site_label_atom_id())][atommap.get(a1.getAtom_site_label_atom_id())] = ddist;
		}
		
		//https://ja.wikipedia.org/wiki/%E3%83%AF%E3%83%BC%E3%82%B7%E3%83%A3%E3%83%AB%E2%80%93%E3%83%95%E3%83%AD%E3%82%A4%E3%83%89%E6%B3%95
		for(int kk = 0;kk < numatoms;kk++) {
			for(int ii = 0;ii < numatoms;ii++) {
				for(int jj = 0;jj < numatoms;jj++) {
					if(distance[ii][jj] > distance[ii][kk]+distance[kk][jj]) {
						distance[ii][jj] =  distance[ii][kk]+distance[kk][jj];
					}
				}
			}
		}
		
		for(int jj = 0;jj < numatoms;jj++) {//必要ないはず。。
				for(int ii = 0;ii < numatoms;ii++) {
					if(distance[ii][jj] > distance[jj][ii]) {
						distance[ii][jj] = distance[jj][ii];
					}	
			}
		}
		
		ArrayList<Double> dal = new ArrayList<>();
		for(int ii = 0;ii < numatoms;ii++) {
			if(ii == targetid) {
				continue;
			}
			if(existing_string.contains(mol.getAtoms().get(ii).getAtom_site_label_atom_id())){
				if(distance[targetid][ii] < Double.POSITIVE_INFINITY) {
					dal.add(distance[targetid][ii]);
				}
			}
		}
		
		if(dal.size() < 3) {
			//原子が 3 つ残らない
			return null;
		}
		
		//ソートのクラスを作るのが面倒くさいので適当。
		Collections.sort(dal);
		ArrayList<String> ret = new ArrayList<>();
		HashSet<String> ret_chk = new HashSet<>();
		outer:for(int dd = 0;dd < 3;dd++) {
			double td = dal.get(dd);
			for(int ii = 0;ii < numatoms;ii++) {
				if(ii == targetid) {
					continue;
				}
				if(!existing_string.contains(mol.getAtoms().get(ii).getAtom_site_label_atom_id())){
					continue;
				}
				if(ret_chk.contains(mol.getAtoms().get(ii).getAtom_site_label_atom_id())) {
					continue;
				}
				if(distance[targetid][ii] == td){
					ret.add(mol.getAtoms().get(ii).getAtom_site_label_atom_id());
					ret_chk.add(mol.getAtoms().get(ii).getAtom_site_label_atom_id());
					if(ret.size() >= 3) {
						break outer;
					}
				}
			}
		}
		if(ret.size() < 3) {
			throw new RuntimeException("??? error in code.");
		}
		return ret;
		
	}
}

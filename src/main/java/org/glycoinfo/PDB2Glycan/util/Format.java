package org.glycoinfo.PDB2Glycan.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import javax.vecmath.Point3d;

import org.glycoinfo.PDB2Glycan.io.model.Protein;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxBranchScheme;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranch;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchDescriptor;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchLink;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchList;
import org.glycoinfo.PDB2Glycan.structure.Atom;
import org.glycoinfo.PDB2Glycan.structure.Bond;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.atomtype.CDKAtomTypeMatcher;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomType;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.io.SDFWriter;

public class Format {

  // added. 6.Dec.2022.
  public static IAtomContainer iatomcontainer(LinkedList<Atom> atoms, LinkedList<Bond> bonds, String id) {
    IAtomContainer mol = new AtomContainer();
    for(Atom at : atoms) {
      IAtom atm = new org.openscience.cdk.Atom();
      atm.setSymbol(at.getAtom_site_type_symbol());
      Double x = Double.valueOf(String.format("%10.4f", at.getX()));
      Double y = Double.valueOf(String.format("%10.4f", at.getY()));
      Double z = Double.valueOf(String.format("%10.4f", at.getZ()));
      atm.setPoint3d(new Point3d(x, y, z));
      mol.addAtom(atm);
      mol.setTitle(id);
    }
    for (Bond bd : bonds) {
      int b_from = bd.getFromAtom().getCtfile_id();
      int b_to = bd.getToAtom().getCtfile_id();
      int order_num = bd.getBondOrder();
      IBond.Order order = IBond.Order.SINGLE;
      if (order_num == 2) {
    	  order = IBond.Order.DOUBLE;
      } else if (order_num == 3) {
    	  order = IBond.Order.TRIPLE;
      } else if (order_num == 4) {
    	  order = IBond.Order.QUADRUPLE;
      } else if (order_num == 5) {
    	  order = IBond.Order.QUINTUPLE;
      } else if (order_num == 6) {
    	  order = IBond.Order.SEXTUPLE;
      } else {
    	  if (order_num != 1) {
    	      order = IBond.Order.UNSET;
    	  }
      }
      org.openscience.cdk.interfaces.IBond bond = 
    		  new org.openscience.cdk.Bond(mol.getAtom(b_from - 1), mol.getAtom(b_to - 1), order);
      mol.addBond(bond);
    }

    CDKAtomTypeMatcher matcher = CDKAtomTypeMatcher.getInstance(mol.getBuilder());
    for (IAtom atom : mol.atoms()) {
      try {
        IAtomType type = matcher.findMatchingAtomType(mol, atom);
        atom.setImplicitHydrogenCount(type.getFormalNeighbourCount() - mol.getConnectedBondsCount(atom));
      } catch (CDKException e) {
        e.printStackTrace();
      }
    }

    return mol;
  }

  // added. 12.Dec.2022
  public static String toSDF(IAtomContainer mol) {
	String sdf = "";
	try {
	  StringWriter sw = new StringWriter();
	  SDFWriter writer = new SDFWriter(sw);
	  writer.write(mol);
	  writer.close();
	  sdf = sw.toString();
	  sw.close();
	} catch (CDKException c) {
	  c.printStackTrace();
	} catch (IOException ioe) {
	  ioe.printStackTrace();
	}
	return sdf;
  }

  // modified and added. 10.Jan.2023. 23.Jan.2023
  public static String glycoMmcifByEachEntry(ArrayList<Atom> atoms, Protein protein, ArrayList<String> allAsymIds, HashMap<String, ArrayList<Atom>> proBindRes) {
    String pdbCode = protein.getPdbCode();
    String glycoMmcif = "data_" + pdbCode.toUpperCase() + "\n";
    glycoMmcif += "#\n";
    glycoMmcif += "_entry.id " + pdbCode.toUpperCase() + "\n";
    glycoMmcif += "#\n";
    glycoMmcif = atom_site(glycoMmcif);
    String saccharideAsymId = "";
    String saccharideEntityId = "";
    for (Atom atom : atoms) {
      if (atom.getGroup_PDB() == null)
        continue;
      if (atom.getGroup_PDB().equals("ATOM")) {
          String atomInfo = atom.getAtom_site_auth_comp_id() + atom.getAtom_site_auth_seq_id() + atom.getAtom_site_auth_asym_id();
          if (proBindRes.get(atomInfo) != null) {
        	ArrayList<Atom> proaList = proBindRes.get(atomInfo);
        	for (Atom aatom : proaList) {
        	  glycoMmcif = atom_site_contents(glycoMmcif, aatom);
            }
        	continue;
          }
      }
      if (saccharideAsymId == "")
        saccharideAsymId = atom.getAtom_site_label_asym_id();
      if (saccharideEntityId == "")
      	saccharideEntityId = atom.getAtom_site_label_entity_id();
      glycoMmcif = atom_site_contents(glycoMmcif, atom);
    }
    glycoMmcif += "#\n";
    // _pdbx_branch_scheme.
	if (protein.getPdbxBranchScheme().size() > 0) {
      boolean dataHit = false;
	  for (PdbxBranchScheme branch : protein.getPdbxBranchScheme()) {
        if (! branch.getAsym_id().equals(saccharideAsymId))
		  continue;
        dataHit = true;
	  }
	  if (dataHit == true) {
        glycoMmcif = pdbx_branch_scheme(glycoMmcif);
	    for (PdbxBranchScheme branch : protein.getPdbxBranchScheme()) {
		  if (! branch.getAsym_id().equals(saccharideAsymId))
		    continue;
		  glycoMmcif = pdbx_branch_scheme_contents(glycoMmcif, branch);
        }
        glycoMmcif += "#\n";
	  }
	}
	// _pdbx_entity_branch.
	if (protein.getPdbxEntityBranch().size() > 0) {
      boolean dataHit = false;
      for (PdbxEntityBranch entity : protein.getPdbxEntityBranch()) {
        if ( ! entity.getEntity_id().equals(saccharideEntityId))
		  continue;
        dataHit = true;
      }
      if (dataHit == true) {
	    glycoMmcif = pdbx_entity_branch(glycoMmcif);
	    for (PdbxEntityBranch entity : protein.getPdbxEntityBranch()) {
		  if ( ! entity.getEntity_id().equals(saccharideEntityId))
		    continue;
	      glycoMmcif += entity.getEntity_id() + " " + entity.getType() + "\n";
	    }
	    glycoMmcif += "#\n";
      }
	}
	
	// _pdbx_entity_branch_descriptor
	if (protein.getPdbxEntityBranchDescriptor().size() > 0) {
      boolean dataHit = false;
      for (PdbxEntityBranchDescriptor desc : protein.getPdbxEntityBranchDescriptor()) {
        if ( ! desc.getEntity_id().equals(saccharideEntityId))
          continue;
        dataHit = true;
      }
      if (dataHit == true) {
        glycoMmcif = pdbx_entity_branch_descriptor(glycoMmcif);
	    for (PdbxEntityBranchDescriptor desc : protein.getPdbxEntityBranchDescriptor()) {
  	      if ( ! desc.getEntity_id().equals(saccharideEntityId))
		    continue;
          glycoMmcif = pdbx_entity_branch_descriptor_contents(glycoMmcif, desc);
	    }
		glycoMmcif += "#\n";
	  }
    }
	// _pdbx_entity_branch_link
	if (protein.getPdbxEntityBranchLink().size() > 0) {
      boolean dataHit = false;
      for (PdbxEntityBranchLink link : protein.getPdbxEntityBranchLink()) {
        if ( ! link.getEntity_id().equals(saccharideEntityId))
          continue;
        dataHit = true;
      }
      if (dataHit == true) {
        glycoMmcif = pdbx_entity_branch_link(glycoMmcif);
	    for (PdbxEntityBranchLink link : protein.getPdbxEntityBranchLink()) {
		  if ( ! link.getEntity_id().equals(saccharideEntityId))
		    continue;
          glycoMmcif = pdbx_entity_branch_link_contents(glycoMmcif, link);
	    }
		glycoMmcif += "#\n";
      }
    }
	// _pdbx_entity_branch_list
	if (protein.getPdbxEntityBranchList().size() > 0) {
      boolean dataHit = false;
      for (PdbxEntityBranchList list : protein.getPdbxEntityBranchList()) {
        if ( ! list.getEntity_id().equals(saccharideEntityId))
          continue;
        dataHit = true;
      }
      if (dataHit == true) {
        glycoMmcif = pdbx_entity_branch_list(glycoMmcif);
	    for (PdbxEntityBranchList list : protein.getPdbxEntityBranchList()) {
		  if ( ! list.getEntity_id().equals(saccharideEntityId))
		    continue;
          glycoMmcif = pdbx_entity_branch_list_contents(glycoMmcif, list);
	    }
		glycoMmcif += "#\n";
      }
    }
	return glycoMmcif;
  }

  private static String atom_site(String glycoMmcif) {
    glycoMmcif += "loop_\n";
    glycoMmcif += "_atom_site.group_PDB\n";
    glycoMmcif += "_atom_site.id\n";
    glycoMmcif += "_atom_site.type_symbol\n";
    glycoMmcif += "_atom_site.label_atom_id\n";
    glycoMmcif += "_atom_site.label_alt_id\n";
    glycoMmcif += "_atom_site.label_comp_id\n";
    glycoMmcif += "_atom_site.label_asym_id\n";
    glycoMmcif += "_atom_site.label_entity_id\n";
    glycoMmcif += "_atom_site.label_seq_id\n";
    glycoMmcif += "_atom_site.pdbx_PDB_ins_code\n";
    glycoMmcif += "_atom_site.Cartn_x\n";
    glycoMmcif += "_atom_site.Cartn_y\n";
    glycoMmcif += "_atom_site.Cartn_z\n";
    glycoMmcif += "_atom_site.occupancy\n";
    glycoMmcif += "_atom_site.B_iso_or_equiv\n";
    glycoMmcif += "_atom_site.pdbx_formal_charge\n";
    glycoMmcif += "_atom_site.auth_seq_id\n";
    glycoMmcif += "_atom_site.auth_comp_id\n";
    glycoMmcif += "_atom_site.auth_asym_id\n";
    glycoMmcif += "_atom_site.auth_atom_id\n";
    glycoMmcif += "_atom_site.pdbx_PDB_model_num\n";
    return glycoMmcif;
  }
  private static String atom_site_contents(String glycoMmcif, Atom atom) {
    glycoMmcif += atom.getGroup_PDB() + " ";
    glycoMmcif += atom.getAtom_site_id() + " ";
    glycoMmcif += atom.getAtom_site_type_symbol() + " ";
    glycoMmcif += atom.getAtom_site_label_atom_id() + " ";
    if (atom.getAtom_site_label_alt_id() == "" || atom.getAtom_site_label_alt_id() == null) {
      glycoMmcif += ". "; // label_alt_id.
    } else {
      glycoMmcif += atom.getAtom_site_label_alt_id() + " ";
    }
	glycoMmcif += atom.getAtom_site_label_comp_id() + " ";
	glycoMmcif += atom.getAtom_site_label_asym_id() + " ";
	glycoMmcif += atom.getAtom_site_label_entity_id() + " ";
	if (atom.getAtom_site_label_seq_id() == "" || atom.getAtom_site_label_seq_id() == null) {
      glycoMmcif += ". "; // label_seq
	} else {
	  glycoMmcif += atom.getAtom_site_label_seq_id() + " ";
	}
	if (atom.getAtom_site_pdbx_PDB_ins_code() == "" || atom.getAtom_site_pdbx_PDB_ins_code() == null) {
	  glycoMmcif += ". "; // Pdbx_PDB_ins_code
	} else {
	  glycoMmcif += atom.getAtom_site_pdbx_PDB_ins_code() + " ";
	}
	glycoMmcif += String.format("%.4f", atom.getX()) + " ";
	glycoMmcif += String.format("%.4f", atom.getY()) + " ";
	glycoMmcif += String.format("%.4f", atom.getZ()) + " ";
	if (atom.getAtom_site_occupancy() == null) { // double
	  glycoMmcif += ". ";
	} else {
	  glycoMmcif += atom.getAtom_site_occupancy() + " ";
	}
	if (atom.getAtom_site_B_iso_or_equiv() == null) { // double
	  glycoMmcif += ". ";
	} else {
	  glycoMmcif += atom.getAtom_site_B_iso_or_equiv() + " ";
	}
	if (atom.getAtom_site_pdbx_formal_charge() == null) { // int
	  glycoMmcif += ". ";
	} else {
	  glycoMmcif += atom.getAtom_site_pdbx_formal_charge() + " ";
	}
	glycoMmcif += atom.getAtom_site_auth_seq_id()+ " ";
	glycoMmcif += atom.getAtom_site_auth_comp_id()+ " ";
	glycoMmcif += atom.getAtom_site_auth_asym_id() + " ";
	glycoMmcif += atom.getAtom_site_auth_atom_id() + " ";
	if (atom.getAtom_site_pdbx_PDB_model_num() == "" || atom.getAtom_site_pdbx_PDB_model_num() == null) {
	  glycoMmcif += ".\n";
	} else {
	  glycoMmcif += atom.getAtom_site_pdbx_PDB_model_num() + "\n";
	}
    return glycoMmcif;
  }

  private static String pdbx_branch_scheme(String glycoMmcif) {
    glycoMmcif += "loop_\n";
	glycoMmcif += "_pdbx_branch_scheme.asym_id\n";
	glycoMmcif += "_pdbx_branch_scheme.entity_id\n";
	glycoMmcif += "_pdbx_branch_scheme.mon_id\n";
	glycoMmcif += "_pdbx_branch_scheme.num\n";
	glycoMmcif += "_pdbx_branch_scheme.pdb_asym_id\n";
	glycoMmcif += "_pdbx_branch_scheme.pdb_mon_id\n";
	glycoMmcif += "_pdbx_branch_scheme.pdb_seq_num\n";
	glycoMmcif += "_pdbx_branch_scheme.auth_asym_id\n";
	glycoMmcif += "_pdbx_branch_scheme.auth_mon_id\n";
	glycoMmcif += "_pdbx_branch_scheme.auth_seq_num\n";
	glycoMmcif += "_pdbx_branch_scheme.hetero\n";
	return glycoMmcif;
  }
  private static String pdbx_branch_scheme_contents(String glycoMmcif, PdbxBranchScheme branch) {
	glycoMmcif += branch.getAsym_id() + " ";
	glycoMmcif += branch.getEntity_id() + " ";
	glycoMmcif += branch.getMon_id() + " ";
	glycoMmcif += branch.getNum() + " ";
	glycoMmcif += branch.getPdb_asym_id() + " ";
	glycoMmcif += branch.getPdb_mon_id() + " ";
	glycoMmcif += branch.getPdb_seq_num() + " ";
	if (branch.getAuth_asym_id() == "" || branch.getAuth_asym_id() == null) {
	  glycoMmcif += ". ";
	} else {
	  glycoMmcif += branch.getAuth_asym_id() + " ";
	}
	if (branch.getAuth_mon_id() == "" || branch.getAuth_mon_id() == null) {
	  glycoMmcif += ". ";
	} else {
	  glycoMmcif += branch.getAuth_mon_id() + " ";
	}
	if (branch.getAuth_seq_num() == "" || branch.getAuth_seq_num() == null) {
	  glycoMmcif += ". ";
	} else {
	  glycoMmcif += branch.getAuth_seq_num() + " ";
	}
	glycoMmcif += branch.getHetero() + "\n";
    return glycoMmcif;
  }

  private static String pdbx_entity_branch (String glycoMmcif){
    glycoMmcif += "loop_\n";
    glycoMmcif += "_pdbx_entity_branch.entity_id\n";
    glycoMmcif += "_pdbx_entity_branch.type\n";
    return glycoMmcif;
  }

  private static String pdbx_entity_branch_descriptor(String glycoMmcif) {
    glycoMmcif += "loop_\n";
    glycoMmcif += "_pdbx_entity_branch_descriptor.ordinal\n";
    glycoMmcif += "_pdbx_entity_branch_descriptor.entity_id\n";
    glycoMmcif += "_pdbx_entity_branch_descriptor.descriptor\n";
    glycoMmcif += "_pdbx_entity_branch_descriptor.type\n";
    glycoMmcif += "_pdbx_entity_branch_descriptor.program\n";
    glycoMmcif += "_pdbx_entity_branch_descriptor.program_version\n";
    return glycoMmcif;
  }
  private static String pdbx_entity_branch_descriptor_contents(String glycoMmcif, PdbxEntityBranchDescriptor desc) { 
    glycoMmcif += desc.getOrdinal() + " ";
    glycoMmcif += desc.getEntity_id() + " ";
    glycoMmcif += "'" + desc.getDescriptor() + "' ";
    glycoMmcif += "'" + desc.getType() + "' ";
    glycoMmcif += "'" + desc.getProgram() + "' ";
    if (desc.getProgram_version() == "") {
      glycoMmcif += ".\n";
    } else {
      glycoMmcif += desc.getProgram_version() + "\n";
    }
    return glycoMmcif;
  }

  private static String pdbx_entity_branch_link (String glycoMmcif) {
    glycoMmcif += "loop_\n";
    glycoMmcif += "_pdbx_entity_branch_link.link_id\n";
    glycoMmcif += "_pdbx_entity_branch_link.entity_id\n";
    glycoMmcif += "_pdbx_entity_branch_link.entity_branch_list_num_1\n";
    glycoMmcif += "_pdbx_entity_branch_link.comp_id_1\n";
    glycoMmcif += "_pdbx_entity_branch_link.atom_id_1\n";
    glycoMmcif += "_pdbx_entity_branch_link.leaving_atom_id_1\n";
    glycoMmcif += "_pdbx_entity_branch_link.entity_branch_list_num_2\n";
    glycoMmcif += "_pdbx_entity_branch_link.comp_id_2\n";
    glycoMmcif += "_pdbx_entity_branch_link.atom_id_2\n";
    glycoMmcif += "_pdbx_entity_branch_link.leaving_atom_id_2\n";
    glycoMmcif += "_pdbx_entity_branch_link.value_order\n";
    glycoMmcif += "_pdbx_entity_branch_link.details\n";
    return glycoMmcif;
  }
  private static String pdbx_entity_branch_link_contents(String glycoMmcif, PdbxEntityBranchLink link) {
    glycoMmcif += link.getLink_id() + " ";
	glycoMmcif += link.getEntity_id() + " ";
	glycoMmcif += link.getEntity_branch_list_num_1() + " ";
	glycoMmcif += link.getComp_id_1() + " ";
	glycoMmcif += link.getAtom_id_1() + " ";
	glycoMmcif += link.getLeaving_atom_id_1() + " ";
	glycoMmcif += link.getEntity_branch_list_num_2() + " ";
	glycoMmcif += link.getComp_id_2() + " ";
	glycoMmcif += link.getAtom_id_2() + " ";
	glycoMmcif += link.getLeaving_atom_id_2() + " ";
	glycoMmcif += link.getValue_order() + " ";
	if (link.getDetails() == "") {
	  glycoMmcif += ".\n";
	} else {
	  glycoMmcif += link.getDetails() + "\n";
	}
    return glycoMmcif;
  }

  private static String pdbx_entity_branch_list(String glycoMmcif) {
    glycoMmcif += "loop_\n";
    glycoMmcif += "_pdbx_entity_branch_list.entity_id\n";
    glycoMmcif += "_pdbx_entity_branch_list.comp_id\n";
    glycoMmcif += "_pdbx_entity_branch_list.num\n";
    glycoMmcif += "_pdbx_entity_branch_list.hetero\n";
    return glycoMmcif;
  }
  private static String pdbx_entity_branch_list_contents(String glycoMmcif, PdbxEntityBranchList list) {
    glycoMmcif += list.getEntity_id() + " ";
	glycoMmcif += list.getComp_id() + " ";
	glycoMmcif += list.getNum()+ " ";
	glycoMmcif += list.getHetero() + "\n";
    return glycoMmcif;
  }
 
  // modified. 11.Jan.2023
  public static String glycoMmcifByBranchEntry(LinkedList<Atom> atoms, Protein protein, String entityId) {
    String pdbCode = protein.getPdbCode();
	String glycoMmcif = "data_" + pdbCode.toUpperCase() + "\n";
	glycoMmcif += "_entry.id   " + pdbCode.toUpperCase() + "\n";
	glycoMmcif += "#\n";
	glycoMmcif = atom_site(glycoMmcif);
	int atomId = 1;
    for (Atom atom : atoms) {
      String groupPDB = "HETATM";
      if (atom.getComp_id().equals("THR") || atom.getComp_id().equals("SER") || atom.getComp_id().equals("ASN")) {
        groupPDB = "ATOM";
      }
      glycoMmcif += groupPDB + " ";
      glycoMmcif += atomId +  " ";
      atomId++;
      glycoMmcif += atom.getAtom_site_type_symbol() + " ";
      glycoMmcif += atom.getAtom_site_label_atom_id() + " ";
      if (atom.getAtom_site_label_alt_id() != null && atom.getAtom_site_label_alt_id() != null) {
        glycoMmcif += atom.getAtom_site_label_alt_id() + " ";
      } else {
        glycoMmcif += ". "; // label_alt_id.
      }
      glycoMmcif += atom.getComp_id() + " ";
      if (atom.getAtom_site_label_asym_id() != null && atom.getAtom_site_label_asym_id() != null) {
        glycoMmcif += atom.getAtom_site_label_asym_id() + " ";
      } else {
        glycoMmcif += ". ";
      }
      glycoMmcif += entityId + " ";
      if (atom.getAtom_site_label_seq_id() != null && atom.getAtom_site_label_seq_id() != null) {
        glycoMmcif += atom.getAtom_site_label_seq_id() + " ";
      } else {
        glycoMmcif += ". "; // label_seq
      }
      if (atom.getAtom_site_pdbx_PDB_ins_code() != null && atom.getAtom_site_pdbx_PDB_ins_code() != null) {
        glycoMmcif += atom.getAtom_site_pdbx_PDB_ins_code() + " ";
      } else {
        glycoMmcif += ". "; // Pdbx_PDB_ins_code
      }
      glycoMmcif += String.format("%.4f", atom.getX()) + " ";
      glycoMmcif += String.format("%.4f", atom.getY()) + " ";
      glycoMmcif += String.format("%.4f", atom.getZ()) + " ";
      if (atom.getAtom_site_occupancy() != null) {
        glycoMmcif += atom.getAtom_site_occupancy() + " ";
      } else {
    	glycoMmcif += ". ";
      }
      glycoMmcif += ". ";
      glycoMmcif += ". ";
      glycoMmcif += atom.getAtom_site_auth_seq_id()+ " ";
      glycoMmcif += ". ";
      glycoMmcif += ". ";
      glycoMmcif += ". ";
      glycoMmcif += ". \n";
  	}
	glycoMmcif += "#\n";
	// _pdbx_branch_scheme.
	if (protein.getPdbxBranchScheme().size() > 0) {
      boolean dataHit = false;
      for (PdbxBranchScheme branch : protein.getPdbxBranchScheme()) {
        if (! branch.getEntity_id().equals(entityId))
          continue;
        dataHit = true;
      }
      if (dataHit == true) {
	    glycoMmcif = pdbx_branch_scheme(glycoMmcif);
	    for (PdbxBranchScheme branch : protein.getPdbxBranchScheme()) {
		  if (! branch.getEntity_id().equals(entityId))
		    continue;
		  glycoMmcif = pdbx_branch_scheme_contents(glycoMmcif, branch);
        }
		glycoMmcif += "#\n";
      }
	}
	// _pdbx_entity_branch.
	if (protein.getPdbxEntityBranch().size() > 0) {
      boolean dataHit = false;
      for (PdbxEntityBranch entity : protein.getPdbxEntityBranch()) {
        if (! entity.getEntity_id().equals(entityId))
          continue;
        dataHit = true;
      }
      if (dataHit == true) {
        glycoMmcif = pdbx_entity_branch(glycoMmcif);
	    for (PdbxEntityBranch entity : protein.getPdbxEntityBranch()) {
	      if (! entity.getEntity_id().equals(entityId))
		    continue;
	      glycoMmcif += entity.getEntity_id() + " " + entity.getType() + "\n";
	    }
		glycoMmcif += "#\n";
      }
	}
	// _pdbx_entity_branch_descriptor
	if (protein.getPdbxEntityBranchDescriptor().size() > 0) {
      boolean dataHit = false;
      for (PdbxEntityBranchDescriptor desc : protein.getPdbxEntityBranchDescriptor()) {
  	    if (! desc.getEntity_id().equals(entityId))
  		  continue;
  	    dataHit = true;
      }
      if (dataHit == true) {
        glycoMmcif = pdbx_entity_branch_descriptor(glycoMmcif);
	    for (PdbxEntityBranchDescriptor desc : protein.getPdbxEntityBranchDescriptor()) {
	      if (! desc.getEntity_id().equals(entityId))
		    continue;
          glycoMmcif = pdbx_entity_branch_descriptor_contents(glycoMmcif, desc);
	    }
		glycoMmcif += "#\n";
	  }
    }
	// _pdbx_entity_branch_link
	if (protein.getPdbxEntityBranchLink().size() > 0) {
      boolean dataHit = false;
	  for (PdbxEntityBranchLink link : protein.getPdbxEntityBranchLink()) {
        if (! link.getEntity_id().equals(entityId))
          continue;
        dataHit = true;
	  }
	  if (dataHit == true) {
        glycoMmcif = pdbx_entity_branch_link(glycoMmcif);
	    for (PdbxEntityBranchLink link : protein.getPdbxEntityBranchLink()) {
	      if (! link.getEntity_id().equals(entityId))
		    continue;
          glycoMmcif = pdbx_entity_branch_link_contents(glycoMmcif, link);
	    }
		glycoMmcif += "#\n";
      }
    }
	// _pdbx_entity_branch_list
	if (protein.getPdbxEntityBranchList().size() > 0) {
      boolean dataHit = false;
	  for (PdbxEntityBranchList list : protein.getPdbxEntityBranchList()) {
		if (! list.getEntity_id().equals(entityId))
		  continue;
		dataHit = true;
	  }
	  if (dataHit == true) {
        glycoMmcif = pdbx_entity_branch_list(glycoMmcif);
	    for (PdbxEntityBranchList list : protein.getPdbxEntityBranchList()) {
		  if (! list.getEntity_id().equals(entityId))
		    continue;
          glycoMmcif = pdbx_entity_branch_list_contents(glycoMmcif, list);
        }
		glycoMmcif += "#\n";
	  }
    }
	return glycoMmcif;
  }

}

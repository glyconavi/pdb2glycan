package org.glycoinfo.PDB2Glycan.util;

import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2Glycan.structure.Atom;

import static com.google.common.base.Strings.padStart;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;

public class StringUtils {

  public static String formatTime(long milliSeconds) {
//    long msec = milliSeconds % 1000;
//    long sec = milliSeconds / 1000;
//    long min = (sec % 3600) / 60;
//    long hour = sec / 3600;

    return String.format("%2.3f s", (float)milliSeconds / 1000 );
  }

  public static String modelCode(String pdbModelNum) {
    return padStart(pdbModelNum, 7, '0');
  }

  /**
   * 
   * @param obj PdbxStructModResidue
   * @return "***{label_asym_id}###{label_comp_id}###{label_seq_id}***"
   */
  public static String modResidueLabel(PdbxStructModResidue obj) {
    return codeFormat(obj.getLabel_asym_id(), obj.getLabel_comp_id(), obj.getLabel_seq_id());
  }

  /**
   * 
   * @param obj Atom
   * @return "***{atom_site_label_asym_id}###{atom_site_label_comp_id}###{atom_site_label_seq_id}***"
   */
  public static String atomModResidueLabel(Atom obj) {
    return codeFormat(obj.getAtom_site_label_asym_id(), obj.getAtom_site_label_comp_id(),
        obj.getAtom_site_label_seq_id());
  }

  public static String codeFormat(String s1, String s2, String s3) {
    return String.format("***%s###%s###%s***", s1, s2, s3);
  }

  public static String codeFormat(String s1, String s2, String s3, String s4) {
    return String.format("***%s###%s###%s###%s***", s1, s2, s3, s4);
  }

  /**
   * Change camel case to snake case.
   * e.g. AtomSite -> atom_site, PdbxUnobsOrZeroOccAtoms -> pdbx_unobs_or_zero_occ_atoms
   * @param camel
   * @return snake case of the given camel case
   */
  public static String convertCaseCamelToSnake(String camel) {
    StringBuilder snake = new StringBuilder();
    for ( int i=0;i<camel.length(); i++ ) {
      char c = camel.charAt(i);
      if ( Character.isUpperCase(c) ) {
        if ( i!=0 )
          snake.append('_');
        c = Character.toLowerCase(c);
      }
      snake.append(c);
    }
    return snake.toString();
  }

  /**
   * Change snake case to camel case.
   * e.g. atom_site -> AtomSite, pdbx_unobs_or_zero_occ_atoms -> PdbxUnobsOrZeroOccAtoms
   * @param snake
   * @return camel case of the given snake case
   */
  public static String convertCaseSnakeToCamel(String snake) {
    StringBuilder camel = new StringBuilder();
    boolean isHead = false;
    for ( int i=0;i<snake.length(); i++ ) {
      char c = snake.charAt(i);
      if ( c == '_' ) {
        isHead = true;
        continue;
      }
      if ( i==0 || isHead ) {
        c = Character.toUpperCase(c);
        isHead = false;
      }
      camel.append(c);
    }
    return camel.toString();
  }

  public static String urlEncode(String str) throws UnsupportedEncodingException {
    String encoded = URLEncoder.encode(str, "UTF-8");
    // Encode asterisk
    encoded = encoded.replaceAll("\\*", "%2A");
    return encoded;
  }

  public static String getSHA256(String input){
    String toReturn = null;
    try {
      MessageDigest digest = MessageDigest.getInstance("SHA-256");
      digest.reset();
      digest.update(input.getBytes("utf8"));
      toReturn = String.format("%064x", new BigInteger(1, digest.digest()));
    } catch (Exception e) {
      e.printStackTrace();
    }

    return toReturn;
  }

  public static String getSHA512(String input){
    String toReturn = null;
    try {
      MessageDigest digest = MessageDigest.getInstance("SHA-512");
      digest.reset();
      digest.update(input.getBytes("utf8"));
      toReturn = String.format("%0128x", new BigInteger(1, digest.digest()));
    } catch (Exception e) {
      e.printStackTrace();
    }

    return toReturn;
  }
}

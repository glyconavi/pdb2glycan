package org.glycoinfo.PDB2Glycan.util;

import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VDW {
	
  private static final Logger logger = LoggerFactory.getLogger(VDW.class);

	private static HashMap<String,Double> VDWRadii = new HashMap<>();
	static{
		//A Bondi (1964) J Phys Chem 68:441
		VDWRadii.put("H",1.20);
		VDWRadii.put("He",1.40);
		VDWRadii.put("Li",1.82);
		VDWRadii.put("C",1.70);
		VDWRadii.put("N",1.55);
		VDWRadii.put("O",1.52);
		VDWRadii.put("F",1.47);
		VDWRadii.put("Ne",1.54);
		VDWRadii.put("Na",2.27);
		VDWRadii.put("Mg",1.73);
		VDWRadii.put("Si",2.10);
		VDWRadii.put("P",1.80);
		VDWRadii.put("S",1.80);
		VDWRadii.put("Cl",1.75);
		VDWRadii.put("Ar",1.88);
		VDWRadii.put("K",2.75);
		VDWRadii.put("Ni",1.63);
		VDWRadii.put("Cu",1.40);
		VDWRadii.put("Zn",1.39);
		VDWRadii.put("Ga",1.87);
		VDWRadii.put("As",1.85);
		VDWRadii.put("Se",1.90); 
		VDWRadii.put("Br",1.85);
		VDWRadii.put("Kr",2.02);
		VDWRadii.put("Pd",1.63);
		VDWRadii.put("Ag",1.72);
		VDWRadii.put("Cd",1.58);
		VDWRadii.put("In",1.93);
		VDWRadii.put("Sn",2.17);
		VDWRadii.put("Te",2.06);
		VDWRadii.put("I",1.98);
		VDWRadii.put("Xe",2.16);
		VDWRadii.put("Pt",1.75);
		VDWRadii.put("Au",1.66);
		VDWRadii.put("Hg",1.55);
		VDWRadii.put("TI",1.96);
		VDWRadii.put("Pb",2.02);
		VDWRadii.put("U",1.86);
		VDWRadii.put("default",2.0);
	}
	public static double getVDWRadii(String s) {
		if(!VDWRadii.containsKey(s)) {
      logger.warn("VDW radii of " + s + "is not registered. 2.0 is used.");
			return VDWRadii.get("default");
		}
		return  VDWRadii.get(s);
	}
}

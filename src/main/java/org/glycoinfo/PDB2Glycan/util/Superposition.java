package org.glycoinfo.PDB2Glycan.util;

import java.util.ArrayList;

import org.glycoinfo.PDB2Glycan.structure.Location3D;
import org.glycoinfo.PDB2Glycan.structure.Vector3D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Superposition {
	//単純な重ね合わせユーティリティ
	  private static final Logger logger = LoggerFactory.getLogger(Superposition.class);
		public static void adjustVector3D_BioJava(Vector3D targetstart,
			Vector3D targetend,
			Vector3D targetend2,
			Vector3D refstart,
			Vector3D refend,
			Vector3D refend2,
			ArrayList<Vector3D> al){
			javax.vecmath.Point3d[] target = new javax.vecmath.Point3d[3];
			javax.vecmath.Point3d[] ref = new javax.vecmath.Point3d[3];
			target[0] = new javax.vecmath.Point3d(targetstart.getX(),targetstart.getY(),targetstart.getZ());
			target[1] = new javax.vecmath.Point3d(targetend.getX(),targetend.getY(),targetend.getZ());
			target[2] = new javax.vecmath.Point3d(targetend2.getX(),targetend2.getY(),targetend2.getZ());
			
			ref[0] = new javax.vecmath.Point3d(refstart.getX(),refstart.getY(),refstart.getZ());
			ref[1] = new javax.vecmath.Point3d(refend.getX(),refend.getY(),refend.getZ());
			ref[2] = new javax.vecmath.Point3d(refend2.getX(),refend2.getY(),refend2.getZ());
			
			
			javax.vecmath.Matrix4d mat = org.biojava.nbio.structure.geometry.SuperPositions.superposeAndTransform(ref,target);
			for(Vector3D v:al) {
				javax.vecmath.Point3d p = new javax.vecmath.Point3d(v.getX(),v.getY(),v.getZ());
				mat.transform(p);
				v.setCoordinates(p.x,p.y,p.z);
			}
	}
	
	//原点からの距離を返す。
	public static double len(Vector3D a){
		double len = a.getX()*a.getX()+a.getY()*a.getY()+a.getZ()*a.getZ();
		if(len > 0){
			return Math.sqrt(len);
		}
		return 0;
	}
	
	public static double len(double x,double y){
		double len = x*x+y*y;
		if(len > 0){
			return Math.sqrt(len);
		}
		return len;
	}
	
	/**
	 * 
	 * targetstart -> targetend 
	 * というベクトルが
	 * refstart->refend
	 * になるような回転を、al 内のPoint に適用する。
	 * 一度 target のベクトルを X 要素以外ゼロのベクトルに直して、ref のベクトルに合わせる計算を
	 * べた書きしている。
	 * @param targetstart
	 * @param targetend
	 * @param refstart
	 * @param refend
	 * @param al 
	 */
	public static void adjustVector3D(Vector3D targetstart,
			Vector3D targetend,
			Vector3D refstart,
			Vector3D refend,
			ArrayList<Vector3D> al){
		
		
		
		Location3D a = subtracted(targetend,targetstart);
		Location3D b = subtracted(refend,refstart);
		double tx = targetstart.getX();
		double ty = targetstart.getY();
		double tz = targetstart.getZ();
		
		double rx = refstart.getX();
		double ry = refstart.getY();
		double rz = refstart.getZ();
		
		
		
		
		
		double alen = len(a);
		double blen = len(b);
		if(alen <= 0 || blen <= 0){
			for(Vector3D pp:al){
				pp.setX(pp.getX() - tx);
				pp.setY(pp.getY() - ty);
				pp.setZ(pp.getZ() - tz);
				
				pp.setX(pp.getX() + rx);
				pp.setY(pp.getY() + ry);
				pp.setZ(pp.getZ() + rz);
			}
			return;
		}
		
		double a2len = len(a.getX(),a.getY());
		double b2len = len(b.getX(),b.getY());
		
		boolean a2zeroflag = false;
		
		double cost = 0;
		double sint = 0;
		if(a2len > 0){
			cost = a.getX()/a2len;
			sint = a.getY()/a2len;
		}else{
			a2zeroflag = true;
		}
		double costk = a2len/alen;
		double sintk = a.getZ()/alen;
		
		
		boolean b2zeroflag = false;
		double coss = 0;
		double sins = 0;
		if(b2len > 0){
			sins = b.getY()/b2len;
			coss = b.getX()/b2len;
		}else{
			b2zeroflag = true;
		}
		
		double cossk = b2len/blen;
		double sinsk = b.getZ()/blen;
		
		for(Vector3D ppz:al){
			ppz.setX(ppz.getX() - tx);
			ppz.setY(ppz.getY() - ty);
			ppz.setZ(ppz.getZ() - tz);
			
			
			double dddx = ppz.getX();
			double dddy = ppz.getY();
			if(!a2zeroflag){
				dddx = cost*ppz.getX()+sint*ppz.getY();
				dddy = -sint*ppz.getX()+cost*ppz.getY();
			}
			Location3D ppp = new Location3D(
			costk*dddx+sintk*ppz.getZ(),
					dddy,
			-sintk*dddx+costk*ppz.getZ()
			);
			
			
			double zzx = ppp.getX()*cossk-ppp.getZ()*sinsk;
			double zzz = ppp.getZ()*cossk+ppp.getX()*sinsk;
			if(!b2zeroflag){
				ppp.setX(coss*zzx-sins*ppp.getY());
				ppp.setY(sins*zzx+coss*ppp.getY());
			}
			ppp.setZ(zzz);
			
			ppp.setX(ppp.getX() + rx);
			ppp.setY(ppp.getY() + ry);
			ppp.setZ(ppp.getZ() + rz);
			ppz.setCoordinates(ppp.getX(),ppp.getY(),ppp.getZ());
		}
		return;
	}
	
	/**
	 *  targetstart -> targetend 
	 *  targetstart -> targetend2
	 * という面が
	 * refstart->refend
	 * refstart-> refend2
	 * と同じ方向の法線ベクトルを持つようになるような回転を、al 内のPoint に適用する。
	 * 一度 target のベクトルを Z 要素ゼロの面に直して、ref のベクトルに合わせる計算を
	 * べた書きしている。
	 * @param targetstart
	 * @param targetend
	 * @param targetend2
	 * @param refstart
	 * @param refend
	 * @param refend2
	 * @param al 
	 */
	
	public static void adjustVector3D(Vector3D targetstart,
			Vector3D targetend,
			Vector3D targetend2,
			Vector3D refstart,
			Vector3D refend,
			Vector3D refend2,
			ArrayList<Vector3D> al){
		
		
		/*
		 * biojava の superposition を使う場合
		ArrayList<Vector3D> chklist = new ArrayList<>();
		for(Vector3D v:al) {
			chklist.add(new Location3D(v.getX(),v.getY(),v.getZ()));
		}
		adjustVector3D_BioJava(targetstart,targetend,targetend2,refstart,refend,refend2,chklist);
		*/
		
		
		Location3D a = subtracted(targetend,targetstart);
		Location3D b = subtracted(refend,refstart);
		
		double[] ttrot = getRotValues(targetstart,targetend,targetend2);
		double[] rrrot = getRotValues(refstart,refend,refend2);
		double tx = targetstart.getX();
		double ty = targetstart.getY();
		double tz = targetstart.getZ();
		
		double rx = refstart.getX();
		double ry = refstart.getY();
		double rz = refstart.getZ();
		
		
		for(Vector3D ppz:al){
			//原点基準になるように平行移動
			ppz.setX(ppz.getX()- tx);
			ppz.setY(ppz.getY() - ty);
			ppz.setZ(ppz.getZ() - tz);
			
			//回転
			Location3D ppp = new Location3D(ppz.getX(),ppz.getY(),ppz.getZ());
			apply3D(ppp,ttrot,true);
			apply3D(ppp,rrrot,false);
			
			//refstart 点合うように平行移動
			ppp.setX(ppp.getX() + rx);
			ppp.setY(ppp.getY() + ry);
			ppp.setZ(ppp.getZ() + rz);
			
			ppz.setCoordinates(ppp.getX(),ppp.getY(),ppp.getZ());
		}
		
		//biojava の superposition との違いのチェック
		//for(int ii = 0;ii < al.size();ii++) {
    //  logger.info(""+(Vector3D.distance(al.get(ii),chklist.get(ii))));
		//}
	}
	
	
	public static Location3D subtracted(Vector3D a,Vector3D b){
		return new Location3D(a.getX()-b.getX(),a.getY()-b.getY(),a.getZ()-b.getZ());
	}
	public static Location3D added(Vector3D a,Vector3D b){
		return new Location3D(a.getX()+b.getX(),a.getY()+b.getY(),a.getZ()+b.getZ());
	}
	
	public static final int COST  = 0;
	public static final int SINT  = 1;
	public static final int COSTK = 2;
	public static final int SINTK = 3;
	public static final int COSTU = 4;
	public static final int SINTU = 5;
	/**
	 *  X  要素以外ゼロのベクトルを
	 * targetstart-> end というベクトル方向に合わせるための回転要素を計算し返す。
	 * @param targetstart
	 * @param targetend
	 * @return 
	 */
	public static double[] getRotValues(Vector3D targetstart,
			Vector3D targetend){
		Vector3D a = subtracted(targetend,targetstart);
		double cost = 1;
		double sint = 0;
		double costk = 1;
		double sintk = 0;
		double alen = len(a);
		double a2len = len(a.getX(),a.getY());
		if(a2len > 0){
			cost = a.getX()/a2len;
			sint = a.getY()/a2len;
		}else{
			//a2zeroflag = true;
		}
		if(alen > 0){
			costk = a2len/alen;
			sintk = a.getZ()/alen;
		}
		double[] ret = new double[4];
		ret[COST] = cost;
		ret[SINT] = sint;
		ret[COSTK] = costk;
		ret[SINTK] = sintk;
		return ret;
	}
	/**
	 * X,Y 平面（Z 要素がゼロ）を
	 * tst->ten
	 * tst->ten2 という三角形の面と同じ法線を持つよう変換するための回転の要素を返す。
	 * ここで計算した値と変換したい点のリストを apply3D 関数に渡すと変換してくれる。
	 * @param tst
	 * @param ten
	 * @param ten2
	 * @return 
	 */
	
	public static double[] getRotValues(Vector3D tst,
		Vector3D ten,Vector3D ten2){
		double[] rota =getRotValues(tst,ten);
		
		double tx = ten2.getX()-tst.getX();
		double ty = ten2.getY()-tst.getY();
		double tz = ten2.getZ()-tst.getZ();	
		
		double dddx = rota[COST]*tx+rota[SINT]*ty;
		double dddy = -rota[SINT]*tx+rota[COST]*ty;
		double ddx = rota[COSTK]*dddx+rota[SINTK]*tz;
		double ddz = -rota[SINTK]*dddx+rota[COSTK]*tz;
		
		double len3 = len(dddy,ddz);
		
		double costu = 1;
		double sintu = 0;
		if(len3 > 0){
			costu = dddy/len3;
			sintu = ddz/len3;
		}
		double[] ret = new double[6];
		for(int ii = 0;ii < 4;ii++){
			ret[ii] = rota[ii];
		}
		ret[COSTU] = costu;
		ret[SINTU] = sintu;
		return ret;
	}
	
	/**
	 * getRotValues 関数で得た要素を利用し、与えられた点を回転する。
	 * revflag を true にすると、rotvalue を計算するために使用した面の Z 要素ゼロになるような回転を適用する。
	 * @param p
	 * @param rot
	 * @param revflag 
	 */
	public static void apply3D(Vector3D p,double[] rot,boolean revflag){
		if(revflag){
			double dx = rot[COST]*p.getX()+rot[SINT]*p.getY();
			double dy = -rot[SINT]*p.getX()+rot[COST]*p.getY();
			double ddx = rot[COSTK]*dx+rot[SINTK]*p.getZ();
			double dz = -rot[SINTK]*dx+rot[COSTK]*p.getZ();
			double ddy = rot[COSTU]*dy+rot[SINTU]*dz;
			double ddz = -rot[SINTU]*dy+rot[COSTU]*dz;
			p.setCoordinates(ddx,ddy,ddz);
			return;
		}else{
			double dy = rot[COSTU]*p.getY()-rot[SINTU]*p.getZ();
			double dz = rot[SINTU]*p.getY()+rot[COSTU]*p.getZ();
			double dx = rot[COSTK]*p.getX()-rot[SINTK]*dz;
			double ddz = rot[SINTK]*p.getX()+rot[COSTK]*dz;

			double ddx = rot[COST]*dx-rot[SINT]*dy;
			double ddy = rot[SINT]*dx+rot[COST]*dy;
			p.setCoordinates(ddx,ddy,ddz);
		}
	}

	public static Location3D normalized(Vector3D p){
		double len = p.getX()*p.getX()
				+p.getY()*p.getY()
				+p.getZ()*p.getZ();
		
		Location3D ret =  new Location3D(p.getX(),p.getY(),p.getZ());
		
		if(len == 0){
			return ret;
		}
		len = Math.sqrt(len);
		ret.setX(ret.getX() / len);
		ret.setY(ret.getY() / len);
		ret.setZ(ret.getZ() / len);
		return ret;
	}
	
	public static Location3D norm(Vector3D v1,Vector3D v2){
		return norm(v1.getX(),v1.getY(),v1.getZ()
				,v2.getX(),v2.getY(),v2.getZ());
	}

	//法線ベクトルを返す	
	public static Location3D norm(double v1x,double v1y,double v1z,double v2x,double v2y,double v2z){
		
		Location3D  ret = new Location3D(
		(v1y*v2z-v1z*v2y),
		(v1z*v2x-v1x*v2z),
		(v1x*v2y-v1y*v2x));
		
		return normalized(ret);
		
	}
	
	/**
	 * vec を軸にして radian 度 targey を回転したベクトルを返す。
	 * @param target
	 * @param vec
	 * @param radian
	 * @return
	 */
	public static Location3D rotated(Vector3D target,Vector3D vec,double radian){
		double len = vec.getX()*vec.getX()+vec.getY()*vec.getY()+vec.getZ()*vec.getZ();
		if(len == 0){
			logger.warn("can not calculate rotated point with zero vector.");
			return new Location3D(target);
		}
		len = Math.sqrt(len);
		double vx = vec.getX();
		double vy = vec.getY();
		double vz = vec.getZ();
		if(len != 1){
		vx /= len;
		vy /= len;
		vz /= len;
		}
		Location3D ret = new Location3D();
		double cos = Math.cos(radian);
		double sin = Math.sin(radian);
		ret.setX((vx*vx*(1-cos)+cos)*target.getX() +(vx*vy*(1-cos)-vz*sin)*target.getY()
		+(vz*vx*(1-cos)+vy*sin)*target.getZ());
		ret.setY((vx*vy*(1-cos)+vz*sin)*target.getX()+(vy*vy*(1-cos)+cos)*target.getY()
		+(vz*vy*(1-cos)-vx*sin)*target.getZ());
		ret.setZ((vx*vz*(1-cos)-vy*sin)*target.getX()
		+(vz*vy*(1-cos)+vx*sin)*target.getY() +(vz*vz*(1-cos)+cos)*target.getZ());
		return ret;
		}
	
}

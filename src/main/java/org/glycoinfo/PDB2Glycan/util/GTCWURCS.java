
package org.glycoinfo.PDB2Glycan.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


  // JSON result class of https://api.glycosmos.org/glycanformatconverter/2.7.0/wurcs2wurcs/ 
  public class GTCWURCS {
    @JsonProperty
    private String id;
    @JsonProperty
    private String WURCS;

    @Override
    public String toString() {
      return "GTC [id=" + id + ", wurcs=" + WURCS + "]";
    }

    public String getGTC() {
        return this.id;
    }
    
    public String getWURCS() {
        return this.WURCS;
    }
  }
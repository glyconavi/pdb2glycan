package org.glycoinfo.PDB2Glycan.util;

import org.glycoinfo.PDB2Glycan.io.file.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;


public class WURCS2GlyTouCan {

  private static final Logger logger = LoggerFactory.getLogger(WURCS2GlyTouCan.class);

  private static boolean useNetwork = true;
  private static Map<String, String> mapping;

  public static void setUseNetwork(boolean useNetwork) {
    WURCS2GlyTouCan.useNetwork = useNetwork;
  }

  // get GlytouCan ID using glycanformatconverter api
  public static String get(String key) throws IOException {
    if (key == null || key.isEmpty()) {
      logger.info("No WURCS");
      return null;
    }

    String gtcid = null;
    if (useNetwork) {
      ObjectMapper mapper = new ObjectMapper();
      String url = String.format("https://api.glycosmos.org/glycanformatconverter/2.7.0/wurcs2wurcs/%s",
          URLEncoder.encode(key, "UTF-8"));
      
      String json = HTTP.httpGet(url);
      GTCWURCS gtcwurcs = mapper.readValue(json, GTCWURCS.class);
      gtcid = gtcwurcs.getGTC();
    } else {
      gtcid = lookup(key);
    }
    if (gtcid != null)
      logger.info("GTCID \"" + gtcid + "\" - " + key);
    else
      logger.info("No GTCID - " + key);
    return gtcid;
  }

  // het GlyTouCan ID using sparqlist of GlycoNAVI
  public static String getREST(String key) throws IOException {
    if ( key == null || key.isEmpty() ) {
      logger.info("No WURCS");
      return null;
    }

    String gctid = null;
    if (useNetwork) {
      String url = String.format("https://sparqlist.glyconavi.org/api/WURCS2GlyTouCan_text?WURCS=%s",
          URLEncoder.encode(key, "UTF-8"));

      gctid = HTTP.httpGet(url);
      if (gctid != null)
        gctid = gctid.replaceAll("\r\n", "").replaceAll("\n", "");
    } else {
      gctid = lookup(key);
    }
    if ( gctid != null )
        logger.info("GTCID \""+gctid+"\" - " + key);
    else
        logger.info("No GTCID - " + key);
    return gctid;
  }

  public static void load(Path file) throws IOException {
    Map<String, String> mapping = new HashMap<>();

    try (BufferedReader reader = Files.newBufferedReader(file.toFile())) {
      reader.lines().forEach(x -> {
        String[] pair = x.split("\t");
        String key = pair[0].replaceAll("^\"", "").replaceAll("\"$", "");
        String value = pair[1].replaceAll("^\"", "").replaceAll("\"$", "");
        mapping.put(key, value);
      });
    }

    WURCS2GlyTouCan.mapping = mapping;

    logger.info("Successfully loaded " + file.toAbsolutePath().toString());
  }

  private static String lookup(String key) {
    if (mapping == null) {
//      throw new RuntimeException("Mapping file is not set");
      // Do not map GTCID
      logger.warn("Mapping file is not set");
      return null;
    }

    return mapping.get(key);
  }
}

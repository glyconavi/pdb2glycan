package org.glycoinfo.PDB2Glycan.util;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.MolWURCS.exchange.toWURCS.MoleculeToWURCSGraph;
import org.glycoinfo.MolWURCS.io.WURCSWriter;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactoryForAglycone;
//import org.glycoinfo.WURCSFramework.util.WURCSValidator;
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CTFile2WURCS {

	private static final Logger logger = LoggerFactory.getLogger(CTFile2WURCS.class);

	private List<String> m_lWURCSsSeparated;
	private List<String> m_lWURCSsSeparatedWithAglycone;
	private List<String> m_lWURCSAglycones;

	private List<String> m_lErrors;

	public List<String> getWURCSsSeparated() {
		return this.m_lWURCSsSeparated;
	}

	public List<String> getWURCSsSeparatedWithAglycone() {
		return this.m_lWURCSsSeparatedWithAglycone;
	}

	public List<String> getWURCSAglycones() {
		return this.m_lWURCSAglycones;
	}

	public List<String> getErrors() {
		return this.m_lErrors;
	}

	@SuppressWarnings("deprecation")
	public String getWURCS(IAtomContainer mol) { // added. 10.Dec.2022
		String WURCS = "";
		this.m_lErrors = new ArrayList<>();
		try {

            // IAtomContainerはSDF一つ分の情報のみ格納している。
  		    StringWriter sw = new StringWriter();
  		    WURCSWriter writer = new WURCSWriter(sw);
  		    writer.setOutputWithAglycone(true);
  			writer.writeAtomContainer(mol);
			writer.close();
  			String[] writer_results = sw.toString().split("\t");
  			WURCS = writer_results[writer_results.length - 1];

			MoleculeToWURCSGraph wurcs_graph = new MoleculeToWURCSGraph();
			wurcs_graph.setOutputWithAglycone(true);
			List<WURCSGraph> graphs = wurcs_graph.start(mol);
			WURCSFactoryForAglycone w_factory = new WURCSFactoryForAglycone(graphs.get(0));
           // Validate WURCS
            WURCS = this.validateWURCS(WURCS);

			if (! w_factory.hasAglycone())
			  return WURCS;
			logger.info("The WURCS has been separated with aglycone: {}", WURCS);

			// Aglycones can not be validated
			this.m_lWURCSAglycones = w_factory.getSeparatedAglycones();
			this.m_lWURCSsSeparated = new ArrayList<>();
			for ( String t_strWURCS : w_factory.getStandardWURCSs() ) {
				String t_strWURCSSeparated = this.validateWURCS(t_strWURCS);
				this.m_lWURCSsSeparated.add(t_strWURCSSeparated);
			}
			// WURCSs with aglycone can not be validated
			this.m_lWURCSsSeparatedWithAglycone = w_factory.getSeparatedWURCSsWithAglycone();
			// WURCSs with aglycone can not be validated
    	} catch (WURCSException e) {
			this.m_lErrors.add("[WURCSException] "+e.getMessage());
		} catch (Exception e) {
			logger.warn(e.getMessage());
			this.m_lErrors.add("[Exception] "+e.getMessage());
		}
        return WURCS;
	}

	private String validateWURCS(String a_strWURCS) {
		String t_strWURCS = a_strWURCS;
		WURCSValidator val = new WURCSValidator();
		val.start(t_strWURCS);
//		if (val.getStandardWURCS() == null) { // modified. 16.Dec.2022
		if (val.getReport().getStandardString() == null) {
        logger.warn("Validation error(s) {} in converted WURCS: {}",
//				val.getErrors().toString(), t_strWURCS); // modified. 16.Dec.2022
			    val.getReport().getErrors().toString(), t_strWURCS);
//			for ( String err : val.getErrors() ) { // modified. 16.Dec.2022
    		for ( String err : val.getReport().getErrors() ) {
                if ( err.contains("CarbonDescriptor") )
					err = "Uncommon backbone carbon is contained.";
				err = "[WURCSValidation] "+err;
				if ( !this.m_lErrors.contains(err) )
					this.m_lErrors.add(err);
			}
			return "";
		}

//		t_strWURCS = val.getStandardWURCS(); // modified. 16.Dec.2022
		t_strWURCS = val.getReport().getStandardString();

		// TODO: Temporary repairs for MAP
		if (t_strWURCS.contains("*OP^XO*/3O/3=O")) {
			t_strWURCS = t_strWURCS.replaceAll("\\*OP\\^XO\\*/3O/3=O", "*OPO*/3O/3=O");
		}
		return t_strWURCS;
	}
}

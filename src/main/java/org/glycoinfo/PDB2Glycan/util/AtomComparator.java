package org.glycoinfo.PDB2Glycan.util;

import org.glycoinfo.PDB2Glycan.structure.Atom;

public class AtomComparator implements java.util.Comparator<Atom> {

  public int compare(Atom a1, Atom a2) {
    int i1 = 0;
    int i2 = 0;
    boolean failed_s1 = false;
    try {
      i1 = Integer.parseInt(a1.getAtom_site_id());
    } catch (NumberFormatException exx) {
      //exx.printStackTrace();
    	failed_s1 = true;
    }
    
    try {
      i2 = Integer.parseInt(a2.getAtom_site_id());
    } catch (NumberFormatException exx) {
      //exx.printStackTrace();
      if(failed_s1) {
      	return 0;
      }else {
      	return -1;
      }
    }
    
    if(failed_s1) {
    	return 1;
    }
    
    return Integer.compare(i1, i2);
  }
}

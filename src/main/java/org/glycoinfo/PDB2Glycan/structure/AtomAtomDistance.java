package org.glycoinfo.PDB2Glycan.structure;

public class AtomAtomDistance {
	private Atom source;
	private Atom target;
	
	public AtomAtomDistance(Atom s,Atom t) {
		this.source = s;
		this.target = t;
	}
	
	public double distance() {
		return source.distance(target);
	}

	public Atom getSource() {
		return source;
	}

	public void setSource(Atom source) {
		this.source = source;
	}

	public Atom getTarget() {
		return target;
	}

	public void setTarget(Atom target) {
		this.target = target;
	}

}

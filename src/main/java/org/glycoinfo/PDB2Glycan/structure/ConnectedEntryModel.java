package org.glycoinfo.PDB2Glycan.structure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.biojava.nbio.structure.io.mmcif.model.PdbxPolySeqScheme;
import org.glycoinfo.PDB2Glycan.util.AtomComparator;
import org.glycoinfo.PDB2Glycan.util.BondComparator;
import org.glycoinfo.PDB2Glycan.util.BondComparator_CTFile;
import org.glycoinfo.PDB2Glycan.util.Format;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectedEntryModel {

  private static final Logger logger = LoggerFactory.getLogger(ConnectedEntryModel.class);

  public ArrayList<Atom> atoms = new ArrayList<>();
  public ArrayList<Bond> bonds = new ArrayList<>();
  private ConnectedEntry parent = null;
  private HashMap<Atom, String> atom_rep = new HashMap<>();
  public ArrayList<AtomAtomDistance> nearbyAtoms = new ArrayList<>();

  public Atom linkedAtom = null;
  public PdbxPolySeqScheme linkedResidue = null;

  public String label_alt_id = null;

  public HashMap<String, ArrayList<Atom>> proBindRes = new HashMap<>(); // added. 20.Jan.2023
  
  private ConnectedEntryModel(ArrayList<Atom> a, ArrayList<Bond> b) {
    atoms.addAll(a);
    bonds.addAll(b);
  }

  public ConnectedEntry getParent() {
    return parent;
  }

  public void setParent(ConnectedEntry ce) {
    parent = ce;
  }

  /**
   * Adds atoms surrounded saccharide atoms.
   * @param threshold
   * @param att
   * @param attNonSaccharide List of non-saccharide atoms which are contained in this atoms
   */
  public void addNearbyAtoms(double threshold,List<Atom> att, List<Atom> attNonSaccharide) {
  	String modelnum = this.getModelNum();
  	HashSet<Atom> atoms_hm = new HashSet<>(this.atoms);

  	double base_longest = 0.0;
  	Atom baseatom = atoms.get(0); 
  	for(Atom b:this.atoms) {
  		// Calculate longest distance in this entry to reduce calculation
  		base_longest = Math.max(b.distance(baseatom),base_longest);
  	}

  	for(Atom a:att) {
  		if(!modelnum.equals(a.getAtom_site_pdbx_PDB_model_num()))
  			continue;

  		if ( !a.getAtom_site_label_alt_id().isEmpty()
  		&& !a.getAtom_site_label_alt_id().equals(this.label_alt_id) )
  			continue;
        for (Atom nona : attNonSaccharide) { // added. 20.Jan.2023
          String atomInfo = a.getAtom_site_auth_comp_id() + a.getAtom_site_auth_seq_id() + a.getAtom_site_auth_asym_id();
          if (nona.getAtom_site_auth_asym_id().equals(a.getAtom_site_auth_asym_id())) {
            if (nona.getAtom_site_auth_comp_id().equals(a.getAtom_site_auth_comp_id()) &&
            	nona.getAtom_site_auth_seq_id().equals(a.getAtom_site_auth_seq_id())) {
              if (proBindRes.get(atomInfo) == null) {
                proBindRes.put(atomInfo, new ArrayList<>());
                proBindRes.get(atomInfo).add(a);
              } else {
            	proBindRes.get(atomInfo).add(a);
              }
            }
          }
        } // end. 20.Jan.2023
  		if(a.distance(baseatom)-base_longest >= threshold)
  			continue;

  		if(atoms_hm.contains(a))
  			continue;

  		// TODO: do not add waters as target atoms when the interaction class for water is constructed
//  		if ( a.getAtom_site_label_comp_id().equals("HOH") )
//  			continue;
  		for(Atom sa:this.atoms) {
  			// Ignore non-saccharide atoms
  			if ( attNonSaccharide.contains(sa) )
  				continue;
  			if(sa.distance(a) < threshold)
  				this.nearbyAtoms.add(new AtomAtomDistance(sa,a));
  		}
  	}
  }
  public void clearCloseAtoms() {
  	this.nearbyAtoms.clear();
  }
  
  public String getModelNum() {
    String ret = "";
    for (Atom a : atoms) {
      String ss = a.getAtom_site_pdbx_PDB_model_num();
      if (ret.length() != 0 && !ss.equals(ret)) {
        logger.warn("contamination of model id." + ret + " " + ss + " " + ret + " is used.");
      } else {
        ret = ss;
      }
    }
    return ret;

  }

  private HashSet<String> getAtomCodes() {
    HashSet<String> ret = new HashSet<>();
    for (Atom a : atoms) {
      if (!atom_rep.containsKey(a)) {
        String code = String.format("%s#%s#%s#%s#%s", a.getAtom_site_label_atom_id(),
                a.getAtom_site_label_comp_id(), a.getAtom_site_label_asym_id(),
                a.getAtom_site_label_entity_id(), a.getAtom_site_label_seq_id());
        atom_rep.put(a, code);
      }
      ret.add(atom_rep.get(a));

    }
    return ret;
  }

  public List<Atom> getAtoms() {
    List<Atom> ret = this.atoms.stream().collect(Collectors.toList());
    return ret;
  }

//  public List<String> getOcupancies(){
//	  List<String> ret = this.atoms.stream()
//			  .map(a -> a.getAtom_site_occupancy()).map(a->String.valueOf(a)).collect(Collectors.toList());
//	  return ret;
//  }
//
//  
//  public List<String> getBIsoOrEquivs(){
//	  List<String> ret = this.atoms.stream()
//			  .map(a -> a.getAtom_site_B_iso_or_equiv()).map(a->String.valueOf(a)).collect(Collectors.toList());
//	  return ret;
//  }
//  
//
//  public List<String> getPdbxFormalCharges(){
//	  List<String> ret = this.atoms.stream()
//			  .map(a -> a.getAtom_site_pdbx_formal_charge()).map(a->String.valueOf(a)).collect(Collectors.toList());
//	  return ret;
//  }
  
  
  public boolean sameWith(ConnectedEntryModel se) {
    HashSet<String> m = new HashSet<>(this.getAtomCodes());
    HashSet<String> t = new HashSet<>(se.getAtomCodes());

    if (m.size() != t.size()) {
      return false;
    }

    int si = m.size();
    m.retainAll(t);

    return m.size() == si;
  }

  /**
   * 含まれている原子の重複の無い全 AsymId のリストを返す
   */
  public ArrayList<String> getAllAsymIds() {
    HashSet<String> allasym = new HashSet<>();

    for (Atom satom : this.atoms) {
      allasym.add(satom.getAtom_site_label_asym_id());
    }
    ArrayList<String> sal = new ArrayList<>(allasym);
    Collections.sort(sal);
    return sal;
  }

  /**
   * 含まれている原子の重複の無い全 EntityId のリストを返す
   */
  public ArrayList<String> getAllEntityIds() {
    HashSet<String> allent = new HashSet<>();

    for (Atom satom : this.atoms) {
      allent.add(satom.getAtom_site_label_entity_id());
    }
    ArrayList<String> sal = new ArrayList<>(allent);
    Collections.sort(sal);
    return sal;
  }

  /**
   * asym+comp+seqid で重複の無い CompID のリストを返す
   */
  public ArrayList<String> getAllCompLabels() {
    HashSet<String> alllabel = new HashSet<>();
    for (Atom satom : this.atoms) {
      alllabel.add(
          satom.getAtom_site_label_asym_id() + ";" + satom.getAtom_site_label_comp_id() + ";"
//              + satom.getAtom_site_label_seq_id());
              + satom.getAtom_site_auth_seq_id()); // needs to distinguish rather than label_seq_id
      // asym と並びを合わせるために結合している
    }
    ArrayList<String> cal = new ArrayList<>(alllabel);
//    StringBuffer sbb = new StringBuffer();
    Collections.sort(cal);
    return cal;
  }

  public IAtomContainer getIAtomContainer(String pdbcode) {

	  for (int ii = 0; ii < this.atoms.size(); ii++) {
	    this.atoms.get(ii).setCtfile_id(ii + 1);
	  }
	  Collections.sort(this.bonds, new BondComparator_CTFile());

	  String additionalcode = "";
	  if (true) {
	    ArrayList<String> sal = this.getAllAsymIds();

	    Collections.sort(sal);

	    StringBuffer sb = new StringBuffer();
	    for (String saa : sal) {
	      if (sb.length() > 0) {
	        sb.append("_");
	      }
	      sb.append(saa);
	    }

	    ArrayList<String> cal = this.getAllCompLabels();
	    StringBuffer sbb = new StringBuffer();
	    Collections.sort(cal);
	    for (String saa : cal) {
	      if (sbb.length() > 0) {
	        sbb.append("_");
	      }
	      sbb.append(saa.split(";")[1]);
	    }
	    additionalcode = "." + sb.toString() + "." + sbb.toString();
	  }

	  String ctitle = pdbcode.toUpperCase() + "."
	                  + this.atoms.get(0).getAtom_site_pdbx_PDB_model_num() + ".";
	  if (this.label_alt_id != null)
	      ctitle += this.label_alt_id + ".";
	  if (this.linkedResidue != null) {
	      ctitle += this.linkedResidue.getAsym_id() + "."
	              + this.linkedResidue.getMon_id() + "."
	              + this.linkedResidue.getSeq_id() + "."
	              + this.linkedResidue.getPdb_strand_id();
	  } else {
	      ctitle += "none" + "." + "none" + "." + "none" + "." + "none";
	  }
	  ctitle += additionalcode;

	  return Format
			  .iatomcontainer(new LinkedList<Atom>(this.atoms), new LinkedList<Bond>(this.bonds), ctitle);
  }

  public static ArrayList<ConnectedEntryModel> separateWithResidue(List<Atom> a, List<Bond> b) {
    Map<String, List<Atom>> mapResToAtoms = new HashMap<>();
    Map<String, List<Bond>> mapResToBonds = new HashMap<>();
    for (Atom atom: a) {
      String id = atom.getAtom_site_label_asym_id()+":"+atom.getAtom_site_auth_seq_id();
      if ( !mapResToAtoms.containsKey(id) )
        mapResToAtoms.put(id, new ArrayList<>());
      mapResToAtoms.get(id).add(atom);
    }

    // Groups connected residues
    List<Set<String>> lConnRess = new ArrayList<>();
    for (Bond bond : b) {
      String resid1 = bond.getFromAtom().getAtom_site_label_asym_id()
          +":"+bond.getFromAtom().getAtom_site_auth_seq_id();
      String resid2 = bond.getToAtom().getAtom_site_label_asym_id()
          +":"+bond.getToAtom().getAtom_site_auth_seq_id();

      if ( !mapResToBonds.containsKey(resid1) )
        mapResToBonds.put(resid1, new ArrayList<>());
      mapResToBonds.get(resid1).add(bond);
      if ( !mapResToBonds.containsKey(resid2) )
        mapResToBonds.put(resid2, new ArrayList<>());
      mapResToBonds.get(resid2).add(bond);

      if ( resid1.equals(resid2) )
        continue;

      // Collect connected residues
      List<Set<String>> lConnConnRess = new ArrayList<>();
      for ( Set<String> connRess : lConnRess ) {
        if ( connRess.contains(resid1) ) {
          lConnConnRess.add(connRess);
          connRess.add(resid2);
        } else if ( connRess.contains(resid2) ) {
          lConnConnRess.add(connRess);
          connRess.add(resid1);
        }
      }
      // Add new group
      if (lConnConnRess.isEmpty()) {
        Set<String> connRess = new HashSet<>();
        connRess.add(resid1);
        connRess.add(resid2);
        lConnRess.add(connRess);
        continue;
      } else if (lConnConnRess.size() < 2)
        continue;
      // Merge duplicated groups
      for ( int i=1; i<lConnConnRess.size(); i++ ) {
        lConnConnRess.get(0).addAll(lConnConnRess.get(i));
        lConnRess.remove(lConnConnRess.get(i));
      }
    }
    // Groups remaining residues
    for (String resid : mapResToAtoms.keySet()) {
      boolean isNew = true;
      for ( Set<String> connRess : lConnRess )
        if ( connRess.contains(resid) )
          isNew = false;
      if (!isNew)
        continue;
      Set<String> connRess = new HashSet<>();
      connRess.add(resid);
      lConnRess.add(connRess);
    }

    // Populates connected model
    ArrayList<ConnectedEntryModel> ret = new ArrayList<>();
    for ( Set<String> connRess : lConnRess ) {
      ArrayList<Atom> connAtoms = new ArrayList<>();
      Set<Bond> connBonds = new HashSet<>();
      for ( String resid : connRess ) {
        connAtoms.addAll(mapResToAtoms.get(resid));
        if ( mapResToBonds.containsKey(resid) )
          connBonds.addAll(mapResToBonds.get(resid));
      }

      ArrayList<Bond> connBondsSorted = new ArrayList<>(connBonds);
      Collections.sort(connAtoms, new AtomComparator());
      Collections.sort(connBondsSorted, new BondComparator());
//      ret.add(new ConnectedEntryModel(connAtoms, connBondsSorted));
      ret.add(new ConnectedEntryModel(connAtoms, connBondsSorted));
    }

    int acnt = 0;
    int bcnt = 0;
    for (ConnectedEntryModel c : ret) {
      acnt += c.atoms.size();
      bcnt += c.bonds.size();
    }

    // Error check
    if (acnt != a.size()) {
      logger.warn(
          "Atom numbers are different ??? Please check the code. " + acnt + " " + a.size());
    }
    if (bcnt != b.size()) {
      logger.warn(
          "Bond numbers are different ??? Please check the code. " + bcnt + " " + b.size());
    }

    ArrayList<ConnectedEntryModel> retNew = new ArrayList<>();
    for (ConnectedEntryModel c : ret) {
      retNew.addAll( separateWithAltId(c) );
    }
    ret = retNew;

    return ret;
  }

  private static ArrayList<ConnectedEntryModel> separateWithAltId(ConnectedEntryModel model) {
    ArrayList<ConnectedEntryModel> ret = new ArrayList<>();

    // Check alt_ids
    List<String> altIds = new ArrayList<>();
    for ( Atom atom : model.atoms ) {
      if ( atom.getAtom_site_label_alt_id() == null
        || atom.getAtom_site_label_alt_id().isEmpty() )
        continue;
      String altId = atom.getAtom_site_label_alt_id();
      if ( altIds.contains(altId) )
        continue;
      altIds.add(altId);
    }

    // Return if no or only one alt_id
    if ( altIds.size() < 2 ) {
      ret.add(model);
      return ret;
    }

    // Separate model with alt_id
    for ( String altId : altIds ) {
      ArrayList<Atom> atomsNew = new ArrayList<>();
      ArrayList<Bond> bondsNew = new ArrayList<>();

      for ( Atom atom : model.atoms ) {
        if ( atom.getAtom_site_label_alt_id() == null
          || atom.getAtom_site_label_alt_id().isEmpty() ) {
          atomsNew.add(atom);
          continue;
        }
        String atomAltId = atom.getAtom_site_label_alt_id();
        if ( !altId.equals(atomAltId) )
          continue;
        atomsNew.add(atom);
      }
      for ( Bond bond : model.bonds ) {
        if ( atomsNew.contains(bond.getFromAtom())
          && atomsNew.contains(bond.getToAtom()) )
          bondsNew.add(bond);
      }

      ConnectedEntryModel modelNew = new ConnectedEntryModel(atomsNew, bondsNew);
      modelNew.label_alt_id = altId;

      ret.add(modelNew);
    }

    return ret;
  }

  public static ArrayList<ConnectedEntryModel> separate(List<Atom> a, List<Bond> b) {
    ArrayList<ConnectedEntryModel> ret = new ArrayList<>();
    ArrayList<Atom> att = new ArrayList<>(a);
    HashMap<Atom, Integer> atom_to_id = new HashMap<>();

    HashMap<Atom, ArrayList<Bond>> atom_to_bond = new HashMap<>();
    HashMap<Atom, Atom> parentatom = new HashMap<>();
    for (int ii = 0; ii < att.size(); ii++) {
      atom_to_id.put(att.get(ii), ii);
      parentatom.put(att.get(ii), att.get(ii));
      atom_to_bond.put(att.get(ii), new ArrayList<Bond>());
    }

    for (Bond bb : b) {

      atom_to_bond.get(bb.getFromAtom()).add(bb);
      atom_to_bond.get(bb.getToAtom()).add(bb);

      Atom fatom = getRootAtom(bb.getFromAtom(), parentatom);
      Atom tatom = getRootAtom(bb.getToAtom(), parentatom);
      if (atom_to_id.get(fatom) > atom_to_id.get(tatom)) {
        parentatom.put(fatom, tatom);
      } else {
        parentatom.put(tatom, fatom);
      }

    }
//    HashMap<Atom, ConnectedEntryModel> group = new HashMap<>();
    HashSet<Atom> pp = new HashSet<>();

    for (Atom aa : att) {
      pp.add(getRootAtom(aa, parentatom));
    }
    for (Atom pa : pp) {
      ArrayList<Atom> rr = new ArrayList<>();
      HashSet<Bond> bb = new HashSet<>();
      for (Atom aa : att) {
        if (getRootAtom(aa, parentatom) == pa) {
          rr.add(aa);
          bb.addAll(atom_to_bond.get(aa));
        }
      }
      ArrayList<Bond> bbsorted = new ArrayList<>(bb);
      Collections.sort(rr, new AtomComparator());
      Collections.sort(bbsorted, new BondComparator());
      ret.add(new ConnectedEntryModel(rr, bbsorted));
    }

    int acou = 0;
    int bcou = 0;
    for (ConnectedEntryModel c : ret) {
      acou += c.atoms.size();
      bcou += c.bonds.size();
    }

    //ここでメッセージが出るとコードを変更したせいて不整合が出ている。
    if (acou != a.size()) {
      logger.warn(
          "Atom numbers are different ??? Please check the code. " + acou + " " + a.size());
    }
    if (bcou != b.size()) {
      logger.warn(
          "Bond numbers are different ??? Please check the code. " + bcou + " " + b.size());
    }

    return ret;
  }

  /**
   * 最もベースとなる原子を返す。
   */
  public static Atom getRootAtom(Atom a, HashMap<Atom, Atom> parentmap) {
    Atom p = parentmap.get(a);
    int loopcount = 0;
    while (p != parentmap.get(p)) {
      p = parentmap.get(p);
      loopcount++;
      if (loopcount > 10000) {
        throw new RuntimeException("Infinite loop?");
      }
    }
    return p;
  }
}

package org.glycoinfo.PDB2Glycan.structure;

public interface Vector3D {
	double getX();
	double getY();
	double getZ();
	void setX(double x);
	void setY(double x);
	void setZ(double x);
	void setCoordinates(double x, double y, double z);
	void set(Vector3D v);
	
	static double distance(Vector3D v1, Vector3D v2){
		double dx = (v1.getX()-v2.getX());
		double dy = (v1.getY()-v2.getY());
		double dz = (v1.getZ()-v2.getZ());
		
		double dd = dx*dx + dy*dy + dz*dz;
		if(dd == 0.0) {
			return 0.0;
		}
		return Math.sqrt(dd);
	}
	
}

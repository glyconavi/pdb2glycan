package org.glycoinfo.PDB2Glycan.structure;

public enum AtomSiteGroupType {
  ATOM,
  HETATM
}

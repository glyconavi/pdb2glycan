package org.glycoinfo.PDB2Glycan.structure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2Glycan.util.Format;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectedEntry {

  private static final Logger logger = LoggerFactory.getLogger(ConnectedEntry.class);

  public ArrayList<ConnectedEntryModel> models = new ArrayList<>();
  public ArrayList<PdbxStructModResidue> parentResidues = new ArrayList<>();

  public void addParentResidue(PdbxStructModResidue pp) {
    parentResidues.add(pp);
  }

  public void addModel(ConnectedEntryModel cm) {
    models.add(cm);
    cm.setParent(this);
  }

  public boolean sameWith(ConnectedEntryModel cm) {
    for (ConnectedEntryModel mm : models) {
      if (mm.sameWith(cm)) {
        return true;
      }
    }
    return false;
  }


  public static ArrayList<ModelCassette> entryModelToCassette(String pdbcode,
                                                              ArrayList<ConnectedEntry> models) {
    ArrayList<ModelCassette> ret = new ArrayList<>();
    HashMap<String, ArrayList<ConnectedEntryModel>> model_i = new HashMap<>();
    for (ConnectedEntry ce : models) {
      for (ConnectedEntryModel cmm : ce.models) {
        if (!model_i.containsKey(cmm.getModelNum())) {
          model_i.put(cmm.getModelNum(), new ArrayList<>());
        }
        model_i.get(cmm.getModelNum()).add(cmm);
      }
    }

    ArrayList<String> da = new ArrayList<>(model_i.keySet());
    Collections.sort(da);

    for (String modelid : da) {
      ArrayList<ConnectedEntryModel> ma = model_i.get(modelid);
      for (ConnectedEntryModel cm : ma) {
        ModelCassette mc = new ModelCassette();
        mc.pdbx_PDB_model_num = modelid;
        // modified. 14.Dec.2022
        IAtomContainer mol = cm.getIAtomContainer(pdbcode.toUpperCase());
        mc.CTfile = Format.toSDF(mol);

        ret.add(mc);
      }

    }

    return ret;
  }

}

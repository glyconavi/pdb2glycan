package org.glycoinfo.PDB2Glycan.structure;

public class Atom implements Vector3D {

  private int id;

  private int ctfile_id;

  private double X;
  private double Y;
  private double Z;

  private String atom_site_id;

  private String atom_site_label_asym_id;
  private String atom_site_label_atom_id;
  private String atom_site_label_atom_id_Position;// ? 何のために作ったか忘れた（小田）
  private String atom_site_label_comp_id;
  private String atom_site_label_comp_id_molCode;// ? 何のために作ったか忘れた（小田）
  private String atom_site_label_entity_id;
  private String atom_site_label_seq_id;
  private String atom_site_label_alt_id;

  private String atom_site_auth_seq_id;
  private String atom_site_auth_comp_id;
  private String atom_site_auth_asym_id;
  private String atom_site_auth_atom_id;

  private String atom_site_pdbx_PDB_model_num;
  private String atom_site_type_symbol;
  private Double atom_site_B_iso_or_equiv;

  private String atom_site_pdbx_PDB_ins_code;

  private String chem_comp_atom_alt_atom_id;

  private Boolean aromatic; // { get; set; }
  private Boolean leaving_atom;
  private String hybrid; // { get; set; }
  private String auth_comp_id; // { get; set; }
  private String description; // { get; set; }
  private String atom_id; // { get; set; }
  private String comp_id; // { get; set; }
  private String seq_id; // { get; set; }
  private String group_PDB; // { get; set; }
  private Double atom_site_occupancy; // { get; set; }
  private Boolean is_terminal_heteroatom; // { get; set; }
  private int chem_comp_atom_charge; // { get; set; }
  private Integer atom_site_pdbx_formal_charge; // { get; set; }

  public String getAtom_site_auth_seq_id() {
    return atom_site_auth_seq_id;
  }

  public void setAtom_site_auth_seq_id(String atom_site_auth_seq_id) {
    this.atom_site_auth_seq_id = atom_site_auth_seq_id;
  }

  public String getAtom_site_auth_comp_id() {
    return atom_site_auth_comp_id;
  }

  public void setAtom_site_auth_comp_id(String atom_site_auth_comp_id) {
    this.atom_site_auth_comp_id = atom_site_auth_comp_id;
  }

  public String getAtom_site_auth_asym_id() {
    return atom_site_auth_asym_id;
  }

  public void setAtom_site_auth_asym_id(String atom_site_auth_asym_id) {
    this.atom_site_auth_asym_id = atom_site_auth_asym_id;
  }

  public String getAtom_site_auth_atom_id() {
    return atom_site_auth_atom_id;
  }

  public void setAtom_site_auth_atom_id(String atom_site_auth_atom_id) {
    this.atom_site_auth_atom_id = atom_site_auth_atom_id;
  }

  public String getAtom_site_label_alt_id() {
    return atom_site_label_alt_id;
  }

  public void setAtom_site_label_alt_id(String atom_site_label_alt_id) {
    this.atom_site_label_alt_id = atom_site_label_alt_id;
  }

  public String getAtom_site_pdbx_PDB_ins_code() {
    return atom_site_pdbx_PDB_ins_code;
  }

  public void setAtom_site_pdbx_PDB_ins_code(String atom_site_pdbx_PDB_ins_code) {
    this.atom_site_pdbx_PDB_ins_code = atom_site_pdbx_PDB_ins_code;
  }

  public Integer getAtom_site_pdbx_formal_charge() {
    return atom_site_pdbx_formal_charge;
  }

  public void setAtom_site_pdbx_formal_charge(Integer atom_site_pdbx_formal_charge) {
    this.atom_site_pdbx_formal_charge = atom_site_pdbx_formal_charge;
  }

  public String getChem_comp_atom_alt_atom_id() {
    return chem_comp_atom_alt_atom_id;
  }

  public void setChem_comp_atom_alt_atom_id(String chem_comp_atom_alt_atom_id) {
    this.chem_comp_atom_alt_atom_id = chem_comp_atom_alt_atom_id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getCtfile_id() {
    return ctfile_id;
  }

  public void setCtfile_id(int ctfile_id) {
    this.ctfile_id = ctfile_id;
  }

  @Override
  public double getX() {
    return X;
  }

  @Override
  public void setX(double x) {
    X = x;
  }

  @Override
  public double getY() {
    return Y;
  }

  @Override
  public void setY(double y) {
    Y = y;
  }

  @Override
  public double getZ() {
    return Z;
  }

  @Override
  public void setZ(double z) {
    Z = z;
  }

  public String getAtom_site_id() {
    return atom_site_id;
  }

  public void setAtom_site_id(String atom_site_id) {
    this.atom_site_id = atom_site_id;
  }

  public String getAtom_site_label_asym_id() {
    return atom_site_label_asym_id;
  }

  public void setAtom_site_label_asym_id(String atom_site_label_asym_id) {
    this.atom_site_label_asym_id = atom_site_label_asym_id;
  }

  public String getAtom_site_label_atom_id() {
    return atom_site_label_atom_id;
  }

  public void setAtom_site_label_atom_id(String atom_site_label_atom_id) {
    this.atom_site_label_atom_id = atom_site_label_atom_id;
  }

  public String getAtom_site_label_atom_id_Position() {
    return atom_site_label_atom_id_Position;
  }

  public void setAtom_site_label_atom_id_Position(String atom_site_label_atom_id_Position) {
    this.atom_site_label_atom_id_Position = atom_site_label_atom_id_Position;
  }

  public String getAtom_site_label_comp_id() {
    return atom_site_label_comp_id;
  }

  public void setAtom_site_label_comp_id(String atom_site_label_comp_id) {
    this.atom_site_label_comp_id = atom_site_label_comp_id;
  }

  public String getAtom_site_label_comp_id_molCode() {
    return atom_site_label_comp_id_molCode;
  }

  public void setAtom_site_label_comp_id_molCode(String atom_site_label_comp_id_molCode) {
    this.atom_site_label_comp_id_molCode = atom_site_label_comp_id_molCode;
  }

  public String getAtom_site_label_entity_id() {
    return atom_site_label_entity_id;
  }

  public void setAtom_site_label_entity_id(String atom_site_label_entity_id) {
    this.atom_site_label_entity_id = atom_site_label_entity_id;
  }

  public String getAtom_site_label_seq_id() {
    return atom_site_label_seq_id;
  }

  public void setAtom_site_label_seq_id(String atom_site_label_seq_id) {
    this.atom_site_label_seq_id = atom_site_label_seq_id;
  }

  public String getAtom_site_pdbx_PDB_model_num() {
    return atom_site_pdbx_PDB_model_num;
  }

  public void setAtom_site_pdbx_PDB_model_num(String atom_site_pdbx_PDB_model_num) {
    this.atom_site_pdbx_PDB_model_num = atom_site_pdbx_PDB_model_num;
  }

  public String getAtom_site_type_symbol() {
    return atom_site_type_symbol;
  }

  public void setAtom_site_type_symbol(String atom_site_type_symbol) {
    this.atom_site_type_symbol = atom_site_type_symbol;
  }

  public Double getAtom_site_B_iso_or_equiv() {
    return atom_site_B_iso_or_equiv;
  }

  public void setAtom_site_B_iso_or_equiv(Double atom_site_B_iso_or_equiv) {
    this.atom_site_B_iso_or_equiv = atom_site_B_iso_or_equiv;
  }

  public Boolean getAromatic() {
    return aromatic;
  }

  public void setAromatic(Boolean aromatic) {
    this.aromatic = aromatic;
  }

  public Boolean getLeaving_atom() {
    return leaving_atom;
  }

  public void setLeaving_atom(Boolean leaving_atom) {
    this.leaving_atom = leaving_atom;
  }

  public String getHybrid() {
    return hybrid;
  }

  public void setHybrid(String hybrid) {
    this.hybrid = hybrid;
  }

  public String getAuth_comp_id() {
    return auth_comp_id;
  }

  public void setAuth_comp_id(String auth_comp_id) {
    this.auth_comp_id = auth_comp_id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getAtom_id() {
    return atom_id;
  }

  public void setAtom_id(String atom_id) {
    this.atom_id = atom_id;
  }

  public String getComp_id() {
    return comp_id;
  }

  public void setComp_id(String comp_id) {
    this.comp_id = comp_id;
  }

  public String getSeq_id() {
    return seq_id;
  }

  public void setSeq_id(String seq_id) {
    this.seq_id = seq_id;
  }

  public String getGroup_PDB() {
    return group_PDB;
  }

  public void setGroup_PDB(String group_PDB) {
    this.group_PDB = group_PDB;
  }

  public Double getAtom_site_occupancy() {
    return atom_site_occupancy;
  }

  public void setAtom_site_occupancy(Double atom_site_occupancy) {
    this.atom_site_occupancy = atom_site_occupancy;
  }

  public Boolean getIs_terminal_heteroatom() {
    return is_terminal_heteroatom;
  }

  public void setIs_terminal_heteroatom(Boolean is_terminal_heteroatom) {
    this.is_terminal_heteroatom = is_terminal_heteroatom;
  }

  public int getChem_comp_atom_charge() {
    return chem_comp_atom_charge;
  }

  public void setChem_comp_atom_charge(int chem_comp_atom_charge) {
    this.chem_comp_atom_charge = chem_comp_atom_charge;
  }

  public boolean isFromSameResidue(Atom b) {

    if (!this.atom_site_label_entity_id.equals(b.getAtom_site_label_entity_id()))
      return false;

    if (!this.atom_site_label_asym_id.equals(b.getAtom_site_label_asym_id()))
      return false;

    if (!this.atom_site_label_comp_id.equals(b.getAtom_site_label_comp_id()))
      return false;

    if (!this.atom_site_label_seq_id.equals(b.getAtom_site_label_seq_id()))
      return false;

    if (!this.atom_site_auth_seq_id.equals(b.getAtom_site_auth_seq_id()))
      return false;

    return this.atom_site_pdbx_PDB_ins_code.equals(b.getAtom_site_pdbx_PDB_ins_code());
  }

  public double distance(Atom atomb) {
    double xd = this.getX() - atomb.getX();
    double yd = this.getY() - atomb.getY();
    double zd = this.getZ() - atomb.getZ();
    double dd = xd * xd + yd * yd + zd * zd;

    if (dd == 0.0) {
      return 0.0;
    }
    return Math.sqrt(dd);
  }

  @Override
  public void set(Vector3D v) {
    this.setCoordinates(v.getX(), v.getY(), v.getZ());
  }

  @Override
  public void setCoordinates(double x, double y, double z) {
    this.setX(x);
    this.setY(y);
    this.setZ(z);
  }

  /**
   * {label_atom_id};{label_comp_id};{label_asym_id};{label_entity_id};{label_seq_id};{auth_comp_id};{auth_asym_id};{auth_seq_id};{pdbx_PDB_model_num};[{pdbx_PDB_ins_code};]
   */
  public String getUniqueCode() {
    StringBuilder sb = new StringBuilder();

    sb.append(getAtom_site_label_atom_id());
    sb.append(';');
    sb.append(getUniqueResidueCode());

    return sb.toString();
  }

  /**
   * {label_comp_id};{label_asym_id};{label_entity_id};{label_seq_id};{auth_comp_id};{auth_asym_id};{auth_seq_id};{pdbx_PDB_model_num};[{pdbx_PDB_ins_code};]
   */
  public String getUniqueResidueCode() {
    StringBuilder sb = new StringBuilder();

    sb.append(getAtom_site_label_comp_id());
    sb.append(';');
    sb.append(getAtom_site_label_asym_id());
    sb.append(';');
    sb.append(getAtom_site_label_entity_id());
    sb.append(';');
    sb.append(getAtom_site_label_seq_id());
    sb.append(';');

    sb.append(getAtom_site_auth_comp_id());
    sb.append(';');
    sb.append(getAtom_site_auth_asym_id());
    sb.append(';');
    sb.append(getAtom_site_auth_seq_id());
    sb.append(';');

    sb.append(getAtom_site_pdbx_PDB_model_num());
    sb.append(';');

    if (this.getAtom_site_pdbx_PDB_ins_code() != null) {
      sb.append(this.getAtom_site_pdbx_PDB_ins_code());
      sb.append(';');
    }

    return sb.toString();
  }

  @Override
  public String toString() {
    return getUniqueCode();
  }
}

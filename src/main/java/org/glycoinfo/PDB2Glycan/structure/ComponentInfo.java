package org.glycoinfo.PDB2Glycan.structure;

import java.util.ArrayList;

import org.glycoinfo.PDB2Glycan.io.model.protein.StructConf;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetRange;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ComponentInfo {
	//ASYM-RESIDUE-ATOM という構成が正しいと思うが、再構成するのが面倒だし
	//Model をどのレベルで扱うかという問題もある。
	//また Atom が二つ以上の Residue に入る可能性があるので
	//ただ単にデータをまとめるクラスとして機能させている

	private String atom_site_pdbx_PDB_model_num;
	private String atom_site_label_comp_id;
	private String atom_site_label_asym_id;
	private String atom_site_label_entity_id;
	private String atom_site_label_seq_id;
	private String atom_site_pdbx_PDB_ins_code;

	private ArrayList<String> structureInfo = new ArrayList<>();

	// For structure info
	private StructConf struct_conf;
	private StructSheetRange struct_sheet_range;

	public ComponentInfo(Atom baseatom) {
		this.setAtom_site_pdbx_PDB_model_num(baseatom.getAtom_site_pdbx_PDB_model_num());
		this.setAtom_site_label_comp_id(baseatom.getAtom_site_label_comp_id());
		this.setAtom_site_label_asym_id(baseatom.getAtom_site_label_asym_id());
		this.setAtom_site_label_entity_id(baseatom.getAtom_site_label_entity_id());
		this.setAtom_site_label_seq_id(baseatom.getAtom_site_label_seq_id());
		this.setAtom_site_pdbx_PDB_ins_code(baseatom.getAtom_site_pdbx_PDB_ins_code());
	}

	public String getAtom_site_pdbx_PDB_model_num() {
		return atom_site_pdbx_PDB_model_num;
	}
	public void setAtom_site_pdbx_PDB_model_num(String atom_site_pdbx_PDB_model_num) {
		this.atom_site_pdbx_PDB_model_num = atom_site_pdbx_PDB_model_num;
	}
	public String getAtom_site_label_comp_id() {
		return atom_site_label_comp_id;
	}
	public void setAtom_site_label_comp_id(String atom_site_label_comp_id) {
		this.atom_site_label_comp_id = atom_site_label_comp_id;
	}
	public String getAtom_site_label_asym_id() {
		return atom_site_label_asym_id;
	}
	public void setAtom_site_label_asym_id(String atom_site_label_asym_id) {
		this.atom_site_label_asym_id = atom_site_label_asym_id;
	}
	public String getAtom_site_label_entity_id() {
		return atom_site_label_entity_id;
	}
	public void setAtom_site_label_entity_id(String atom_site_label_entity_id) {
		this.atom_site_label_entity_id = atom_site_label_entity_id;
	}
	public String getAtom_site_label_seq_id() {
		return atom_site_label_seq_id;
	}
	public void setAtom_site_label_seq_id(String atom_site_label_seq_id) {
		this.atom_site_label_seq_id = atom_site_label_seq_id;
	}
	public String getAtom_site_pdbx_PDB_ins_code() {
		return atom_site_pdbx_PDB_ins_code;
	}
	public void setAtom_site_pdbx_PDB_ins_code(String atom_site_pdbx_PDB_ins_code) {
		this.atom_site_pdbx_PDB_ins_code = atom_site_pdbx_PDB_ins_code;
	}

	@JsonProperty("structure_info")
	public ArrayList<String> getStructureInfo() {
		return structureInfo;
	}
	public void addStructureInfo(String str) {
		this.structureInfo.add(str);
	}

	public StructConf getStruct_conf() {
		return struct_conf;
	}
	public void setStruct_conf(StructConf struct_conf) {
		this.struct_conf = struct_conf;
	}
	public StructSheetRange getStruct_sheet_range() {
		return struct_sheet_range;
	}
	public void setStruct_sheet_range(StructSheetRange struct_sheet_range) {
		this.struct_sheet_range = struct_sheet_range;
	}

}

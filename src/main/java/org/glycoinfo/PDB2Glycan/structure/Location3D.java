package org.glycoinfo.PDB2Glycan.structure;

public class Location3D implements Vector3D {
	double x=0;
	double y=0;
	double z=0;
	public Location3D() {
		
	}
	
	public Location3D(Vector3D v) {
		this(v.getX(),v.getY(),v.getZ());
	}
	
	public Location3D(double xx,double yy,double zz) {
		this.setCoordinates(xx,yy,zz);
	}
	
	@Override
	public double getX() {
		return x;
	}
	@Override
	public void setX(double x) {
		this.x = x;
	}
	@Override
	public double getY() {
		return y;
	}
	@Override
	public void setY(double y) {
		this.y = y;
	}
	@Override
	public double getZ() {
		return z;
	}
	@Override
	public void setZ(double z) {
		this.z = z;
	}
	

	@Override
	public void setCoordinates(double xx, double yy, double zz) {
		this.x = xx;
		this.y = yy;
		this.z = zz;
	}
	@Override
	public void set(Vector3D v) {
		this.x = v.getX();
		this.y = v.getY();
		this.z = v.getZ();
	}
	
}



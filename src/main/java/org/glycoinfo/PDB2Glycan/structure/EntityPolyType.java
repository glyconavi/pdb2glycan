package org.glycoinfo.PDB2Glycan.structure;

/**
 * The type of the polymer.
 */
public enum EntityPolyType {

  CYCLIC_PSEUDO_PEPTIDE("cyclic-pseudo-peptide", true),
  OTHER("other", false),
  PEPTIDE_NUCLEIC_ACID("peptide nucleic acid", false),
  POLYDEOXYRIBONUCLEOTIDE("polydeoxyribonucleotide", false),
  POLYDEOXYRIBONUCLEOTIDE_POLYRIBONUCLEOTIDE_HYBRID("polydeoxyribonucleotide/polyribonucleotide hybrid", false),
  POLYPEPTIDE_D("polypeptide(D)", true),
  POLYPEPTIDE_L("polypeptide(L)", true),
  POLYRIBONUCLEOTIDE("polyribonucleotide", false),
  POLYSACCHARIDE_D("polysaccharide(D)", false),
  POLYSACCHARIDE_L("polysaccharide(L)", false);

  public static EntityPolyType lookUp(String label) {
    for (EntityPolyType ept : EntityPolyType.values()) {
      if (ept.toString().equals(label)) {
        return ept;
      }
    }

    return null;
  }

  private final String label;
  private final boolean isPeptide;

  EntityPolyType(final String label, final boolean isPeptide) {
    this.label = label;
    this.isPeptide = isPeptide;
  }

  @Override
  public String toString() {
    return label;
  }

  public boolean isPeptide() {
    return isPeptide;
  }
}

package org.glycoinfo.PDB2Glycan.structure;

import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.file.ChemicalComponentLoader;
import org.glycoinfo.PDB2Glycan.structure.glyco.ValidationReport;
import org.glycoinfo.PDB2Glycan.util.AtomComparator;
import org.glycoinfo.PDB2Glycan.util.CCSuperposition;
import org.glycoinfo.PDB2Glycan.util.Superposition;
import org.glycoinfo.PDB2Glycan.util.VDW;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ChemicalComponentDictionary {

  private static final Logger logger = LoggerFactory.getLogger(ChemicalComponentDictionary.class);

  private ChemicalComponentLoader ccLoader;

  public ChemicalComponentDictionary(ChemicalComponentLoader ccLoader) {
    this.ccLoader = ccLoader;
  }

  /**
   * Adds dummy atoms to the given atom list {@code atoms} to complement lacking atoms
   * with checking the corresponding chemical component. The coordinates of dummy atoms
   * are computed with superposition.
   * 20200214 Masaaki
   * Additional atoms are limited to atoms listed in unobserbed atoms {@code atomsUnobs}.
   * 
   * @param atoms
   * @param bonds
   * @param atomsUnobs
   * @return
   * @throws IOException
   * @throws MMcifParseException
   */
  public ValidationReport addMissingAtoms(List<Atom> atoms, List<Bond> bonds, List<Atom> atomsUnobs)
      throws IOException, MMcifParseException {
//    Set<String> saccharides = new HashSet<>(ccLoader.saccharideIds());

    ValidationReport ret = new ValidationReport();
    HashMap<String, ArrayList<Atom>> res_grouped = new HashMap<>();
    ArrayList<Atom> atoms_constructed = new ArrayList<>();
    ArrayList<Bond> bonds_constructed = new ArrayList<>();
    String modelid = ChemicalComponentDictionary.checkModelNum(atoms);

    for (int i = 0; i < atoms.size(); i++) {
      if ( !ccLoader.isSaccharideAtom(atoms.get(i)) )
//      if (!saccharides.contains(atoms.get(i).getComp_id()))
        continue;
      String res = atoms.get(i).getAtom_site_label_asym_id()
          +":"+atoms.get(i).getAtom_site_auth_seq_id(); // needs to distinguish residue
      if (!res_grouped.containsKey(res))
        res_grouped.put(res, new ArrayList<>());
      res_grouped.get(res).add(atoms.get(i));
    }

    for (String s : res_grouped.keySet()) {

      ArrayList<Atom> res_atoms_constructed = new ArrayList<>();
      ArrayList<Bond> res_bonds_constructed = new ArrayList<>();

      ArrayList<Atom> aas = res_grouped.get(s);// Atoms in the same residue
      HashSet<Atom> aas_hs = new HashSet<>(aas);
      Mol i_cc_mol = ccLoader.load(aas.get(0).getComp_id());

      HashMap<Atom, HashSet<Atom>> atoms_connected = new HashMap<>();
      HashMap<Atom, HashSet<Atom>> connection_assigned = new HashMap<>();
      for (Atom a : aas) {
        atoms_connected.put(a, new HashSet<>());
        connection_assigned.put(a, new HashSet<>());
      }

      for (Bond b : bonds) {
        if (aas_hs.contains(b.getToAtom())) {
          atoms_connected.get(b.getToAtom()).add(b.getFromAtom());
        }

        if (aas_hs.contains(b.getFromAtom())) {
          atoms_connected.get(b.getFromAtom()).add(b.getToAtom());
        }
      }
      for (Atom a : atoms_connected.keySet()) {
        for (Atom b : atoms_connected.get(a)) {
          if (aas_hs.contains(b)) {
            connection_assigned.get(a).add(b);
          }
        }
      }

      HashMap<Atom, HashSet<Atom>> mol_atoms_connected = new HashMap<>();
      for (Atom a : i_cc_mol.getAtoms()) {
        if (a.getAtom_site_type_symbol().equals("H")) {
          continue;
        }
        mol_atoms_connected.put(a, new HashSet<>());
      }
      for (Bond b : i_cc_mol.getBonds()) {

        if (b.getFromAtom().getAtom_site_type_symbol().equals("H")
            || b.getToAtom().getAtom_site_type_symbol().equals("H")) {
          continue;
        }

        mol_atoms_connected.get(b.getFromAtom()).add(b.getToAtom());
        mol_atoms_connected.get(b.getToAtom()).add(b.getFromAtom());
      }

      ArrayList<Atom> missing_candidates = new ArrayList<>();
      for (Atom a : mol_atoms_connected.keySet()) {
        String mol_atomcode = a.getAtom_site_label_atom_id();
        Atom t = null;
        for (Atom a2 : atoms_connected.keySet()) {
          if (a2.getAtom_site_label_atom_id().equals(mol_atomcode)) {
            if (t != null) {
              ret.addAtomRepo("atom_id duplication: " + t.getUniqueCode() + "#" + a2.getUniqueCode());
            }
            t = a2;
          }
        }
        // Check bonds here if necessary.
        // There is no different bond from mol because all bonds were coming from the mol.
        // There might be additional or lacking bonds.
        if (t == null) {
          // Only add unobs atom
          boolean isUnobs = false;
          for ( Atom atomToAdd : atomsUnobs ) {
            // Check atom id
            if (!atomToAdd.getAtom_site_label_atom_id().equals(a.getAtom_site_label_atom_id()))
              continue;
            // Check res id
            if (!s.equals(atomToAdd.getAtom_site_label_asym_id()+":"+atomToAdd.getAtom_site_auth_seq_id()))
              continue;
            isUnobs = true;
            break;
          }
          if ( isUnobs )
            missing_candidates.add(a);
        }
      }

      HashSet<Atom> once_assigned = new HashSet<>();
      Collections.sort(missing_candidates, new AtomComparator());
      for (Atom a : missing_candidates) {

        boolean assign_ok = false;
        boolean possibly_misassign = false;

        ArrayList<Atom> bsorted = new ArrayList<>(mol_atoms_connected.get(a));
        Collections.sort(bsorted, new AtomComparator());

        for (Atom b : bsorted) {// Collects atoms originally connecting to lacking atoms
          String mol_atomcode_b = b.getAtom_site_label_atom_id();
          Atom t = Mol.getAtomByLabelAtomId(aas, mol_atomcode_b);
          if (t == null) {
            // There are more than two lacking atoms
            continue;
          }

          HashSet<Atom> aconn1 = new HashSet<>(atoms_connected.get(t));
          aconn1.removeAll(connection_assigned.get(t)); // These must be used as bond pair
          for (Atom acc : aconn1) {
            if ((acc.getAtom_site_auth_comp_id().equals("ASN") && acc.getAtom_site_type_symbol().equals("N")
                && a.getAtom_site_type_symbol().equals("O"))
                || (acc.getAtom_site_type_symbol().equals(a.getAtom_site_type_symbol()))) {
              if (!once_assigned.contains(acc)) {
                assign_ok = true;
                once_assigned.add(acc);
                // Don't break because missing all when the other atom assigned
              } else {
                possibly_misassign = true;
              }
            }
          }
        }

        if (assign_ok) {

        } else if (possibly_misassign) {
          ret.addAtomRepo(
              "Possibly missing atom: (Because multiple atoms are missing, I coundn't assign atoms correctly.) "
                  + a.getAtom_site_label_atom_id() + " in model:" + modelid + ";asym:" + s + ";comp:"
                  + aas.get(0).getComp_id());
        } else {
          String missinglabel = a.getAtom_site_label_atom_id();
          ret.addAtomRepo("Missing atom: " + missinglabel + " in model:" + modelid + ";asym:" + s + ";comp:"
              + aas.get(0).getComp_id());

          // System.out.println("Missing atom: "+missinglabel+" in
          // model:"+modelid+";asym:"+s+";comp:"+aas.get(0).getComp_id());
          ArrayList<String> nearestatom3 = CCSuperposition
              .getThreeNearestAtomIds(a.getAtom_site_label_atom_id(), i_cc_mol, aas);

          if (nearestatom3 == null) {
            ret.addAtomRepo(
                "Atom construction error: " + a.getAtom_site_label_atom_id() + " in model:" + modelid
                    + ";asym:" + s + ";comp:" + aas.get(0).getComp_id() + " can not be predicted.");
          } else {
            // Adds atom positions to atoms, computing from the references
            Atom refatom = i_cc_mol.getAtomByLabelAtomId(missinglabel);
            Location3D target = new Location3D(refatom);
            ArrayList<Location3D> referencepos = new ArrayList<>();
            ArrayList<Location3D> targetpos = new ArrayList<>();

            for (String ss : nearestatom3) {
              referencepos.add(new Location3D(i_cc_mol.getAtomByLabelAtomId(ss)));
              targetpos.add(new Location3D(Mol.getAtomByLabelAtomId(aas, ss)));
            }
            ArrayList<Vector3D> dummy = new ArrayList<>();
            dummy.add(target);
            Superposition.adjustVector3D(referencepos.get(0), referencepos.get(1), referencepos.get(2),
                targetpos.get(0), targetpos.get(1), targetpos.get(2), dummy);
            Atom newatom = new Atom();
            newatom.setAtom_site_label_atom_id(refatom.getAtom_site_label_atom_id());
            newatom.setAtom_site_label_asym_id("dummy" + s);
            newatom.setAtom_site_label_entity_id(aas.get(0).getAtom_site_label_entity_id());

            // newatom.setAtom_site_label_comp_id(refatom.getAtom_site_label_comp_id());
            newatom.setAtom_site_pdbx_PDB_model_num(aas.get(0).getAtom_site_pdbx_PDB_model_num());
            newatom.setAtom_site_type_symbol(refatom.getAtom_site_type_symbol());
            newatom.set(dummy.get(0));
            res_atoms_constructed.add(newatom);

            ret.addAtomRepo("Dummy atom: in model:" + modelid + ";asym:" + s + ";comp:"
                + aas.get(0).getComp_id() + " as " + newatom.getUniqueCode());
          }
          // System.out.println(a.getUniqueCode());
          // System.out.println(nearestatom3.toString());
        }
      }

      for (Atom a : res_atoms_constructed) {
//        HashMap<String, Integer> atomandorder = new HashMap<>();
        for (Bond b : i_cc_mol.getBonds()) {
          if (b.getFromAtom().getAtom_site_label_atom_id().equals(a.getAtom_site_label_atom_id())) {
            String pname = b.getToAtom().getAtom_site_label_atom_id();
            Atom m = Mol.getAtomByLabelAtomId(aas, pname);
            if (m != null) {
              res_bonds_constructed.add(new Bond(a, m, b.getBondOrder()));
            } else {
              // In case of new two atoms having Bonds, adds only "From" atoms
              Atom m2 = Mol.getAtomByLabelAtomId(res_atoms_constructed, pname);
              if (m2 != null) {
                res_bonds_constructed.add(new Bond(a, m2, b.getBondOrder()));
              }
            }
          }
        }

        for (Bond b : i_cc_mol.getBonds()) {
          if (b.getToAtom().getAtom_site_label_atom_id().equals(a.getAtom_site_label_atom_id())) {
            String pname = b.getFromAtom().getAtom_site_label_atom_id();
            Atom m = Mol.getAtomByLabelAtomId(aas, pname);
            if (m != null) {
              res_bonds_constructed.add(new Bond(m, a, b.getBondOrder()));
            }
          }
        }
      }
      atoms_constructed.addAll(res_atoms_constructed);
      bonds_constructed.addAll(res_bonds_constructed);

    }

    for (Bond b : bonds_constructed) {
      ret.addBondRepo("Dummy bond: " + b.getFromAtom().getUniqueCode() + "#" + b.getToAtom().getUniqueCode() + "#"
          + b.getBondOrder());
    }

    atoms.addAll(atoms_constructed);
    bonds.addAll(bonds_constructed);

    for (int ii = 0; ii < atoms.size(); ii++) {
      for (int jj = ii + 1; jj < atoms.size(); jj++) {
        double ddist = atoms.get(ii).distance(atoms.get(jj));
        if (ddist < 1.0) {
          ret.addAtomRepo("Atoms too close( < 1.0): distance:" + ddist + "#" + atoms.get(ii).getUniqueCode()
              + "#" + atoms.get(jj).getUniqueCode());
        }
      }
    }

    return ret;
  }

  public static String checkModelNum(List<Atom> atoms) {
    String modelid = null;
    for (Atom a : atoms) {
      if (modelid == null) {
        modelid = a.getAtom_site_pdbx_PDB_model_num();
      } else {
        if (!modelid.equals(a.getAtom_site_pdbx_PDB_model_num())) {
          throw new RuntimeException("Error in code. atom_site_pdbx_PDB_model_num must be one");
        }
      }
    }
    return modelid;
  }

  public ValidationReport checkAnomerState(List<Atom> atoms_with_h, List<Bond> bonds)
      throws IOException, MMcifParseException {
    Set<String> saccharides = new HashSet<>(ccLoader.saccharideIds());
    List<Atom> atoms = atoms_with_h.stream().filter(o -> !o.getAtom_site_type_symbol().equals("H"))
        .collect(Collectors.toList());

    ValidationReport ret = new ValidationReport();
    HashMap<String, ArrayList<Atom>> res_grouped = new HashMap<>();

    ChemicalComponentDictionary.checkModelNum(atoms);

    Set<String> modifications = new HashSet<>(ccLoader.modificationIds());
    for (int i = 0; i < atoms.size(); i++) {
      if (!saccharides.contains(atoms.get(i).getComp_id()))
        continue;
      if (modifications.contains(atoms.get(i).getComp_id()))
        continue;
      String res = atoms.get(i).getUniqueResidueCode();
      if (!res_grouped.containsKey(res)) {
        res_grouped.put(res, new ArrayList<>());
      }
      res_grouped.get(res).add(atoms.get(i));
    }

    for (String s : res_grouped.keySet()) {

      ArrayList<Atom> aas = res_grouped.get(s);// atoms in the same residue
//      HashSet<Atom> aas_hs = new HashSet<>(aas);
      Mol i_cc_mol = ccLoader.load(aas.get(0).getComp_id());

      HashSet<Atom> in_ring = new HashSet<>();
      for (Atom a : aas) {
        if (a.getAtom_site_type_symbol().equals("H")) {
          continue;
        }
        if (isInRingStructure(a, atoms, bonds)) {
          in_ring.add(a);
        }
      }
      for (Atom a : in_ring) {
        if (a.getAtom_site_type_symbol().equals("C")) {

          HashSet<Atom> conn = Bond.getConnectedAtoms(a, bonds);

          HashSet<Atom> conn_in = new HashSet<>(conn);
          conn_in.retainAll(in_ring);

          HashSet<Atom> conn_out = new HashSet<>(conn);
          conn_out.removeAll(in_ring);

          if (conn_in.size() == 2 && conn_out.size() == 1) {
            Atom outatom = conn_out.iterator().next();
            if (outatom.getAtom_site_type_symbol().equals("O")) {
              Atom inref = i_cc_mol.getAtomByLabelAtomId(a.getAtom_site_label_atom_id());
              boolean anomercheck = possiblyDifferentAnomer(a, aas, bonds, inref, i_cc_mol.getAtoms(),
                  i_cc_mol.getBonds());
              if (anomercheck) {
                ret.addAtomRepo("Possibly different anomer: " + a.getUniqueCode());
              }
            }
          }
        }
      }
    }
    return ret;
  }

  /**
   * Returns {@code true} if the target atom is a member of ring.
   * 
   * @param target
   * @param atoms
   * @param bonds
   * @return
   */
  public static boolean isInRingStructure(Atom target, List<Atom> atoms, List<Bond> bonds) {
    HashMap<Atom, HashSet<Atom>> connected_with = new HashMap<>();
    for (Atom a : atoms) {
      connected_with.put(a, new HashSet<>());
    }
    if (!connected_with.containsKey(target)) {
      return false;
    }
    for (Bond b : bonds) {
      Atom fa = b.getFromAtom();
      Atom ta = b.getToAtom();
      if (connected_with.containsKey(fa) && connected_with.containsKey(ta)) {
        connected_with.get(fa).add(ta);
        connected_with.get(ta).add(fa);
      }
    }

    while (connected_with.get(target).size() > 1) {
      ArrayList<Atom> updated = new ArrayList<>();
      Atom a = connected_with.get(target).iterator().next();
      connected_with.get(a).remove(target);
      connected_with.get(target).remove(a);
      updated.add(a);

      while (updated.size() > 0) {
        Atom tt = updated.remove(0);
        if (connected_with.get(tt).size() > 0) {
          ArrayList<Atom> taa = new ArrayList<>(connected_with.get(tt));
          for (Atom b : taa) {
            connected_with.get(b).remove(tt);
            connected_with.get(tt).remove(b);
            updated.add(b);
            if (b == target) {
              return true;
            }
          }
        }
      }

    }
    return false;
  }

  // Dirty
  // target にアノマーの中心になるつまり酸素と、環に含まれる原子二つと結合する
  // 炭素
  // atoms には Mol に含まれる全 Atom を渡す。
  // bonds には Mol に含まれる全 Bond を渡す。
  // 1 の方に CC からの Atom と Bond を渡すことを想定している。
  public boolean possiblyDifferentAnomer(Atom target0,
      List<Atom> atoms0, List<Bond> bonds0,
      Atom target1,
      List<Atom> atoms1, List<Bond> bonds1) {
    HashSet<Atom> connected0 = Bond.getConnectedAtoms(target0, bonds0);
    HashSet<Atom> connected1 = Bond.getConnectedAtoms(target1, bonds1);

    ArrayList<Atom> in_ring0 = new ArrayList<>();
    ArrayList<Atom> in_ring1 = new ArrayList<>();

    ArrayList<Atom> out_ring0 = new ArrayList<>();
    ArrayList<Atom> out_ring1 = new ArrayList<>();

    for (Atom a : connected0) {
      if (a.getAtom_site_type_symbol().equals("H")) {
        continue;
      }
      if (isInRingStructure(a, atoms0, bonds0)) {
        in_ring0.add(a);
      } else {
        if (a.getAtom_site_type_symbol().equals("O")) {
          out_ring0.add(a);
        }
      }
    }
    for (Atom a : connected1) {
      if (a.getAtom_site_type_symbol().equals("H")) {
        continue;
      }
      if (isInRingStructure(a, atoms1, bonds1)) {
        in_ring1.add(a);
      } else {
        if (a.getAtom_site_type_symbol().equals("O")) {
          out_ring1.add(a);
        }
      }
    }
    if (in_ring0.size() != 2 || in_ring1.size() != 2) {
      logger.warn("Couldn't check anomer state. The number of connected atoms in ring must be 2.");
      return false;
    }

    if (out_ring0.size() != 1 || out_ring1.size() != 1) {
      logger.warn("Couldn't check anomer state. The number of connected atoms out of ring must be 1.");
      return false;
    }

    Atom tatom = out_ring0.get(0);// Position for an atom in entry
    Atom t_sideA = null;
    Atom t_sideB = null;

    Atom ratom = out_ring1.get(0);// Position for an oxygen of CC
    Atom r_sideA = in_ring1.get(0);
    Atom r_sideB = in_ring1.get(1);

    t_sideA = Mol.getAtomByLabelAtomId(in_ring0, r_sideA.getAtom_site_label_atom_id());
    t_sideB = Mol.getAtomByLabelAtomId(in_ring0, r_sideB.getAtom_site_label_atom_id());

    ArrayList<Location3D> referencepos = new ArrayList<>();
    ArrayList<Location3D> targetpos = new ArrayList<>();
    targetpos.add(new Location3D(target0));
    targetpos.add(new Location3D(t_sideA));
    targetpos.add(new Location3D(t_sideB));

    Location3D rref = new Location3D(ratom);
    Location3D rref2 = new Location3D(ratom);
    referencepos.add(new Location3D(target1));
    referencepos.add(new Location3D(r_sideA));
    referencepos.add(new Location3D(r_sideB));

    ArrayList<Vector3D> dummy = new ArrayList<>();
    dummy.add(rref);
    Superposition.adjustVector3D(referencepos.get(0), referencepos.get(1), referencepos.get(2), targetpos.get(0),
        targetpos.get(1), targetpos.get(2), dummy);

    ArrayList<Vector3D> dummy2 = new ArrayList<>();
    dummy2.add(rref2);
    Superposition.adjustVector3D(referencepos.get(0), referencepos.get(1), referencepos.get(2), targetpos.get(0),
        targetpos.get(2), targetpos.get(1), dummy2);

    double samedist = Vector3D.distance(dummy.get(0), tatom);
    double diffdist = Vector3D.distance(dummy2.get(0), tatom);
    // System.out.println(samedist+";"+diffdist);

    return samedist > diffdist;
  }

  public ValidationReport checkBondLength(List<Bond> bonds) {
    ValidationReport ret = new ValidationReport();

    for (Bond b : bonds) {
      Atom iatom = b.getFromAtom();
      Atom jatom = b.getToAtom();
      double adist = iatom.distance(jatom);
      double dthreshold = VDW.getVDWRadii(iatom.getAtom_site_type_symbol())
          + VDW.getVDWRadii(jatom.getAtom_site_type_symbol());
      if (dthreshold < adist) {
        ret.addBondRepo("Long bond: " + iatom.getUniqueCode() + "#" + jatom.getUniqueCode() + "#entry:" + adist
            + "#threshold:" + dthreshold);
      }
    }
    return ret;
  }

  public List<Bond> bondsFor(List<Atom> atoms) throws IOException, MMcifParseException {
    List<Bond> out_bonds = new LinkedList<>();

    // Group by label_asym_id, auth_seq_id and comp_id
    Map<String, List<Atom>> mapGroupIdToAtoms = new HashMap<>();
    for (Atom atom : atoms) {
      String groupId = atom.getAtom_site_label_asym_id()+";;;"
                     + atom.getAtom_site_auth_seq_id()+";;;"
                     + atom.getComp_id();
      // Use label_alt_id if exists
      if ( !atom.getAtom_site_label_alt_id().isEmpty() )
        groupId += ";;;"+atom.getAtom_site_label_alt_id();
      if ( !mapGroupIdToAtoms.containsKey(groupId) )
        mapGroupIdToAtoms.put(groupId, new ArrayList<>());
      mapGroupIdToAtoms.get(groupId).add(atom);
    }

    for ( String groupId : mapGroupIdToAtoms.keySet() ) {
      String compId = groupId.split(";;;")[2];
      if ( compId.isEmpty() )
        continue;

      Mol molCC = ccLoader.load(compId);
      boolean useAltId = false;

      List<Atom> groupAtoms = mapGroupIdToAtoms.get(groupId);

      // Check all atom_site.label_atom_ids are contained in chem_comp_atom.atom_ids
      boolean isContained = true;
      for ( Atom atom : groupAtoms ) {
        if ( molCC.containsAtomForAtomId(atom.getAtom_site_label_atom_id()) )
          continue;
        isContained = false;
        logger.warn("label_atom_id {} is not found in {} as chem_comp_atom.atom_id, try to find it as chem_comp_atom.alt_atom_id.",
                atom.getAtom_site_label_atom_id(), compId);
        break;
      }
      // If not, check all atom_site.label_atom_ids are contained in chem_comp_atom.alt_atom_ids
      if ( !isContained ) {
        isContained = true;
        for ( Atom atom : groupAtoms ) {
          if ( !molCC.containsAtomForAltAtomId(atom.getChem_comp_atom_alt_atom_id()) )
            continue;
          isContained = false;
          logger.warn("label_atom_id {} is not found in {} as chem_comp_atom.alt_atom_id.",
            atom.getAtom_site_label_atom_id(), compId);
        }
        // If all atoms are contained as alt_atom_ids,
        // find bonds using alt_atom_ids instead of label_atom_ids
        if ( isContained ) {
          useAltId = true;
          logger.info("Use chem_comp_atom.alt_atom_id for {} to bond atoms.", compId);
        }
      }

      // bond atoms
      for (int i = 0; i < groupAtoms.size()-1; i++) {
        Atom atomi = groupAtoms.get(i);
        for (int j = i+1; j < groupAtoms.size(); j++) {
          Atom atomj = groupAtoms.get(j);

          int bond_order = molCC.getBondOrder(
              atomi.getAtom_site_label_atom_id(),
              atomj.getAtom_site_label_atom_id(),
              useAltId
            );

          if (bond_order > 0) {
            Bond bd = new Bond(atomi, atomj);
            bd.setBondOrder(bond_order);
            out_bonds.add(bd);
          }

        }
      }
    }

    return out_bonds;
  }
}

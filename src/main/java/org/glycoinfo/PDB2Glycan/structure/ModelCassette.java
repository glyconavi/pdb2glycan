package org.glycoinfo.PDB2Glycan.structure;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ModelCassette {

  public static class Saccharide {
    // ISK
    public String WURCS;
    //public String WURCS_URI;
    public String GTC; // GlyTouCan ID
    //public String GTC_URI; // GlyTouCan ID
    // TODO: Aglycones when it is contained the WURCS
    public List<String> aglycones;
    public WURCSType type;
  }

  public String pdbx_PDB_model_num;
  public String label_alt_id;
  public String CTfile;
  public String mmCIF; // added. 6.Jan.2023
  public String mmJSON; // added. 6.Jan.2023

  public List<Saccharide> saccharides;

  @JsonIgnore
  public List<Atom> atoms;
  // TODO: add container for atom properties (charge, occupancy, and B-factor)

  public List<NearbyComponent> interacting_components;
}

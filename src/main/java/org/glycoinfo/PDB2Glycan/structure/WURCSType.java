package org.glycoinfo.PDB2Glycan.structure;


public enum WURCSType {
    /** Standard */
    STD(),
    /** Separated with aglycone */
    SEP(),
    /** Contains aglycone */
    CON();
}

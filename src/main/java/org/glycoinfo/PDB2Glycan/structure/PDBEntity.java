package org.glycoinfo.PDB2Glycan.structure;

import java.util.ArrayList;
import java.util.LinkedList;

public class PDBEntity {

  public ArrayList<String> asym_ids_n = new ArrayList<>();
  public ArrayList<String> entity_ids_n = new ArrayList<>();
  public ArrayList<ModelCassette> model = new ArrayList<>();
  public LinkedList<Proteindb> protein_db = new LinkedList<Proteindb>();
}

package org.glycoinfo.PDB2Glycan.structure;

public class TransformMol {
  public static void translateMol(Mol mol, Vector3D v) {
    for ( Atom atom : mol.getAtoms() )
      addVector(atom, v);
  }

  private static void addVector(Vector3D v, Vector3D toAdd) {
    v.setX( v.getX() + toAdd.getX() );
    v.setY( v.getY() + toAdd.getY() );
    v.setZ( v.getZ() + toAdd.getZ() );
  }

//  private static void subtractVector(Vector3D v, Vector3D toSub) {
//    v.setX( v.getX() - toSub.getX() );
//    v.setY( v.getY() - toSub.getY() );
//    v.setZ( v.getZ() - toSub.getZ() );
//  }

  public static void rotateMol(Mol mol, Vector3D v, double deg) {
    for ( Atom atom : mol.getAtoms() )
      rotateVector(atom, v, deg);
  }

  private static void rotateVector(Vector3D v, Vector3D vn, double rad) {
    double nr = norm(vn);
    Location3D n = new Location3D( vn.getX()/nr, vn.getY()/nr, vn.getZ()/nr );

    double cos = Math.cos(rad);
    double sin = Math.sin(rad);
    double Rxx = cos+n.x*n.x*(1-cos);
    double Rxy = n.x*n.y*(1-cos)-n.z*sin;
    double Rxz = n.z*n.x*(1-cos)+n.y*sin;
    double Ryx = n.x*n.y*(1-cos)+n.z*sin;
    double Ryy = cos+n.y*n.y*(1-cos);
    double Ryz = n.y*n.z*(1-cos)-n.x*sin;
    double Rzx = n.z*n.x*(1-cos)-n.y*sin;
    double Rzy = n.y*n.z*(1-cos)+n.x*sin;
    double Rzz = cos+n.z*n.z*(1-cos);
    double dx = v.getX()*Rxx+v.getY()*Rxy+v.getZ()*Rxz;
    double dy = v.getX()*Ryx+v.getY()*Ryy+v.getZ()*Ryz;
    double dz = v.getX()*Rzx+v.getY()*Rzy+v.getZ()*Rzz;

    v.setCoordinates(dx, dy, dz);
  }

  public static Location3D normalOnVector(Vector3D v, Location3D pos) {
    double r = v.getX()*pos.x+v.getY()*pos.y+v.getZ()*pos.z;
    r /= norm(v);
    return new Location3D(r*v.getX(), r*v.getY(), r*v.getZ());
  }

  public static Location3D center(Vector3D ... vs) {
    double dx=0, dy=0, dz=0;
    for ( Vector3D v : vs ) {
      dx += v.getX();
      dy += v.getY();
      dz += v.getZ();
    }
    return new Location3D( dx/vs.length, dy/vs.length, dz/vs.length );
  }

  public static Location3D center(Mol mol) {
    return center(mol.getAtoms().toArray(new Vector3D[0]) );
  }

  public static Location3D vector(Vector3D from, Vector3D to) {
    Location3D loc = new Location3D(
        to.getX()-from.getX(),
        to.getY()-from.getY(),
        to.getZ()-from.getZ()
      );
    return loc;
  }

  public static Location3D outer(Vector3D v1, Vector3D v2) {
    return new Location3D(
        v1.getY()*v2.getZ()-v1.getZ()*v2.getY(),
        v1.getZ()*v2.getX()-v1.getX()*v2.getZ(),
        v1.getX()*v2.getY()-v1.getY()*v2.getX()
      );
  }

  public static double inner(Vector3D v1, Vector3D v2) {
    return v1.getX()*v2.getX()+v1.getY()*v2.getY()+v1.getZ()*v2.getZ();
  }

  public static double norm(Vector3D v) {
    return Math.sqrt(inner(v,v));
  }

  public static double angle(Vector3D v1, Vector3D v2) {
    return Math.acos(inner(v1, v2)/(norm(v1)*norm(v2)));
  }
}

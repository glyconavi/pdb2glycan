package org.glycoinfo.PDB2Glycan.structure;


public abstract class StructureRange {

  public abstract String getBeg_label_asym_id();

  public abstract String getBeg_label_seq_id();

  public abstract String getEnd_label_asym_id();

  public abstract String getEnd_label_seq_id();

  public boolean contains(String label_asym_id, String label_seq_id) {
    if (!this.getBeg_label_asym_id().equals(this.getEnd_label_asym_id())) {
      // In case it has two or more asym_id in the range, return true when seq_id is after beg or before end.
      if (this.getBeg_label_asym_id().equals(label_asym_id)) {
        if (Integer.parseInt(this.getBeg_label_seq_id()) <= Integer.parseInt(label_seq_id)) {
          return true;
        }
      }

      if (this.getEnd_label_asym_id().equals(label_asym_id)) {
        return Integer.parseInt(this.getEnd_label_seq_id()) >= Integer.parseInt(label_seq_id);
      }
      return false;
    }

    if (!this.getBeg_label_asym_id().equals(label_asym_id)) {
      return false;
    }

    if (Integer.parseInt(this.getBeg_label_seq_id()) > Integer.parseInt(label_seq_id)) {
      return false;
    }

    return Integer.parseInt(this.getEnd_label_seq_id()) >= Integer.parseInt(label_seq_id);
  }
}

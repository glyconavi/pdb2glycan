package org.glycoinfo.PDB2Glycan.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;



public class NearbyComponent {
	@JsonIgnore
	private AtomAtomDistance nearestEntry;
	
	
	@JsonIgnore
	private ComponentInfo componentInfo;
	
	
	public NearbyComponent(AtomAtomDistance d) {
		this.nearestEntry = d;
		this.componentInfo = new ComponentInfo(this.nearestEntry.getTarget());
	}
	
	public AtomAtomDistance getNearestEntry() {
		return nearestEntry;
	}
	
	public static ArrayList<NearbyComponent> mergeTarget(List<AtomAtomDistance> lis){
		ArrayList<NearbyComponent> ret = new ArrayList<>();
		HashMap<String,ArrayList<AtomAtomDistance>> hm = new HashMap<>();
		for(AtomAtomDistance aa:lis) {
			String rc = aa.getTarget().getUniqueResidueCode();
			if(!hm.containsKey(rc)) {
				hm.put(rc,new ArrayList<>());
			}
			hm.get(rc).add(aa);
		}
		for(String ss:hm.keySet()) {
			ArrayList<AtomAtomDistance> al = hm.get(ss);
			double mindist = al.get(0).distance();
			AtomAtomDistance naa = al.get(0);
			for(AtomAtomDistance d:al) {
				if(d.distance() < mindist) {
					mindist = d.distance();
					naa = d;
				}
			}
			ret.add(new NearbyComponent(naa));
		}
		
		return ret;
	}
	
	@JsonProperty("source_atom")
	public HashMap<String,String> getSourceAtomStringMap(){
		return getAtomStringMap(this.nearestEntry.getSource());
	}
	
	@JsonProperty("nearest_target_atom")
	public HashMap<String,String> getTargetAtomStringMap(){
		return getAtomStringMap(this.nearestEntry.getTarget());
	}
		
	@JsonProperty("target_component")
	public ComponentInfo getResidueInfo(){
		return this.componentInfo;
	}
	
	private static HashMap<String,String> getAtomStringMap(Atom a){
		HashMap<String,String> ret = new HashMap<>();
		ret.put("atom_site_pdbx_PDB_model_num",a.getAtom_site_pdbx_PDB_model_num());
		ret.put("atom_site_id",a.getAtom_site_id());
		ret.put("atom_site_label_atom_id",a.getAtom_site_label_atom_id());
		ret.put("atom_site_label_alt_id",a.getAtom_site_label_alt_id());
		ret.put("atom_site_label_comp_id",a.getAtom_site_label_comp_id());
		ret.put("atom_site_label_asym_id",a.getAtom_site_label_asym_id());
		ret.put("atom_site_label_entity_id",a.getAtom_site_label_entity_id());
		ret.put("atom_site_label_seq_id",a.getAtom_site_label_seq_id());
		ret.put("atom_site_pdbx_PDB_ins_code",a.getAtom_site_pdbx_PDB_ins_code());
		return ret;
	}
	
	@JsonProperty("distance")
	public String getDistanceString() {
		return String.valueOf(this.nearestEntry.distance());
	}
	
}

package org.glycoinfo.PDB2Glycan.structure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Mol {

	private static final Logger logger = LoggerFactory.getLogger(Mol.class);

	private List<Atom> atoms;
	private List<Bond> bonds;

	public Mol(List<Atom> a_atoms, List<Bond> a_bonds) {
		this.atoms = a_atoms;
		this.bonds = a_bonds;
	}

	public List<Atom> getAtoms() {
		return atoms;
	}

	public void setAtoms(List<Atom> atoms) {
		this.atoms = atoms;
	}

	public List<Bond> getBonds() {
		return bonds;
	}

	public void setBonds(List<Bond> bonds) {
		this.bonds = bonds;
	}

	public Atom getAtomByLabelAtomId(String s) {
		return Mol.getAtomByLabelAtomId(this.atoms, s);
	}

	public static Atom getAtomByLabelAtomId(List<Atom> alis, String s) {
		Atom ret = null;
		for (Atom a : alis) {
			if (a.getAtom_site_label_atom_id().equals(s)) {
				if (ret != null) {
					logger.warn("Multiple atoms were found for " + s + ".");
				} else {
					ret = a;
				}
			}
		}
		return ret;
	}

	public boolean containsAtomForAtomId(String atom_id) {
		for ( Atom atom : this.atoms )
			if ( atom.getAtom_site_label_atom_id().equals(atom_id) )
				return true;
		return false;
	}

	public boolean containsAtomForAltAtomId(String alt_atom_id) {
		for ( Atom atom : this.atoms )
			if ( atom.getChem_comp_atom_alt_atom_id().equals(alt_atom_id) )
				return true;
		return false;
	}

	public int getBondOrder(String a_atom_id_1, String a_atom_id_2, boolean useAltId) {
		return (!useAltId)? getBondOrder(a_atom_id_1, a_atom_id_2) :
				getBondOrderWithAltAtomId(a_atom_id_1, a_atom_id_2);
	}

	private int getBondOrder(String a_atom_id_1, String a_atom_id_2) {
		// Check chem_comp_atom.alt_atom_id as well
		for (int i = 0; i < getBonds().size(); i++) {
			if (getBonds().get(i).getFromAtom().getAtom_site_label_atom_id().equals(a_atom_id_1)) {
				if (getBonds().get(i).getToAtom().getAtom_site_label_atom_id().equals(a_atom_id_2)) {
					return getBonds().get(i).getBondOrder();
				}
			} else if (getBonds().get(i).getFromAtom().getAtom_site_label_atom_id().equals(a_atom_id_2)) {
				if (getBonds().get(i).getToAtom().getAtom_site_label_atom_id().equals(a_atom_id_1)) {
					return getBonds().get(i).getBondOrder();
				}
			}
		}
		return 0;
	}

	private int getBondOrderWithAltAtomId(String a_atom_id_1, String a_atom_id_2) {
		// Check chem_comp_atom.alt_atom_id as well
		for (int i = 0; i < getBonds().size(); i++) {
			if (getBonds().get(i).getFromAtom().getChem_comp_atom_alt_atom_id().equals(a_atom_id_1)) {
				if (getBonds().get(i).getToAtom().getChem_comp_atom_alt_atom_id().equals(a_atom_id_2)) {
					return getBonds().get(i).getBondOrder();
				}
			} else if (getBonds().get(i).getFromAtom().getChem_comp_atom_alt_atom_id().equals(a_atom_id_2)) {
				if (getBonds().get(i).getToAtom().getAtom_site_label_atom_id().equals(a_atom_id_1)) {
					return getBonds().get(i).getBondOrder();
				}
			}
		}
		return 0;
	}

	public double calcAtomAtomDistance(String a_atom_id_1, String a_atom_id_2) {
		for (int i = 0; i < atoms.size(); i++) {
			if (atoms.get(i).getAtom_site_label_atom_id().contentEquals(a_atom_id_1)) {
//				Atom aatom = atoms.get(1);
				for (int j = 0; j < atoms.size(); j++) {
					if (atoms.get(j).getAtom_site_label_atom_id().contentEquals(a_atom_id_2)) {
						return atoms.get(i).distance(atoms.get(j));
					}
				}
			}
		}
		throw new RuntimeException("Could not find atom pairs. " + a_atom_id_1 + " " + a_atom_id_2);
	}

	public List<Bond> getBondsForAtom(Atom atom) {
		List<Bond> bonds = new ArrayList<>();
		for (Bond bond : this.bonds) {
			if (bond.getToAtom().equals(atom) || bond.getFromAtom().equals(atom))
				bonds.add(bond);
		}
		return bonds;
	}
}

package org.glycoinfo.PDB2Glycan.structure.glyco;

import static org.glycoinfo.PDB2Glycan.util.StringUtils.modelCode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.biojava.nbio.structure.io.mmcif.model.PdbxPolySeqScheme;
import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.file.ChemicalComponentLoader;
import org.glycoinfo.PDB2Glycan.io.file.RemoteChemicalComponentLoader;
import org.glycoinfo.PDB2Glycan.io.model.Protein;
import org.glycoinfo.PDB2Glycan.io.model.protein.AtomSite;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxUnobsOrZeroOccAtoms;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxValidateCloseContact;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;
import org.glycoinfo.PDB2Glycan.structure.Atom;
import org.glycoinfo.PDB2Glycan.structure.AtomSiteGroupType;
import org.glycoinfo.PDB2Glycan.structure.Bond;
import org.glycoinfo.PDB2Glycan.structure.ChemicalComponentDictionary;
import org.glycoinfo.PDB2Glycan.structure.ConnectedEntryModel;
import org.glycoinfo.PDB2Glycan.util.MessageBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModelBuilder {
  private static final double INTERACTION_THRESHOLD = 6.0;// Distance for atom-atom interaction

  private static final Logger logger = LoggerFactory.getLogger(ModelBuilder.class);

  private static final String[] ions = { "CA", "MG", "NA", "CL" };

  private Protein protein;
  private ChemicalComponentLoader ccLoader;

  private GlycoProtein glycoProtein;
  private List<String> atomSiteLabelAsymId;
//  private List<ConnectedEntryModel> connectedEntries;
  private List<String> molCodes;

  private static List<String> ions() {
    return Arrays.asList(ions);
  }

  public ModelBuilder(Protein protein) {
    this(protein, new RemoteChemicalComponentLoader());
  }

  public ModelBuilder(Protein protein, ChemicalComponentLoader ccLoader) {
    this.protein = protein;
    this.ccLoader = ccLoader;
    this.ccLoader.setProtein(protein);
  }

  public GlycoProtein build() {
    if (this.glycoProtein != null) {
      return this.glycoProtein;
    }

    ValidationReport vr = new ValidationReport();
    atomSiteLabelAsymId = new ArrayList<>();
    List<ConnectedEntryModel> connectedEntries = new ArrayList<>();
    molCodes = protein.getAtomSites().stream()
        .map(x -> molCode(protein.getPdbCode(), x.getPdbx_PDB_model_num()))
        .collect(Collectors.toList());

    ChemicalComponentDictionary ccdic = new ChemicalComponentDictionary(ccLoader);

    molCodes.stream().distinct().sorted().forEach(code -> {
      logger.debug("Processing model: {}", code);

      connectedEntries.addAll( processModel(code, ccdic) );
    });

    GlycoProtein glycoProtein = new GlycoProtein();

    glycoProtein.setAtomSiteLabelAsymId(atomSiteLabelAsymId);
    for (ConnectedEntryModel cm : connectedEntries) {
      // Collect unobserved atoms for missing atoms
      List<Atom> atomsToBeAdded = new ArrayList<>();
      for ( PdbxUnobsOrZeroOccAtoms atomUnobs : protein.getPdbxUnobsOrZeroOccAtoms() )
        if ( cm.getAllAsymIds().contains(atomUnobs.getLabel_asym_id()) )
          atomsToBeAdded.add(convertAtom(atomUnobs));

      try {
        vr.append(ccdic.addMissingAtoms(cm.atoms, cm.bonds, atomsToBeAdded));
        vr.append(ccdic.checkAnomerState(cm.atoms, cm.bonds));
        vr.append(ccdic.checkBondLength(cm.bonds));
      } catch (IOException e) {
        e.printStackTrace();
        System.exit(-1);
      } catch (MMcifParseException e) {
        e.printStackTrace();
        System.exit(-1);
      }
    }
    glycoProtein.setConnectedEntries(connectedEntries);
    glycoProtein.setProtein(protein);
    glycoProtein.setValidationReport(vr);
    this.glycoProtein = glycoProtein;
    
    return this.glycoProtein;
  }

  private List<ConnectedEntryModel> processModel(String code, ChemicalComponentDictionary ccdic) {

    // Populate atoms with trimming low occupancy atoms
    List<Atom> allAtoms = new ArrayList<>();
    Map<String, Atom> atomcode_to_atom = new HashMap<>();
    for (AtomSite as : protein.getAtomSites()) {

      String molcode = molCode(protein.getPdbCode(), as.getPdbx_PDB_model_num());

      if (!code.equals(molcode))
        continue;

      Atom atom = convertAtom(as);

      allAtoms.add(atom);

      // Check occupancies
      String uniqueCode = atom.getUniqueCode();

      if (!atomcode_to_atom.containsKey(uniqueCode)) {
        atomcode_to_atom.put(uniqueCode, atom);
        continue;
      }

      // Never remove saccharide atoms
      if ( ccLoader.isSaccharideAtom(atom) ) {
        atomcode_to_atom.put(uniqueCode+";"+atom.getAtom_site_label_alt_id() , atom);
        continue;
      }

      // Never remove atoms connecting saccharide atoms
      boolean noRemove = false;
      for ( StructConn sConn : saccharideStructConnections() ) {
        if ( !sConn.getConn_type_id().equals("covale") )
          continue;
        if ( !sConn.isFromAtom(atom, false) && !sConn.isToAtom(atom, false) )
          continue;
        noRemove = true;
        break;
      }
      if ( noRemove ) {
        atomcode_to_atom.put(uniqueCode+";"+atom.getAtom_site_label_alt_id() , atom);
        continue;
      }

      // Use higher occupancy if alternative atom
      // Earlier one remains if the occupancies are the same value (non-check)
      if (atomcode_to_atom.get(uniqueCode).getAtom_site_occupancy() < atom.getAtom_site_occupancy()) {
        atomcode_to_atom.put(uniqueCode, atom);
      }
    }

    removeLowOccupancyAtoms(allAtoms, atomcode_to_atom.values());

    int atom_count = 1;
    int saccharide_atom_count = 1;
    int ion_count = 1;

    // Collect saccharide atoms
    List<Atom> atoms = new LinkedList<>();
    List<Atom> saccharideAtoms = new LinkedList<>();
    for (Atom at : allAtoms) {
      if (at.getGroup_PDB().equals(AtomSiteGroupType.ATOM.toString())) {
        at.setCtfile_id(atom_count);
        atom_count++;
        atoms.add(at);
      } else if (at.getGroup_PDB().equals(AtomSiteGroupType.HETATM.toString())) {

        if ( ccLoader.isSaccharideAtom(at) ) {
//        if (saccharideIds.contains(at.getAtom_site_label_comp_id())) {
          at.setCtfile_id(saccharide_atom_count);
          saccharide_atom_count++;
          saccharideAtoms.add(at);
        } else {
          at.setCtfile_id(atom_count);
          atom_count++;
          atoms.add(at);
        }

        if (ions().contains(at.getAtom_site_label_comp_id())) {
          at.setCtfile_id(ion_count);
          ion_count++;
        }
      } else {
        logger.warn(at.getUniqueCode() + " was not processed.");
      }
    }

    if (saccharideAtoms.size() == 0)
      return new ArrayList<>();


//    List<Atom> saccharide_atoms_noaa = new LinkedList<>(saccharideAtoms);
    List<Bond> het_bonds = null;
    try {
      het_bonds = ccdic.bondsFor(saccharideAtoms);
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(-1);
    } catch (MMcifParseException e) {
      e.printStackTrace();
      System.exit(-1);
    }

    // Add atoms connected to saccharide atoms
    int add_atom_count = saccharideAtoms.size() + 1;

    HashSet<Atom> saccharide_hs = new HashSet<>(saccharideAtoms);
    ArrayList<ArrayList<Atom>> conn_pair = getBondPair_StructConn(saccharideAtoms, atoms,
        saccharideStructConnections());

    ArrayList<Atom> atomsAdded = new ArrayList<Atom>();
    for (ArrayList<Atom> scc : conn_pair) {
      Atom from_atom = scc.get(0);
      Atom to_atom = scc.get(1);

      if (!saccharide_hs.contains(from_atom)) {
        // Adds atom which is non-saccharide but connecting saccharide
        // TODO: Remove non-saccharide atoms
        from_atom.setCtfile_id(add_atom_count);
        saccharideAtoms.add(from_atom);
        atomsAdded.add(from_atom);
        add_atom_count++;
      }

      if (!saccharide_hs.contains(to_atom)) {
        // Adds atom which is non-saccharide but connecting saccharide
        // TODO: Remove non-saccharide atoms
        to_atom.setCtfile_id(add_atom_count);
        saccharideAtoms.add(to_atom);
        atomsAdded.add(to_atom);
        add_atom_count++;
      }
    }

    // Collect label_asym_id
    for (Atom at : saccharideAtoms) {
      if (!atomSiteLabelAsymId.contains(at.getAtom_site_label_asym_id())) {
        atomSiteLabelAsymId.add(at.getAtom_site_label_asym_id());
      }
    }

    for (Atom at : atoms) {
      if (!atomSiteLabelAsymId.contains(at.getAtom_site_label_asym_id())) {
        atomSiteLabelAsymId.add(at.getAtom_site_label_asym_id());
      }
    }

    // Bond atoms
    List<Bond> str_conn_bonds = new LinkedList<>();
    HashMap<Atom, HashSet<Atom>> bondpair = new HashMap<>();
    for (ArrayList<Atom> scc : conn_pair) {
      Atom from_atom = scc.get(0);
      Atom to_atom = scc.get(1);
      if (!bondpair.containsKey(from_atom)) {
        bondpair.put(from_atom, new HashSet<>());
      }

      if (!bondpair.containsKey(to_atom)) {
        bondpair.put(to_atom, new HashSet<>());
      }

      if (bondpair.get(from_atom).contains(to_atom) || bondpair.get(to_atom).contains(from_atom)) {
        continue;
      }

      Bond bd = new Bond();
      bd.setFromAtom(scc.get(0));
      bd.setToAtom(scc.get(1));
      bd.setBondOrder(1);

      bondpair.get(from_atom).add(to_atom);
      bondpair.get(to_atom).add(from_atom);
      str_conn_bonds.add(bd);
    }

    // remove duplicated atoms
    HashSet<Atom> atomfilt = new HashSet<>();
    Iterator<Atom> ati = saccharideAtoms.iterator();
    while (ati.hasNext()) {
      Atom att = ati.next();
      if (atomfilt.contains(att)) {
        ati.remove();
        // Often happens when multiple bonds for one atom. Mostly alternative atom.
        logger.warn(MessageBuilder.multipleBond(att));
      }
      atomfilt.add(att);
    }

    het_bonds.addAll(str_conn_bonds);

    List<ConnectedEntryModel> seps =  ConnectedEntryModel.separateWithResidue(saccharideAtoms, het_bonds);

    for (ConnectedEntryModel cm : seps) {
      // Set linked atom and residue if exists
      for ( Atom atom : cm.atoms ) {
        if ( !atomsAdded.contains(atom) )
          continue;
        for ( PdbxPolySeqScheme poly : protein.getPdbxPolySeqScheme() ) {
          if ( !poly.getMon_id().equals( atom.getAtom_site_label_comp_id() ) )
            continue;
          if ( !poly.getSeq_id().equals( atom.getAtom_site_label_seq_id() ) )
            continue;
          if ( !poly.getAsym_id().equals( atom.getAtom_site_label_asym_id() ) )
            continue;
          cm.linkedAtom = atom;
          cm.linkedResidue = poly;

          break;
        }
      }

      // Adds atoms within the threshold distance. They are duplicated in some residues.
      cm.addNearbyAtoms(INTERACTION_THRESHOLD, allAtoms, atomsAdded);
    }

    return seps;
  }

  public ArrayList<ArrayList<Atom>> getBondPair_StructConn(List<Atom> saccharide_atoms_noaa,
      List<Atom> nonsaccharideatoms, List<StructConn> structconn) {

    ArrayList<ArrayList<Atom>> ret = new ArrayList<>();
    HashMap<String, ArrayList<Atom>> atomcode_to_atoms = new HashMap<>();

    for (Atom att : nonsaccharideatoms) {
      String attcode = att.getAtom_site_label_comp_id() + ";;;"
                     + att.getAtom_site_label_atom_id() + ";;;"
                     + att.getAtom_site_label_asym_id() + ";;;"
//                     + att.getAtom_site_label_seq_id();
                     + att.getAtom_site_auth_seq_id();
      if ( !att.getAtom_site_label_alt_id().isEmpty() )
        attcode += ";;;" + att.getAtom_site_label_alt_id();

      if (!atomcode_to_atoms.containsKey(attcode)) {
        atomcode_to_atoms.put(attcode, new ArrayList<Atom>());
      }
      atomcode_to_atoms.get(attcode).add(att);
    }

    for (int i = 0; i < structconn.size(); i++) {
      StructConn sconn = structconn.get(i);
      // Only allow for covalent bonds
      if (!sconn.getConn_type_id().equals("covale"))
        continue;

      Atom from_atom = null;
      for (Atom jatom : saccharide_atoms_noaa) {
        if (!sconn.isFromAtom(jatom, true))
          continue;
        if (from_atom != null) {
          logger.warn(MessageBuilder.fromAtomDuplication(sconn, jatom, from_atom));
          continue;
        }
        from_atom = jatom;
      }

      Atom to_atom = null;
      for (Atom jatom : saccharide_atoms_noaa) {
        if (!sconn.isToAtom(jatom, true))
          continue;
        if (to_atom != null) {
          logger.warn(MessageBuilder.toAtomDuplication(sconn, jatom, to_atom));
          continue;
        }
        to_atom = jatom;
      }

      String pt1code = sconn.getPtnr1_label_comp_id() + ";;;"
                     + sconn.getPtnr1_label_atom_id() + ";;;"
                     + sconn.getPtnr1_label_asym_id() + ";;;"
//                     + sconn.getPtnr1_label_seq_id();
                     + sconn.getPtnr1_auth_seq_id();
      if ( !sconn.getPdbx_ptnr1_label_alt_id().isEmpty() )
        pt1code += ";;;" + sconn.getPdbx_ptnr1_label_alt_id();

      String pt2code = sconn.getPtnr2_label_comp_id() + ";;;"
                     + sconn.getPtnr2_label_atom_id() + ";;;"
                     + sconn.getPtnr2_label_asym_id() + ";;;"
//                     + sconn.getPtnr2_label_seq_id();
                     + sconn.getPtnr2_auth_seq_id();
      if ( !sconn.getPdbx_ptnr2_label_alt_id().isEmpty() )
        pt2code += ";;;" + sconn.getPdbx_ptnr2_label_alt_id();

      if (atomcode_to_atoms.containsKey(pt1code)) {
        ArrayList<Atom> atlis = atomcode_to_atoms.get(pt1code);
        if (from_atom != null || atlis.size() > 1) {
          logger.warn(MessageBuilder.fromAtomDuplication(sconn));
        } else {
          from_atom = atlis.get(0);
        }
      }
      if (atomcode_to_atoms.containsKey(pt2code)) {
        ArrayList<Atom> atlis = atomcode_to_atoms.get(pt2code);
        if (to_atom != null || atlis.size() > 1) {
          logger.warn(MessageBuilder.toAtomDuplication(sconn));
        } else {
          to_atom = atlis.get(0);
        }
      }

      if (from_atom == null && to_atom == null)
        continue;

      if (from_atom == null) {
        logger.warn(MessageBuilder.fromAtomNotFound(sconn, protein.getPdbCode()));
        continue;
      }

      if (to_atom == null) {
        logger.warn(MessageBuilder.toAtomNotFound(sconn, protein.getPdbCode()));
        continue;
      }
      ArrayList<Atom> al = new ArrayList<>();
      al.add(from_atom);
      al.add(to_atom);
      ret.add(al);
    }

    return ret;
  }

  public ArrayList<ArrayList<Atom>> getBondPair_PdbxValidateCloseContact(List<Atom> saccharide_atoms_noaa,
      List<Atom> nonsaccharideatoms, List<PdbxValidateCloseContact> closecontact) {

    ArrayList<ArrayList<Atom>> ret = new ArrayList<>();
    HashMap<String, ArrayList<Atom>> atomcode_to_atoms = new HashMap<>();

    for (Atom att : nonsaccharideatoms) {
      String attcode = att.getAtom_site_auth_comp_id() + ";;;"
                     + att.getAtom_site_auth_atom_id() + ";;;"
                     + att.getAtom_site_auth_asym_id() + ";;;"
                     + att.getAtom_site_auth_seq_id() + ";;;"
                     + att.getAtom_site_pdbx_PDB_ins_code();
      if ( !att.getAtom_site_label_alt_id().isEmpty() )
        attcode += ";;;" + att.getAtom_site_label_alt_id();

      if (!atomcode_to_atoms.containsKey(attcode)) {
        atomcode_to_atoms.put(attcode, new ArrayList<Atom>());
      }
      atomcode_to_atoms.get(attcode).add(att);
    }

    for (int i = 0; i < closecontact.size(); i++) {
      PdbxValidateCloseContact sconn = closecontact.get(i);
      Atom from_atom = null;
      Atom to_atom = null;

      // TODO: Message builder to be modified properly
      String pt1code = sconn.getAuth_comp_id_1() + ";;;"
                     + sconn.getAuth_atom_id_1() + ";;;"
                     + sconn.getAuth_asym_id_1() + ";;;"
                     + sconn.getAuth_seq_id_1() + ";;;"
                     + sconn.getPDB_ins_code_1();
      if ( sconn.getLabel_alt_id_1().isEmpty() )
        pt1code += ";;;"+sconn.getLabel_alt_id_1();
      String pt2code = sconn.getAuth_comp_id_2() + ";;;"
                     + sconn.getAuth_atom_id_2() + ";;;"
                     + sconn.getAuth_asym_id_2() + ";;;"
                     + sconn.getAuth_seq_id_2() + ";;;"
                     + sconn.getPDB_ins_code_2();
      if ( sconn.getLabel_alt_id_2().isEmpty() )
          pt2code += ";;;"+sconn.getLabel_alt_id_2();

      for (Atom jatom : saccharide_atoms_noaa) {
        if (sconn.isAtom1(jatom)) {
          if (from_atom != null) {
            logger.warn("Atom1 " + pt1code + " duplication.");
            continue;
          }
          from_atom = jatom;
        }
      }

      for (Atom jatom : saccharide_atoms_noaa) {
        if (sconn.isAtom2(jatom)) {
          if (to_atom != null) {
            logger.warn("Atom2 " + pt2code + " duplication.");
            continue;
          }
          to_atom = jatom;
        }
      }

      if (atomcode_to_atoms.containsKey(pt1code)) {
        ArrayList<Atom> atlis = atomcode_to_atoms.get(pt1code);
        if (from_atom != null || atlis.size() > 1) {
          logger.warn("Atom1 " + pt1code + " duplication.");
        } else {
          // Adds atom which is non-saccharide but connecting saccharide
          // TODO: Check removing this
          from_atom = atlis.get(0);
        }
      }
      if (atomcode_to_atoms.containsKey(pt2code)) {
        ArrayList<Atom> atlis = atomcode_to_atoms.get(pt2code);
        if (to_atom != null || atlis.size() > 1) {
          logger.warn("Atom2 " + pt2code + " duplication.");
        } else {
          // Adds atom which is non-saccharide but connecting saccharide
          // TODO: Check removing this
          to_atom = atlis.get(0);
        }
      }

      if (from_atom == null) {
        logger.warn("Atom1 " + pt1code + " not found.");
        continue;
      }

      if (to_atom == null) {

        logger.warn("Atom2 " + pt2code + " not found.");
        continue;
      }
      ArrayList<Atom> al = new ArrayList<>();
      al.add(from_atom);
      al.add(to_atom);
      ret.add(al);
    }
    return ret;
  }

  private void removeLowOccupancyAtoms(List<Atom> source, Collection<Atom> atoms) {
    Set<Atom> set = new HashSet<>(atoms);

    if (set.size() != source.size()) {
      source.retainAll(set);
    }

    if (source.size() != (new HashSet<>(source)).size()) {
      throw new RuntimeException("Duplicated atoms found");
    }
  }

  /**
   * Returns Atom with the given {@link PdbxUnobsOrZeroOccAtoms} info to be added as missing atoms
   * 
   * @param atom {@link PdbxUnobsOrZeroOccAtoms}
   * @return
   */
  private Atom convertAtom(PdbxUnobsOrZeroOccAtoms atom) {
    Atom a = new Atom();
    a.setAtom_site_label_alt_id(atom.getLabel_alt_id());
    a.setAtom_site_label_asym_id(atom.getLabel_asym_id());
    a.setAtom_site_label_comp_id(atom.getLabel_comp_id());
    a.setAtom_site_label_seq_id(atom.getLabel_seq_id());
    a.setAtom_site_label_atom_id(atom.getLabel_atom_id());

    a.setAtom_site_auth_seq_id(atom.getAuth_seq_id());
    a.setAtom_site_auth_comp_id(atom.getAuth_comp_id());
    a.setAtom_site_auth_asym_id(atom.getAuth_asym_id());
    a.setAtom_site_auth_atom_id(atom.getAuth_atom_id());

    a.setAtom_site_pdbx_PDB_ins_code(atom.getPDB_ins_code());

    a.setSeq_id(atom.getLabel_seq_id());
//    a.setX(Double.parseDouble(atom.getCartn_x()));
//    a.setY(Double.parseDouble(atom.getCartn_y()));
//    a.setZ(Double.parseDouble(atom.getCartn_z()));
//    a.setAtom_site_type_symbol(atom.getType_symbol());
//    a.setGroup_PDB(atom.getGroup_PDB());
    a.setComp_id(atom.getLabel_comp_id());
    a.setAtom_site_id(atom.getId());
    a.setAtom_site_pdbx_PDB_model_num(atom.getPDB_model_num());

    return a;
  }

  /**
   * Creates Atom instance from AtomSite having mmCIF info
   * 
   * @param atom
   * @return
   */
  private Atom convertAtom(AtomSite atom) {
    Atom a = new Atom();

    a.setAtom_id(atom.getId());
    a.setAtom_site_label_entity_id(atom.getLabel_entity_id());
    a.setAtom_site_label_asym_id(atom.getLabel_asym_id());
    a.setAtom_site_label_atom_id(atom.getLabel_atom_id());
    a.setAtom_site_label_comp_id(atom.getLabel_comp_id());
    a.setAtom_site_label_seq_id(atom.getLabel_seq_id());
    a.setAtom_site_label_alt_id(atom.getLabel_alt_id());

    a.setAtom_site_auth_seq_id(atom.getAuth_seq_id());
    a.setAtom_site_auth_comp_id(atom.getAuth_comp_id());
    a.setAtom_site_auth_asym_id(atom.getAuth_asym_id());
    a.setAtom_site_auth_atom_id(atom.getAuth_atom_id());

    a.setAtom_site_pdbx_PDB_ins_code(atom.getPdbx_PDB_ins_code());

    a.setSeq_id(atom.getLabel_seq_id());
    a.setX(Double.parseDouble(atom.getCartn_x()));
    a.setY(Double.parseDouble(atom.getCartn_y()));
    a.setZ(Double.parseDouble(atom.getCartn_z()));
    a.setAtom_site_type_symbol(atom.getType_symbol());
    a.setGroup_PDB(atom.getGroup_PDB());
    a.setComp_id(atom.getLabel_comp_id());
    a.setAtom_site_id(atom.getId());
    a.setAtom_site_pdbx_PDB_model_num(atom.getPdbx_PDB_model_num());

    if (atom.getB_iso_or_equiv() != null && atom.getB_iso_or_equiv().length() > 0) {
      // http://mmcif.wwpdb.org/dictionaries/mmcif_pdbx_v50.dic/Items/_atom_site.B_iso_or_equiv.html
      // default is unknown
      try {
        a.setAtom_site_B_iso_or_equiv(Double.parseDouble(atom.getB_iso_or_equiv()));
      } catch (NumberFormatException e) {
        if (!atom.getB_iso_or_equiv().equalsIgnoreCase("null")
            && !atom.getPdbx_formal_charge().equalsIgnoreCase("?")) {
          e.printStackTrace();
        }
      }
    }

    if (atom.getPdbx_formal_charge() != null && atom.getPdbx_formal_charge().length() > 0) {
      // http://mmcif.wwpdb.org/dictionaries/mmcif_pdbx_v50.dic/Items/_atom_site.pdbx_formal_charge.html
      // default is 0
      try {
        a.setAtom_site_pdbx_formal_charge(Integer.parseInt(atom.getPdbx_formal_charge()));
      } catch (NumberFormatException e) {
        if (!atom.getPdbx_formal_charge().equalsIgnoreCase("null")
            && !atom.getPdbx_formal_charge().equalsIgnoreCase("?")) {
          e.printStackTrace();
        }
      }
    }

    if (atom.getOccupancy() != null && atom.getOccupancy().length() > 0) {
      double occupancy;
      try {
        occupancy = Double.parseDouble(atom.getOccupancy());
      } catch (NumberFormatException e) {
        if (!atom.getOccupancy().equalsIgnoreCase("null")
            && !atom.getPdbx_formal_charge().equalsIgnoreCase("?")) {
          e.printStackTrace();
        }
        // http://mmcif.wwpdb.org/dictionaries/mmcif_pdbx_v50.dic/Items/_atom_site.occupancy.html
        // default is 1.0, but ...
        occupancy = 0.0;
      }
      a.setAtom_site_occupancy(occupancy);
    }

    return a;
  }

  private List<StructConn> saccharideStructConnections() {
    HashSet<String> saccharideIds = new HashSet<>(ccLoader.saccharideIds());

    return protein.getStructConns().stream()
        .filter(sc -> saccharideIds.contains(sc.getPtnr1_label_comp_id())
               || saccharideIds.contains(sc.getPtnr2_label_comp_id()))
        .collect(Collectors.toList());
  }

//  private List<String> saccharideIds() {
//    return ccLoader.saccharideIds();
//  }
//

  /**
   * pdbCode: xxXx, pdbModelNum: 324 -> XXXX.0000324
   */
  private String molCode(String pdbCode, String pdbModelNum) {
    return String.format("%s.%s", pdbCode.toUpperCase(), modelCode(pdbModelNum));
  }

}

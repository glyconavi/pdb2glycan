package org.glycoinfo.PDB2Glycan.structure.glyco;

import static org.glycoinfo.PDB2Glycan.util.StringUtils.atomModResidueLabel;
import static org.glycoinfo.PDB2Glycan.util.StringUtils.modResidueLabel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.biojava.nbio.structure.io.mmcif.model.PdbxPolySeqScheme;
import org.glycoinfo.PDB2Glycan.io.MMcifEncoder;
import org.glycoinfo.PDB2Glycan.io.model.Glycan;
import org.glycoinfo.PDB2Glycan.io.model.Protein;
import org.glycoinfo.PDB2Glycan.io.model.protein.AtomSite;
import org.glycoinfo.PDB2Glycan.io.model.protein.EntityPoly;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConf;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetRange;
import org.glycoinfo.PDB2Glycan.structure.Atom;
import org.glycoinfo.PDB2Glycan.structure.ConnectedEntry;
import org.glycoinfo.PDB2Glycan.structure.ConnectedEntryModel;
import org.glycoinfo.PDB2Glycan.structure.ModelCassette;
import org.glycoinfo.PDB2Glycan.structure.NearbyComponent;
import org.glycoinfo.PDB2Glycan.structure.PDBEntity;
import org.glycoinfo.PDB2Glycan.structure.Proteindb;
import org.glycoinfo.PDB2Glycan.structure.WURCSType;
import org.glycoinfo.PDB2Glycan.util.CTFile2WURCS;
import org.glycoinfo.PDB2Glycan.util.Format;
import org.glycoinfo.PDB2Glycan.util.GlycoEntryComparator;
import org.glycoinfo.PDB2Glycan.util.ModelCassetteComparator;
import org.glycoinfo.PDB2Glycan.util.NumStrComparator;
import org.glycoinfo.PDB2Glycan.util.WURCS2GlyTouCan;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlycoProtein {

  private static final Logger logger = LoggerFactory.getLogger(GlycoProtein.class);

  private List<String> atomSiteLabelAsymId;
  private List<ConnectedEntryModel> connectedEntries;
  private Protein protein;
  private ValidationReport validationReport;

  public ValidationReport getValidationReport() {
    return validationReport;
  }

  public void setValidationReport(ValidationReport validationReport) {
    this.validationReport = validationReport;
  }

  public List<String> getAtomSiteLabelAsymId() {
    return atomSiteLabelAsymId;
  }

  public void setAtomSiteLabelAsymId(List<String> atomSiteLabelAsymId) {
    this.atomSiteLabelAsymId = atomSiteLabelAsymId;
  }

  public List<ConnectedEntryModel> getConnectedEntries() {
    return connectedEntries;
  }

  public void setConnectedEntries(
      List<ConnectedEntryModel> connectedEntries) {
    this.connectedEntries = connectedEntries;
  }

  public void setProtein(Protein protein) {
    this.protein = protein;
  }

  public Protein getProtein() {
    return protein;
  }

  public Glycan toJson() {
    List<PDBEntity> entities = new ArrayList<>();
    List<ConnectedEntry> connectedEntities = new ArrayList<>();

    combineModels(entities, connectedEntities);

    // Stores analyzed data into a class for writing results
    Glycan json = new Glycan();

    json.setPDB_id(protein.getPdbCode());
    json.setAtom_site_label_asym_id(new ArrayList<>(atomSiteLabelAsymId));

    if (protein.getStructs().size() > 1) {
      logger.warn("This method expects only one '_struct' entry. But there were " + protein.getStructs().size() + " entries.\nThe first one was retained.");
    }
    if (!protein.getStructs().isEmpty())
      json.setStruct(protein.getStructs().get(0));
    json.setCitations(protein.getCitations());
    json.setEntity_src_nat(protein.getEntitySrcNats());
    json.setExptl(protein.getExptls());
    json.setCitations(protein.getCitations());
    json.setStruct_refs(protein.getStructRefs());
    json.setValidationReport(validationReport);

    // TODO: Must PDBEntities be separated?
    json.setProtein_db(new ArrayList<>());
    for (PDBEntity pe : entities)
      json.getProtein_db().addAll(pe.protein_db);

    json.glyco_entries = new ArrayList<>();
    for (ConnectedEntry ce : connectedEntities) {
      json.glyco_entries.add(glycoEntry(ce, entities));
    }

    json.glyco_entries.sort(new GlycoEntryComparator());

    return json;
  }

  private GlycoEntry glycoEntry(ConnectedEntry connectedEntity, List<PDBEntity> entities) {
    List<String> entityIds = new ArrayList<>();
    List<String> asymIds = new ArrayList<>();
    List<ModelCassette> models = new ArrayList<>();

    Map<String, String> chemCompMap = new TreeMap<>();
    for (ConnectedEntryModel cm : connectedEntity.models) {
      entityIds.addAll(cm.getAllEntityIds());
      asymIds.addAll(cm.getAllAsymIds());
      for (Atom aa : cm.atoms) {
        String dlab;
        // Dirty hack for sorting strings
        if (aa.getAtom_site_label_seq_id() == null) {
          dlab = "9999999";
//        } else if (aa.getAtom_site_label_seq_id().equals("null")) {
        } else if (aa.getAtom_site_label_seq_id().isEmpty()) {
          dlab = "9999999";
        } else {
          dlab = "0000000" + aa.getAtom_site_label_seq_id();
        }
        if (aa.getAtom_site_label_seq_id() != null && aa.getAtom_site_label_seq_id().length() > 6) {
          throw new RuntimeException("Atom number is too big??? You should change the code.");
        }

        String dcode =
            aa.getAtom_site_label_asym_id() + dlab.substring(dlab.length() - 6);

        String key = String.format("%s;%s", dcode, aa.getAtom_site_label_comp_id());

        chemCompMap.putIfAbsent(key, aa.getAtom_site_label_comp_id());
      }

      models.add(modelCassette(connectedEntity, cm));
    }

    List<String> chemCompIds = new ArrayList<>(chemCompMap.values());

    // for GlycosylationSite
    GlycosylationSite gSite = null;
    if ( connectedEntity.models.get(0).linkedAtom != null ) {
      gSite = new GlycosylationSite();

      if ( !connectedEntity.parentResidues.isEmpty() )
        gSite.setModResidueId(connectedEntity.parentResidues.get(0).getId());

      // Get connected atom symbol
      Atom linkedAtom = connectedEntity.models.get(0).linkedAtom;
      gSite.setGlycanTypeSymbol(linkedAtom.getAtom_site_type_symbol());
      gSite.setAtomId(linkedAtom.getAtom_site_label_atom_id());

      PdbxPolySeqScheme seqScheme = connectedEntity.models.get(0).linkedResidue;

      gSite.setAsymId(seqScheme.getAsym_id());
      gSite.setEntityId(seqScheme.getEntity_id());
      gSite.setSeqId(Integer.valueOf(seqScheme.getSeq_id()));
      gSite.setCompId(seqScheme.getMon_id());

      gSite.setStrandId(seqScheme.getPdb_strand_id());

      for (StructConf sc : protein.getStructConf()) {
        if (!sc.contains(seqScheme.getAsym_id(), seqScheme.getSeq_id()))
          continue;
        gSite.setHelixInfo(sc);
        break;
      }
      for (StructSheetRange ssr : protein.getStructSheetRange()) {
        if (!ssr.contains(seqScheme.getAsym_id(), seqScheme.getSeq_id()))
          continue;
        gSite.setSheetInfo(ssr);
        break;
      }

      // Get sequence info from strand_id
      String seqStrand = null;
      for ( PDBEntity pe : entities ) {
        for ( Proteindb db : pe.protein_db ) {
          if ( db.getStrand_id().equals(gSite.getStrandId()) )
            seqStrand = db.getPdbx_seq_one_letter_code_can();
        }
      }
      if ( seqStrand != null ) {
        int seqId = gSite.getSeqId();
        gSite.setOneLetterCode(Character.toString(seqStrand.charAt(seqId-1)));
        int seqId0 = seqId - 4;
        if ( seqId0 < 0 )
          seqId0 = 0;
        gSite.setSeqBefore( seqStrand.substring(seqId0, seqId-1) );
        seqId0 = seqId + 3;
        if ( seqId0 > seqStrand.length() )
          seqId0 = seqStrand.length();
        gSite.setSeqAfter(  seqStrand.substring(seqId, seqId0) );
      } else {
        logger.warn("No strand sequence for strand_id \"{}\"", gSite.getStrandId());
      }

    }

    return glycoEntry(connectedEntity.parentResidues, entityIds, asymIds, chemCompIds, models, gSite);
  }

  private ModelCassette modelCassette(ConnectedEntry connectedEntity, ConnectedEntryModel cm) {
    ModelCassette mc = new ModelCassette();

    mc.pdbx_PDB_model_num = cm.getModelNum();
    mc.label_alt_id = cm.label_alt_id;
    IAtomContainer iatomc = cm.getIAtomContainer(protein.getPdbCode());
    // added. 18.Dec.2022
    mc.CTfile = Format.toSDF(iatomc);
    // added. 10.Jan.2023. modified. 23.Jan.2023.
    //mc.mmCIF = Format.glycoMmcifByEachEntry(cm.atoms, this.protein, cm.getAllEntityIds(), cm.proBindRes);
    mc.mmCIF = Format.glycoMmcifByEachEntry(cm.atoms, this.protein, cm.getAllAsymIds(), cm.proBindRes);
    mc.mmJSON = MMcifEncoder.getMMjsonString(new BufferedReader(new StringReader(mc.mmCIF)));

    mc.atoms = cm.getAtoms();
//    mc.occupancy = cm.getOcupancies().stream().map(a -> String.valueOf(a)).collect(Collectors.toList());
//    mc.B_iso_or_equiv = cm.getBIsoOrEquivs().stream().map(a -> String.valueOf(a)).collect(Collectors.toList());
//    mc.pdbx_formal_charge = cm.getPdbxFormalCharges().stream().map(a -> String.valueOf(a)).collect(Collectors.toList());

    // Sets surrounded residues
    mc.interacting_components = NearbyComponent.mergeTarget(cm.nearbyAtoms);
    for (NearbyComponent nr : mc.interacting_components) {
      Atom tAtom = nr.getNearestEntry().getTarget();

      for (StructSheetRange sr : protein.getStructSheetRange()) {
        if (sr.contains(tAtom.getAtom_site_label_asym_id(), tAtom.getAtom_site_label_seq_id())) {
          logger.debug("Interacting target atom is in struct_sheet_range.sheet_id: {}", sr.getSheet_id());
          nr.getResidueInfo().addStructureInfo("struct_sheet_range.sheet_id:" + sr.getSheet_id());
          nr.getResidueInfo().setStruct_sheet_range(sr);
        }
      }

      for (StructConf sc : protein.getStructConf()) {
        if (sc.contains(tAtom.getAtom_site_label_asym_id(), tAtom.getAtom_site_label_seq_id())) {
          logger.debug("Interacting target atom is in struct_conf.id: {}", sc.getId());
          nr.getResidueInfo().addStructureInfo("struct_conf.id:" + sc.getId());
          nr.getResidueInfo().setStruct_conf(sc);
        }
      }
    }

    Iterator<NearbyComponent> nite = mc.interacting_components.iterator();
    while (nite.hasNext()) {
      Atom nr = nite.next().getNearestEntry().getTarget();
      boolean remflag = false;
      for (Atom aa : cm.atoms) {
        if (aa.isFromSameResidue(nr)) {
          remflag = true;
          break;
        }
      }
      if (remflag) {
        nite.remove();
      }
    }

    // Convert CTFile to WURCS
    CTFile2WURCS conv = new CTFile2WURCS();
    String WURCSOrig = conv.getWURCS(iatomc); // modified. 10.Dec.2022

    List<String> WURCSsStandard = new ArrayList<>();
    if ( conv.getWURCSsSeparated() == null ) {
      // Standard WURCSs
      WURCSsStandard = new ArrayList<>();
      WURCSsStandard.add(WURCSOrig);
    } else {
      WURCSsStandard = conv.getWURCSsSeparated();
    }

    mc.saccharides = new ArrayList<>();
    for ( String WURCS : WURCSsStandard ) {
      ModelCassette.Saccharide saccharide = new ModelCassette.Saccharide();
      saccharide.WURCS = WURCS;
      saccharide.type = WURCSType.STD;
      try {
        saccharide.GTC = WURCS2GlyTouCan.get(WURCS);
      } catch (IOException e) {
        logger.error("Error in getting GlyTouCan ID from WURCS: "+WURCS, e);
      }
      mc.saccharides.add(saccharide);
    }

    if ( conv.getWURCSAglycones() != null ) {
      ModelCassette.Saccharide saccharide = new ModelCassette.Saccharide();
      saccharide.WURCS = WURCSOrig;
      saccharide.type = WURCSType.CON;
      saccharide.aglycones = conv.getWURCSAglycones();
      mc.saccharides.add(saccharide);
    }

    if ( conv.getWURCSsSeparatedWithAglycone() != null ) {
      for ( String WURCSWithAglycone : conv.getWURCSsSeparatedWithAglycone() ) {
        ModelCassette.Saccharide saccharide = new ModelCassette.Saccharide();
        String[] WURCSSep = WURCSWithAglycone.split("\t");
        saccharide.WURCS = WURCSSep[0];
        if ( WURCSSep.length == 2 ) {
          saccharide.aglycones = new ArrayList<>();
          saccharide.aglycones.add(WURCSSep[1]);
          saccharide.type = WURCSType.SEP;
        } else if ( WURCSSep.length == 1 && !WURCSSep[0].contains("@") ) {
          saccharide.type = WURCSType.STD;
        } else {
          logger.error("Illegal format for WURCSWithAglycone: {}", WURCSWithAglycone);
        }
        mc.saccharides.add(saccharide);
      }
    }

    // Add validation errors // comment out. 12.Dec.2022
//    for ( String error : conv.getErrors() )
//      this.getValidationReport().addSugarRepo(error);

    return mc;
  }

  private GlycoEntry glycoEntry(List<PdbxStructModResidue> parentResidues,
                                List<String> entityIds,
                                List<String> asymIds,
                                List<String> saccharides,
                                List<ModelCassette> models,
                                GlycosylationSite gSite) {
    GlycoEntry glycoEntry = new GlycoEntry();

    glycoEntry.setEntity_ids(entityIds
        .stream()
        .distinct()
        .sorted(new NumStrComparator())
        .collect(Collectors.toList()));

    glycoEntry.setAsym_ids(asymIds
        .stream()
        .distinct()
        .sorted()
        .collect(Collectors.toList()));

    glycoEntry.setChem_comp_ids(saccharides);

    glycoEntry.setMod_residue_asym_ids(parentResidues
        .stream()
        .map(PdbxStructModResidue::getLabel_asym_id)
        .collect(Collectors.toList()));

    glycoEntry.setPdbx_struct_mod_residue_ids(parentResidues
        .stream()
        .map(PdbxStructModResidue::getId)
        .collect(Collectors.toList()));

    glycoEntry.setModels(models.stream().sorted(new ModelCassetteComparator()).collect(Collectors.toList()));

    glycoEntry.setGlycosylationSite(gSite);

    return glycoEntry;
  }

  private void combineModels(List<PDBEntity> entities, List<ConnectedEntry> connectedEntities) {

    if (getConnectedEntries().size() > 0) {

      // Group connectedEntryModel containing the same atom group as ConnectedEntry
      for (ConnectedEntryModel ce : getConnectedEntries()) {
        ConnectedEntry cmm = null;

        for (ConnectedEntry c : connectedEntities) {
          if (c.sameWith(ce)) {
            c.addModel(ce);
            if (cmm == null) {
              cmm = c;
            } else {
              logger.warn("???? found duplicate entries.");
            }
          }
        }

        if (cmm == null) {
          ConnectedEntry ez = new ConnectedEntry();
          ez.addModel(ce);
          connectedEntities.add(ez);
        }
      }

      // Set PdbxStructModResidue to ConnectedEntry as parent residue
      // if the residue have an atom contained in the entry
//      ArrayList<String> entityIdList = new ArrayList<>(); // added. 12.Jan.2023
      HashMap<String, ArrayList<AtomSite>> proAtom = new HashMap<>(); // added. 12.Jan.2023
      for (PdbxStructModResidue pp : protein.arPdbxStructModResidue()) {
        String smlabel = modResidueLabel(pp);
        for (ConnectedEntry ce : connectedEntities) {
          boolean matchflag = false;
          for (ConnectedEntryModel ss : ce.models) {
            if (ss.linkedAtom == null)
              continue;
            if (!smlabel.equals(atomModResidueLabel(ss.linkedAtom)))
              continue;
            matchflag = true;
            break;
          }
          if (matchflag) {
            ce.addParentResidue(pp);
            // added. 5.Jan.2023. modified. 23.Jan.2023
            // to get an entitiy id of saccharides
//            String entityId = "";
            String glycoAsymId = "";
            for (StructConn strCon : protein.getStructConns()) { // get covale lines in a _str_conn lines.
              if (!strCon.getConn_type_id().equals("covale")) { // skip lines except for covale lines.
                continue;
              }
              if (pp.getLabel_asym_id().equals(strCon.getPtnr1_label_asym_id()) &&
                  pp.getLabel_comp_id().equals(strCon.getPtnr1_label_comp_id()) &&
                  pp.getLabel_seq_id().equals(strCon.getPtnr1_label_seq_id())) { // get a covale line corresponded to the residue in atom_site lines.
                glycoAsymId = strCon.getPtnr2_label_asym_id(); // get a asym id of saccharide in the covale line.

//                List<StructAsym> strAsymList = protein.getStructAsyms(); // get _struct_asym lines.
//                for (StructAsym strAsym : strAsymList) { // get a entity id corresponded to the asym id.
//                  if (strAsym.getId().equals(glycoAsymId)) {
////                    entityId = strAsym.getEntity_id();
//                	glycoAsymId = strAsym.getId();
//                    break;
//                  }
//                }
              }
//              if (entityId != "") { // TODO
              if (glycoAsymId != "") {
                break;
              }
            }
//            int atomSiteIdx = 0; // added. 5.Jan.2023
            ArrayList<AtomSite> atomList = new ArrayList<>();
            for (AtomSite asite : protein.getAtomSites()) {
              if (asite.getLabel_seq_id().equals(pp.getLabel_seq_id()) &&
                  asite.getLabel_comp_id().equals(pp.getLabel_comp_id()) &&
                  asite.getLabel_asym_id().equals(pp.getLabel_asym_id())) {
            	  atomList.add(asite);
//                protein.getGlycoAtomSiteSet().get(entityId).add(atomSiteIdx, asite);
//                atomSiteIdx++;
              }
            }
            proAtom.put(glycoAsymId, atomList);
            // end. 5.Jan.2023
          }
        }
      }
      protein.setGlycoAtomSiteSet(proAtom);
    }

    // Populate PDBEntries
    List<String> idss = protein.getEntityPolies()
        .stream()
        .map(EntityPoly::getEntity_id)
        .distinct().sorted().collect(Collectors.toList());

    Map<String, Set<String>> entityToStructAsym = protein.entityToStructAsym();

    for (String id : idss) {
      List<String> asymss = new ArrayList<>(entityToStructAsym.get(id));
      Collections.sort(asymss);

      PDBEntity pe = new PDBEntity();
      pe.entity_ids_n.add(id);
      pe.asym_ids_n.addAll(asymss);
      entities.add(pe);
    }

    for (PDBEntity pe : entities) {
//      HashSet<String> asymids = new HashSet<>(pe.asym_ids_n);
      HashSet<String> entids = new HashSet<>(pe.entity_ids_n);
      Iterator<ConnectedEntry> cite = connectedEntities.iterator();
      ArrayList<ConnectedEntry> childent = new ArrayList<>();

      while (cite.hasNext()) {
        ConnectedEntry cc = cite.next();
        outer:
        for (ConnectedEntryModel cmm : cc.models) {
          for (Atom a : cmm.atoms) {
            if (entids.contains(a.getAtom_site_label_entity_id())) {
              childent.add(cc);
              break outer;
            }
          }
        }
      }

      for (ConnectedEntry cc : childent) {
        pe.asym_ids_n.addAll(cc.models.get(0).getAllAsymIds());
        pe.entity_ids_n.addAll(cc.models.get(0).getAllEntityIds());
      }
      pe.asym_ids_n = new ArrayList<>(new HashSet<>(pe.asym_ids_n));
      pe.entity_ids_n = new ArrayList<>(new HashSet<>(pe.entity_ids_n));

      Collections.sort(pe.asym_ids_n);
      Collections.sort(pe.entity_ids_n);

      pe.model.addAll(ConnectedEntry.entryModelToCassette(protein.getPdbCode(), childent));

      for (Proteindb pp : protein.proteinDb()) {
        if (entids.contains(pp.getEntity_id())) {
          pe.protein_db.add(pp);
        }
      }
//      for (PdbxStructModResidue pp : protein.arPdbxStructModResidue()) {
//        if (asymids.contains(pp.getLabel_asym_id())) {
//          pe.ar_pdbx_struct_mod_residue.add(pp);
//        }
//      }
    }

    Collections.sort(atomSiteLabelAsymId);
  }
}

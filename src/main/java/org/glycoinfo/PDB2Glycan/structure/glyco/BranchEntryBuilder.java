package org.glycoinfo.PDB2Glycan.structure.glyco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.MMcifEncoder;
import org.glycoinfo.PDB2Glycan.io.file.ChemicalComponentLoader;
import org.glycoinfo.PDB2Glycan.io.model.Protein;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxBranchScheme;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranch;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchLink;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchList;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;
import org.glycoinfo.PDB2Glycan.structure.Atom;
import org.glycoinfo.PDB2Glycan.structure.Bond;
import org.glycoinfo.PDB2Glycan.structure.Location3D;
import org.glycoinfo.PDB2Glycan.structure.Mol;
import org.glycoinfo.PDB2Glycan.structure.TransformMol;
import org.glycoinfo.PDB2Glycan.structure.Vector3D;
import org.glycoinfo.PDB2Glycan.util.CTFile2WURCS;
import org.glycoinfo.PDB2Glycan.util.Format;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BranchEntryBuilder {

  private static final Logger logger = LoggerFactory.getLogger(BranchEntryBuilder.class);

  private Protein protein;
  private ChemicalComponentLoader ccLoader;

  private List<BranchEntry> branchEntries;
  private boolean forOneDep;

  private String mmCIF; // added. 11.Jan.2023

  public BranchEntryBuilder(Protein protein, ChemicalComponentLoader ccLoader) {
    this.protein = protein;
    this.ccLoader = ccLoader;
    this.branchEntries = new ArrayList<>();
  }

  public List<BranchEntry> getBranchEntries() {
    return this.branchEntries;
  }

  public void setForOneDep(boolean forOneDep) {
    this.forOneDep = forOneDep;
  }

  public void build() throws IOException, MMcifParseException {
    Map<String, BranchEntry> mapIdToBranchEntry = new HashMap<>();
    for ( PdbxEntityBranch entity : protein.getPdbxEntityBranch() ) {
      String id = entity.getEntity_id();
      // Initialize branch entry
      BranchEntry branch = new BranchEntry();
      branch.setEntity_id(id);
      branch.setResidues(new ArrayList<>());
      branch.setLinkages(new ArrayList<>());
      mapIdToBranchEntry.put(id, branch);
    }

    // Collects residue info
    boolean hasHetero = false;
    for ( PdbxEntityBranchList list : protein.getPdbxEntityBranchList() ) {
      String id = list.getEntity_id();
      BranchEntry branch = mapIdToBranchEntry.get(id);
      branch.getResidues().add(list);
      if ( list.getHetero().equals("y") )
        hasHetero = true;
    }

    // Collects linkage info
    for ( PdbxEntityBranchLink link : protein.getPdbxEntityBranchLink() ) {
      String id = link.getEntity_id();
      BranchEntry branch = mapIdToBranchEntry.get(id);
      branch.getLinkages().add(link);
    }

    // Collect residue info
    Map<String, BranchEntry> mapAsymToBranch = new HashMap<>();
    Set<String> lResInfo = new HashSet<>();
    for ( PdbxBranchScheme scheme : protein.getPdbxBranchScheme() ) {
      String id = scheme.getEntity_id();
      BranchEntry branch = mapIdToBranchEntry.get(id);
      mapAsymToBranch.put(scheme.getPdb_asym_id(), branch);
      for ( PdbxEntityBranchList res : branch.getResidues() ) {
        if ( !res.getNum().equals(scheme.getPdb_seq_num()) )
          continue;
        if ( !res.getComp_id().equals(scheme.getPdb_mon_id()) )
          continue;
        String strRes = scheme.getPdb_asym_id()
            +":"+scheme.getPdb_mon_id()
            +":"+scheme.getPdb_seq_num();
        lResInfo.add(strRes);
        break;
      }
    }

    // Set glycosylation info
    for ( StructConn sconn : protein.getStructConns() ) {
      if ( !sconn.getConn_type_id().equals("covale") )
        continue;
      if ( sconn.getPdbx_role().equals("null") )
        continue;
      String strRes1 = sconn.getPtnr1_auth_asym_id()+":"
          +sconn.getPtnr1_auth_comp_id()+":"
          +sconn.getPtnr1_auth_seq_id();
      String strRes2 = sconn.getPtnr2_auth_asym_id()+":"
          +sconn.getPtnr2_auth_comp_id()+":"
          +sconn.getPtnr2_auth_seq_id();
      if ( !lResInfo.contains(strRes1) && !lResInfo.contains(strRes2) )
        continue;
      if ( lResInfo.contains(strRes1) && lResInfo.contains(strRes2) )
        continue;
      String asym_id = (lResInfo.contains(strRes1))?
          sconn.getPtnr1_auth_asym_id() : sconn.getPtnr2_auth_asym_id();
      BranchEntry branch = mapAsymToBranch.get(asym_id);
      branch.setGlycosylationConn(sconn);
    }

    // Separate entries for heterogeneous residues
    if ( hasHetero )
      mapIdToBranchEntry = separateWithHetero(mapIdToBranchEntry);

    // Build molecule for each entity
    for ( String id : mapIdToBranchEntry.keySet() ) {
      BranchEntry branch = mapIdToBranchEntry.get(id);

      this.branchEntries.add(branch);

      // Validate entry
      BranchEntryValidator validator = new BranchEntryValidator(branch, this.ccLoader);
      validator.start();
      if ( !validator.hasError() ) {
        // Write mol file
    	IAtomContainer mol = buildMolecule(branch); // modified. 12.Dec.2022
    	String ctFile = Format.toSDF(mol); // modified. 12.Dec.2022
    	branch.setCtFile( ctFile ); // 12.Dec.2022
        branch.setMMJSON(MMcifEncoder.getMMjsonString(new BufferedReader(new StringReader(this.mmCIF)))); // added. 23.Dec.2022
        branch.setMMCIF(this.mmCIF.replaceAll("\n", "\\\n")); // added. 23.Dec.2022
        CTFile2WURCS conv = new CTFile2WURCS();
        String wurcs = conv.getWURCS(mol); // modified. 12.Dec.2022

        // Only allow glycans with no aglycone
        if ( conv.getWURCSsSeparated() != null ) {
          if( conv.getWURCSsSeparated().size() > 1 ) {
            String error = "The WURCS has two or more structures connecting to the same aglycone: "+wurcs;
            logger.error(error);
            branch.getValidation().get("errors").add("[WURCSValidation] "+error);
            continue;
          }
          logger.info("Aglycone in the WURCS has been removed: {}", wurcs);
          // Set WURCS without aglycone
          wurcs = conv.getWURCSsSeparated().get(0);
        }
        List<String> conversionErrors = conv.getErrors();
        if (wurcs != null && wurcs.isEmpty()) {
          wurcs = null;
          branch.getValidation().get("errors").addAll(conversionErrors);
        } else
          logger.info("WURCS for entity {}: {}", id, wurcs);
        branch.setWurcs(wurcs);
      } else {
        logger.info("Building molecule for entity {} has been skipped due to the error.", branch.getEntity_id());
      }

      // Clear some fields for OneDep

      if ( !this.forOneDep )
        continue;

      branch.setModelId(null);
      branch.setResidues(null);
      branch.setLinkages(null);
      branch.setGlycosylationConn(null);
      branch.setCtFile(null);
    }
  }

  private Map<String, BranchEntry> separateWithHetero(Map<String, BranchEntry> mapIdToBranchEntry) {
    Map<String, BranchEntry> mapNewIdToBranchEntry = new HashMap<>();
    for ( String id : mapIdToBranchEntry.keySet() ) {
      BranchEntry branch = mapIdToBranchEntry.get(id);
      // Classify with num
      Set<String> uniqueNums = new TreeSet<>();
      HashMap<String, List<PdbxEntityBranchList>> mapNumToRes = new HashMap<>();
      for ( PdbxEntityBranchList list : branch.getResidues() ) {
        String num = list.getNum();
        uniqueNums.add(num);
        if ( !mapNumToRes.containsKey(num) )
          mapNumToRes.put(num, new ArrayList<>());
        mapNumToRes.get(num).add(list);
      }
      if ( branch.getResidues().size() == uniqueNums.size() ) {
        mapNewIdToBranchEntry.put(id, branch);
        continue;
      }

      // Make all combinations for residues
      List<List<PdbxEntityBranchList>> listCombo = new ArrayList<>();
      for ( String num : uniqueNums ) {
        if ( listCombo.isEmpty() ) {
          for ( PdbxEntityBranchList list : mapNumToRes.get(num) ) {
            List<PdbxEntityBranchList> lists = new ArrayList<>();
            lists.add(list);
            listCombo.add(lists);
          }
          continue;
        }
        List<List<PdbxEntityBranchList>> listComboNew = new ArrayList<>();
        for ( List<PdbxEntityBranchList> lists : listCombo ) {
          for ( PdbxEntityBranchList list : mapNumToRes.get(num) ) {
            List<PdbxEntityBranchList> listsNew = new ArrayList<>();
            listsNew.addAll(lists);
            listsNew.add(list);
            listComboNew.add(listsNew);
          }
        }
        listCombo = listComboNew;
      }

      int nCombo = listCombo.size();
      // Only allow first combination for OneDep
      if ( this.forOneDep ) {
        logger.info("An entity contains heteromonomers. The first instances of the heteromonomers are used for the entry construction.");
        nCombo = 1;
      }

      for ( int i=0; i<nCombo; i++ ) {
        List<PdbxEntityBranchList> lists = listCombo.get(i);
        String modelId = ""+(i+1);
        String idNew = id+"-"+modelId;
        BranchEntry branchNew = new BranchEntry();
        branchNew.setEntity_id(branch.getEntity_id());
        branchNew.setModelId(modelId);
        branchNew.setGlycosylationConn(branch.getGlycosylationConn());

        branchNew.setResidues(lists);

        branchNew.setLinkages(new ArrayList<>());
        for ( PdbxEntityBranchLink link : branch.getLinkages() ) {
          PdbxEntityBranchList list1 = null;
          PdbxEntityBranchList list2 = null;
          for ( PdbxEntityBranchList list : lists ) {
            if ( !link.getEntity_branch_list_num_1().equals(list.getNum()) )
              continue;
            if ( !link.getComp_id_1().equals(list.getComp_id()) )
              continue;
            list1 = list;
          }
          for ( PdbxEntityBranchList list : lists ) {
            if ( !link.getEntity_branch_list_num_2().equals(list.getNum()) )
              continue;
            if ( !link.getComp_id_2().equals(list.getComp_id()) )
              continue;
            list2 = list;
          }
          if ( list1 == null || list2 == null )
            continue;
          branchNew.getLinkages().add(link);
        }

        mapNewIdToBranchEntry.put(idNew, branchNew);
      }

    }

    return mapNewIdToBranchEntry;
  }

  private IAtomContainer buildMolecule(BranchEntry branch) throws IOException, MMcifParseException {
    Map<String, Mol> mapNumToMol = new HashMap<>();
    LinkedList<Atom> atoms = new LinkedList<>();
    LinkedList<Bond> bonds = new LinkedList<>();
    for ( PdbxEntityBranchList res : branch.getResidues() ) {
      Mol mol = populateMolFromResidue(res);
      mapNumToMol.put(res.getNum(), mol);
      atoms.addAll(mol.getAtoms());
      bonds.addAll(mol.getBonds());
    }

    Set<Atom> linkAtoms = new HashSet<>();
    Set<Mol> fixedMols = new HashSet<>();
    List<PdbxEntityBranchLink> remainingLinkage = new ArrayList<>();
    remainingLinkage.addAll(branch.getLinkages());
    while ( !remainingLinkage.isEmpty() ) {
      for ( PdbxEntityBranchLink lin : branch.getLinkages() ) {
        if ( !remainingLinkage.contains(lin) )
          continue;

        Mol mol1 = mapNumToMol.get( lin.getEntity_branch_list_num_1() );
        Atom atom1 = mol1.getAtomByLabelAtomId( lin.getAtom_id_1() );
        linkAtoms.add(atom1);
        Atom atomLeaving1 = mol1.getAtomByLabelAtomId( lin.getLeaving_atom_id_1() );
        if ( atomLeaving1 == null )
          atomLeaving1 = atom1;
        else if ( linkAtoms.contains(atomLeaving1) ) {
          atomLeaving1 = atom1;
        } else {
          atoms.remove(atomLeaving1);
          for ( Bond bond : mol1.getBondsForAtom(atomLeaving1) ) {
            bonds.remove(bond);
            Atom atomConn = bond.getFromAtom();
            if ( atomConn.equals(atomLeaving1) )
              atomConn = bond.getToAtom();
            if ( !atomConn.getAtom_site_type_symbol().equals("H") )
              continue;
            if ( mol1.getBondsForAtom(atomConn).size() != 1 )
              continue;
            atoms.remove(atomConn);
          }
        }
        Mol mol2 = mapNumToMol.get( lin.getEntity_branch_list_num_2() );
        Atom atom2 = mol2.getAtomByLabelAtomId( lin.getAtom_id_2() );
        linkAtoms.add(atom2);
        Atom atomLeaving2 = mol2.getAtomByLabelAtomId( lin.getLeaving_atom_id_2() );
        if ( atomLeaving2 == null )
          atomLeaving2 = atom2;
        else if ( linkAtoms.contains(atomLeaving2) ) {
          atomLeaving1 = atom2;
        } else {
          atoms.remove(atomLeaving2);
          for ( Bond bond : mol2.getBondsForAtom(atomLeaving2) ) {
            bonds.remove(bond);
            Atom atomConn = bond.getFromAtom();
            if ( atomConn.equals(atomLeaving2) )
              atomConn = bond.getToAtom();
            if ( !atomConn.getAtom_site_type_symbol().equals("H") )
              continue;
            if ( mol2.getBondsForAtom(atomConn).size() != 1 )
              continue;
            atoms.remove(atomConn);
          }
        }

        int order = getBondOrder(lin.getValue_order());
        bonds.add( new Bond(atom1, atom2, order) );

        // Fix atom coordinates
        boolean swap = false;
        if ( fixedMols.isEmpty() ) {
          fixedMols.add(mol1);
          fixedMols.add(mol2);
        } else if ( !fixedMols.contains(mol1) && !fixedMols.contains(mol2) ) {
          logger.error("Something wrong in connecting residues. Maybe one or more linkages have lost.");
          remainingLinkage.remove(lin);
        } else if ( fixedMols.contains(mol1) && fixedMols.contains(mol2) ) {
          remainingLinkage.remove(lin);
          continue;
        } else if ( fixedMols.contains(mol1) && !fixedMols.contains(mol2) ) {
          fixedMols.add(mol2);
        } else if ( !fixedMols.contains(mol1) && fixedMols.contains(mol2) ) {
          fixedMols.add(mol1);
          swap = true;
        }

        if (swap)
          transformMol(mol2, atom2, atomLeaving2, mol1, atom1, atomLeaving1);
        else
          transformMol(mol1, atom1, atomLeaving1, mol2, atom2, atomLeaving2);

        remainingLinkage.remove(lin);
      }
    }

    // Number atoms
    for ( int i=0; i<atoms.size(); i++ )
      atoms.get(i).setCtfile_id(i+1);

//    // Write mol file
    // added. 11.Jan.2023
    this.mmCIF = Format.glycoMmcifByBranchEntry(atoms, protein, branch.getEntity_id());
    // modified. 12.Dec.2022
    return Format.iatomcontainer(atoms, bonds,
            "PDB_id:"+protein.getPdbCode()+",Entity_id:"+branch.getEntity_id());
  }

//  private Mol populateMolFromResidue(PdbxEntityBranchList res, String asym_id) throws IOException, MMjsonParseException {
  private Mol populateMolFromResidue(PdbxEntityBranchList res) throws IOException, MMcifParseException {
    // Load mol without cache
    Mol mol = this.ccLoader.load(res.getComp_id(), false);
    for ( Atom atom : mol.getAtoms() ) {
//      atom.setAtom_site_auth_asym_id(asym_id);
      atom.setAtom_site_auth_seq_id(res.getNum());
//      atom.setAtom_site_label_asym_id(asym_id);
      atom.setSeq_id(res.getNum());
      atom.setAtom_id(atom.getAtom_site_label_atom_id());
    }
    return mol;
  }

  private static int getBondOrder(String value_order) {
    switch (value_order) {
    case "sing":
      return 1;
    case "doub":
      return 2;
    case "trip":
      return 3;
    case "arom":
    case "pi":
    case "delo":
      return 4;
    default:
      return 8;
    }
  }

  private static void transformMol(
      Mol mol1, Atom atom1, Atom atomLeaving1,
      Mol mol2, Atom atom2, Atom atomLeaving2) {

    Vector3D center1 = atom1;
    Vector3D center2 = atomLeaving2;
    if ( atom1.distance(atomLeaving1) > atom2.distance(atomLeaving2) ) {
      center1 = atomLeaving1;
      center2 = atom2;
    }
    Location3D linkCenter1 = new Location3D(center1);
    Location3D linkCenter2 = new Location3D(center2);

    // Set each center of linkage between leaving atom and connecting atom as origin
    Location3D origin = new Location3D(0.0, 0.0, 0.0);
    Vector3D move1 = TransformMol.vector(linkCenter1, origin);
    Vector3D move2 = TransformMol.vector(linkCenter2, origin);

    // Translate mols to origin
    TransformMol.translateMol(mol1, move1);
    TransformMol.translateMol(mol2, move2);

    // Rotate mols to fit vectors of two bonds to be connected
    Vector3D v1 = TransformMol.vector(atom1, atomLeaving1);
    Vector3D v2 = TransformMol.vector(atomLeaving2, atom2);
    Vector3D outer = TransformMol.outer(v1, v2);
    double rad = TransformMol.angle(v1, v2);
    TransformMol.rotateMol(mol2, outer, -rad);

    Location3D molCenter1 = TransformMol.center(mol1);
    Location3D molCenter2 = TransformMol.center(mol2);

    Location3D rotCenter1 = TransformMol.normalOnVector(v1, molCenter1);
    Location3D rotCenter2 = TransformMol.normalOnVector(v1, molCenter2);

    // Rotate mols with vector 
    v1 = TransformMol.vector(rotCenter1, molCenter1);
    v2 = TransformMol.vector(rotCenter2, molCenter2);
    rad = TransformMol.angle(v1, v2);
    outer = TransformMol.outer(v1, v2);
    TransformMol.rotateMol(mol2, outer, Math.PI-rad);

    // Translate mols to original place of mol1
    TransformMol.translateMol(mol1, linkCenter1);
    TransformMol.translateMol(mol2, linkCenter1);
  }

}

package org.glycoinfo.PDB2Glycan.structure.glyco;

import java.util.List;
import java.util.Map;

import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchLink;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchList;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BranchEntry {

  private String entity_id; // from PdbxEntityBranch
  private String modelId; // for hetero combinations, null if no combination

  @JsonProperty("residues")
  private List<PdbxEntityBranchList> residues;
  private List<PdbxEntityBranchLink> linkages;
  @JsonProperty("glycosylation_conn")
  private StructConn glyConn;

  private String ctFile;
  // added. 28.Nov.2022
  private String mmcif;
  private String mmjson;
  // added. end.
  private String wurcs;

  @JsonProperty("validation")
  private Map<String, List<String>> validation;

  public String getEntity_id() {
    return entity_id;
  }

  public void setEntity_id(String entity_id) {
    this.entity_id = entity_id;
  }

  public String getModelId() {
    return modelId;
  }

  public void setModelId(String modelId) {
    this.modelId = modelId;
  }

  public List<PdbxEntityBranchList> getResidues() {
    return residues;
  }

  public void setResidues(List<PdbxEntityBranchList> residues) {
    this.residues = residues;
  }

  public List<PdbxEntityBranchLink> getLinkages() {
    return linkages;
  }

  public void setLinkages(List<PdbxEntityBranchLink> linkages) {
    this.linkages = linkages;
  }

  @JsonIgnore
  public StructConn getGlycosylationConn() {
    return glyConn;
  }

  public void setGlycosylationConn(StructConn glyConn) {
    this.glyConn = glyConn;
  }

  public String getCtFile() {
    return ctFile;
  }

  public void setCtFile(String ctFile) {
    this.ctFile = ctFile;
  }
  // added. 28.Nov.2022
  public String getMMCIF() {
	return mmcif;
  }

  public void setMMCIF(String mmcif) {
	this.mmcif = mmcif;
  }

  public String getMMJSON() {
	return mmjson;
  }

  public void setMMJSON(String mmjson) {
	this.mmjson = mmjson;
  }
  // added. end.

  public String getWurcs() {
    return wurcs;
  }

  public void setWurcs(String wurcs) {
    this.wurcs = wurcs;
  }

  public Map<String, List<String>> getValidation() {
    return validation;
  }

  public void setValidation(Map<String, List<String>> validation) {
    this.validation = validation;
  }

}

package org.glycoinfo.PDB2Glycan.structure.glyco;

import org.glycoinfo.PDB2Glycan.io.model.protein.StructConf;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetRange;

public class GlycosylationSite {

	private String glycanTypeSymbol;

	private String asymId;
	private String entityId;
	private int seqId;
	private String compId;
	private String atomId;

	private String modResidueId;
	private String strandId;

	private StructConf helixInfo;
	private StructSheetRange sheetInfo;

	private String oneLetterCode;
	private String seqBefore;
	private String seqAfter;

	public String getGlycanTypeSymbol() {
		return glycanTypeSymbol;
	}
	public void setGlycanTypeSymbol(String glycanTypeSymbol) {
		this.glycanTypeSymbol = glycanTypeSymbol;
	}
	public String getAsymId() {
		return asymId;
	}
	public void setAsymId(String asymId) {
		this.asymId = asymId;
	}
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	public int getSeqId() {
		return seqId;
	}
	public void setSeqId(int seqId) {
		this.seqId = seqId;
	}
	public String getCompId() {
		return compId;
	}
	public void setCompId(String compId) {
		this.compId = compId;
	}
	public String getAtomId() {
		return atomId;
	}
	public void setAtomId(String atomId) {
		this.atomId = atomId;
	}
	public String getModResidueId() {
		return modResidueId;
	}
	public void setModResidueId(String modResidueId) {
		this.modResidueId = modResidueId;
	}
	public String getStrandId() {
		return strandId;
	}
	public void setStrandId(String strandId) {
		this.strandId = strandId;
	}
	public StructConf getHelixInfo() {
		return helixInfo;
	}
	public void setHelixInfo(StructConf helixInfo) {
		this.helixInfo = helixInfo;
	}
	public StructSheetRange getSheetInfo() {
		return sheetInfo;
	}
	public void setSheetInfo(StructSheetRange sheetInfo) {
		this.sheetInfo = sheetInfo;
	}
	public String getOneLetterCode() {
		return oneLetterCode;
	}
	public void setOneLetterCode(String oneLetterCode) {
		this.oneLetterCode = oneLetterCode;
	}
	public String getSeqBefore() {
		return seqBefore;
	}
	public void setSeqBefore(String seqBefore) {
		this.seqBefore = seqBefore;
	}
	public String getSeqAfter() {
		return seqAfter;
	}
	public void setSeqAfter(String seqAfter) {
		this.seqAfter = seqAfter;
	}

}

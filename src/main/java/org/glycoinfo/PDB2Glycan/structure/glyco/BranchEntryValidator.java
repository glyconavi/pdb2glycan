package org.glycoinfo.PDB2Glycan.structure.glyco;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.file.ChemicalComponentLoader;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchLink;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchList;
import org.glycoinfo.PDB2Glycan.structure.Atom;
import org.glycoinfo.PDB2Glycan.structure.Mol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BranchEntryValidator {
  private static final Logger logger = LoggerFactory.getLogger(BranchEntryValidator.class);

  private BranchEntry branch;
  private ChemicalComponentLoader ccLoader;

  private List<String> errors;
  private List<String> warnings;

  public BranchEntryValidator(BranchEntry branch, ChemicalComponentLoader ccLoader) {
    this.branch = branch;
    this.ccLoader = ccLoader;

    this.errors = new ArrayList<>();
    this.warnings = new ArrayList<>();
  }

  public boolean hasError() {
    return !this.errors.isEmpty();
  }

  public void start() throws IOException, MMcifParseException {
    validateResidues();
    validateLinkages();
    Map<String, List<String>> mapValidations = new HashMap<>();
    List<String> errors = new ArrayList<>();
    for ( String err : this.errors )
      errors.add("[EntityBranchValidation] "+err);
    mapValidations.put("errors", errors);
    List<String> warnings = new ArrayList<>();
    for ( String warn : this.warnings )
      warnings.add("[EntityBranchValidation] "+warn);
    mapValidations.put("warnings", warnings);
    branch.setValidation(mapValidations);
  }

  private void validateResidues() throws IOException, MMcifParseException {
    // Check hetero flag
//    for ( PdbxEntityBranchList list : this.branch.getResidues() ) {
//      // TODO: handle heterogeneous residues
//      if ( list.getHetero().equals("y") ) {
//          String err = String.format("pdbx_entity_branch_list: entity_id=%s, comp_id=%s, num=%s : "
//                  + "The residue %s has hetero flag which can not be handled for now.",
//                  list.getEntity_id(), list.getComp_id(), list.getNum(), list.getComp_id());
//          logger.error(err);
//          this.errors.add(err);
//      }
//    }
  }

  private void validateLinkages() throws IOException, MMcifParseException {
    // Collect linked and leaving atoms
    List<String[]> linkedAtoms = new ArrayList<>();
    List<String[]> leavingAtoms = new ArrayList<>();
    for ( PdbxEntityBranchLink link : this.branch.getLinkages() ) {
      String[] atomInfo = new String[4];
      atomInfo[0] = link.getLink_id();
      atomInfo[1] = link.getEntity_branch_list_num_1();
      atomInfo[2] = link.getComp_id_1();
      atomInfo[3] = link.getAtom_id_1();
      linkedAtoms.add(atomInfo);
      atomInfo = new String[4];
      atomInfo[0] = link.getLink_id();
      atomInfo[1] = link.getEntity_branch_list_num_2();
      atomInfo[2] = link.getComp_id_2();
      atomInfo[3] = link.getAtom_id_2();
      linkedAtoms.add(atomInfo);

      atomInfo = new String[4];
      atomInfo[0] = link.getLink_id();
      atomInfo[1] = link.getEntity_branch_list_num_1();
      atomInfo[2] = link.getComp_id_1();
      atomInfo[3] = link.getLeaving_atom_id_1();
      leavingAtoms.add(atomInfo);
      atomInfo = new String[4];
      atomInfo[0] = link.getLink_id();
      atomInfo[1] = link.getEntity_branch_list_num_2();
      atomInfo[2] = link.getComp_id_2();
      atomInfo[3] = link.getLeaving_atom_id_2();
      leavingAtoms.add(atomInfo);
    }

    // Check linked atoms
    for ( String[] a : linkedAtoms ) {
      Mol mol = this.ccLoader.load(a[2]);
      Atom atom = mol.getAtomByLabelAtomId(a[3]);
      if ( atom == null ) {
        String err = String.format("pdbx_entity_branch_link.link_id %s: "
            + "The atom %s making a linkage is not in %s.", a[0], a[3], a[2]);
        logger.error(err);
        this.errors.add(err);
      }
    }

    // Check leaving atoms
    for ( String[] a : leavingAtoms ) {
      Mol mol = this.ccLoader.load(a[2]);
      Atom atom = mol.getAtomByLabelAtomId(a[3]);
      if ( atom == null ) {
        String err = String.format("pdbx_entity_branch_link.link_id %s: "
            + "The leaving atom %s is not in %s.", a[0], a[3], a[2]);
        logger.error(err);
        this.errors.add(err);
        continue;
      }
      if ( !atom.getLeaving_atom() ) {
        String warn = String.format("pdbx_entity_branch_link.link_id %s: "
            + "The atom %s in %s is not a leaving atom.", a[0], a[3], a[2]);
        logger.warn(warn);
        this.warnings.add(warn);
      }
    }

    // Check leaving atoms which are in the list of linked atoms
    for ( String[] a1 : leavingAtoms ) {
      for ( String[] a2 : linkedAtoms ) {
        if ( !matchAtoms(a1, a2) )
          continue;
        String err = String.format("pdbx_entity_branch_link.link_id %s: "
            + "The leaving atom %s in %s:%s of entity %s"
            + " is making a linkage at link_id %s.",
            a1[0], a1[3], a1[1], a1[2], branch.getEntity_id(), a2[0]);
        logger.error(err);
        this.errors.add(err);
      }
    }
  }

  private boolean matchAtoms(String[] atom1, String[] atom2) {
    for ( int i=1; i<atom1.length; i++ )
      if ( !atom1[i].equals(atom2[i]) )
        return false;
    return true;
  }
}

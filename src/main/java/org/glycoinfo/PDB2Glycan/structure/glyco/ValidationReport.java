package org.glycoinfo.PDB2Glycan.structure.glyco;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ValidationReport {
	@JsonProperty("atom")
	private ArrayList<String> atomRepo = new ArrayList<>();
	
	@JsonProperty("bond")
	private ArrayList<String> bondRepo = new ArrayList<>();
	
	@JsonProperty("sugar")
	private ArrayList<String> sugarRepo = new ArrayList<>();
	
	
	public void addBondRepo(String s) {
		bondRepo.add(s);
	}
	public void addAtomRepo(String s) {
		atomRepo.add(s);
	}
	public void addSugarRepo(String s) {
		sugarRepo.add(s);
	}
	
	public void clearBondRepo() {
		bondRepo.clear();
	}
	public void clearAtomRepo() {
		atomRepo.clear();
	}
	public void clearSugarRepo() {
		sugarRepo.clear();
	}
	
	
	
	public ArrayList<String> getAtomRepo() {
		return atomRepo;
	}
	public void setAtomRepo(ArrayList<String> atomRepo) {
		this.atomRepo = atomRepo;
	}
	public ArrayList<String> getBondRepo() {
		return bondRepo;
	}
	public void setBondRepo(ArrayList<String> bondRepo) {
		this.bondRepo = bondRepo;
	}
	public ArrayList<String> getSugarRepo() {
		return sugarRepo;
	}
	public void setSugarRepo(ArrayList<String> sugarRepo) {
		this.sugarRepo = sugarRepo;
	}
	
	public void append(ValidationReport r) {
		this.atomRepo.addAll(r.getAtomRepo());
		this.bondRepo.addAll(r.getBondRepo());
		this.sugarRepo.addAll(r.getSugarRepo());
	}
	
	
}

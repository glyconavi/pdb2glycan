package org.glycoinfo.PDB2Glycan.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class MMcifEncoder {
	//https://github.com/gjbekker/cif-parsers
	/*
	  The MIT License (MIT)

		Copyright (c) 2015 Gert-Jan Bekker
		
		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:
		
		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.
	 */
	
	//with several modifications.
	//1. Added function Partition.
	//2. Made loadCIF to return JSON string.
	//3. Moved the block, which changes "?" and "." to null, before the block checking CnT flag.
	public static String converterJS = "cif-parsers-master/cif.js";
	public static String jsString =
			"/*!\r\n" + 
			" * cif.js\r\n" + 
			" *\r\n" + 
			" * JavaScript CIF parser: https://github.com/gjbekker/cif-parsers\r\n" + 
			" * \r\n" + 
			" * By Gert-Jan Bekker\r\n" + 
			" * License: MIT\r\n" + 
			" *   See https://github.com/gjbekker/cif-parsers/blob/master/LICENSE\r\n" + 
			" */\r\n" + 
			"\r\n" + 
			"// pdbml\r\n" + 
			"\r\n" + 
			"function PDBMLparser() {\r\n" + 
			"  this.data = {};\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"PDBMLparser.prototype.parse = function(data) {\r\n" + 
			"  var root = data.documentElement;\r\n" + 
			"  var rootJS = this.data[\"data_\"+root.getAttribute(\"datablockName\")] = {};\r\n" + 
			"  var category, catName, loopMode, cat, scat, skip, item, n;\r\n" + 
			"  for (var i=0, j, k; i<root.childNodes.length; i++) {\r\n" + 
			"    cat = root.childNodes[i];\r\n" + 
			"    catName = cat.localName;\r\n" + 
			"    if (! catName) continue;\r\n" + 
			"    catName = catName.substr(0, catName.length-8);\r\n" + 
			"    category = rootJS[catName] = {};\r\n" + 
			"    loopMode = cat.childNodes.length > 3;\r\n" + 
			"    n = 0;\r\n" + 
			"    for (j=0; j<cat.childNodes.length; j++) {\r\n" + 
			"      scat = cat.childNodes[j];\r\n" + 
			"      if (! scat.localName) continue;\r\n" + 
			"      skip = [];\r\n" + 
			"      for (k=0; k<scat.attributes.length; k++) {\r\n" + 
			"        item = scat.attributes.item(k);\r\n" + 
			"        if (loopMode) {\r\n" + 
			"          if (! category.hasOwnProperty(item.localName)) category[item.localName] = new Array(n);\r\n" + 
			"          category[item.localName].push(item.nodeValue);\r\n" + 
			"          skip.push(item.localName);\r\n" + 
			"        }\r\n" + 
			"        else category[item.localName] = [item.nodeValue];\r\n" + 
			"      }\r\n" + 
			"      for (k=0; k<scat.childNodes.length; k++) {\r\n" + 
			"        item = scat.childNodes[k];\r\n" + 
			"        if (! item.localName) continue;\r\n" + 
			"        if (loopMode) {\r\n" + 
			"          if (! category.hasOwnProperty(item.localName)) category[item.localName] = new Array(n);\r\n" + 
			"          category[item.localName].push(item.textContent);\r\n" + 
			"          skip.push(item.localName);\r\n" + 
			"        }\r\n" + 
			"        else category[item.localName] = [item.textContent];\r\n" + 
			"      }\r\n" + 
			"      if (loopMode) for (k in category) {if (skip.indexOf(k) == -1) category[k].push(null);}\r\n" + 
			"      n++;\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"}\r\n" + 
			"/*\r\n" + 
			"function loadPDBMLdic() {\r\n" + 
			"  var request = new CallRemote(\"GET\");\r\n" + 
			"  request.Send(\"/schema/pdbx-v40.xsd\");\r\n" + 
			"  \r\n" + 
			"  var typing = {};\r\n" + 
			"  \r\n" + 
			"  var root = request.request.responseXML.documentElement, cat, catName, stuff;\r\n" + 
			"  for (var i=0, j; i<root.childNodes.length; i++) {\r\n" + 
			"    cat = root.childNodes[i];\r\n" + 
			"    if (! cat.localName) continue;\r\n" + 
			"    catName = cat.getAttribute(\"name\");\r\n" + 
			"    if (catName.substr(0, 9) == \"datablock\" || catName.substr(catName.length-4) != \"Type\") continue;\r\n" + 
			"    catName = catName.substr(0, catName.length-4);\r\n" + 
			"    typing[catName] = {}\r\n" + 
			"    stuff = cat.getElementsByTagNameNS(\"*\", \"element\");\r\n" + 
			"    for (j=0; j<stuff.length; j++) {\r\n" + 
			"      if (catName == \"atom_site\") console.log(stuff[j].getAttribute(\"name\"), stuff[j].getAttribute(\"type\"));\r\n" + 
			"      if (stuff[j].getAttribute(\"type\") == \"xsd:integer\") typing[catName][stuff[j].getAttribute(\"name\")] = parseInt;\r\n" + 
			"      else if (stuff[j].getAttribute(\"type\") == \"xsd:decimal\") typing[catName][stuff[j].getAttribute(\"name\")] = parseFloat;\r\n" + 
			"    }\r\n" + 
			"    stuff = cat.getElementsByTagNameNS(\"*\", \"attribute\");\r\n" + 
			"    for (j=0; j<stuff.length; j++) {\r\n" + 
			"      if (catName == \"atom_site\") console.log(stuff[j].getAttribute(\"name\"), stuff[j].getAttribute(\"type\"));\r\n" + 
			"      if (stuff[j].getAttribute(\"type\") == \"xsd:integer\") typing[catName][stuff[j].getAttribute(\"name\")] = parseInt;\r\n" + 
			"      else if (stuff[j].getAttribute(\"type\") == \"xsd:decimal\") typing[catName][stuff[j].getAttribute(\"name\")] = parseFloat;\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"  __PDBMLDICT__ = typing;\r\n" + 
			"}*/\r\n" + 
			"\r\n" + 
			"//registerPublic::loadPDBML\r\n" + 
			"function loadPDBML(data, noCnT) {\r\n" + 
			"  var parser = new PDBMLparser();\r\n" + 
			"  parser.parse(data);\r\n" + 
			"\r\n" + 
			"  if (noCnT) return parser.data;\r\n" + 
			"  //if (! window.__PDBMLDICT__) loadPDBMLdic();\r\n" + 
			"  if (! window.__CIFDICT__) loadCIFdic();\r\n" + 
			"\r\n" + 
			"  var func, e, e2, e3, i;\r\n" + 
			"  for (e in parser.data) {\r\n" + 
			"    for (e2 in parser.data[e]) {\r\n" + 
			"      if (! __CIFDICT__.hasOwnProperty(e2)) continue;\r\n" + 
			"      for (e3 in parser.data[e][e2]) {\r\n" + 
			"        if (! __CIFDICT__[e2].hasOwnProperty(e3)) continue;\r\n" + 
			"        func = __CIFDICT__[e2][e3];\r\n" + 
			"        if (parser.data[e][e2][e3] instanceof Array) {\r\n" + 
			"          for (i=0; i<parser.data[e][e2][e3].length; i++) parser.data[e][e2][e3][i] = func.call(null, parser.data[e][e2][e3][i]);\r\n" + 
			"        }\r\n" + 
			"        else parser.data[e][e2][e3] = func.call(null, parser.data[e][e2][e3]);\r\n" + 
			"      }\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"  return parser.data;\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"// mmjson tree\r\n" + 
			"\r\n" + 
			"//registerPublic::setupCIFTree\r\n" + 
			"function setupCIFTree(target, jso, expandableTarget) {\r\n" + 
			"  var point = target.pushNode(\"DIV\");\r\n" + 
			"  point.expandableTarget = expandableTarget;\r\n" + 
			"  point.popup = target.parentNode; point.topNode = true;\r\n" + 
			"  renderChildCIFTree(point, jso);\r\n" + 
			"  if (point.childNodes.length == 1 && point.childNodes[0].childNodes[0].className.indexOf(\"optCat_p\") != -1) point.childNodes[0].childNodes[0].onclick();\r\n" + 
			"  return point;\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"function renderChildCIFTree(target, jso) {\r\n" + 
			"  var item;\r\n" + 
			"  var keys = Object.keys(jso);\r\n" + 
			"  var table = null, row;\r\n" + 
			"  if (jso[keys[0]] instanceof Array || \"splice\" in jso[keys[0]]) {\r\n" + 
			"    var table = new drawTable(), row;\r\n" + 
			"    if (! target.showAll || jso[keys[0]].length < 10000) table.tbl.setClass(\"dTO eqSpacedTbl\");\r\n" + 
			"    table.tbl.border = \"1\";\r\n" + 
			"    table.tbl.style.width = \"\";\r\n" + 
			"    row = table.addRowXH(keys);\r\n" + 
			"    for (var i=0, j; i<(target.showAll ? jso[keys[0]].length : Math.min(jso[keys[0]].length, 25)); i++) {\r\n" + 
			"      row = table.addRow();\r\n" + 
			"      for (j=0; j<keys.length; j++) row.addCell(jso[keys[j]][i]);\r\n" + 
			"    }\r\n" + 
			"    target.pushNode(table.tbl);\r\n" + 
			"    if (! target.showAll && jso[keys[0]].length > 25) {\r\n" + 
			"      row = target.pushNode(\"a\", \"Show all (\"+jso[keys[0]].length+(jso[keys[0]].length > 2500 ? \" rows, which will take some time to process and might cause your browser to become unresponsive. Alternatively, switch to flat file representation.\" : \" rows\")+\")\");\r\n" + 
			"      row.targetObj = target; row.jso = jso; row.style.cursor = \"pointer\";\r\n" + 
			"      row.onclick = function() {\r\n" + 
			"        Clear(this.targetObj);\r\n" + 
			"        this.targetObj.showAll = true;\r\n" + 
			"        renderChildCIFTree(this.targetObj, this.jso);\r\n" + 
			"      };\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"  else {\r\n" + 
			"    for (var i=0; i<keys.length; i++) {\r\n" + 
			"      item = target.pushNode(\"DIV\");\r\n" + 
			"      item.plus = item.pushNode(\"a\", \"+\");\r\n" + 
			"      item.plus.setClass(\"optCat_p\");\r\n" + 
			"      item.name = item.pushNode(\"a\", keys[i]);\r\n" + 
			"      item.name.setClass(\"optCat_n\");\r\n" + 
			"      item.plus.onclick = item.name.onclick = expandCatTree;\r\n" + 
			"      item.payload = jso[keys[i]];\r\n" + 
			"      item.expandFunc = renderChildCIFTree;\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"// mmcif parser\r\n" + 
			"\r\n" + 
			"function _loop(parserObj) {\r\n" + 
			"  this.parserObj = parserObj;\r\n" + 
			"  this.length = 0;\r\n" + 
			"  this.refID = -1;\r\n" + 
			"  this.refList = [];\r\n" + 
			"  this.namesDefined = false;\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"_loop.prototype.addName = function(name) {\r\n" + 
			"  var catName = Partition(name, \".\");\r\n" + 
			"  var ref = this.parserObj.currentTarget[this.parserObj.currentTarget.length-2];\r\n" + 
			"  if (catName[1]) {\r\n" + 
			"    if (! ref.hasOwnProperty(catName[0])) ref[catName[0]] = {};\r\n" + 
			"    if (! ref[catName[0]].hasOwnProperty(catName[2])) ref[catName[0]][catName[2]] = [];\r\n" + 
			"    this.refList.push(ref[catName[0]][catName[2]]);\r\n" + 
			"  }\r\n" + 
			"  else {\r\n" + 
			"    if (! ref.hasOwnProperty(catName[0])) ref[catName[0]] = [];\r\n" + 
			"    this.refList.push(ref[catName[0]]);\r\n" + 
			"  }\r\n" + 
			"  this.length = this.refList.length;\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"_loop.prototype.pushValue = function(value) {\r\n" + 
			"  this.namesDefined = true;\r\n" + 
			"  var target = this.nextTarget();\r\n" + 
			"  if (value == \"stop_\") return this.stopPush();\r\n" + 
			"  target.push(value);\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"_loop.prototype.nextTarget = function() {\r\n" + 
			"  this.refID = (this.refID+1)%this.length;\r\n" + 
			"  return this.refList[this.refID];\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"_loop.prototype.stopPush = function() {\r\n" + 
			"  this.refID = -1;\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"function CIFparser() {\r\n" + 
			"  this.data = {};\r\n" + 
			"  this.currentTarget = null;\r\n" + 
			"  this.loopPointer = null;\r\n" + 
			"  this.selectGlobal();\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.parse = function(data) {\r\n" + 
			"  var lines = data.split(\"\\n\"), line, buffer = [], multi_line_mode = false, Z;\r\n" + 
			"  for (var i=0; i<lines.length; i++) {\r\n" + 
			"    Z = lines[i].substr(0, 1);\r\n" + 
			"    line = lines[i].trim();\r\n" + 
			"    if (Z == \";\") {\r\n" + 
			"      if (multi_line_mode) this.setDataValue(buffer.join(\"\\n\"));\r\n" + 
			"      else buffer = [];\r\n" + 
			"      multi_line_mode = ! multi_line_mode;\r\n" + 
			"      line = line.substr(1).trim();\r\n" + 
			"    }\r\n" + 
			"    if (multi_line_mode) buffer.push(line);\r\n" + 
			"    else this.processContent(this.specialSplit(line));\r\n" + 
			"  }\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.specialSplit = function(content) {\r\n" + 
			"  var output = [[\"\", false]], quote = false, length = content.length, isWS, olast=0;\r\n" + 
			"  for (var i=0; i<length; i++) {\r\n" + 
			"    isWS = content[i] == \" \" || content[i] == \"\\t\";\r\n" + 
			"    if ((content[i] == \"'\" || content[i] == '\"') && (i == 0 || content[i-1] == \" \" || content[i-1] == \"\\t\" || i == length-1 || content[i+1] == \" \" || content[i+1] == \"\\t\")) quote = ! quote;\r\n" + 
			"    else if (! quote && isWS && output[olast][0] != \"\") {output.push([\"\", false]); olast++;}\r\n" + 
			"    else if (! quote && content[i] == \"#\") break;\r\n" + 
			"    else if (! isWS || quote) {output[olast][0] += content[i]; output[olast][1] = quote;}\r\n" + 
			"  }\r\n" + 
			"  if (output[olast][0] == \"\") output.pop();\r\n" + 
			"  return output;\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.processContent = function(content) {\r\n" + 
			"  for (var i=0; i<content.length; i++) {\r\n" + 
			"    if (content[i][0] == \"global_\" && ! content[i][0]) {\r\n" + 
			"      this.loopPointer = null;\r\n" + 
			"      this.selectGlobal();\r\n" + 
			"    }\r\n" + 
			"    else if (content[i][0].substr(0, 5) == \"data_\" && ! content[i][1]) {\r\n" + 
			"      this.loopPointer = null;\r\n" + 
			"      this.selectData(content[i][0]);\r\n" + 
			"    }\r\n" + 
			"    else if (content[i][0].substr(0, 5) == \"save_\" && ! content[i][1]) {\r\n" + 
			"      this.loopPointer = null;\r\n" + 
			"      if (content[i][0].substr(5).length) this.selectFrame(content[i][0]);\r\n" + 
			"      else this.endFrame();\r\n" + 
			"    }\r\n" + 
			"    else if (content[i][0] == \"loop_\" && ! content[i][1]) this.loopPointer = new _loop(this);\r\n" + 
			"    else if (content[i][0].substr(0, 1) == \"_\" && ! content[i][1]) this.setDataName(content[i][0].substr(1));\r\n" + 
			"    else this.setDataValue(content[i][0]);\r\n" + 
			"  }\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.setDataName = function(name) {\r\n" + 
			"  if (this.loopPointer != null) {\r\n" + 
			"    if (this.loopPointer.namesDefined) this.loopPointer = null;\r\n" + 
			"    else return this.loopPointer.addName(name);\r\n" + 
			"  }\r\n" + 
			"  var name = Partition(name, \".\");\r\n" + 
			"  this.currentTarget.pop();\r\n" + 
			"  if (name[1]) {\r\n" + 
			"    if (! this.currentTarget[this.currentTarget.length-1].hasOwnProperty(name[0])) this.currentTarget[this.currentTarget.length-1][name[0]] = {};\r\n" + 
			"    this.currentTarget[this.currentTarget.length-1][name[0]][name[2]] = \"\";\r\n" + 
			"    this.currentTarget.push([this.currentTarget[this.currentTarget.length-1][name[0]], name[2]]);\r\n" + 
			"  }\r\n" + 
			"  else {\r\n" + 
			"    this.currentTarget[this.currentTarget.length-1][name[0]] = \"\";\r\n" + 
			"    this.currentTarget.push([this.currentTarget[this.currentTarget.length-1], name[0]]);\r\n" + 
			"  }\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.setDataValue = function(value) {\r\n" + 
			"  if (this.loopPointer != null) this.loopPointer.pushValue(value);\r\n" + 
			"  else {var tmp = this.currentTarget[this.currentTarget.length-1]; tmp[0][tmp[1]] = [value];}\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.selectGlobal = function() {this.currentTarget = [this.data, this.data, null];};\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.selectData = function(name) {\r\n" + 
			"  if (! this.data.hasOwnProperty(name)) this.data[name] = {};\r\n" + 
			"  this.currentTarget = [this.data, this.data[name], null];\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.selectFrame = function(name) {\r\n" + 
			"  if (! this.currentTarget[1].hasOwnProperty(name)) this.currentTarget[1][name] = {};\r\n" + 
			"  this.currentTarget = this.currentTarget.slice(0, 2); this.currentTarget.push(this.currentTarget[1][name]); this.currentTarget.push(null);\r\n" + 
			"};\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.endData = function() {this.currentTarget = this.currentTarget.slice(0, 2);};\r\n" + 
			"\r\n" + 
			"CIFparser.prototype.endFrame = function() {this.currentTarget = this.currentTarget.slice(0, 3);};\r\n" + 
			"\r\n" + 
			"function loadCIFdic() {\r\n" + 
			"  var request = new CallRemote(\"GET\");\r\n" + 
			"  request.Send(\"/mmcif_pdbx_v40.dic\");\r\n" + 
			"  \r\n" + 
			"  var parser = new CIFparser();\r\n" + 
			"  parser.parse(request.request.responseText);\r\n" + 
			"  \r\n" + 
			"  var ref = parser.data[\"data_mmcif_pdbx.dic\"], name, dic = {};\r\n" + 
			"  for (var e in ref) {\r\n" + 
			"    if (typeof ref[e] != \"object\" || ref[e] instanceof Array || ! ref[e].hasOwnProperty(\"item_type\")) continue;\r\n" + 
			"    name = Partition(e.substr(6), \".\");\r\n" + 
			"    if (! dic.hasOwnProperty(name[0])) dic[name[0]] = {};\r\n" + 
			"    dic[name[0]][name[2]] = ref[e].item_type.code.trim()\r\n" + 
			"  }\r\n" + 
			"  \r\n" + 
			"  var typing = {}, e2;\r\n" + 
			"  for (var e in dic) {\r\n" + 
			"    for (e2 in dic[e]) {\r\n" + 
			"      if (dic[e][e2] == \"int\") {\r\n" + 
			"        if (! typing.hasOwnProperty(e)) typing[e] = {};\r\n" + 
			"        typing[e][e2] = parseInt;\r\n" + 
			"      }\r\n" + 
			"      else if (dic[e][e2] == \"float\") {\r\n" + 
			"        if (! typing.hasOwnProperty(e)) typing[e] = {};\r\n" + 
			"        typing[e][e2] = parseFloat;\r\n" + 
			"      }\r\n" + 
			"      else if (dic[e][e2] == \"int-range\") {\r\n" + 
			"        if (! typing.hasOwnProperty(e)) typing[e] = {};\r\n" + 
			"        typing[e][e2] = parseIntRange;\r\n" + 
			"      }\r\n" + 
			"      else if (dic[e][e2] == \"float-range\") {\r\n" + 
			"        if (! typing.hasOwnProperty(e)) typing[e] = {};\r\n" + 
			"        typing[e][e2] = parseFloatRange;\r\n" + 
			"      }\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"  __CIFDICT__ = typing;\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"function parseIntRange(inp) {\r\n" + 
			"  try {\r\n" + 
			"    var pos = inp.indexOf(\"-\", 1);\r\n" + 
			"    if (pos == -1) throw -1;\r\n" + 
			"    return [parseInt(inp.substr(0, pos)), parseInt(inp.substr(pos+1))];\r\n" + 
			"  }\r\n" + 
			"  catch (e) {return [parseInt(inp)];}\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"function parseFloatRange() {\r\n" + 
			"  try {\r\n" + 
			"    var pos = inp.indexOf(\"-\", 1);\r\n" + 
			"    if (pos == -1) throw -1;\r\n" + 
			"    return [parseFloat(inp.substr(0, pos)), parseFloat(inp.substr(pos+1))];\r\n" + 
			"  }\r\n" + 
			"  catch (e) {return [parseFloat(inp)];}\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"//registerPublic::loadCIF\r\n" + 
			"function loadCIF(data, noCnT) {\r\n" + 
			"  var parser = new CIFparser();\r\n" + 
			"  parser.parse(data);\r\n" + 
			"  \r\n" + 
			"  var e, e2, e3, i;\r\n" + 
			"  for (e in parser.data) {\r\n" + 
			"    for (e2 in parser.data[e]) {\r\n" + 
			"      for (e3 in parser.data[e][e2]) {\r\n" + 
			"        if (parser.data[e][e2][e3] instanceof Array) {\r\n" + 
			"        	for (i=0; i<parser.data[e][e2][e3].length; i++){\r\n" + 
			"	        	if(parser.data[e][e2][e3][i] == \"?\" || parser.data[e][e2][e3][i] == \".\"){\r\n" + 
			"	        		parser.data[e][e2][e3][i] = null;\r\n" + 
			"	        	}\r\n" + 
			"        	}\r\n" + 
			"        }else{\r\n" + 
			"        	if(parser.data[e][e2][e3] == \"?\" || parser.data[e][e2][e3] == \".\"){\r\n" + 
			"        		parser.data[e][e2][e3] = null;\r\n" + 
			"        	}\r\n" + 
			"        }\r\n" + 
			"      }\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"  \r\n" + 
			"  if (noCnT) return JSON.stringify(parser.data);\r\n" + 
			"  if (! window.__CIFDICT__) loadCIFdic();\r\n" + 
			"  \r\n" + 
			"  var func;\r\n" + 
			"  for (e in parser.data) {\r\n" + 
			"    for (e2 in parser.data[e]) {\r\n" + 
			"      if (! __CIFDICT__.hasOwnProperty(e2)) continue;\r\n" + 
			"      for (e3 in parser.data[e][e2]) {\r\n" + 
			"        if (! __CIFDICT__[e2].hasOwnProperty(e3)) continue;\r\n" + 
			"        func = __CIFDICT__[e2][e3];\r\n" + 
			"        if (parser.data[e][e2][e3] instanceof Array) {for (i=0; i<parser.data[e][e2][e3].length; i++) parser.data[e][e2][e3][i] = func.call(null, parser.data[e][e2][e3][i]);}\r\n" + 
			"        else parser.data[e][e2][e3] = func.call(null, parser.data[e][e2][e3]);\r\n" + 
			"      }\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"  return JSON.stringify(parser.data);\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"//registerPublic::dumpCIF\r\n" + 
			"function dumpCIF(data) {\r\n" + 
			"  var cifStrCheck = new RegExp(\"[\\\\s\\(\\)]\");\r\n" + 
			"  var cifStrNLCheck = new RegExp(\"[\\n]\");\r\n" + 
			"  \r\n" + 
			"  var sliceConst = '';\r\n" + 
			"  for (var i=0; i<1024; i++) sliceConst += ' ';\r\n" + 
			"  var padString = function(inp, flength) {\"use strict\";\r\n" + 
			"    return inp+sliceConst.slice(inp.length, flength);\r\n" + 
			"  };\r\n" + 
			"  \r\n" + 
			"  var dumpStr = function(inp) {\r\n" + 
			"    if (inp == null) return \"?\";\r\n" + 
			"    else {\r\n" + 
			"      if (typeof(inp) != \"string\") return inp+\"\";\r\n" + 
			"      if (cifStrNLCheck.test(inp)) return \"\\n;\"+inp+\"\\n;\";\r\n" + 
			"      if (cifStrCheck.test(inp)) return \"'\"+inp+\"'\";\r\n" + 
			"      return inp;\r\n" + 
			"    }\r\n" + 
			"  };\r\n" + 
			"  \r\n" + 
			"  var dumpCat = function(k, v) {\r\n" + 
			"    var output = \"#\\n\", k, i, noi, pad, tmp1, tmp2, j;\r\n" + 
			"    noi = v[Object.keys(v)[0]].length;\r\n" + 
			"    if (noi == 1) {\r\n" + 
			"      pad = 0\r\n" + 
			"      for (k2 in v) if (k2.length > pad) pad = k2.length;\r\n" + 
			"      pad += 3;\r\n" + 
			"      for (k2 in v) output += \"_\"+k+\".\"+padString(k2, pad)+dumpStr(v[k2][0], pad)+\"\\n\";\r\n" + 
			"    }\r\n" + 
			"    else {\r\n" + 
			"      output += \"loop_\\n\";\r\n" + 
			"      pad = [];\r\n" + 
			"      for (k2 in v) {\r\n" + 
			"        output += \"_\"+k+\".\"+k2+\"\\n\";\r\n" + 
			"        pad.push(0);\r\n" + 
			"      }\r\n" + 
			"      tmp1 = [];\r\n" + 
			"      for (i=0; i<noi; i++) {\r\n" + 
			"        tmp1.push(tmp2=[]);\r\n" + 
			"        for (k2 in v) tmp2.push(dumpStr(v[k2][i]));\r\n" + 
			"      }\r\n" + 
			"      \r\n" + 
			"      for (j=0; j<tmp1[0].length; j++) {\r\n" + 
			"        pad = 0;\r\n" + 
			"        for (i=0; i<tmp1.length; i++) {\r\n" + 
			"          if (tmp1[i][j].substr(0,2) != '\\n;' && tmp1[i][j].length > pad) pad = tmp1[i][j].length;\r\n" + 
			"        }\r\n" + 
			"        pad += 1;\r\n" + 
			"        for (i=0; i<tmp1.length; i++) {\r\n" + 
			"          if (tmp1[i][j].substr(0,2) != '\\n;') tmp1[i][j] = padString(tmp1[i][j], pad);\r\n" + 
			"        }\r\n" + 
			"      }\r\n" + 
			"      for (i=0; i<noi; i++) output += tmp1[i].join(\"\")+\"\\n\";\r\n" + 
			"    }\r\n" + 
			"    return output.trim()+\"\\n\";\r\n" + 
			"  }\r\n" + 
			"  \r\n" + 
			"  var inner = true;\r\n" + 
			"  var dumpPart = function(part, skip) {\r\n" + 
			"    var output = \"\", k;\r\n" + 
			"    for (k in part) {\r\n" + 
			"      if (typeof(part[k]) == \"object\" && ! Array.isArray(part[k])) {\r\n" + 
			"        if (k.substr(0, 5) != \"data_\" && k.substr(0, 5) != \"save_\" && k.substr(0, 7) != \"global_\") output += dumpCat(k, part[k], true);\r\n" + 
			"        else {output += k+\"\\n\" + dumpPart(part[k]); inner = false;}\r\n" + 
			"      }\r\n" + 
			"    }\r\n" + 
			"    if (skip || ! inner) return output;\r\n" + 
			"    else return output+\"#\\n\";\r\n" + 
			"  }\r\n" + 
			"  return dumpPart(data);\r\n" + 
			"}\r\n" + 
			"\r\n" + 
			"\r\n" + 
			"\r\n" + 
			"function Partition(code,sep){\r\n" + 
			"  var tmp = code.split(sep);\r\n" + 
			"  return [tmp.shift(), sep, tmp.join(sep)];\r\n" + 
			"}";
	public static StringBuffer loadContents(Reader reader) throws IOException{
		BufferedReader br = new BufferedReader(reader);//BufferedReader を Reader にキャストしてさらに BufferedReader にするのは問題ないのだろうか、。。 
		StringBuffer sb = new StringBuffer();
		String line = null;
		while((line = br.readLine()) != null){
			sb.append(line);
			sb.append("\n");
		}
		return sb;
	}
	public static StringBuffer loadContents(String filename) throws IOException{
		File file = new File(filename);
		BufferedReader br = org.glycoinfo.PDB2Glycan.io.file.Files.newBufferedReader(file);
		StringBuffer ret =  loadContents(br);
		br.close();
		return ret;
	}
	public static String getMMjsonString(Reader reader) {
		ScriptEngineManager manager = new ScriptEngineManager();
//		ScriptEngine engine = manager.getEngineByName("nashorn"); // comment out. 23.Dec.2022
		// TODO: Nashorn will remove in future. need to use the other js engine (maybe graal?).
		ScriptEngine engine = manager.getEngineByName("graal.js"); // uncomment. 23.Dec.2022
		try {
			engine.eval(
					jsString
					);
			Invocable inv = (Invocable) engine;
			String contents = loadContents(reader).toString();
			
			String res = (String)inv.invokeFunction("loadCIF",contents,1);
			
			//try(BufferedWriter bw = Files.newBufferedWriter(Paths.get("testest.dat"))){
			//	bw.write(jsString);
			//}
			return res;
		}catch(IOException e) {
			e.printStackTrace();
		} catch(ScriptException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return null;
	}
}

package org.glycoinfo.PDB2Glycan.io.file;

import static java.lang.Thread.sleep;
import static org.glycoinfo.PDB2Glycan.cli.App.NETWORK_ERROR_RETRY;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;

import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.ChemicalComponentJsonParser;
import org.glycoinfo.PDB2Glycan.io.model.ChemicalComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteChemicalComponentLoader extends ChemicalComponentLoader {

  private static final Logger logger = LoggerFactory.getLogger(RemoteChemicalComponentLoader.class);

  private static URL DEFAULT_BASE_URL;

  static {
    try {
      DEFAULT_BASE_URL = new URL("ftp://data.pdbj.org/pdbjplus/data/cc/mmjson/");
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public ChemicalComponent loadChemicalComponent(String code) throws IOException, MMcifParseException {
    code = code.toUpperCase();

    String fileName = String.format("%s.json.gz", code);
    URL url = new URL(DEFAULT_BASE_URL, fileName);

    logger.info("Load " + url.getFile() + " from " + url.getHost());

    for (int i = 1; ; i++) {
      try {
        try (BufferedReader br = Files.newBufferedReader(url)) {
          return new ChemicalComponentJsonParser(br).parse();
        }
      } catch (ConnectException e) {
        if (i >= NETWORK_ERROR_RETRY) {
          throw e;
        }

        int wait = (int) Math.pow(2, i);

        logger.warn(e.getMessage());
        logger.warn(String.format("Retry after %d [s]", wait));

        try {
          sleep(wait * 1000);
        } catch (InterruptedException ignored) {
        }
      }
    }
  }

}

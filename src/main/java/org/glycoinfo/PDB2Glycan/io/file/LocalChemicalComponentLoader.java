package org.glycoinfo.PDB2Glycan.io.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedList;

import org.glycoinfo.PDB2Glycan.cli.InputFormat;
import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.ChemicalComponentJsonParser;
import org.glycoinfo.PDB2Glycan.io.ChemicalComponentMMcifParser;
import org.glycoinfo.PDB2Glycan.io.model.ChemicalComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocalChemicalComponentLoader extends ChemicalComponentLoader {

  private static final Logger logger = LoggerFactory.getLogger(LocalChemicalComponentLoader.class);

  private String basePath;
  private InputFormat inputFormat;
  private InputFormat usedFormat;

  public LocalChemicalComponentLoader(String basePath) {
    this(basePath, InputFormat.MMJSON);
  }

  public LocalChemicalComponentLoader(String basePath, InputFormat inputFormat) {
    this.basePath = basePath;
    this.inputFormat = inputFormat;
  }

  @Override
  public ChemicalComponent loadChemicalComponent(String code) throws IOException, MMcifParseException {
    code = code.toUpperCase();

    usedFormat = this.inputFormat;
    String fileName = this.findCCDFile(code);

    logger.info("Load " + fileName);

    try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName).toFile())) {
      ChemicalComponent chemComp;
      if (usedFormat == InputFormat.MMCIF)
        chemComp = new ChemicalComponentMMcifParser().parse(br);
//        chemComp = new ChemicalComponentJsonParser(MMcifEncoder.getMMjsonString(br)).parse();
      else
        chemComp = new ChemicalComponentJsonParser(br).parse();

      return chemComp;
    }
  }

  /**
   * Find file containing {@code code} in the name from basePath
   * @param code The saccharide ID in CCD
   * @return The absolute file path of CCD file
   * @throws FileNotFoundException 
   */
  private String findCCDFile(String code) throws FileNotFoundException {
    File dir = new File(this.basePath);
    LinkedList<File> dirs = new LinkedList<>();
    dirs.add(dir);
    while (!dirs.isEmpty()) {
      dir = dirs.pop();
      File[] list = dir.listFiles();
      for (int i=0; i<list.length; i++) {
        // Only consider directories containing the CCD file with the code
        // e.g. NAG: <basePath>/ -> N/ -> NAG/ -> NAG.cif
        if (list[i].isDirectory() && list[i].getName().charAt(0) == code.charAt(0)
          && (list[i].getName().length() == 1 || list[i].getName().equals(code)) ) {
            dirs.add(list[i]);
          continue;
        }
        // Check code
        if (!list[i].getName().contains(code))
          continue;

        // Check format
        if (list[i].getName().contains(this.inputFormat.getExtension()))
          return list[i].getAbsolutePath();

        // Try to find the other format
        for ( InputFormat format : InputFormat.values() ) {
          if (format == this.inputFormat)
            continue;
          if (!list[i].getName().contains(format.getExtension()) )
            continue;
          logger.warn("CCD file with format \""+format+"\" different from the input format \""+this.inputFormat+"\" is used for the code: "+code);
          usedFormat = format;
          return list[i].getAbsolutePath();
        }
      }
    }
    throw new FileNotFoundException("No file for the code \"" + code + "\"");
  }

}

package org.glycoinfo.PDB2Glycan.io.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.model.ChemicalComponent;
import org.glycoinfo.PDB2Glycan.io.model.Protein;
import org.glycoinfo.PDB2Glycan.io.model.protein.ChemComp;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;
import org.glycoinfo.PDB2Glycan.structure.Atom;
import org.glycoinfo.PDB2Glycan.structure.Mol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ChemicalComponentLoader {

  private static final Logger logger = LoggerFactory.getLogger(ChemicalComponentLoader.class);

  private List<String> saccharideIds;
  private List<String> modificationIds;
  private Set<String> connectedResIds;

  private static final Map<String, Mol> cacheMol = new HashMap<>();
  private static final Map<String, ChemicalComponent> cacheCC = new HashMap<>();

  /**
   * Sets protein to be extracted saccharide chem_comp elements
   * @param protein Protein to be extracted chem_comp elements
   */
  public void setProtein(Protein protein) {
    this.saccharideIds   = new ArrayList<>();
    this.modificationIds = new ArrayList<>();
    this.connectedResIds = new HashSet<>();
    extractSaccharideIds(protein);

    logger.info("Saccharide chem comps: {}", this.saccharideIds);
    logger.info("Connected modifications: {}", this.modificationIds);
  }

  private void extractSaccharideIds(Protein protein) {
    // Use pdbx_entity_branch category if exists
//    if ( protein.getPdbxEntityBranch() != null && !protein.getPdbxEntityBranch().isEmpty() ) {
//      for ( PdbxEntityBranchList bList : protein.getPdbxEntityBranchList() ) {
//        if ( !this.saccharideIds.contains(bList.getComp_id()) )
//          this.saccharideIds.add(bList.getComp_id());
//      }
//      return;
//    }

    // Assume all chemComps as saccharides if no chemComp category
    if ( protein.getChemComps() == null || protein.getChemComps().isEmpty() ) {
      Set<String> lIDs = new HashSet<>();
      for ( StructConn structConn : protein.getStructConns() ) {
        lIDs.add( structConn.getPtnr1_label_comp_id() );
        lIDs.add( structConn.getPtnr2_label_comp_id() );
      }
      this.saccharideIds.addAll(lIDs);
      return;
    }

    List<String> porimerIds = new ArrayList<>();
    for ( ChemComp chemComp : protein.getChemComps() ) {
      // Collect saccharide chemComps
      String type = chemComp.getType().toLowerCase();
      if ( type.contains("saccharide") )
        this.saccharideIds.add(chemComp.getId());
      // Collect peptide, DNA and RNA chemComps
      if ( type.contains("peptide") || type.contains("dna") || type.contains("rna"))
        porimerIds.add(chemComp.getId());
    }

    // Collect all modification chemComps connecting to saccharide chemComps other than polymer residue
    Map<String, String> mapResAsymIdToCompId = new HashMap<>();
    Set<String> connectedResIds = new HashSet<>();
    List<StructConn> structConnsToCheck = new ArrayList<>();
    for ( StructConn structConn : protein.getStructConns() ) {
      // Only connections with covalent bond
      if ( !structConn.getConn_type_id().equals("covale") )
        continue;
      mapResAsymIdToCompId.put(
        structConn.getPtnr1_label_asym_id(),
        structConn.getPtnr1_label_comp_id()
      );
      mapResAsymIdToCompId.put(
        structConn.getPtnr2_label_asym_id(),
        structConn.getPtnr2_label_comp_id()
      );

      // For partner 1
      if ( this.saccharideIds.contains(structConn.getPtnr1_label_comp_id()) ) {
        // Ignore the other saccharide
        if ( this.saccharideIds.contains(structConn.getPtnr2_label_comp_id()) )
          continue;
        // Ignore polymer residues
        if ( porimerIds.contains(structConn.getPtnr2_label_comp_id()) )
          continue;
        connectedResIds.add(structConn.getPtnr2_label_asym_id());
        continue;
      }
      // For partner 2
      if ( this.saccharideIds.contains(structConn.getPtnr2_label_comp_id()) ) {
        // Ignore the other saccharide
        if ( this.saccharideIds.contains(structConn.getPtnr1_label_comp_id()) )
          continue;
        // Ignore polymer residues
        if ( porimerIds.contains(structConn.getPtnr1_label_comp_id()) )
          continue;
        connectedResIds.add(structConn.getPtnr1_label_asym_id());
        continue;
      }
      structConnsToCheck.add(structConn);
    }

    Set<String> connectedResIdsAll = new HashSet<>();
    while ( !connectedResIds.isEmpty() ) {
      connectedResIdsAll.addAll(connectedResIds);
      Set<String> connectedResIdsOld = connectedResIds;
      connectedResIds = new HashSet<>();

      List<StructConn> structConnChecked = new ArrayList<>();
      for ( StructConn structConn : structConnsToCheck ) {
        // For partner 1
        if ( connectedResIdsOld.contains(structConn.getPtnr1_label_asym_id()) ) {
          if ( connectedResIdsAll.contains(structConn.getPtnr2_label_asym_id()) )
            continue;
          // Ignore polymer residues
          if ( porimerIds.contains(structConn.getPtnr2_label_comp_id()) )
            continue;
          connectedResIds.add(structConn.getPtnr2_label_asym_id());
          structConnChecked.add(structConn);
          continue;
        }
        // For partner 2
        if ( connectedResIdsOld.contains(structConn.getPtnr2_label_asym_id()) ) {
          if ( connectedResIdsAll.contains(structConn.getPtnr1_label_asym_id()) )
            continue;
          // Ignore polymer residues
          if ( porimerIds.contains(structConn.getPtnr1_label_comp_id()) )
            continue;
          connectedResIds.add(structConn.getPtnr1_label_asym_id());
          structConnChecked.add(structConn);
          continue;
        }
      }
      structConnsToCheck.removeAll(structConnChecked);
    }

    for ( String connectedResId : connectedResIdsAll ) {
      String resSeq = mapResAsymIdToCompId.get(connectedResId);
      if ( this.modificationIds.contains(resSeq) )
        continue;
      this.modificationIds.add(resSeq);
    }
    this.connectedResIds = connectedResIdsAll;
    // Add all modifications as saccharides for now
//    this.saccharideIds.addAll(this.modificationIds);
  }

  public List<String> saccharideIds() {
   return this.saccharideIds;
  }

  public List<String> modificationIds() {
    return this.modificationIds;
  }

  public boolean isSaccharideAtom(Atom at) {
    if ( this.saccharideIds.contains(at.getAtom_site_label_comp_id()))
      return true;
    // ignore non-modification
    if ( !this.modificationIds.contains(at.getAtom_site_label_comp_id()) )
      return false;
    // Only connected ones
    if ( !this.connectedResIds.contains(at.getAtom_site_label_asym_id()) )
      return false;

    return true;
  }

  public Mol load(String code) throws IOException, MMcifParseException {
    return load(code, true);
  }

  public Mol load(String code, boolean useCache) throws IOException, MMcifParseException {
    code = code.toUpperCase();

    if (useCache && cacheMol.containsKey(code))
      return cacheMol.get(code);

    if (cacheCC.containsKey(code)) {
      Mol mol = cacheCC.get(code).toMol();
      synchronized (cacheMol) {
        cacheMol.putIfAbsent(code, mol);
      }
      return mol;
    }

    ChemicalComponent chemComp = loadChemicalComponent(code);

    synchronized (cacheCC) {
      cacheCC.putIfAbsent(code, chemComp);
    }

    Mol mol = chemComp.toMol();
    synchronized (cacheMol) {
      cacheMol.putIfAbsent(code, mol);
    }

    return mol;
  }

  public abstract ChemicalComponent loadChemicalComponent(String code) throws IOException, MMcifParseException;

}

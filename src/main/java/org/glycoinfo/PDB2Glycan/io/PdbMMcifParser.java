package org.glycoinfo.PDB2Glycan.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.biojava.nbio.structure.io.mmcif.model.PdbxPolySeqScheme;
import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.model.Protein;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxBranchScheme;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranch;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchDescriptor;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchLink;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchList;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.Citation;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.EntitySrcNat;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.Exptl;
import org.glycoinfo.PDB2Glycan.io.model.protein.AtomSite;
import org.glycoinfo.PDB2Glycan.io.model.protein.ChemComp;
import org.glycoinfo.PDB2Glycan.io.model.protein.EntityPoly;
import org.glycoinfo.PDB2Glycan.io.model.protein.Entry;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxNonpolyScheme;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructSheetHbond;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxUnobsOrZeroOccAtoms;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxValidateCloseContact;
import org.glycoinfo.PDB2Glycan.io.model.protein.Struct;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructAsym;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConf;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructRef;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructRefSeq;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetOrder;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetRange;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSite;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSiteGen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PdbMMcifParser {

  private static final Logger logger = LoggerFactory.getLogger(PdbMMcifParser.class);

  private MMcifParser parser;

  private static final List<Class<?>> classesToLoad;
  static {
    classesToLoad = new ArrayList<>();

    classesToLoad.add(Entry.class);

    classesToLoad.add(AtomSite.class);

    classesToLoad.add(ChemComp.class);
    classesToLoad.add(EntityPoly.class);
    classesToLoad.add(PdbxStructModResidue.class);
    classesToLoad.add(StructAsym.class);
    classesToLoad.add(StructRefSeq.class);
    classesToLoad.add(StructConn.class);

    classesToLoad.add(Struct.class);
    classesToLoad.add(Citation.class);
    classesToLoad.add(EntitySrcNat.class);
    classesToLoad.add(Exptl.class);
    classesToLoad.add(StructRef.class);
    classesToLoad.add(StructSite.class);
    classesToLoad.add(StructSiteGen.class);
    classesToLoad.add(StructSheetRange.class);
    classesToLoad.add(StructSheetOrder.class);
    classesToLoad.add(PdbxStructSheetHbond.class);
    classesToLoad.add(StructConf.class);
    classesToLoad.add(PdbxValidateCloseContact.class);

    classesToLoad.add(PdbxPolySeqScheme.class);
    classesToLoad.add(PdbxNonpolyScheme.class);
    classesToLoad.add(PdbxBranchScheme.class);

    classesToLoad.add(PdbxEntityBranch.class);
    classesToLoad.add(PdbxEntityBranchList.class);
    classesToLoad.add(PdbxEntityBranchLink.class);
    classesToLoad.add(PdbxEntityBranchDescriptor.class);

    classesToLoad.add(PdbxUnobsOrZeroOccAtoms.class);
  }

  public PdbMMcifParser() {
    parser = new MMcifParser();
    for ( Class<?> c : classesToLoad )
      parser.addCategoryClassToLoad(c);
  }

  public Protein parse(InputStream inStream) throws IOException, MMcifParseException {
    parser.parse(inStream);
    return getProtein();
  }

  public Protein parse(BufferedReader buf) throws IOException, MMcifParseException {
    parser.parse(buf);
    return getProtein();
  }

  private Protein getProtein() throws MMcifParseException {
    List<Entry> entries = parser.getCategoryClassObjects(Entry.class);

    String id = "none";
    if ( !entries.isEmpty() )
      id = entries.get(0).getId();
    logger.debug("Parsing pdb entry: " + id);

    Protein protein = new Protein();

    protein.setEntry(entries);

    protein.setAtomSites(getAtomSites(parser)); // use specific parser for checking PDB model number

    protein.setChemComps(parser.getCategoryClassObjects(ChemComp.class));
    protein.setEntityPolies(parser.getCategoryClassObjects(EntityPoly.class));
    protein.setPdbxStructModResidues(parser.getCategoryClassObjects(PdbxStructModResidue.class));
    protein.setStructAsyms(parser.getCategoryClassObjects(StructAsym.class));
    protein.setStructRefSeqs(parser.getCategoryClassObjects(StructRefSeq.class));
    protein.setStructConns(parser.getCategoryClassObjects(StructConn.class));

    protein.setStructs(parser.getCategoryClassObjects(Struct.class));
    protein.setCitations(parser.getCategoryClassObjects(Citation.class));
    protein.setEntitySrcNats(parser.getCategoryClassObjects(EntitySrcNat.class));
    protein.setExptls(parser.getCategoryClassObjects(Exptl.class));
    protein.setStructRefs(parser.getCategoryClassObjects(StructRef.class));
    protein.setStructSite(parser.getCategoryClassObjects(StructSite.class));
    protein.setStructSiteGen(parser.getCategoryClassObjects(StructSiteGen.class));
    protein.setStructSheetRange(parser.getCategoryClassObjects(StructSheetRange.class));
    protein.setStructSheetOrder(parser.getCategoryClassObjects(StructSheetOrder.class));
    protein.setPdbxStructSheetHbond(parser.getCategoryClassObjects(PdbxStructSheetHbond.class));
    protein.setStructConf(parser.getCategoryClassObjects(StructConf.class));
    protein.setPdbxValidateCloseContact(parser.getCategoryClassObjects(PdbxValidateCloseContact.class));

    protein.setPdbxPolySeqScheme(parser.getCategoryClassObjects(PdbxPolySeqScheme.class));
    protein.setPdbxNonpolyScheme(parser.getCategoryClassObjects(PdbxNonpolyScheme.class));
    protein.setPdbxBranchScheme(parser.getCategoryClassObjects(PdbxBranchScheme.class));

    protein.setPdbxEntityBranch(parser.getCategoryClassObjects(PdbxEntityBranch.class));
    protein.setPdbxEntityBranchList(parser.getCategoryClassObjects(PdbxEntityBranchList.class));
    protein.setPdbxEntityBranchLink(parser.getCategoryClassObjects(PdbxEntityBranchLink.class));
    protein.setPdbxEntityBranchDescriptor(parser.getCategoryClassObjects(PdbxEntityBranchDescriptor.class));

    protein.setPdbxUnobsOrZeroOccAtoms(parser.getCategoryClassObjects(PdbxUnobsOrZeroOccAtoms.class));

    correctUnobsWithShemes(protein);

    return protein;
  }

  private List<AtomSite> getAtomSites(MMcifParser parser) throws MMcifParseException {
    List<AtomSite> ret = parser.getCategoryClassObjects(AtomSite.class);
    for ( AtomSite atomSite : ret) {
      if ( atomSite.getPdbx_PDB_model_num().length() <= 7 )
        continue;
      throw new MMcifParseException(String.format(
            "%s is too long for model number, expects 7 characters or less",
            atomSite.getPdbx_PDB_model_num()
         ));
    }

    return ret;
  }

  private void correctUnobsWithShemes(Protein protein) {
    if ( protein.getPdbxUnobsOrZeroOccAtoms() == null )
      return;

    // Residue info correction in unobs section using scheme sections
    for ( PdbxUnobsOrZeroOccAtoms unobs : protein.getPdbxUnobsOrZeroOccAtoms() ) {
      if ( protein.getPdbxNonpolyScheme() != null ) {
        for (PdbxNonpolyScheme scheme : protein.getPdbxNonpolyScheme()) {
          // Trust auth info than label info
          if ( unobs.getAuth_comp_id() != null
             && !unobs.getAuth_comp_id().equals(scheme.getAuth_mon_id()) )
            continue;
          if ( unobs.getAuth_seq_id() != null
             && !unobs.getAuth_seq_id().equals(scheme.getAuth_seq_num()) )
            continue;
          if ( unobs.getAuth_asym_id() != null
             && !unobs.getAuth_asym_id().equals(scheme.getPdb_strand_id()) )
            continue;

          // Correction
          if ( !unobs.getLabel_asym_id().equals(scheme.getAsym_id()) )
            unobs.setLabel_asym_id(scheme.getAsym_id());
          if ( !unobs.getLabel_comp_id().equals(scheme.getMon_id()) )
            unobs.setLabel_comp_id(scheme.getMon_id());
        }
      }

    // TODO: correct using PdbxBranchScheme if necessary
    }
	  }
}

package org.glycoinfo.PDB2Glycan.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.biojava.nbio.structure.io.mmcif.model.ChemComp;
import org.biojava.nbio.structure.io.mmcif.model.ChemCompAtom;
import org.biojava.nbio.structure.io.mmcif.model.ChemCompBond;
import org.biojava.nbio.structure.io.mmcif.model.PdbxChemCompIdentifier;
import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.model.ChemicalComponent;
import org.glycoinfo.PDB2Glycan.io.model.chem_comp.PdbxChemCompAtomRelated;
import org.glycoinfo.PDB2Glycan.io.model.chem_comp.PdbxChemCompRelated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChemicalComponentMMcifParser {

  private static final Logger logger = LoggerFactory.getLogger(ChemicalComponentMMcifParser.class);

  private MMcifParser parser;

  private static final List<Class<?>> classesToLoad;
  static {
    classesToLoad = new ArrayList<>();

    classesToLoad.add(ChemComp.class);
    classesToLoad.add(ChemCompAtom.class);
    classesToLoad.add(ChemCompBond.class);

    classesToLoad.add(PdbxChemCompIdentifier.class);
    classesToLoad.add(PdbxChemCompRelated.class);
    classesToLoad.add(PdbxChemCompAtomRelated.class);
    classesToLoad.add(PdbxChemCompIdentifier.class);
  }

  public ChemicalComponentMMcifParser() {
    parser = new MMcifParser();
    for ( Class<?> c : classesToLoad )
      parser.addCategoryClassToLoad(c);
  }

  public ChemicalComponent parse(InputStream inStream) throws IOException, MMcifParseException {
    parser.parse(inStream);
    return getChemicalComponent();
  }

  public ChemicalComponent parse(BufferedReader buf) throws IOException, MMcifParseException {
    parser.parse(buf);
    return getChemicalComponent();
  }

  public ChemicalComponent getChemicalComponent() throws IOException, MMcifParseException {

    List<ChemComp> lChemComps = parser.getCategoryClassObjects(ChemComp.class);
    String id = lChemComps.get(0).getId();
    logger.debug("Parsing chemical component: " + id);

    ChemicalComponent cc = new ChemicalComponent();

    cc.setId(id);
    cc.setChemComps(lChemComps);
    cc.setChemCompAtoms(parser.getCategoryClassObjects(ChemCompAtom.class));
    cc.setChemCompBonds(parser.getCategoryClassObjects(ChemCompBond.class));

    cc.setPdbxChemCompIdentifier(parser.getCategoryClassObjects(PdbxChemCompIdentifier.class));
    cc.setPdbxChemCompReleated(parser.getCategoryClassObjects(PdbxChemCompRelated.class));
    cc.setPdbxChemCompAtomReleated(parser.getCategoryClassObjects(PdbxChemCompAtomRelated.class));
    cc.setPdbxChemCompIdentifier(parser.getCategoryClassObjects(PdbxChemCompIdentifier.class));

    return cc;
  }

}
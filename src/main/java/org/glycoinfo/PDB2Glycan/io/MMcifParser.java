package org.glycoinfo.PDB2Glycan.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.biojava.nbio.structure.io.mmcif.MMCIFFileTools;
import org.biojava.nbio.structure.io.mmcif.model.CIFLabel;
import org.biojava.nbio.structure.io.mmcif.model.IgnoreField;
import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @seeAlso org.biojava.nbio.structure.io.mmcif.SimpleMMcifParser
 * @author Masaaki Matsubara
 *
 */
public class MMcifParser {
  private static final Logger logger = LoggerFactory.getLogger(MMcifParser.class);

  /**
   * The header appearing at the beginning of a mmCIF file.
   * A "block code" can be added to it of no more than 32 chars.
   * See http://www.iucr.org/__data/assets/pdf_file/0019/22618/cifguide.pdf
   */
  public static final String MMCIF_TOP_HEADER = "data_";

  public static final String COMMENT_CHAR = "#";
  public static final String LOOP_START = "loop_";
  public static final String FIELD_LINE = "_";

  // the following are the 3 valid quoting characters in CIF
  /**
   * Quoting character '
   */
  private static final char S1 = '\'';

  /**
   * Quoting character "
   */
  private static final char S2 = '\"';

  /**
   * Quoting character ; (multi-line quoting)
   */
  public static final String STRING_LIMIT = ";";


  private List<Class<?>> classesToLoad;
  private Map<String, List<Object>> mapClassNameToObjects;

  public MMcifParser(){
    classesToLoad = new ArrayList<>();
    mapClassNameToObjects = new HashMap<>();
  }

  public void addCategoryClassToLoad(Class<?> clazz) {
    classesToLoad.add(clazz);
    mapClassNameToObjects.put(clazz.getName(), new ArrayList<>());
  }

  public <T> List<T> getCategoryClassObjects(Class<T> clazz) {
    List<T> objects = new ArrayList<T>();
    for ( Object obj : this.mapClassNameToObjects.get(clazz.getName()) )
      if ( obj != null )
        objects.add((T) obj);
    return objects;
  }

  public void parse(InputStream inStream) throws IOException, MMcifParseException {
    parse(new BufferedReader(new InputStreamReader(inStream)));
  }

  public void parse(BufferedReader buf) throws IOException, MMcifParseException {
    String line = null;

    boolean inLoop = false;
    boolean inLoopData = false;

    List<String> loopFields = new ArrayList<String>();
    List<String> lineData   = new ArrayList<String>();
    Set<String> loopWarnings = new HashSet<String>(); // used only to reduce logging statements

    String category = null;

    boolean foundHeader = false;

    while ( (line = buf.readLine ()) != null ){

      if (line.isEmpty() || line.startsWith(COMMENT_CHAR)) continue;

      if (!foundHeader) {
        // the first non-comment line is a data_PDBCODE line, test if this looks like a mmcif file
        if (line.startsWith(MMCIF_TOP_HEADER)){
          foundHeader = true;
          continue;
        } else {
          throw new MMcifParseException("This does not look like a valid mmCIF file! The first line should start with 'data_', but is: '" + line+"'");
        }
      }

      logger.debug(inLoop + " " + line);

      if (line.startsWith(MMCIF_TOP_HEADER)){
        // either first line in file, or beginning of new section (data block in CIF parlance)
        if ( inLoop) {
          //System.out.println("new data and in loop: " + line);
          inLoop = false;
          inLoopData = false;
          lineData.clear();
          loopFields.clear();
        }

      }

      if (inLoop) {

        if ( line.startsWith(LOOP_START)){
          loopFields.clear();
          inLoop = true;
          inLoopData = false;
          continue;
        }

        if ( line.matches("\\s*"+FIELD_LINE+"\\w+.*")) {

          if (inLoopData && line.startsWith(FIELD_LINE)) {
            logger.debug("Found a field line after reading loop data. Toggling to inLoop=false");
            inLoop = false;
            inLoopData = false;
            loopFields.clear();


            // a boring normal line
            List<String> data = processLine(line, buf, 2);

            if ( data.size() < 1){
              // this can happen if empty lines at end of file
              lineData.clear();
              continue;
            }
            String key = data.get(0);
            int pos = key.indexOf(".");
            if ( pos < 0 ) {
              // looks like a chem_comp file
              // line should start with data, otherwise something is wrong!
              if (! line.startsWith(MMCIF_TOP_HEADER)){
                logger.warn("This does not look like a valid mmCIF file! The first line should start with 'data_', but is '" + line+"'");
                return;
              }
              // ignore the first line...
              category=null;
              lineData.clear();
              continue;
            }
            category = key.substring(0,pos);
            String value = data.get(1);
            loopFields.add(key.substring(pos+1,key.length()));
            lineData.add(value);

            logger.debug("Found data for category {}: {}", key, value);
            continue;
          }

          // found another field.
          String txt = line.trim();
          if ( txt.indexOf('.') > -1){

            String[] spl = txt.split("\\.");
            category = spl[0];
            String attribute = spl[1];
            loopFields.add(attribute);
            logger.debug("Found category: {}, attribute: {}",category, attribute);
            if ( spl.length > 2){
              logger.warn("Found nested attribute in {}, not supported yet!",txt);
            }

          } else {
            category = txt;
            logger.debug("Found category without attribute: {}",category);
          }


        } else {

          // in loop and we found a data line
          lineData = processLine(line, buf, loopFields.size());
          logger.debug("Found a loop data line with {} data fields", lineData.size());
          logger.debug("Data fields: {}", lineData.toString());
          if ( lineData.size() != loopFields.size()){
            logger.warn("Expected {} data fields, but found {} in line: {}",loopFields.size(),lineData.size(),line);

          }

          endLineChecks(category, loopFields, lineData, loopWarnings);

          lineData.clear();

          inLoopData = true;
        }

      } else {
        // not in loop

        if ( line.startsWith(LOOP_START)){
          if ( category != null)
            endLineChecks(category, loopFields, lineData, loopWarnings);

          resetBuffers(loopFields, lineData, loopWarnings);
          category = null;
          inLoop = true;
          inLoopData = false;
          logger.debug("Detected LOOP_START: '{}'. Toggling to inLoop=true", LOOP_START);
          continue;
        } else {
          logger.debug("Normal line ");
          inLoop = false;

          // a boring normal line
          List<String> data = processLine(line, buf, 2);

          if ( data.size() < 1){
            // this can happen if empty lines at end of file
            lineData.clear();
            continue;
          }
          String key = data.get(0);
          int pos = key.indexOf(".");
          if ( pos < 0 ) {
            // looks like a chem_comp file
            // line should start with data, otherwise something is wrong!
            if (! line.startsWith(MMCIF_TOP_HEADER)){
              logger.warn("This does not look like a valid mmCIF file! The first line should start with 'data_', but is '" + line+"'");
              return;
            }
            // ignore the first line...
            category=null;
            lineData.clear();
            continue;
          }

          if (category!=null && !key.substring(0,pos).equals(category)) {
            // we've changed category: need to flush the previous one
            endLineChecks(category, loopFields, lineData, loopWarnings);
            resetBuffers(loopFields, lineData, loopWarnings);
          }

          category = key.substring(0,pos);

          String value = data.get(1);
          loopFields.add(key.substring(pos+1,key.length()));
          lineData.add(value);

          logger.debug("Found data for category {}: {}", key, value);

        }
      }
    }

    if (category!=null && lineData.size()>0 && lineData.size()==loopFields.size()) {
      // the last category in the file will still be missing, we add it now
      endLineChecks(category, loopFields, lineData, loopWarnings);
      resetBuffers(loopFields, lineData, loopWarnings);
    }
  }

  private void resetBuffers(List<String> loopFields, List<String> lineData, Set<String> loopWarnings) {
    loopFields.clear();
    lineData.clear();
    loopWarnings.clear();
  }

  private List<String> processSingleLine(String line){

    List<String> data = new ArrayList<String>();

    if ( line.trim().length() == 0){
      return data;
    }

    if ( line.trim().length() == 1){
      if ( line.startsWith(STRING_LIMIT))
        return data;
    }
    boolean inString = false; // semicolon (;) quoting
    boolean inS1     = false; // single quote (') quoting
    boolean inS2     = false; // double quote (") quoting
    String word    = "";

    for (int i=0; i< line.length(); i++ ){

      Character c = line.charAt(i);

      Character nextC = null;
      if (i < line.length() - 1)
        nextC = line.charAt(i+1);

      Character prevC = null;
      if (i>0)
        prevC = line.charAt(i-1);

      if  (c == ' ') {

        if ( ! inString){
          if ( ! word.equals(""))
            data.add(word.trim());
          word = "";
        } else {
          // we are in a string, add the space
          word += c;
        }

      } else if (c == S1 )  {

        if ( inString){

          boolean wordEnd = false;
          if (! inS2) {
            if (nextC==null || Character.isWhitespace(nextC)){
              i++;
              wordEnd = true;
            }
          }

          if ( wordEnd ) {

            // at end of string
            if ( ! word.equals(""))
              data.add(word.trim());
            word     = "";
            inString = false;
            inS1     = false;
          } else {
            word += c;
          }

        } else if (prevC==null || prevC==' ') {
          // the beginning of a new string
          inString = true;
          inS1     = true;
        } else {
          word += c;
        }
      } else if ( c == S2 ){
        if ( inString){

          boolean wordEnd = false;
          if (! inS1) {
            if (nextC==null || Character.isWhitespace(nextC)){
              i++;
              wordEnd = true;
            }
          }

          if ( wordEnd ) {

            // at end of string
            if ( ! word.equals(""))
              data.add(word.trim());
            word     = "";
            inString = false;
            inS2     = false;
          } else {
            word += c;
          }
        } else if (prevC==null || prevC==' ') {
          // the beginning of a new string
          inString = true;
          inS2     = true;
        } else {
          word += c;
        }
      } else {
        word += c;
      }

    }
    if ( ! word.trim().equals(""))
      data.add(word);


    return data;

  }


  /**
   * Get the content of a cif entry
   *
   * @param line
   * @param buf
   * @return
   */
  private List<String> processLine(String line,
      BufferedReader buf,
      int fieldLength)
          throws IOException{

    //System.out.println("XX processLine " + fieldLength + " " + line);
    // go through the line and process each character
    List<String> lineData = new ArrayList<String>();

    boolean inString = false;

    StringBuilder bigWord = null;

    while ( true ){

      if ( line.startsWith(STRING_LIMIT)){
        if (! inString){

          inString = true;
          if ( line.length() > 1)
            bigWord = new StringBuilder(line.substring(1));
          else
            bigWord = new StringBuilder("");


        } else {
          // the end of a word
          lineData.add(bigWord.toString());
          bigWord = null;
          inString = false;

        }
      } else {
        if ( inString )
          bigWord.append(line);
        else {

          List<String> dat = processSingleLine(line);

          for (String d : dat){
            lineData.add(d);
          }
        }
      }

      //System.out.println("in process line : " + lineData.size() + " " + fieldLength);

      if ( lineData.size() > fieldLength){

        logger.warn("wrong data length ("+lineData.size()+
            ") should be ("+fieldLength+") at line " + line + " got lineData: " + lineData);
        return lineData;
      }

      if ( lineData.size() == fieldLength)
        return lineData;


      line = buf.readLine();
      if ( line == null)
        break;
    }
    return lineData;

  }

  private void endLineChecks(String category,List<String> loopFields, List<String> lineData, Set<String> loopWarnings ) throws IOException, MMcifParseException{

    logger.debug("Processing category {}, with fields: {}",category,loopFields.toString());
    //    System.out.println("parsed the following data: " +category + " fields: "+
    //        loopFields + " DATA: " +
    //        lineData);

    if ( loopFields.size() != lineData.size()){
      logger.warn("looks like we got a problem with nested string quote characters:");
      throw new MMcifParseException("data length ("+ lineData.size() +
          ") != fields length ("+loopFields.size()+
          ") category: " +category + " fields: "+
          loopFields + " DATA: " +
          lineData );
    }

    for ( Class<?> c : this.classesToLoad ) {
      String categoryFromClass = "_"+StringUtils.convertCaseCamelToSnake(c.getSimpleName());
      if ( !category.equals(categoryFromClass) )
        continue;
      this.mapClassNameToObjects.get(c.getName()).add(buildObject(c, loopFields, lineData, loopWarnings));
    }
  }

  /**
   * Populates a bean object from  the {@link org.biojava.nbio.structure.io.mmcif.model} package,
   * from the data read from a CIF file.
   * It uses reflection to lookup the field and setter method names given the category
   * found in the CIF file.
   * <p>
   * Due to limitations in variable names in java, not all fields can have names
   * exactly as defined in the CIF categories. In those cases the {@link CIFLabel} tag
   * can be used in the field names to give the appropriate name that corresponds to the
   * CIF category, which is the name that will be then looked up here.
   * The {@link IgnoreField} tag can also be used to exclude fields from being looked up.
   * @param Class<T>
   * @param loopFields
   * @param lineData
   * @param warnings
   * @return
   */
  private <T> T buildObject(Class<T> c, List<String> loopFields, List<String> lineData, Set<String> warnings) {

    T o = null;

    try {
      // build up the Entity object from the line data...
      o = c.getDeclaredConstructor().newInstance();

    } catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException
      | SecurityException | InstantiationException | IllegalAccessException e){
      logger.error( "Error while constructing {}: {}", c.getName(), e.getMessage());
      return null;
    }

    // these methods get the fields but also looking at the IgnoreField and CIFLabel annotations
    Field[] fields = MMCIFFileTools.getFields(c);
    String[] names = MMCIFFileTools.getFieldNames(fields);

    // let's build a map of all methods so that we can look up the setter methods later
    Method[] methods = c.getMethods();

    Map<String,Method> methodMap = new HashMap<String, Method>();
    for (Method m : methods) {
      methodMap.put(m.getName(),m);
    }

    // and a map of all the fields so that we can lookup them up later
    Map<String, Field> names2fields = new HashMap<>();
    for (int i=0;i<fields.length;i++) {
      names2fields.put(names[i], fields[i]);
    }

    int pos = -1 ;
    for (String key: loopFields){
      pos++;

      String val = lineData.get(pos);

      // we first start looking up the field which can be annotated with a CIFLabel if they
      // need alternative names (e.g. for field _symmetry.space_group_name_H-M, since hyphen is not allowed in var names in java)
      Field field = names2fields.get(key);

      if (field == null) {
        produceWarning(key, val, c, warnings);
        continue;
      }
      // now we need to find the corresponding setter
      // note that we can't use the field directly and then call Field.set() because many setters
      // have more functionality than just setting the value (e.g. some setters in ChemComp)

      // building up the setter method name: need to upper case the first letter, leave the rest untouched
      String setterMethodName = "set" + field.getName().substring(0,1).toUpperCase() + field.getName().substring(1, field.getName().length());

      Method setter = methodMap.get(setterMethodName);

      if (setter==null) {
        produceWarning(key, val, c, warnings);
        continue;
      }



      // now we populate the object with the values by invoking the corresponding setter method,
      // note that all of the mmCif container classes have only one argument (they are beans)
      Class<?>[] pType  = setter.getParameterTypes();


      try {
        if ( pType[0].getName().equals(Integer.class.getName())) {
          if ( val != null && ! val.equals("?") && !val.equals(".")) {

            Integer intVal = Integer.parseInt(val);
            setter.invoke(o, intVal);

          }
        } else {
          // default val is a String
          if ( val != null && (val.equals("?") || val.equals(".")))
            val = "";
          setter.invoke(o, val);
        }
      } catch (IllegalAccessException|InvocationTargetException e) {
        logger.error("Could not invoke setter {} with value {} for class {}", setterMethodName, val, c.getName());
      }

    }

    return o;
  }

  private void produceWarning(String key, String val, Class<?> c, Set<String> warnings) {

    String warning = "Trying to set field " + key + " in "+ c.getName() +" found in file, but no corresponding field could be found in model class (value:" + val + ")";
    String warnkey = key+"-"+c.getName();
    // Suppress duplicate warnings or attempts to store empty data
    if( val.equals("?") || val.equals(".") || ( warnings != null && warnings.contains(warnkey)) ) {
//      logger.debug(warning);
    } else {
//      logger.info(warning);
      logger.debug(warning);
    }

    if(warnings != null) {
      warnings.add(warnkey);
    }

  }


}

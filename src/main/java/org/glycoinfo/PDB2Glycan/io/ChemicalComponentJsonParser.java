package org.glycoinfo.PDB2Glycan.io;

import java.io.IOException;
import java.io.Reader;

import org.biojava.nbio.structure.io.mmcif.model.ChemCompAtom;
import org.biojava.nbio.structure.io.mmcif.model.ChemCompBond;
import org.biojava.nbio.structure.io.mmcif.model.PdbxChemCompIdentifier;
import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.model.ChemicalComponent;
import org.glycoinfo.PDB2Glycan.io.model.chem_comp.PdbxChemCompAtomRelated;
import org.glycoinfo.PDB2Glycan.io.model.chem_comp.PdbxChemCompRelated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

public class ChemicalComponentJsonParser extends AbstractMMjsonParser {

  private static final Logger logger = LoggerFactory.getLogger(ChemicalComponentJsonParser.class);

  public ChemicalComponentJsonParser(Reader in) {
    super(in);
  }

  public ChemicalComponentJsonParser(String content) {
    super(content);
  }

  public ChemicalComponent parse() throws IOException, MMcifParseException {
    JsonNode data = getData();

    String id = getData().get("chem_comp").get("id").get(0).asText();
    logger.debug("Parsing chemical component: " + id);

    ChemicalComponent cc = new ChemicalComponent();

    cc.setId(id);
    cc.setChemCompAtoms(parseCategory(ChemCompAtom.class, data));
    cc.setChemCompBonds(parseCategory(ChemCompBond.class, data));

    cc.setPdbxChemCompIdentifier(parseCategory(PdbxChemCompIdentifier.class, data));
    cc.setPdbxChemCompReleated(parseCategory(PdbxChemCompRelated.class, data));
    cc.setPdbxChemCompAtomReleated(parseCategory(PdbxChemCompAtomRelated.class, data));
    cc.setPdbxChemCompIdentifier(parseCategory(PdbxChemCompIdentifier.class, data));

    return cc;
  }

}

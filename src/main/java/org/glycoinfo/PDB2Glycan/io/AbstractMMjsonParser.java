package org.glycoinfo.PDB2Glycan.io;

import com.google.common.collect.Iterators;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import org.atteo.evo.inflector.English;
import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractMMjsonParser {

  private static final Logger logger = LoggerFactory.getLogger(AbstractMMjsonParser.class);

  private Reader in;
  private JsonNode data;

  public AbstractMMjsonParser(Reader in) {
    this.in = in;
  }

  public AbstractMMjsonParser(String content) {
    this.in = new StringReader(content);
  }

  protected JsonNode getData() throws IOException, MMcifParseException {
    if (data != null) {
      return data;
    }

    JsonNode json = new ObjectMapper().readTree(in);

    validateJson(json);

    String dataKey = Iterators.get(json.fields(), 0).getKey();

    data = json.get(dataKey);

    return data;
  }

  private void validateJson(JsonNode json) throws MMcifParseException {
    if (json == null) {
      throw new MMcifParseException("cannot parse null");
    }

    if (json.getNodeType() != JsonNodeType.OBJECT) {
      throw new MMcifParseException(
          "expect OBJECT for root element but got " + json.getNodeType());
    }

    int rootEntrySize = Iterators.size(json.fieldNames());
    if (rootEntrySize != 1) {
      throw new MMcifParseException(
          "expect only one entry on root object but got " + rootEntrySize);
    }
  }

  /**
   * Parses JSON data and builds objects as the given Class.<br>
   * Note: Category name is converted from the given Class name.
   * Therefore, the class name must be the camel case of the category name.
   * @param c Class for the object to build
   * @param data JSON data containing the field corresponding to the given class
   * @return
   * @throws MMcifParseException
   */
  protected static <T> List<T> parseCategory(Class<T> c, JsonNode data) throws MMcifParseException {
    // Get category name from the corresponding class name
    String categoryName = StringUtils.convertCaseCamelToSnake(c.getSimpleName());
    JsonNode node = data.get(categoryName);
    if (node == null) {
      logger.debug("No {} category.", categoryName);
      return Collections.emptyList();
    }

    List<T> ret = new ArrayList<>();
    Map<String, Iterator<JsonNode>> mapFieldNameToIter = getFieldIterators(node);

    // Get iterator for a field in the node
    Iterator<JsonNode> itr = mapFieldNameToIter.get(mapFieldNameToIter.keySet().iterator().next());
    Set<String> undefinedFields = new HashSet<>();
    while (itr.hasNext()) {
      try {
        T obj = c.getDeclaredConstructor().newInstance();
        for (String field : mapFieldNameToIter.keySet()) {
          String value = mapFieldNameToIter.get(field).next().asText();
          // Replace "null" to ""
          if ( value.equals("null") )
            value = "";
          if (undefinedFields.contains(field))
            continue;
          try {
            // Set parameter to field in the object directly
            Field f = c.getDeclaredField(field);
            f.setAccessible(true);
            f.set(obj, value);
          } catch (NoSuchFieldException e) {
            logger.debug("No {} field in {}.", field, c.getName());
            undefinedFields.add(field);
          }
        }
        ret.add(obj);
      } catch (IllegalAccessException | InstantiationException
             | IllegalArgumentException | InvocationTargetException
             | NoSuchMethodException | SecurityException e) {
        throw new MMcifParseException(e.getMessage());
      }
    }

    logger.debug("Parsed {} {}", ret.size(), English.plural(categoryName, ret.size()));
    return ret;
  }

  private static Map<String, Iterator<JsonNode>> getFieldIterators(JsonNode node) {
    Iterator<String> itrField = node.fieldNames();
    Map<String, Iterator<JsonNode>> mapNameToIter = new HashMap<>();
    while(itrField.hasNext()) {
      String field = itrField.next();
      Iterator<JsonNode> itrNode = node.get(field).iterator();
      mapNameToIter.put(field, itrNode);
    }
    return mapNameToIter;
  }
}

package org.glycoinfo.PDB2Glycan.io;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.biojava.nbio.structure.io.mmcif.model.PdbxPolySeqScheme;
import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.Citation;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.EntitySrcNat;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.Exptl;
import org.glycoinfo.PDB2Glycan.io.model.Protein;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxBranchScheme;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranch;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchDescriptor;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchLink;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchList;
import org.glycoinfo.PDB2Glycan.io.model.protein.AtomSite;
import org.glycoinfo.PDB2Glycan.io.model.protein.ChemComp;
import org.glycoinfo.PDB2Glycan.io.model.protein.EntityPoly;
import org.glycoinfo.PDB2Glycan.io.model.protein.Entry;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxNonpolyScheme;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructSheetHbond;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxUnobsOrZeroOccAtoms;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxValidateCloseContact;
import org.glycoinfo.PDB2Glycan.io.model.protein.Struct;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructAsym;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConf;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructRef;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructRefSeq;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetOrder;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetRange;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSite;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSiteGen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

public class PdbJsonParser extends AbstractMMjsonParser {

  private static final Logger logger = LoggerFactory.getLogger(PdbJsonParser.class);

  public PdbJsonParser(Reader in) {
    super(in);
  }

  public PdbJsonParser(String content) {
    super(content);
  }

  public Protein parse() throws IOException, MMcifParseException {
    JsonNode data = getData();

    String id = "none";
    if ( data.get("entry") != null )
      id = data.get("entry").get("id").get(0).asText();
    logger.debug("Parsing pdb entry: " + id);

    Protein protein = new Protein();

    protein.setEntry(parseCategory(Entry.class, data));

    protein.setAtomSites(parseAtomSite(data)); // use specific parser for checking PDB model number

    protein.setChemComps(parseCategory(ChemComp.class, data));
    protein.setEntityPolies(parseCategory(EntityPoly.class, data));
    protein.setPdbxStructModResidues(parseCategory(PdbxStructModResidue.class, data));
    protein.setStructAsyms(parseCategory(StructAsym.class, data));
    protein.setStructRefSeqs(parseCategory(StructRefSeq.class, data));
    protein.setStructConns(parseCategory(StructConn.class, data));

    protein.setStructs(parseCategory(Struct.class, data));
    protein.setCitations(parseCategory(Citation.class, data));
    protein.setEntitySrcNats(parseCategory(EntitySrcNat.class, data));
    protein.setExptls(parseCategory(Exptl.class, data));
    protein.setStructRefs(parseCategory(StructRef.class, data));
    protein.setStructSite(parseCategory(StructSite.class, data));
    protein.setStructSiteGen(parseCategory(StructSiteGen.class, data));
    protein.setStructSheetRange(parseCategory(StructSheetRange.class, data));
    protein.setStructSheetOrder(parseCategory(StructSheetOrder.class, data));
    protein.setPdbxStructSheetHbond(parseCategory(PdbxStructSheetHbond.class, data));
    protein.setStructConf(parseCategory(StructConf.class, data));
    protein.setPdbxValidateCloseContact(parseCategory(PdbxValidateCloseContact.class, data));

    protein.setPdbxPolySeqScheme(parseCategory(PdbxPolySeqScheme.class, data));
    protein.setPdbxNonpolyScheme(parseCategory(PdbxNonpolyScheme.class, data));
    protein.setPdbxBranchScheme(parseCategory(PdbxBranchScheme.class, data));

    protein.setPdbxEntityBranch(parseCategory(PdbxEntityBranch.class, data));
    protein.setPdbxEntityBranchList(parseCategory(PdbxEntityBranchList.class, data));
    protein.setPdbxEntityBranchLink(parseCategory(PdbxEntityBranchLink.class, data));
    protein.setPdbxEntityBranchDescriptor(parseCategory(PdbxEntityBranchDescriptor.class, data));

    protein.setPdbxUnobsOrZeroOccAtoms(parseCategory(PdbxUnobsOrZeroOccAtoms.class, data));

    correctUnobsWithShemes(protein);

    return protein;
  }

  private List<AtomSite> parseAtomSite(JsonNode data) throws MMcifParseException {
    List<AtomSite> ret = parseCategory(AtomSite.class, data);
    for ( AtomSite atomSite : ret) {
      if ( atomSite.getPdbx_PDB_model_num().length() <= 7 )
        continue;
      throw new MMcifParseException(String.format(
            "%s is too long for model number, expects 7 characters or less",
            atomSite.getPdbx_PDB_model_num()
         ));
    }

    return ret;
  }

  private void correctUnobsWithShemes(Protein protein) {
    if ( protein.getPdbxUnobsOrZeroOccAtoms() == null )
      return;

    // Residue info correction in unobs section using scheme sections
    for ( PdbxUnobsOrZeroOccAtoms unobs : protein.getPdbxUnobsOrZeroOccAtoms() ) {
      if ( protein.getPdbxNonpolyScheme() != null ) {
        for (PdbxNonpolyScheme scheme : protein.getPdbxNonpolyScheme()) {
          // Trust auth info than label info
          if ( unobs.getAuth_comp_id() != null
             && !unobs.getAuth_comp_id().equals(scheme.getAuth_mon_id()) )
            continue;
          if ( unobs.getAuth_seq_id() != null
             && !unobs.getAuth_seq_id().equals(scheme.getAuth_seq_num()) )
            continue;
          if ( unobs.getAuth_asym_id() != null
             && !unobs.getAuth_asym_id().equals(scheme.getPdb_strand_id()) )
            continue;

          // Correction
          if ( !unobs.getLabel_asym_id().equals(scheme.getAsym_id()) )
            unobs.setLabel_asym_id(scheme.getAsym_id());
          if ( !unobs.getLabel_comp_id().equals(scheme.getMon_id()) )
            unobs.setLabel_comp_id(scheme.getMon_id());
        }
      }

    // TODO: correct using PdbxBranchScheme if necessary
    }
  }

}

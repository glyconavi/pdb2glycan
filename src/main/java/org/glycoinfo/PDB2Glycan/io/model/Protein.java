package org.glycoinfo.PDB2Glycan.io.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.biojava.nbio.structure.io.mmcif.model.PdbxPolySeqScheme;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxBranchScheme;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranch;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchDescriptor;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchLink;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchList;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.Citation;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.EntitySrcNat;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.Exptl;
import org.glycoinfo.PDB2Glycan.io.model.protein.AtomSite;
import org.glycoinfo.PDB2Glycan.io.model.protein.ChemComp;
import org.glycoinfo.PDB2Glycan.io.model.protein.EntityPoly;
import org.glycoinfo.PDB2Glycan.io.model.protein.Entry;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxNonpolyScheme;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructSheetHbond;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxUnobsOrZeroOccAtoms;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxValidateCloseContact;
import org.glycoinfo.PDB2Glycan.io.model.protein.Struct;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructAsym;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConf;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructRef;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructRefSeq;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetOrder;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetRange;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSite;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSiteGen;
import org.glycoinfo.PDB2Glycan.structure.Proteindb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Protein {

  private static final Logger logger = LoggerFactory.getLogger(Protein.class);

  private List<Entry> entries;

  private List<AtomSite> atomSites;
  private List<ChemComp> chemComps;
  private List<EntityPoly> entityPolies;
  private List<PdbxStructModResidue> pdbxStructModResidues;
  private List<StructAsym> structAsyms;
  private List<StructConn> structConns;
  private List<StructRefSeq> structRefSeqs;
  private List<Struct> struct; // assumed only one record
  private List<Citation> citations;
  private List<EntitySrcNat> entitySrcNats;
  private List<Exptl> exptls;
  private List<StructRef> structRefs;
  private List<StructSite> structSite;
  private List<StructSiteGen> structSiteGen;
  private List<StructSheetRange> structSheetRange;
  private List<StructSheetOrder> structSheetOrder;
  private List<PdbxStructSheetHbond> pdbxStructSheetHbond;
  private List<StructConf> structConf;
  private List<PdbxValidateCloseContact> pdbxValidateCloseContact;
  private List<PdbxUnobsOrZeroOccAtoms> pdbxUnobsOrZeroOccAtoms;

  private List<PdbxPolySeqScheme> pdbxPolySeqScheme;
  private List<PdbxNonpolyScheme> pdbxNonpolyScheme;
  private List<PdbxBranchScheme> pdbxBranchScheme;

  private List<PdbxEntityBranch> pdbxEntityBranch;
  private List<PdbxEntityBranchLink> pdbxEntityBranchLink;
  private List<PdbxEntityBranchList> pdbxEntityBranchList;
  private List<PdbxEntityBranchDescriptor> pdbxEntityBranchDescriptor;

  private HashMap<String, ArrayList<AtomSite>> glycoAtomSiteSet; // added. 20.Dec.2022.


  @JsonIgnore
  public List<PdbxEntityBranch> getPdbxEntityBranch() {
    return pdbxEntityBranch;
  }

  public void setPdbxEntityBranch(List<PdbxEntityBranch> pdbxEntityBranch) {
    this.pdbxEntityBranch = pdbxEntityBranch;
  }

  @JsonIgnore
  public List<PdbxEntityBranchLink> getPdbxEntityBranchLink() {
    return pdbxEntityBranchLink;
  }

  public void setPdbxEntityBranchLink(List<PdbxEntityBranchLink> pdbxEntityBranchLink) {
    this.pdbxEntityBranchLink = pdbxEntityBranchLink;
  }

  @JsonIgnore
  public List<PdbxEntityBranchList> getPdbxEntityBranchList() {
    return pdbxEntityBranchList;
  }

  public void setPdbxEntityBranchList(List<PdbxEntityBranchList> pdbxEntityBranchList) {
    this.pdbxEntityBranchList = pdbxEntityBranchList;
  }

  @JsonIgnore
  public List<PdbxEntityBranchDescriptor> getPdbxEntityBranchDescriptor() {
    return pdbxEntityBranchDescriptor;
  }

  public void setPdbxEntityBranchDescriptor(List<PdbxEntityBranchDescriptor> pdbxEntityBranchDescriptor) {
    this.pdbxEntityBranchDescriptor = pdbxEntityBranchDescriptor;
  }

  @JsonIgnore
  public List<PdbxPolySeqScheme> getPdbxPolySeqScheme() {
    return pdbxPolySeqScheme;
  }

  public void setPdbxPolySeqScheme(List<PdbxPolySeqScheme> pdbxPolySeqScheme) {
    this.pdbxPolySeqScheme = pdbxPolySeqScheme;
  }

  @JsonIgnore
  public List<PdbxNonpolyScheme> getPdbxNonpolyScheme() {
    return pdbxNonpolyScheme;
  }

  public void setPdbxNonpolyScheme(List<PdbxNonpolyScheme> pdbxNonpolyScheme) {
    this.pdbxNonpolyScheme = pdbxNonpolyScheme;
  }

  @JsonIgnore
  public List<PdbxBranchScheme> getPdbxBranchScheme() {
    return pdbxBranchScheme;
  }

  public void setPdbxBranchScheme(List<PdbxBranchScheme> pdbxBranchScheme) {
    this.pdbxBranchScheme = pdbxBranchScheme;
  }

  @JsonIgnore
  public List<PdbxUnobsOrZeroOccAtoms> getPdbxUnobsOrZeroOccAtoms() {
    return pdbxUnobsOrZeroOccAtoms;
  }

  public void setPdbxUnobsOrZeroOccAtoms(List<PdbxUnobsOrZeroOccAtoms> pdbxUnobsOrZeroOccAtoms) {
    this.pdbxUnobsOrZeroOccAtoms = pdbxUnobsOrZeroOccAtoms;
  }

  public List<PdbxValidateCloseContact> getPdbxValidateCloseContact() {
    return pdbxValidateCloseContact;
  }

  public void setPdbxValidateCloseContact(List<PdbxValidateCloseContact> pdbxValidateCloseContact) {
    this.pdbxValidateCloseContact = pdbxValidateCloseContact;
  }

  public List<StructConf> getStructConf() {
    return structConf;
  }

  public void setStructConf(List<StructConf> structConf) {
    this.structConf = structConf;
  }

  public List<StructSite> getStructSite() {
    return structSite;
  }

  public void setStructSite(List<StructSite> structSite) {
    this.structSite = structSite;
  }

  public List<PdbxStructSheetHbond> getPdbxStructSheetHbond() {
    return pdbxStructSheetHbond;
  }

  public void setPdbxStructSheetHbond(List<PdbxStructSheetHbond> pdbxStructSheetHbond) {
    this.pdbxStructSheetHbond = pdbxStructSheetHbond;
  }

  public List<StructSheetOrder> getStructSheetOrder() {
    return structSheetOrder;
  }

  public void setStructSheetOrder(List<StructSheetOrder> structSheetOrder) {
    this.structSheetOrder = structSheetOrder;
  }

  public List<StructSheetRange> getStructSheetRange() {
    return structSheetRange;
  }

  public void setStructSheetRange(List<StructSheetRange> structSheetRange) {
    this.structSheetRange = structSheetRange;
  }

  public List<EntitySrcNat> getEntitySrcNats() {
    return entitySrcNats;
  }

  public void setEntitySrcNats(List<EntitySrcNat> entitySrcNats) {
    this.entitySrcNats = entitySrcNats;
  }

  public List<StructRef> getStructRefs() {
    return structRefs;
  }

  public void setStructRefs(List<StructRef> structRefs) {
    this.structRefs = structRefs;
  }

  public List<StructSiteGen> getStructSiteGen() {
    return structSiteGen;
  }

  public void setStructSiteGen(List<StructSiteGen> structSiteGen) {
    this.structSiteGen = structSiteGen;
  }

  public List<Exptl> getExptls() {
    return exptls;
  }

  public void setExptls(List<Exptl> exptls) {
    this.exptls = exptls;
  }

  public String getPdbCode() {
    if (entries == null || entries.isEmpty() || entries.get(0) == null) {
      return "????";
    } else {
      return entries.get(0).getId();
    }
  }

  public List<Entry> getEntry() {
    return entries;
  }

  public void setEntry(List<Entry> entries) {
    this.entries = entries;
  }

  @JsonIgnore
  public List<Citation> getCitations() {
    return citations;
  }

  public void setCitations(List<Citation> citations) {
    this.citations = citations;
  }

  @JsonIgnore
  public List<Struct> getStructs() {
    return struct;
  }

  public void setStructs(List<Struct> s) {
    this.struct = s;
  }

  @JsonProperty("atom_site")
  public List<AtomSite> getAtomSites() {
    return atomSites;
  }

  public void setAtomSites(List<AtomSite> atomSites) {
    this.atomSites = atomSites;
  }

  @JsonIgnore
  public List<StructAsym> getStructAsyms() {
    return structAsyms;
  }

  public void setStructAsyms(List<StructAsym> structAsyms) {
    this.structAsyms = structAsyms;
  }

  @JsonIgnore
  public List<EntityPoly> getEntityPolies() {
    return entityPolies;
  }

  public void setEntityPolies(List<EntityPoly> entityPolies) {
    this.entityPolies = entityPolies;
  }

  @JsonIgnore
  public List<StructRefSeq> getStructRefSeqs() {
    return structRefSeqs;
  }

  public void setStructRefSeqs(List<StructRefSeq> structRefSeqs) {
    this.structRefSeqs = structRefSeqs;
  }

  @JsonIgnore
  public List<ChemComp> getChemComps() {
    return chemComps;
  }

  public void setChemComps(List<ChemComp> chemComps) {
    this.chemComps = chemComps;
  }

  @JsonIgnore
  public List<PdbxStructModResidue> getPdbxStructModResidues() {
    return pdbxStructModResidues;
  }

  public void setPdbxStructModResidues(List<PdbxStructModResidue> pdbxStructModResidues) {
    this.pdbxStructModResidues = pdbxStructModResidues;
  }

  @JsonIgnore
  public List<StructConn> getStructConns() {
    return structConns;
  }

  public void setStructConns(List<StructConn> structConns) {
    this.structConns = structConns;
  }

  public List<PdbxStructModResidue> arPdbxStructModResidue() {
    return pdbxStructModResidues.stream().filter(x -> x.getDetails().equals("GLYCOSYLATION SITE"))
        .collect(Collectors.toList());
  }

  private List<Proteindb> proteinDb;
//  private Map<String, Set<String>> connected_asym;

  public List<Proteindb> proteinDb() {
    if (proteinDb != null && !proteinDb.isEmpty()) {
      return proteinDb;
    }

    proteinDb = new ArrayList<>();

    for (StructRefSeq srs : structRefSeqs) {
      Proteindb pd = new Proteindb();

      pd.setId(srs.getPdbx_db_accession());
      pd.setStrand_id(srs.getPdbx_strand_id());

      for (StructRef sr : structRefs) {
        if ( sr.getPdbx_db_accession().equals(srs.getPdbx_db_accession()) )
          pd.setDb_name(sr.getDb_name());
      }
      if ( pd.getDb_name() == null )
        logger.warn("Corresponding db_name to the pdbx_db_accession \"{}\" cannot be found.", pd.getId());

      for (EntityPoly ep : entityPolies) {
        if (ep.getPdbx_strand_id().contains(srs.getPdbx_strand_id())) {
          pd.setEntity_id(ep.getEntity_id());
          pd.setType(ep.getType());
          pd.setPdbx_seq_one_letter_code_can(ep.getPdbx_seq_one_letter_code_can());
        }
      }


      proteinDb.add(pd);
    }

    return proteinDb;
  }

/*
  private void structConn() {
    connected_asym = new HashMap<>();

    for (StructConn sc : structConns) {
      String s1 = sc.getPtnr1_label_asym_id();
      String s2 = sc.getPtnr2_label_asym_id();

      connected_asym.putIfAbsent(s1, new HashSet<>());
      connected_asym.putIfAbsent(s2, new HashSet<>());

      connected_asym.get(s1).add(s2);
      connected_asym.get(s2).add(s1);
    }

  }
*/
  public Map<String, Set<String>> entityToStructAsym() {
    Map<String, Set<String>> map = new HashMap<>();

    for (StructAsym sa : structAsyms) {
      map.putIfAbsent(sa.getEntity_id(), new HashSet<>());
      map.get(sa.getEntity_id()).add(sa.getId());
    }

    return map;
  }

  // added. 20.Dec.2022.
  public HashMap<String, ArrayList<AtomSite>> getGlycoAtomSiteSet() {
	  return this.glycoAtomSiteSet;
  }
  // added. 20.Dec.2022.
  public void setGlycoAtomSiteSet(HashMap<String, ArrayList<AtomSite>> glycoAtomSiteSet) {
	  this.glycoAtomSiteSet = glycoAtomSiteSet;
  }
}

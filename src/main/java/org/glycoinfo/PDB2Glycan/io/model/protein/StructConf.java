package org.glycoinfo.PDB2Glycan.io.model.protein;

import org.glycoinfo.PDB2Glycan.structure.StructureRange;

public class StructConf extends StructureRange{
	private String id="";
	private String conf_type_id="";
	private String pdbx_PDB_helix_id="";
	private String pdbx_PDB_helix_class="";
	private String pdbx_PDB_helix_length="";
	private String beg_label_comp_id="";
	private String beg_label_asym_id="";
	private String beg_label_seq_id="";
	private String end_label_comp_id="";
	private String end_label_asym_id="";
	private String end_label_seq_id="";
	private String details="";
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getConf_type_id() {
		return conf_type_id;
	}
	public void setConf_type_id(String conf_type_id) {
		this.conf_type_id = conf_type_id;
	}
	public String getPdbx_PDB_helix_id() {
		return pdbx_PDB_helix_id;
	}
	public void setPdbx_PDB_helix_id(String pdbx_PDB_helix_id) {
		this.pdbx_PDB_helix_id = pdbx_PDB_helix_id;
	}
	public String getPdbx_PDB_helix_class() {
		return pdbx_PDB_helix_class;
	}
	public void setPdbx_PDB_helix_class(String pdbx_PDB_helix_class) {
		this.pdbx_PDB_helix_class = pdbx_PDB_helix_class;
	}
	public String getPdbx_PDB_helix_length() {
		return pdbx_PDB_helix_length;
	}
	public void setPdbx_PDB_helix_length(String pdbx_PDB_helix_length) {
		this.pdbx_PDB_helix_length = pdbx_PDB_helix_length;
	}
	public String getBeg_label_comp_id() {
		return beg_label_comp_id;
	}
	public void setBeg_label_comp_id(String beg_label_comp_id) {
		this.beg_label_comp_id = beg_label_comp_id;
	}
	public String getBeg_label_asym_id() {
		return beg_label_asym_id;
	}
	public void setBeg_label_asym_id(String beg_label_asym_id) {
		this.beg_label_asym_id = beg_label_asym_id;
	}
	public String getBeg_label_seq_id() {
		return beg_label_seq_id;
	}
	public void setBeg_label_seq_id(String beg_label_seq_id) {
		this.beg_label_seq_id = beg_label_seq_id;
	}
	public String getEnd_label_comp_id() {
		return end_label_comp_id;
	}
	public void setEnd_label_comp_id(String end_label_comp_id) {
		this.end_label_comp_id = end_label_comp_id;
	}
	public String getEnd_label_asym_id() {
		return end_label_asym_id;
	}
	public void setEnd_label_asym_id(String end_label_asym_id) {
		this.end_label_asym_id = end_label_asym_id;
	}
	public String getEnd_label_seq_id() {
		return end_label_seq_id;
	}
	public void setEnd_label_seq_id(String end_label_seq_id) {
		this.end_label_seq_id = end_label_seq_id;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	
}
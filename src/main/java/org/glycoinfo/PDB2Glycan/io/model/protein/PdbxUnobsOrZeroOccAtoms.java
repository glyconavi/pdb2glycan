package org.glycoinfo.PDB2Glycan.io.model.protein;

public class PdbxUnobsOrZeroOccAtoms {
  private String id;
  private String PDB_model_num;
  private String polymer_flag;
  private String occupancy_flag;
  private String auth_asym_id;
  private String auth_comp_id;
  private String auth_seq_id;
  private String PDB_ins_code;
  private String auth_atom_id;
  private String label_alt_id;
  private String label_asym_id;
  private String label_comp_id;
  private String label_seq_id;
  private String label_atom_id;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPDB_model_num() {
    return PDB_model_num;
  }

  public void setPDB_model_num(String pDB_model_num) {
    PDB_model_num = pDB_model_num;
  }

  public String getPolymer_flag() {
    return polymer_flag;
  }

  public void setPolymer_flag(String polymer_flag) {
    this.polymer_flag = polymer_flag;
  }

  public String getOccupancy_flag() {
    return occupancy_flag;
  }

  public void setOccupancy_flag(String occupancy_flag) {
    this.occupancy_flag = occupancy_flag;
  }

  public String getAuth_asym_id() {
    return auth_asym_id;
  }

  public void setAuth_asym_id(String auth_asym_id) {
    this.auth_asym_id = auth_asym_id;
  }

  public String getAuth_comp_id() {
    return auth_comp_id;
  }

  public void setAuth_comp_id(String auth_comp_id) {
    this.auth_comp_id = auth_comp_id;
  }

  public String getAuth_seq_id() {
    return auth_seq_id;
  }

  public void setAuth_seq_id(String auth_seq_id) {
    this.auth_seq_id = auth_seq_id;
  }

  public String getPDB_ins_code() {
    return PDB_ins_code;
  }

  public void setPDB_ins_code(String pDB_ins_code) {
    PDB_ins_code = pDB_ins_code;
  }

  public String getAuth_atom_id() {
    return auth_atom_id;
  }

  public void setAuth_atom_id(String auth_atom_id) {
    this.auth_atom_id = auth_atom_id;
  }

  public String getLabel_alt_id() {
    return label_alt_id;
  }

  public void setLabel_alt_id(String label_alt_id) {
    this.label_alt_id = label_alt_id;
  }

  public String getLabel_asym_id() {
    return label_asym_id;
  }

  public void setLabel_asym_id(String label_asym_id) {
    this.label_asym_id = label_asym_id;
  }

  public String getLabel_comp_id() {
    return label_comp_id;
  }

  public void setLabel_comp_id(String label_comp_id) {
    this.label_comp_id = label_comp_id;
  }

  public String getLabel_seq_id() {
    return label_seq_id;
  }

  public void setLabel_seq_id(String label_seq_id) {
    this.label_seq_id = label_seq_id;
  }

  public String getLabel_atom_id() {
    return label_atom_id;
  }

  public void setLabel_atom_id(String label_atom_id) {
    this.label_atom_id = label_atom_id;
  }
}

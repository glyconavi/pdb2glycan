package org.glycoinfo.PDB2Glycan.io.model.info;

public class SoftwareInfo {

  private String name;
  private String version;
  private long process_time;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public long getProcess_time() {
    return process_time;
  }

  public void setProcess_time(long process_time) {
    this.process_time = process_time;
  }

}

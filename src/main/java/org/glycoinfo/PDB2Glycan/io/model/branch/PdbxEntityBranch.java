package org.glycoinfo.PDB2Glycan.io.model.branch;

public class PdbxEntityBranch {

  private String entity_id;
  private String type;

  public String getEntity_id() {
    return entity_id;
  }

  public void setEntity_id(String entity_id) {
    this.entity_id = entity_id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

}

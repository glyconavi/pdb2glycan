package org.glycoinfo.PDB2Glycan.io.model.protein;

public class PdbxStructSheetHbond{
	private String sheet_id="";
	private String range_id_1="";
	private String range_id_2="";
	private String range_1_label_atom_id="";
	private String range_1_label_comp_id="";
	private String range_1_label_asym_id="";
	private String range_1_label_seq_id="";
	private String range_1_PDB_ins_code="";
	private String range_1_auth_atom_id="";
	private String range_1_auth_comp_id="";
	private String range_1_auth_asym_id="";
	private String range_1_auth_seq_id="";
	private String range_2_label_atom_id="";
	private String range_2_label_comp_id="";
	private String range_2_label_asym_id="";
	private String range_2_label_seq_id="";
	private String range_2_PDB_ins_code="";
	private String range_2_auth_atom_id="";
	private String range_2_auth_comp_id="";
	private String range_2_auth_asym_id="";
	private String range_2_auth_seq_id="";
	public String getSheet_id() {
		return sheet_id;
	}
	public void setSheet_id(String sheet_id) {
		this.sheet_id = sheet_id;
	}
	public String getRange_id_1() {
		return range_id_1;
	}
	public void setRange_id_1(String range_id_1) {
		this.range_id_1 = range_id_1;
	}
	public String getRange_id_2() {
		return range_id_2;
	}
	public void setRange_id_2(String range_id_2) {
		this.range_id_2 = range_id_2;
	}
	public String getRange_1_label_atom_id() {
		return range_1_label_atom_id;
	}
	public void setRange_1_label_atom_id(String range_1_label_atom_id) {
		this.range_1_label_atom_id = range_1_label_atom_id;
	}
	public String getRange_1_label_comp_id() {
		return range_1_label_comp_id;
	}
	public void setRange_1_label_comp_id(String range_1_label_comp_id) {
		this.range_1_label_comp_id = range_1_label_comp_id;
	}
	public String getRange_1_label_asym_id() {
		return range_1_label_asym_id;
	}
	public void setRange_1_label_asym_id(String range_1_label_asym_id) {
		this.range_1_label_asym_id = range_1_label_asym_id;
	}
	public String getRange_1_label_seq_id() {
		return range_1_label_seq_id;
	}
	public void setRange_1_label_seq_id(String range_1_label_seq_id) {
		this.range_1_label_seq_id = range_1_label_seq_id;
	}
	public String getRange_1_PDB_ins_code() {
		return range_1_PDB_ins_code;
	}
	public void setRange_1_PDB_ins_code(String range_1_PDB_ins_code) {
		this.range_1_PDB_ins_code = range_1_PDB_ins_code;
	}
	public String getRange_1_auth_atom_id() {
		return range_1_auth_atom_id;
	}
	public void setRange_1_auth_atom_id(String range_1_auth_atom_id) {
		this.range_1_auth_atom_id = range_1_auth_atom_id;
	}
	public String getRange_1_auth_comp_id() {
		return range_1_auth_comp_id;
	}
	public void setRange_1_auth_comp_id(String range_1_auth_comp_id) {
		this.range_1_auth_comp_id = range_1_auth_comp_id;
	}
	public String getRange_1_auth_asym_id() {
		return range_1_auth_asym_id;
	}
	public void setRange_1_auth_asym_id(String range_1_auth_asym_id) {
		this.range_1_auth_asym_id = range_1_auth_asym_id;
	}
	public String getRange_1_auth_seq_id() {
		return range_1_auth_seq_id;
	}
	public void setRange_1_auth_seq_id(String range_1_auth_seq_id) {
		this.range_1_auth_seq_id = range_1_auth_seq_id;
	}
	public String getRange_2_label_atom_id() {
		return range_2_label_atom_id;
	}
	public void setRange_2_label_atom_id(String range_2_label_atom_id) {
		this.range_2_label_atom_id = range_2_label_atom_id;
	}
	public String getRange_2_label_comp_id() {
		return range_2_label_comp_id;
	}
	public void setRange_2_label_comp_id(String range_2_label_comp_id) {
		this.range_2_label_comp_id = range_2_label_comp_id;
	}
	public String getRange_2_label_asym_id() {
		return range_2_label_asym_id;
	}
	public void setRange_2_label_asym_id(String range_2_label_asym_id) {
		this.range_2_label_asym_id = range_2_label_asym_id;
	}
	public String getRange_2_label_seq_id() {
		return range_2_label_seq_id;
	}
	public void setRange_2_label_seq_id(String range_2_label_seq_id) {
		this.range_2_label_seq_id = range_2_label_seq_id;
	}
	public String getRange_2_PDB_ins_code() {
		return range_2_PDB_ins_code;
	}
	public void setRange_2_PDB_ins_code(String range_2_PDB_ins_code) {
		this.range_2_PDB_ins_code = range_2_PDB_ins_code;
	}
	public String getRange_2_auth_atom_id() {
		return range_2_auth_atom_id;
	}
	public void setRange_2_auth_atom_id(String range_2_auth_atom_id) {
		this.range_2_auth_atom_id = range_2_auth_atom_id;
	}
	public String getRange_2_auth_comp_id() {
		return range_2_auth_comp_id;
	}
	public void setRange_2_auth_comp_id(String range_2_auth_comp_id) {
		this.range_2_auth_comp_id = range_2_auth_comp_id;
	}
	public String getRange_2_auth_asym_id() {
		return range_2_auth_asym_id;
	}
	public void setRange_2_auth_asym_id(String range_2_auth_asym_id) {
		this.range_2_auth_asym_id = range_2_auth_asym_id;
	}
	public String getRange_2_auth_seq_id() {
		return range_2_auth_seq_id;
	}
	public void setRange_2_auth_seq_id(String range_2_auth_seq_id) {
		this.range_2_auth_seq_id = range_2_auth_seq_id;
	}
	
}
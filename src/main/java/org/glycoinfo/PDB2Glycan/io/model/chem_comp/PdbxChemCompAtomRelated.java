package org.glycoinfo.PDB2Glycan.io.model.chem_comp;

public class PdbxChemCompAtomRelated {

  private String ordinal;
  private String comp_id;
  private String atom_id;
  private String related_comp_id;
  private String related_atom_id;
  private String related_type;

  public String getOrdinal() {
    return ordinal;
  }

  public void setOrdinal(String ordinal) {
    this.ordinal = ordinal;
  }

  public String getComp_id() {
    return comp_id;
  }

  public void setComp_id(String comp_id) {
    this.comp_id = comp_id;
  }

  public String getAtom_id() {
    return atom_id;
  }

  public void setAtom_id(String atom_id) {
    this.atom_id = atom_id;
  }

  public String getRelated_comp_id() {
    return related_comp_id;
  }

  public void setRelated_comp_id(String related_comp_id) {
    this.related_comp_id = related_comp_id;
  }

  public String getRelated_atom_id() {
    return related_atom_id;
  }

  public void setRelated_atom_id(String related_atom_id) {
    this.related_atom_id = related_atom_id;
  }

  public String getRelated_type() {
    return related_type;
  }

  public void setRelated_type(String related_type) {
    this.related_type = related_type;
  }
}

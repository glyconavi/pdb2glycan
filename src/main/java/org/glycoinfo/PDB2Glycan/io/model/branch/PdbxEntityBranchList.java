package org.glycoinfo.PDB2Glycan.io.model.branch;

public class PdbxEntityBranchList {

  private String entity_id;
  private String comp_id;
  private String num;
  private String hetero;

  public String getEntity_id() {
    return entity_id;
  }

  public void setEntity_id(String entity_id) {
    this.entity_id = entity_id;
  }

  public String getComp_id() {
    return comp_id;
  }

  public void setComp_id(String comp_id) {
    this.comp_id = comp_id;
  }

  public String getNum() {
    return num;
  }

  public void setNum(String num) {
    this.num = num;
  }

  public String getHetero() {
    return hetero;
  }

  public void setHetero(String hetero) {
    this.hetero = hetero;
  }

}

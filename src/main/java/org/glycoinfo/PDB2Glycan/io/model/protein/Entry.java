package org.glycoinfo.PDB2Glycan.io.model.protein;

public class Entry {

  private String id;
  // ISK
  private String title;
  private String descriptor;

  public String getId() {
    return id;
  }

  // id
  public void setId(String id) {
    this.id = id;
  }

//    public String getId() {
//    return id;
//  }

  // ISK
  // title
  public void setTitle(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  // ISK
  // descriptor
  public void setDescriptor(String descriptor) {
    this.descriptor = descriptor;
  }

  public String getDescriptor() {
    return descriptor;
  }

}

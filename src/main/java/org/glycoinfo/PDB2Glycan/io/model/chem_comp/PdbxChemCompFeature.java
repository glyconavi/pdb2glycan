package org.glycoinfo.PDB2Glycan.io.model.chem_comp;

public class PdbxChemCompFeature {

  private String comp_id;
  private String type;
  private String value;
  private String source;
  private String support;

  public String getComp_id() {
    return comp_id;
  }

  public void setComp_id(String comp_id) {
    this.comp_id = comp_id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getSupport() {
    return support;
  }

  public void setSupport(String support) {
    this.support = support;
  }
}

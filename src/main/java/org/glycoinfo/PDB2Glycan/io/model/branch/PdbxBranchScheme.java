package org.glycoinfo.PDB2Glycan.io.model.branch;

public class PdbxBranchScheme {

  private String asym_id;
  private String entity_id;
  private String mon_id;
  private String num;
  private String pdb_asym_id;
  private String pdb_mon_id;
  private String pdb_seq_num;
  private String auth_asym_id;
  private String auth_mon_id;
  private String auth_seq_num;
  private String hetero;

  public String getAsym_id() {
    return asym_id;
  }

  public void setAsym_id(String asym_id) {
    this.asym_id = asym_id;
  }

  public String getEntity_id() {
    return entity_id;
  }

  public void setEntity_id(String entity_id) {
    this.entity_id = entity_id;
  }

  public String getMon_id() {
    return mon_id;
  }

  public void setMon_id(String mon_id) {
    this.mon_id = mon_id;
  }

  public String getNum() {
    return num;
  }

  public void setNum(String num) {
    this.num = num;
  }

  public String getPdb_asym_id() {
    return pdb_asym_id;
  }

  public void setPdb_asym_id(String pdb_asym_id) {
    this.pdb_asym_id = pdb_asym_id;
  }

  public String getPdb_mon_id() {
    return pdb_mon_id;
  }

  public void setPdb_mon_id(String pdb_mon_id) {
    this.pdb_mon_id = pdb_mon_id;
  }

  public String getPdb_seq_num() {
    return pdb_seq_num;
  }

  public void setPdb_seq_num(String pdb_seq_num) {
    this.pdb_seq_num = pdb_seq_num;
  }

  public String getAuth_asym_id() {
    return auth_asym_id;
  }

  public void setAuth_asym_id(String auth_asym_id) {
    this.auth_asym_id = auth_asym_id;
  }

  public String getAuth_mon_id() {
    return auth_mon_id;
  }

  public void setAuth_mon_id(String auth_mon_id) {
    this.auth_mon_id = auth_mon_id;
  }

  public String getAuth_seq_num() {
    return auth_seq_num;
  }

  public void setAuth_seq_num(String auth_seq_num) {
    this.auth_seq_num = auth_seq_num;
  }

  public String getHetero() {
    return hetero;
  }

  public void setHetero(String hetero) {
    this.hetero = hetero;
  }

}

package org.glycoinfo.PDB2Glycan.io.model.exinfo;

public class EntitySrcNat {
	
	public String entity_id ="";
	public String pdbx_src_id ="";
	public String common_name ="";
	public String pdbx_organism_scientific ="";
	public String pdbx_ncbi_taxonomy_id ="";
	public String genus ="";
	public String species ="";
	public String tissue ="";
	public String strain ="";
	public String pdbx_cell_line ="";
	
	
	public String getEntity_id() {
		return entity_id;
	}
	public void setEntity_id(String entity_id) {
		this.entity_id = entity_id;
	}
	public String getPdbx_src_id() {
		return pdbx_src_id;
	}
	public void setPdbx_src_id(String pdbx_src_id) {
		this.pdbx_src_id = pdbx_src_id;
	}
	public String getCommon_name() {
		return common_name;
	}
	public void setCommon_name(String common_name) {
		this.common_name = common_name;
	}
	public String getPdbx_organism_scientific() {
		return pdbx_organism_scientific;
	}
	public void setPdbx_organism_scientific(String pdbx_organism_scientific) {
		this.pdbx_organism_scientific = pdbx_organism_scientific;
	}
	public String getPdbx_ncbi_taxonomy_id() {
		return pdbx_ncbi_taxonomy_id;
	}
	public void setPdbx_ncbi_taxonomy_id(String pdbx_ncbi_taxonomy_id) {
		this.pdbx_ncbi_taxonomy_id = pdbx_ncbi_taxonomy_id;
	}
	public String getGenus() {
		return genus;
	}
	public void setGenus(String genus) {
		this.genus = genus;
	}
	public String getSpecies() {
		return species;
	}
	public void setSpecies(String species) {
		this.species = species;
	}
	public String getTissue() {
		return tissue;
	}
	public void setTissue(String tissue) {
		this.tissue = tissue;
	}
	public String getStrain() {
		return strain;
	}
	public void setStrain(String strain) {
		this.strain = strain;
	}
	public String getPdbx_cell_line() {
		return pdbx_cell_line;
	}
	public void setPdbx_cell_line(String pdbx_cell_line) {
		this.pdbx_cell_line = pdbx_cell_line;
	}
	
	
	
}

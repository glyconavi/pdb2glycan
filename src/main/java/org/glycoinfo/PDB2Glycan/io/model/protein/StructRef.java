package org.glycoinfo.PDB2Glycan.io.model.protein;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StructRef {
	
	@JsonProperty("db_name")
	public String db_name = "";
	
	@JsonProperty("db_code")
	public String db_code = "";

	@JsonProperty("pdbx_db_isoform")
	public String pdbx_db_isoform = "";

	@JsonProperty("pdbx_db_accession")
	public String pdbx_db_accession = "";

	@JsonIgnore
	public String id = "";
		
	@JsonIgnore
	public String getDb_name() {
		return db_name;
	}
	public void setDb_name(String db_name) {
		this.db_name = db_name;
	}

	@JsonIgnore
	public String getId() {
		return id;
	}
	public void setId(String i) {
		this.id = i;
	}

	@JsonIgnore
	public String getDb_code() {
		return db_code;
	}
	public void setDb_code(String db_code) {
		this.db_code = db_code;
	}

	@JsonIgnore
	public String getPdbx_db_isoform() {
		return pdbx_db_isoform;
	}
	public void setPdbx_db_isoform(String pdbx_db_isoform) {
		this.pdbx_db_isoform = pdbx_db_isoform;
	}
	
	@JsonIgnore
	public String getPdbx_db_accession() {
		return pdbx_db_accession;
	}
	public void setPdbx_db_accession(String pdbx_db_accession) {
		this.pdbx_db_accession = pdbx_db_accession;
	}
}



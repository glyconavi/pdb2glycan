package org.glycoinfo.PDB2Glycan.io.model.protein;

public class StructSheetOrder{
	private String sheet_id="";
	private String range_id_1="";
	private String range_id_2="";
	private String offset="";
	private String sense="";
	public String getSheet_id() {
		return sheet_id;
	}
	public void setSheet_id(String sheet_id) {
		this.sheet_id = sheet_id;
	}
	public String getRange_id_1() {
		return range_id_1;
	}
	public void setRange_id_1(String range_id_1) {
		this.range_id_1 = range_id_1;
	}
	public String getRange_id_2() {
		return range_id_2;
	}
	public void setRange_id_2(String range_id_2) {
		this.range_id_2 = range_id_2;
	}
	public String getOffset() {
		return offset;
	}
	public void setOffset(String offset) {
		this.offset = offset;
	}
	public String getSense() {
		return sense;
	}
	public void setSense(String sense) {
		this.sense = sense;
	}
	
}


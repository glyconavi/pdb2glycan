package org.glycoinfo.PDB2Glycan.io.model.protein;

import org.glycoinfo.PDB2Glycan.structure.Atom;

public class PdbxValidateCloseContact{
	/*
	  http://mmcif.pdb.org/dictionaries/mmcif_pdbx_v5_rc.dic/Categories/pdbx_validate_close_contact.html
	  Data items in the PDBX_VALIDATE_CLOSE_CONTACT category list the
    atoms within the entry that are in close contact with regard
    the distances expected from either covalent bonding or closest
    approach by van der Waals contacts. Contacts within
    the asymmetric unit are considered.

    For those contacts not involving hydrogen a limit of
    2.2 Angstroms is used. For contacts involving a hydrogen atom 
    a cutoff of 1.6 Angstroms is used.
    
    Asym 内のものしか見ないので、auth のデータしかないのだろうか？
 */
	private String id="";
	private String PDB_model_num="";
	private String auth_atom_id_1="";
	private String auth_asym_id_1="";
	private String auth_comp_id_1="";
	private String auth_seq_id_1="";
	private String PDB_ins_code_1="";
	private String label_alt_id_1="";
	private String auth_atom_id_2="";
	private String auth_asym_id_2="";
	private String auth_comp_id_2="";
	private String auth_seq_id_2="";
	private String PDB_ins_code_2="";
	private String label_alt_id_2="";
	private String dist="";
	
	
	
	public boolean isAtom1(Atom a) {
		if(!this.auth_atom_id_1.equals(a.getAtom_site_auth_atom_id())) {
			return false;
		}
		if(!this.auth_asym_id_1.equals(a.getAtom_site_auth_asym_id())) {
			return false;
		}
		if(!this.auth_comp_id_1.equals(a.getAtom_site_auth_comp_id())) {
			return false;
		}
		if(!this.auth_seq_id_1.equals(a.getAtom_site_auth_seq_id())) {
			return false;
		}
    return this.PDB_ins_code_1.equals(a.getAtom_site_pdbx_PDB_ins_code());
  }
	
	
	public boolean isAtom2(Atom a) {
		if(!this.auth_atom_id_2.contentEquals(a.getAtom_site_auth_atom_id())) {
			return false;
		}
		if(!this.auth_asym_id_2.contentEquals(a.getAtom_site_auth_asym_id())) {
			return false;
		}
		if(!this.auth_comp_id_2.contentEquals(a.getAtom_site_auth_comp_id())) {
			return false;
		}
		if(!this.auth_seq_id_2.contentEquals(a.getAtom_site_auth_seq_id())) {
			return false;
		}
    return this.PDB_ins_code_2.equals(a.getAtom_site_pdbx_PDB_ins_code());
  }
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPDB_model_num() {
		return PDB_model_num;
	}
	public void setPDB_model_num(String pDB_model_num) {
		PDB_model_num = pDB_model_num;
	}
	public String getAuth_atom_id_1() {
		return auth_atom_id_1;
	}
	public void setAuth_atom_id_1(String auth_atom_id_1) {
		this.auth_atom_id_1 = auth_atom_id_1;
	}
	public String getAuth_asym_id_1() {
		return auth_asym_id_1;
	}
	public void setAuth_asym_id_1(String auth_asym_id_1) {
		this.auth_asym_id_1 = auth_asym_id_1;
	}
	public String getAuth_comp_id_1() {
		return auth_comp_id_1;
	}
	public void setAuth_comp_id_1(String auth_comp_id_1) {
		this.auth_comp_id_1 = auth_comp_id_1;
	}
	public String getAuth_seq_id_1() {
		return auth_seq_id_1;
	}
	public void setAuth_seq_id_1(String auth_seq_id_1) {
		this.auth_seq_id_1 = auth_seq_id_1;
	}
	public String getPDB_ins_code_1() {
		return PDB_ins_code_1;
	}
	public void setPDB_ins_code_1(String pDB_ins_code_1) {
		PDB_ins_code_1 = pDB_ins_code_1;
	}
	public String getLabel_alt_id_1() {
		return label_alt_id_1;
	}
	public void setLabel_alt_id_1(String label_alt_id_1) {
		this.label_alt_id_1 = label_alt_id_1;
	}
	public String getAuth_atom_id_2() {
		return auth_atom_id_2;
	}
	public void setAuth_atom_id_2(String auth_atom_id_2) {
		this.auth_atom_id_2 = auth_atom_id_2;
	}
	public String getAuth_asym_id_2() {
		return auth_asym_id_2;
	}
	public void setAuth_asym_id_2(String auth_asym_id_2) {
		this.auth_asym_id_2 = auth_asym_id_2;
	}
	public String getAuth_comp_id_2() {
		return auth_comp_id_2;
	}
	public void setAuth_comp_id_2(String auth_comp_id_2) {
		this.auth_comp_id_2 = auth_comp_id_2;
	}
	public String getAuth_seq_id_2() {
		return auth_seq_id_2;
	}
	public void setAuth_seq_id_2(String auth_seq_id_2) {
		this.auth_seq_id_2 = auth_seq_id_2;
	}
	public String getPDB_ins_code_2() {
		return PDB_ins_code_2;
	}
	public void setPDB_ins_code_2(String pDB_ins_code_2) {
		PDB_ins_code_2 = pDB_ins_code_2;
	}
	public String getLabel_alt_id_2() {
		return label_alt_id_2;
	}
	public void setLabel_alt_id_2(String label_alt_id_2) {
		this.label_alt_id_2 = label_alt_id_2;
	}
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	
}

package org.glycoinfo.PDB2Glycan.io.model;

import java.util.List;

import org.glycoinfo.PDB2Glycan.io.model.exinfo.Citation;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.EntitySrcNat;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.Exptl;
import org.glycoinfo.PDB2Glycan.io.model.info.SoftwareInfo;
import org.glycoinfo.PDB2Glycan.io.model.protein.Struct;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructRef;
import org.glycoinfo.PDB2Glycan.structure.Proteindb;
import org.glycoinfo.PDB2Glycan.structure.glyco.BranchEntry;
import org.glycoinfo.PDB2Glycan.structure.glyco.GlycoEntry;
import org.glycoinfo.PDB2Glycan.structure.glyco.ValidationReport;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Root object for output result
 * @author Masaaki Matsubara
 *
 */
public class Glycan {

  @JsonProperty("PDB_id")
  private String PDB_id;

  @JsonProperty("time")
  private String time;

  @JsonProperty("date")
  private String date;

  @JsonProperty("Atom_site_label_asym_id")
  private List<String> Atom_site_label_asym_id;

  @JsonProperty("protein_db")
  private List<Proteindb> protein_db;

  @JsonProperty("glyco_entries")
  public List<GlycoEntry> glyco_entries;

  @JsonProperty("struct")
  public Struct struct;

  @JsonProperty("citation")
  public List<Citation> citations;

  @JsonProperty("entity_src_nat")
  public List<EntitySrcNat> entity_src_nats;

  @JsonProperty("exptl")
  public List<Exptl> exptls;

  @JsonProperty("struct_ref")
  public List<StructRef> struct_refs;

  @JsonProperty("validation")
  public ValidationReport validationReport;

  @JsonProperty("software")
  public SoftwareInfo softwareInfo;

  @JsonProperty("branch_entries")
  public List<BranchEntry> branchEntries;


  public List<BranchEntry> getBranchEntries() {
    return branchEntries;
  }

  public void setBranchEntries(List<BranchEntry> branchEntries) {
    this.branchEntries = branchEntries;
  }

  public SoftwareInfo getSoftwareInfo() {
    return softwareInfo;
  }

  public void setSoftwareInfo(SoftwareInfo softwareInfo) {
    this.softwareInfo = softwareInfo;
  }

  public ValidationReport getValidationReport() {
    return validationReport;
  }

  public void setValidationReport(ValidationReport validationReport) {
    this.validationReport = validationReport;
  }

  @JsonIgnore
  public List<StructRef> getStruct_refs() {
    return struct_refs;
  }

  public void setStruct_refs(List<StructRef> struct_refs) {
    this.struct_refs = struct_refs;
  }

  @JsonIgnore
  public List<Citation> getCitations() {
    return citations;
  }

  public void setCitations(List<Citation> citations) {
    this.citations = citations;
  }

  @JsonIgnore
  public List<EntitySrcNat> getEntity_src_nats() {
    return entity_src_nats;
  }

  public void setEntity_src_nat(List<EntitySrcNat> entity_src_nats) {
    this.entity_src_nats = entity_src_nats;
  }

  @JsonIgnore
  public List<Exptl> getExptls() {
    return exptls;
  }

  public void setExptl(List<Exptl> exptls) {
    this.exptls = exptls;
  }

  @JsonIgnore
  public String getPDB_id() {
    return PDB_id;
  }

  public void setPDB_id(String PDB_id) {
    this.PDB_id = PDB_id;
  }

  @JsonIgnore
  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  @JsonIgnore
  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  @JsonIgnore
  public Struct getStruct() {
    return this.struct;
  }

  public void setStruct(Struct s) {
    this.struct = s;
  }

  @JsonIgnore
  public List<String> getAtom_site_label_asym_id() {
    return Atom_site_label_asym_id;
  }

  public void setAtom_site_label_asym_id(List<String> atom_site_label_asym_id) {
    Atom_site_label_asym_id = atom_site_label_asym_id;
  }

  @JsonIgnore
  public List<Proteindb> getProtein_db() {
    return protein_db;
  }

  public void setProtein_db(List<Proteindb> protein_db) {
    this.protein_db = protein_db;
  }

  @JsonIgnore
  public List<GlycoEntry> getGlyco_entries() {
    return glyco_entries;
  }

  public void setGlyco_entries(List<GlycoEntry> glyco_entries) {
    this.glyco_entries = glyco_entries;
  }
}

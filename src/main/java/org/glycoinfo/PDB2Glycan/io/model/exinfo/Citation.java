package org.glycoinfo.PDB2Glycan.io.model.exinfo;

public class Citation {
	public String id = "";
	public String title = "";
	public String journal_abbrev = "";
	public String pdbx_database_id_PubMed = "";
	public String pdbx_database_id_DOI = "";
	public String year = "";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getJournal_abbrev() {
		return journal_abbrev;
	}
	public void setJournal_abbrev(String journal_abbrev) {
		this.journal_abbrev = journal_abbrev;
	}
	public String getPdbx_database_id_PubMed() {
		return pdbx_database_id_PubMed;
	}
	public void setPdbx_database_id_PubMed(String pdbx_database_id_PubMed) {
		this.pdbx_database_id_PubMed = pdbx_database_id_PubMed;
	}
	public String getPdbx_database_id_DOI() {
		return pdbx_database_id_DOI;
	}
	public void setPdbx_database_id_DOI(String pdbx_database_id_DOI) {
		this.pdbx_database_id_DOI = pdbx_database_id_DOI;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
}



package org.glycoinfo.PDB2Glycan.io.model.chem_comp;

public class PdbxChemCompRelated {

  private String comp_id;
  private String related_comp_id;
  private String relationship_type;
  private String details;

  public String getComp_id() {
    return comp_id;
  }

  public void setComp_id(String comp_id) {
    this.comp_id = comp_id;
  }

  public String getRelated_comp_id() {
    return related_comp_id;
  }

  public void setRelated_comp_id(String related_comp_id) {
    this.related_comp_id = related_comp_id;
  }

  public String getRelationship_type() {
    return relationship_type;
  }

  public void setRelationship_type(String relationship_type) {
    this.relationship_type = relationship_type;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }
}

package org.glycoinfo.PDB2Glycan.io.model.exinfo;

public class Exptl {
	public String entry_id = "";
	public String method = "";

	public String getEntry_id() {
		return entry_id;
	}

	public void setEntry_id(String entry_id) {
		this.entry_id = entry_id;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
}

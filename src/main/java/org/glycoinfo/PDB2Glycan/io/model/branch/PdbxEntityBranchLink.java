package org.glycoinfo.PDB2Glycan.io.model.branch;

public class PdbxEntityBranchLink {

  private String link_id;
  private String entity_id;
  private String entity_branch_list_num_1;
  private String comp_id_1;
  private String atom_id_1;
  private String leaving_atom_id_1;
  private String atom_stereo_config_1;
  private String entity_branch_list_num_2;
  private String comp_id_2;
  private String atom_id_2;
  private String leaving_atom_id_2;
  private String atom_stereo_config_2;
  private String value_order;
  private String details;

  public String getLink_id() {
    return link_id;
  }

  public void setLink_id(String linkd_id) {
    this.link_id = linkd_id;
  }

  public String getEntity_id() {
    return entity_id;
  }

  public void setEntity_id(String entity_id) {
    this.entity_id = entity_id;
  }

  public String getEntity_branch_list_num_1() {
    return entity_branch_list_num_1;
  }

  public void setEntity_branch_list_num_1(String entity_branch_list_num_1) {
    this.entity_branch_list_num_1 = entity_branch_list_num_1;
  }

  public String getComp_id_1() {
    return comp_id_1;
  }

  public void setComp_id_1(String comp_id_1) {
    this.comp_id_1 = comp_id_1;
  }

  public String getAtom_id_1() {
    return atom_id_1;
  }

  public void setAtom_id_1(String atom_id_1) {
    this.atom_id_1 = atom_id_1;
  }

  public String getLeaving_atom_id_1() {
    return leaving_atom_id_1;
  }

  public void setLeaving_atom_id_1(String leaving_atom_id_1) {
    this.leaving_atom_id_1 = leaving_atom_id_1;
  }

  public String getAtom_stereo_config_1() {
    return atom_stereo_config_1;
  }

  public void setAtom_stereo_config_1(String atom_stereo_config_1) {
    this.atom_stereo_config_1 = atom_stereo_config_1;
  }

  public String getEntity_branch_list_num_2() {
    return entity_branch_list_num_2;
  }

  public void setEntity_branch_list_num_2(String entity_branch_list_num_2) {
    this.entity_branch_list_num_2 = entity_branch_list_num_2;
  }

  public String getComp_id_2() {
    return comp_id_2;
  }

  public void setComp_id_2(String comp_id_2) {
    this.comp_id_2 = comp_id_2;
  }

  public String getAtom_id_2() {
    return atom_id_2;
  }

  public void setAtom_id_2(String atom_id_2) {
    this.atom_id_2 = atom_id_2;
  }

  public String getLeaving_atom_id_2() {
    return leaving_atom_id_2;
  }

  public void setLeaving_atom_id_2(String leaving_atom_id_2) {
    this.leaving_atom_id_2 = leaving_atom_id_2;
  }

  public String getAtom_stereo_config_2() {
    return atom_stereo_config_2;
  }

  public void setAtom_stereo_config_2(String atom_stereo_config_2) {
    this.atom_stereo_config_2 = atom_stereo_config_2;
  }

  public String getValue_order() {
    return value_order;
  }

  public void setValue_order(String value_order) {
    this.value_order = value_order;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

}

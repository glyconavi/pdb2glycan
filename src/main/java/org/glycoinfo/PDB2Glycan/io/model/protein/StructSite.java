package org.glycoinfo.PDB2Glycan.io.model.protein;

public class StructSite{
  private String id="";
  private String pdbx_evidence_code="";
  private String pdbx_auth_asym_id="";
  private String pdbx_auth_comp_id="";
  private String pdbx_auth_seq_id="";
  private String pdbx_auth_ins_code="";
  private String pdbx_num_residues="";
  private String details="";
  
  
  
  public String getId() {
  	return id;
  }
  public void setId(String id) {
  	this.id = id;
  }
  public String getPdbx_evidence_code() {
  	return pdbx_evidence_code;
  }
  public void setPdbx_evidence_code(String pdbx_evidence_code) {
  	this.pdbx_evidence_code = pdbx_evidence_code;
  }
  public String getPdbx_auth_asym_id() {
  	return pdbx_auth_asym_id;
  }
  public void setPdbx_auth_asym_id(String pdbx_auth_asym_id) {
  	this.pdbx_auth_asym_id = pdbx_auth_asym_id;
  }
  public String getPdbx_auth_comp_id() {
  	return pdbx_auth_comp_id;
  }
  public void setPdbx_auth_comp_id(String pdbx_auth_comp_id) {
  	this.pdbx_auth_comp_id = pdbx_auth_comp_id;
  }
  public String getPdbx_auth_seq_id() {
  	return pdbx_auth_seq_id;
  }
  public void setPdbx_auth_seq_id(String pdbx_auth_seq_id) {
  	this.pdbx_auth_seq_id = pdbx_auth_seq_id;
  }
  public String getPdbx_auth_ins_code() {
  	return pdbx_auth_ins_code;
  }
  public void setPdbx_auth_ins_code(String pdbx_auth_ins_code) {
  	this.pdbx_auth_ins_code = pdbx_auth_ins_code;
  }
  public String getPdbx_num_residues() {
  	return pdbx_num_residues;
  }
  public void setPdbx_num_residues(String pdbx_num_residues) {
  	this.pdbx_num_residues = pdbx_num_residues;
  }
  public String getDetails() {
  	return details;
  }
  public void setDetails(String details) {
  	this.details = details;
  }
}
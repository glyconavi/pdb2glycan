package org.glycoinfo.PDB2Glycan.io.model;

import java.util.ArrayList;
import java.util.List;

import org.biojava.nbio.structure.io.mmcif.model.ChemComp;
import org.biojava.nbio.structure.io.mmcif.model.ChemCompAtom;
import org.biojava.nbio.structure.io.mmcif.model.ChemCompBond;
import org.biojava.nbio.structure.io.mmcif.model.PdbxChemCompIdentifier;
import org.glycoinfo.PDB2Glycan.io.model.chem_comp.PdbxChemCompAtomRelated;
import org.glycoinfo.PDB2Glycan.io.model.chem_comp.PdbxChemCompFeature;
import org.glycoinfo.PDB2Glycan.io.model.chem_comp.PdbxChemCompRelated;
import org.glycoinfo.PDB2Glycan.structure.Atom;
import org.glycoinfo.PDB2Glycan.structure.Bond;
import org.glycoinfo.PDB2Glycan.structure.Mol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChemicalComponent {

  private static final Logger logger = LoggerFactory.getLogger(ChemicalComponent.class);
//  private static final Map<String, Mol> cache = new HashMap<>();

  private String id;
  private List<ChemComp> chemComps;
  private List<ChemCompAtom> chemCompAtoms;
  private List<ChemCompBond> chemCompBonds;

  private List<PdbxChemCompIdentifier> pdbxChemCompIdentifier;
  private List<PdbxChemCompRelated> pdbxChemCompReleated;
  private List<PdbxChemCompAtomRelated> pdbxChemCompAtomReleated;
  private List<PdbxChemCompFeature> pdbxChemCompFeature;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<ChemComp> getChemComps() {
    return chemComps;
  }

  public void setChemComps(List<ChemComp> chemComps) {
    this.chemComps = chemComps;
  }

  public List<ChemCompAtom> getChemCompAtoms() {
    return chemCompAtoms;
  }

  public void setChemCompAtoms(List<ChemCompAtom> chemCompAtoms) {
    this.chemCompAtoms = chemCompAtoms;
  }

  public List<ChemCompBond> getChemCompBonds() {
    return chemCompBonds;
  }

  public void setChemCompBonds(List<ChemCompBond> chemCompBonds) {
    this.chemCompBonds = chemCompBonds;
  }

  public List<PdbxChemCompIdentifier> getPdbxChemCompIdentifier() {
    return pdbxChemCompIdentifier;
  }

  public void setPdbxChemCompIdentifier(List<PdbxChemCompIdentifier> pdbxChemCompIdentifier) {
    this.pdbxChemCompIdentifier = pdbxChemCompIdentifier;
  }

  public List<PdbxChemCompRelated> getPdbxChemCompReleated() {
    return pdbxChemCompReleated;
  }

  public void setPdbxChemCompReleated(List<PdbxChemCompRelated> pdbxChemCompReleated) {
    this.pdbxChemCompReleated = pdbxChemCompReleated;
  }

  public List<PdbxChemCompAtomRelated> getPdbxChemCompAtomReleated() {
    return pdbxChemCompAtomReleated;
  }

  public void setPdbxChemCompAtomReleated(List<PdbxChemCompAtomRelated> pdbxChemCompAtomReleated) {
    this.pdbxChemCompAtomReleated = pdbxChemCompAtomReleated;
  }

  public List<PdbxChemCompFeature> getPdbxChemCompFeature() {
    return pdbxChemCompFeature;
  }

  public void setPdbxChemCompFeature(List<PdbxChemCompFeature> pdbxChemCompFeature) {
    this.pdbxChemCompFeature = pdbxChemCompFeature;
  }

  public Mol toMol() {
//    synchronized (cache) {
//      if (cache.containsKey(getId())) {
//        return cache.get(getId());
//      }
//    }

    // Check empty "" coordinate for ideal
    boolean noIdeal = false;
    for (ChemCompAtom cca : chemCompAtoms) {
        String x = cca.getPdbx_model_Cartn_x_ideal();
        String y = cca.getPdbx_model_Cartn_y_ideal();
        String z = cca.getPdbx_model_Cartn_z_ideal();
        if ( x.isEmpty() || y.isEmpty() || z.isEmpty() ) {
          noIdeal = true;
          break;
        }
    }
    if ( noIdeal )
      logger.info("No ideal cartesian coordinate in <{}>. Used normal coordinates.", this.id);

    List<Atom> atoms = new ArrayList<>();

    int atom_count = 1;
    for (ChemCompAtom cca : chemCompAtoms) {
      Atom at = new Atom();

      at.setAtom_site_label_atom_id(cca.getAtom_id().replaceAll("\"", ""));

      String x = (noIdeal)? cca.getModel_Cartn_x() : cca.getPdbx_model_Cartn_x_ideal();
      String y = (noIdeal)? cca.getModel_Cartn_y() : cca.getPdbx_model_Cartn_y_ideal();
      String z = (noIdeal)? cca.getModel_Cartn_z() : cca.getPdbx_model_Cartn_z_ideal();
      at.setX(Double.parseDouble(x));
      at.setY(Double.parseDouble(y));
      at.setZ(Double.parseDouble(z));
      at.setAtom_site_type_symbol(cca.getType_symbol());
      at.setChem_comp_atom_charge(Integer.parseInt(cca.getCharge()));
      at.setComp_id(getId().toUpperCase());
      at.setLeaving_atom(cca.getPdbx_leaving_atom_flag().equals("Y"));
      at.setCtfile_id(atom_count);

      at.setChem_comp_atom_alt_atom_id(cca.getAlt_atom_id());

      atom_count++;

      atoms.add(at);
    }

    List<Bond> bonds = new ArrayList<>();

    for (ChemCompBond ccb : chemCompBonds) {
      Bond bond = new Bond();

      bond.setAromoatic(!ccb.getPdbx_aromatic_flag().equals("N"));

      switch (ccb.getValue_order()) {
      case "SING":
        bond.setBondOrder(1);
        break;
      case "DOUB":
        bond.setBondOrder(2);
        break;
      case "TRIP":
        bond.setBondOrder(3);
        break;
      }

      String atomId1 = ccb.getAtom_id_1().replaceAll("\"", "");

      Atom fromAtom = atoms.stream().filter(x -> atomId1.equals(x.getAtom_site_label_atom_id())).findFirst()
          .orElse(null);

      String atomId2 = ccb.getAtom_id_2().replaceAll("\"", "");

      Atom tatom = atoms.stream().filter(x -> atomId2.equals(x.getAtom_site_label_atom_id())).findFirst()
          .orElse(null);

      bond.setFromAtom(fromAtom);
      bond.setToAtom(tatom);

      bonds.add(bond);
    }

    Mol mol = new Mol(atoms, bonds);

//    synchronized (cache) {
//      cache.putIfAbsent(getId(), mol);
//    }

    return mol;
  }
}

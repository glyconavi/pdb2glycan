package org.glycoinfo.PDB2Glycan.io.model.branch;

public class PdbxEntityBranchDescriptor {

  private String ordinal;
  private String entity_id;
  private String descriptor;
  private String type;
  private String program;
  private String program_version;

  public String getOrdinal() {
    return ordinal;
  }

  public void setOrdinal(String ordinal) {
    this.ordinal = ordinal;
  }

  public String getEntity_id() {
    return entity_id;
  }

  public void setEntity_id(String entity_id) {
    this.entity_id = entity_id;
  }

  public String getDescriptor() {
    return descriptor;
  }

  public void setDescriptor(String descriptor) {
    this.descriptor = descriptor;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getProgram() {
    return program;
  }

  public void setProgram(String program) {
    this.program = program;
  }

  public String getProgram_version() {
    return program_version;
  }

  public void setProgram_version(String program_version) {
    this.program_version = program_version;
  }
}

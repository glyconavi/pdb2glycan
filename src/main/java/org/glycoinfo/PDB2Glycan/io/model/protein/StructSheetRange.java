package org.glycoinfo.PDB2Glycan.io.model.protein;

import org.glycoinfo.PDB2Glycan.structure.StructureRange;

public class StructSheetRange extends StructureRange{
	private String sheet_id="";
	private String id="";
	
	private String beg_label_comp_id="";
	private String beg_label_asym_id="";
	private String beg_label_seq_id="";
	
	private String pdbx_beg_PDB_ins_code="";
	
	private String end_label_comp_id="";
	private String end_label_asym_id="";
	private String end_label_seq_id="";
	
	
	private String pdbx_end_PDB_ins_code="";
	private String beg_auth_comp_id="";
	private String beg_auth_asym_id="";
	private String beg_auth_seq_id="";
	private String end_auth_comp_id="";
	private String end_auth_asym_id="";
	private String end_auth_seq_id="";
	
	public String getSheet_id() {
		return sheet_id;
	}
	public void setSheet_id(String sheet_id) {
		this.sheet_id = sheet_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBeg_label_comp_id() {
		return beg_label_comp_id;
	}
	public void setBeg_label_comp_id(String beg_label_comp_id) {
		this.beg_label_comp_id = beg_label_comp_id;
	}
	public String getBeg_label_asym_id() {
		return beg_label_asym_id;
	}
	public void setBeg_label_asym_id(String beg_label_asym_id) {
		this.beg_label_asym_id = beg_label_asym_id;
	}
	public String getBeg_label_seq_id() {
		return beg_label_seq_id;
	}
	public void setBeg_label_seq_id(String beg_label_seq_id) {
		this.beg_label_seq_id = beg_label_seq_id;
	}
	public String getPdbx_beg_PDB_ins_code() {
		return pdbx_beg_PDB_ins_code;
	}
	public void setPdbx_beg_PDB_ins_code(String pdbx_beg_PDB_ins_code) {
		this.pdbx_beg_PDB_ins_code = pdbx_beg_PDB_ins_code;
	}
	public String getEnd_label_comp_id() {
		return end_label_comp_id;
	}
	public void setEnd_label_comp_id(String end_label_comp_id) {
		this.end_label_comp_id = end_label_comp_id;
	}
	public String getEnd_label_asym_id() {
		return end_label_asym_id;
	}
	public void setEnd_label_asym_id(String end_label_asym_id) {
		this.end_label_asym_id = end_label_asym_id;
	}
	public String getEnd_label_seq_id() {
		return end_label_seq_id;
	}
	public void setEnd_label_seq_id(String end_label_seq_id) {
		this.end_label_seq_id = end_label_seq_id;
	}
	public String getPdbx_end_PDB_ins_code() {
		return pdbx_end_PDB_ins_code;
	}
	public void setPdbx_end_PDB_ins_code(String pdbx_end_PDB_ins_code) {
		this.pdbx_end_PDB_ins_code = pdbx_end_PDB_ins_code;
	}
	public String getBeg_auth_comp_id() {
		return beg_auth_comp_id;
	}
	public void setBeg_auth_comp_id(String beg_auth_comp_id) {
		this.beg_auth_comp_id = beg_auth_comp_id;
	}
	public String getBeg_auth_asym_id() {
		return beg_auth_asym_id;
	}
	public void setBeg_auth_asym_id(String beg_auth_asym_id) {
		this.beg_auth_asym_id = beg_auth_asym_id;
	}
	public String getBeg_auth_seq_id() {
		return beg_auth_seq_id;
	}
	public void setBeg_auth_seq_id(String beg_auth_seq_id) {
		this.beg_auth_seq_id = beg_auth_seq_id;
	}
	public String getEnd_auth_comp_id() {
		return end_auth_comp_id;
	}
	public void setEnd_auth_comp_id(String end_auth_comp_id) {
		this.end_auth_comp_id = end_auth_comp_id;
	}
	public String getEnd_auth_asym_id() {
		return end_auth_asym_id;
	}
	public void setEnd_auth_asym_id(String end_auth_asym_id) {
		this.end_auth_asym_id = end_auth_asym_id;
	}
	public String getEnd_auth_seq_id() {
		return end_auth_seq_id;
	}
	public void setEnd_auth_seq_id(String end_auth_seq_id) {
		this.end_auth_seq_id = end_auth_seq_id;
	}
	
}
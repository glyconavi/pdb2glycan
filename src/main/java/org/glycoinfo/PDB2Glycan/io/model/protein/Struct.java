package org.glycoinfo.PDB2Glycan.io.model.protein;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Struct {
  @JsonProperty("entry_id")
  public String entry_id = "";

  @JsonProperty("title")
  public String title = "";

  @JsonProperty("pdbx_descriptor")
  public String pdbx_descriptor = "";
  
  @JsonIgnore
  public String entity_id = "";

  public Struct(){
  }
  
  public void setEntry_id(String s) {
    this.entry_id = s;
  }
  public void setTitle(String s) {
    this.title = s;
  }
  public void setPdbx_descriptor(String d) {
    this.pdbx_descriptor = d;
  }
  public void setEntity_id(String s) {
    this.entity_id = s;
  }
  
  public String getEntry_id() {
    return this.entry_id;
  }
  public String getTitle() {
    return this.title;
  }
  public String getPdbx_descriptor() {
    return this.pdbx_descriptor;
  }
  public String getEntity_id() {
    return this.entity_id;
  }
  
  
  
}

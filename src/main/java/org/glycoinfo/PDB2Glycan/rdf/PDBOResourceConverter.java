package org.glycoinfo.PDB2Glycan.rdf;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxBranchScheme;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranch;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchDescriptor;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchLink;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchList;
import org.glycoinfo.PDB2Glycan.io.model.protein.AtomSite;
import org.glycoinfo.PDB2Glycan.io.model.protein.ChemComp;
import org.glycoinfo.PDB2Glycan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConf;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetRange;
import org.glycoinfo.PDB2Glycan.rdf.vocabulary.PDBO;
import org.glycoinfo.PDB2Glycan.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convert Java classes to RDF resources as PDB/RDF model
 * @author Masaaki Matsubara
 *
 */
public class PDBOResourceConverter {
  private static Logger logger = LoggerFactory.getLogger(PDBOResourceConverter.class);

  private static final Set<Class<? extends Object>> allowedClasses;
  static {
    allowedClasses = new HashSet<>();

    allowedClasses.add(AtomSite.class);
    allowedClasses.add(StructConn.class);
    allowedClasses.add(ChemComp.class);
    allowedClasses.add(PdbxStructModResidue.class);

    allowedClasses.add(PdbxBranchScheme.class);
    allowedClasses.add(PdbxEntityBranch.class);
    allowedClasses.add(PdbxEntityBranchLink.class);
    allowedClasses.add(PdbxEntityBranchList.class);
    allowedClasses.add(PdbxEntityBranchDescriptor.class);

    allowedClasses.add(StructConf.class);
    allowedClasses.add(StructSheetRange.class);
  }

  public static Resource toRDFResource(Model model, String pdbId, Object o, String id) {
    if ( !allowedClasses.contains(o.getClass()) )
      throw new RuntimeException(o.getClass().getName()+" is not allowed to convert.");

    String className = o.getClass().getSimpleName();
    String category = StringUtils.convertCaseCamelToSnake(className);

    Resource node = model.createResource(
        String.format("%s%s/%s/%s", PDBO.PDBR_PREFIX, pdbId, category, id),
        PDBO.resource(category)
      );

    for ( Field f : o.getClass().getDeclaredFields() ) {
      String field = f.getName();
      try {
        f.setAccessible(true);
        String value = (String)f.get(o);
        if ( value == null )
          continue;
        node.addProperty(PDBO.property(category+"."+field), value);
      } catch (IllegalArgumentException | IllegalAccessException e) {
        logger.warn("Error in getting field "+field, e);
      }
    }

    return node;
  }

}

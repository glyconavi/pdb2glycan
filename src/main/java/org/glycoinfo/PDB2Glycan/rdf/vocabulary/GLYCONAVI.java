package org.glycoinfo.PDB2Glycan.rdf.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.glycoinfo.PDB2Glycan.structure.EntityPolyType;
import org.glycoinfo.PDB2Glycan.structure.WURCSType;

public class GLYCONAVI {

  public static final String uri = "http://glyconavi.org/ontology/pdb#";

  public static String getURI() {
    return uri;
  }

  public static final Resource resource(String local) {
    return ResourceFactory.createResource(getURI() + local);
  }

  protected static final Property property(String local) {
    return ResourceFactory.createProperty(getURI(), local);
  }


  // For TCarpEntry
  public static class TCarpEntry {
    public static final String className = "TCarpEntry";

    public static final Property has_atom_site_label_asym_id
           = GLYCONAVI.property("has_atom_site_label_asym_id");
    /** http://glyconavi.org/Proteins/tool/3d/LitMol.php?id={PDB_id} */
    public static final Property has_link_to_3d_model
           = GLYCONAVI.property("has_link_to_3d_model");

    public static final Property has_struct         = GLYCONAVI.property("has_struct");
    public static final Property has_citation       = GLYCONAVI.property("has_citation");
    public static final Property has_struct_ref     = GLYCONAVI.property("has_struct_ref");
    public static final Property has_entity_src_nat = GLYCONAVI.property("has_entity_src_nat");
    public static final Property has_exptl          = GLYCONAVI.property("has_exptl");

    public static final Property has_validation = GLYCONAVI.property("has_validation");

    public static final Property has_software_info = GLYCONAVI.property("has_software_info");

    public static final Property has_protein_db = GLYCONAVI.property("has_protein_db");
    public static final Property has_strand     = GLYCONAVI.property("has_strand");

    public static final Property has_PDB_model_glycan
           = GLYCONAVI.property("has_PDB_model_glycan");

    public static final Property has_branch_entry = GLYCONAVI.property("has_branch_entry");

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id} */
    public static final String getURI(String PDB_id) {
      return String.format("http://glycoinfo.org/pdb/%s", PDB_id);
    }
    /** http://glycoinfo.org/pdb/ */
    public static final String getURI() {
      return getURI("");
    }
  }

  // For Strand
  public static class Strand {
    private static final String className = "Strand";

    public static final Property has_strand_id = GLYCONAVI.property("has_strand_id");
    public static final Property has_entity_id = GLYCONAVI.property("has_entity_id");
    public static final Property has_sequence  = GLYCONAVI.property("has_sequence");
    public static final Property has_db_name  = GLYCONAVI.property("has_db_name");

    public static final Resource forEntityPolyType(EntityPolyType ept) {
      String className = "UnknownType";

      switch (ept) {
        case CYCLIC_PSEUDO_PEPTIDE:
          className = "CyclicPseudoPeptide";
          break;
        case OTHER:
          className = "Other";
          break;
        case PEPTIDE_NUCLEIC_ACID:
          className = "PeptideNucleicAcid";
          break;
        case POLYDEOXYRIBONUCLEOTIDE:
          className = "Polydeoxyribonucleotide";
          break;
        case POLYDEOXYRIBONUCLEOTIDE_POLYRIBONUCLEOTIDE_HYBRID:
          className = "PolydeoxyribonucleotideOrPolyribonucleotideHybrid";
          break;
        case POLYPEPTIDE_D:
          className = "PolypeptideD";
          break;
        case POLYPEPTIDE_L:
          className = "PolypeptideL";
          break;
        case POLYRIBONUCLEOTIDE:
          className = "Polyribonucleotide";
          break;
        case POLYSACCHARIDE_D:
          className = "PolysaccharideD";
          break;
        case POLYSACCHARIDE_L:
          className = "PolysaccharideL";
          break;
      }

      return GLYCONAVI.resource(className);
    }

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id}/strand/{strand_id} */
    public static final String getURI(String PDB_id, String strand_id) {
      return String.format("%s/strand/%s", TCarpEntry.getURI(PDB_id), strand_id);
    }
  }

  // For PDBModelGlycan
  public static class PDBModelGlycan {
    private static final String className = "PDBModelGlycan";

    public static final Property has_entity_id = GLYCONAVI.property("has_entity_id");
    public static final Property has_asym_id   = GLYCONAVI.property("has_asym_id");

    public static final Property has_glycosylation_site
           = GLYCONAVI.property("has_glycosylation_site");

    public static final Property has_model = GLYCONAVI.property("has_model");

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id}/PDB_model_glycan/{model_id} */
    public static final String getURI(String PDB_id, String model_id) {
      return String.format("%s/PDB_model_glycan/%s", TCarpEntry.getURI(PDB_id), model_id);
    }
  }

  public static abstract class PDBResidue {
    /** alpha-helix info */
    public static final Property has_helix_info
           = GLYCONAVI.property("has_helix_info");
    /** beta-sheet info */
    public static final Property has_sheet_info
           = GLYCONAVI.property("has_sheet_info");
  }

  // For GlycosylationSite
  public static class GlycosylationSite extends PDBResidue {
    private static final String className = "GlycosylationSite";

    public static final Property has_glycan_type = GLYCONAVI.property("has_glycan_type");

    public static final Property has_asym_id   = GLYCONAVI.property("has_asym_id");
    public static final Property has_entity_id = GLYCONAVI.property("has_entity_id");
    public static final Property has_seq_id    = GLYCONAVI.property("has_seq_id");
    public static final Property has_comp_id   = GLYCONAVI.property("has_comp_id");
    public static final Property has_atom_id   = GLYCONAVI.property("has_atom_id");

    public static final Property is_on_strand   = GLYCONAVI.property("is_on_strand");
    public static final Property is_mod_residue = GLYCONAVI.property("is_mod_residue");

    public static final Property has_one_letter_code = GLYCONAVI.property("has_one_letter_code");
    public static final Property has_sequence_before = GLYCONAVI.property("has_sequence_before");
    public static final Property has_sequence_after  = GLYCONAVI.property("has_sequence_after");

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id}/PDB_model_glycan/{model_id}/glycosylation_site */
    public static final String getURI(String PDB_id, String model_id) {
      return String.format("%s/glycosylation_site", PDBModelGlycan.getURI(PDB_id, model_id));
    }
  }

  // For GlycanType
  public static class GlycanType {
    private static final String className = "GlycanType";

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/type/glycan/{glycan_type} */
    public static final String getURI(String glycan_type) {
      return String.format("%stype/glycan/%s", TCarpEntry.getURI(), glycan_type);
    }
  }

  // For PDBModelGlycan
  public static class PDBModel {
    private static final String className = "PDBModel";

    public static final Property has_pdbx_PDB_model_num
           = GLYCONAVI.property("has_pdbx_PDB_model_num");

    public static final Property has_label_alt_id = GLYCONAVI.property("has_label_alt_id");

    public static final Property has_ctfile   = GLYCONAVI.property("has_ctfile");
    // added. 6.Jan.2023
    public static final Property has_mmCIF   = GLYCONAVI.property("has_mmCIF");
    public static final Property has_mmJSON = GLYCONAVI.property("has_mmJSON");

    public static final Property has_interaction = GLYCONAVI.property("has_interaction");
    public static final Property has_atom_list   = GLYCONAVI.property("has_atom_list");

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id}/PDB_model_glycan/{model_id}/model/{pdbx_PDB_model_num} */
    public static final String getURI(String PDB_id, String model_id, String pdbx_PDB_model_num) {
      return String.format("%s/model/%s", PDBModelGlycan.getURI(PDB_id, model_id), pdbx_PDB_model_num);
    }
  }

  // For Interaction
  public static class Interaction extends PDBResidue {
    private static final String className = "Interaction";

    public static final Property has_distance = GLYCONAVI.property("has_distance");
    public static final Property has_source_atom = GLYCONAVI.property("has_source_atom");
    public static final Property has_nearest_target_atom
           = GLYCONAVI.property("has_nearest_target_atom");

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id}/PDB_model_glycan/{model_id}/model/{pdbx_PDB_model_num}/interaction/{source_atom.id},{target_atom.id} */
    public static final String getURI(String PDB_id, String model_id, String pdbx_PDB_model_num, String source_atom_id, String target_atom_id) {
      return String.format("%s/interaction/%s,%s", PDBModel.getURI(PDB_id, model_id, pdbx_PDB_model_num), source_atom_id, target_atom_id);
    }
  }

  public static class PDBAtomList {
    private static final String className = "PDBAtomList";

    public static final Property has_atom = GLYCONAVI.property("has_atom");

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id}/PDB_model_glycan/{model_id}/model/{pdbx_PDB_model_num}/atom_list */
    public static final String getURI(String PDB_id, String model_id, String pdbx_PDB_model_num) {
      return String.format("%s/atom_list",PDBModel.getURI(PDB_id, model_id, pdbx_PDB_model_num));
    }
  }

  // For validation in TCarpEntry
  public static class AtomSiteValidation {
    private static final String className = "AtomSiteValidation";

    public static final Property has_atom_report  = GLYCONAVI.property("has_atom_report");
    public static final Property has_bond_report  = GLYCONAVI.property("has_bond_report");
    public static final Property has_sugar_report = GLYCONAVI.property("has_sugar_report");

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id}/validation */
    public static final String getURI(String PDB_id) {
      return String.format("%s/validation", TCarpEntry.getURI(PDB_id));
    }
  }

  // For branch entry
  public static class BranchEntry {
    private static final String className = "BranchEntry";

    public static final Property has_entity_id  = GLYCONAVI.property("has_entity_id");
    public static final Property has_model_id  = GLYCONAVI.property("has_model_id");

    public static final Property has_linkage    = GLYCONAVI.property("has_linkage");
    public static final Property has_residue    = GLYCONAVI.property("has_residue");
    public static final Property has_glycosylation_struct_conn
                                     = GLYCONAVI.property("has_glycosylation_struct_conn");
    public static final Property has_ctfile     = GLYCONAVI.property("has_ctfile");
    // added. 25.Nov.2022
    public static final Property has_mmCIF   = GLYCONAVI.property("has_mmCIF");
    public static final Property has_mmJSON   = GLYCONAVI.property("has_mmJSON");
    // added. end.
//    public static final Property has_wurcs      = GLYCONAVI.property("has_wurcs");
    public static final Property has_validation = GLYCONAVI.property("has_validation");

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id}/branch_entry/{entity_id} */
    public static final String getURI(String PDB_id, String entity_id) {
      return String.format("%s/branch_entry/%s", TCarpEntry.getURI(PDB_id), entity_id);
    }

    public static class Validation {
      private static final String className = "Validation";

      public static final Property has_error   = GLYCONAVI.property("has_error");
      public static final Property has_warning = GLYCONAVI.property("has_warning");

      public static final Resource resource() {
        return GLYCONAVI.resource(BranchEntry.className+className);
      }
      /** http://glycoinfo.org/pdb/{PDB_id}/branch_entry/{entity_id}/validation */
      public static final String getURI(String PDB_id, String entity_id) {
        return String.format("%s/branch_entry/%s/validation", TCarpEntry.getURI(PDB_id), entity_id);
      }
    }
  }

  // For software info
  public static class Software {
    private static final String className = "SoftwareInfo";

    public static Property has_name         = GLYCONAVI.property("has_name");
    public static Property has_version      = GLYCONAVI.property("has_version");
    public static Property has_process_time = GLYCONAVI.property("has_process_time");

    public static final Resource resource() {
      return GLYCONAVI.resource(className);
    }
    /** http://glycoinfo.org/pdb/{PDB_id}/software_info */
    public static final String getURI(String PDB_id) {
      return String.format("%s/software_info", TCarpEntry.getURI(PDB_id));
    }
  }

  // For Saccharide
  public static class Saccharide {
    public static Property has_aglycone         = GLYCONAVI.property("has_aglycone");
    public static Property has_glytoucan_id     = GLYCONAVI.property("has_glytoucan_id");

    public static final Resource getWURCSType(WURCSType type) {
      String className = "WURCSTypeUnknown";
      switch (type){
      case STD:
        className = "WURCSStandard";
        break;
      case SEP:
        className = "WURCSWithAglycone";
        break;
      case CON:
        className = "SeparatedWURCSWithAglycone";
        break;
      }
      return GLYCONAVI.resource(className);
    }
    /** http://glyconavi.org/saccharide/{URLEncodedWURCS} */
    public static final String getURI(String WURCSEncoded) {
      return String.format("http://glyconavi.org/saccharide/%s", WURCSEncoded);
    }
  }

  // For Glycosequence
  public static class Glycosequence {
    public static Property has_SHA512 = GLYCONAVI.property("has_SHA512");

    /** http://glyconavi.org/saccharide/{URLEncodedWURCS}/wurcs */
    public static final String getURI(String WURCS) {
      return String.format("%s/wurcs", Saccharide.getURI(WURCS));
    }
  }

}

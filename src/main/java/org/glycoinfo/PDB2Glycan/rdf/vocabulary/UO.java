package org.glycoinfo.PDB2Glycan.rdf.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class UO {
  public static final String uri = "http://purl.obolibrary.org/obo/";

  public static final Resource millisecond = resource("UO_0000028");

  public static String getURI() {
    return uri;
  }

  public static final Resource resource(String local) {
    return ResourceFactory.createResource(getURI() + local);
  }

  protected static final Property property(String local) {
    return ResourceFactory.createProperty(getURI(), local);
  }
}

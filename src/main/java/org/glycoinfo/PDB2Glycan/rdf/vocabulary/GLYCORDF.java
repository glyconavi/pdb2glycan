package org.glycoinfo.PDB2Glycan.rdf.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class GLYCORDF {
  public static final String uri = "http://purl.jp/bio/12/glyco/glycan#";

  public static final Property has_glycan = GLYCORDF.property("has_glycan");

  public static final Property has_glycosequence = GLYCORDF.property("has_glycosequence");
  public static final Property has_sequence = GLYCORDF.property("has_sequence");
  public static final Property in_carbohydrate_format = GLYCORDF.property("in_carbohydrate_format");

  public static final Resource Glycoprotein = GLYCORDF.resource("Glycoprotein");
  public static final Resource Protein = GLYCORDF.resource("Protein");
  public static final Resource Saccharide = GLYCORDF.resource("Saccharide");
  public static final Resource Glycosequence = GLYCORDF.resource("Glycosequence");

  public static final Resource carbohydrate_format_wurcs = GLYCORDF.resource("carbohydrate_format_wurcs");


  public static String getURI() {
    return uri;
  }

  public static final Resource resource(String local) {
    return ResourceFactory.createResource(getURI() + local);
  }

  protected static final Property property(String local) {
    return ResourceFactory.createProperty(getURI(), local);
  }
}

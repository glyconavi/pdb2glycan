package org.glycoinfo.PDB2Glycan.rdf.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class PDBO {
  public static final String uri = "https://rdf.wwpdb.org/schema/pdbx-v50.owl#";

  public static final String PDBR_PREFIX = "https://rdf.wwpdb.org/pdb/";
  public static final String PDBCC_PREFIX = "https://rdf.wwpdb.org/cc/";

  /** https://rdf.wwpdb.org/pdb/{PDB_id} */
  public static String getPDBURI(String PDB_id) {
    return PDBR_PREFIX+PDB_id;
  }

  /** https://rdf.wwpdb.org/cc/{comp_id} */
  public static String getCCURI(String comp_id) {
    return PDBCC_PREFIX+comp_id;
  }

  /** https://rdf.wwpdb.org/schema/pdbx-v50.owl# */
  public static String getURI() {
    return uri;
  }

  /** https://rdf.wwpdb.org/schema/pdbx-v50.owl#{local} */
  public static final Resource resource(String local) {
    return ResourceFactory.createResource(getURI() + local);
  }

  /** https://rdf.wwpdb.org/schema/pdbx-v50.owl#{local} */
  public static final Property property(String local) {
    return ResourceFactory.createProperty(getURI(), local);
  }

  // For atom_site category
  public static class atom_site {
    private static final String className = atom_site.class.getSimpleName();

    public static final Property id = property("id");

    // For interaction
    public static final Property label_alt_id    = property("label_alt_id");
    public static final Property label_asym_id   = property("label_asym_id");
    public static final Property label_atom_id   = property("label_atom_id");
    public static final Property label_comp_id   = property("label_comp_id");
    public static final Property label_entity_id = property("label_entity_id");
    public static final Property label_seq_id    = property("label_seq_id");

    public static final Property pdbx_PDB_ins_code  = property("pdbx_PDB_ins_code");
    public static final Property pdbx_PDB_model_num = property("pdbx_PDB_model_num");

    // For the other
    public static final Property pdbx_formal_charge = property("pdbx_formal_charge");
    public static final Property occupancy          = property("occupancy");
    public static final Property B_iso_or_equiv     = property("B_iso_or_equiv");

    private static final Property property(String item) {
      return PDBO.property(className+"."+item);
    }
    public static final Resource resource() {
      return PDBO.resource(className);
    }
    /** https://rdf.wwpdb.org/pdb/{PDB_id}/atom_site/{atom_site.id} */
    public static final String getURI(String PDB_id, String id ) {
      return String.format("%s%s/%s/%s", PDBR_PREFIX, PDB_id, className, id);
    }
  }

  // For struct category
  public static class struct {
    private static final String className = struct.class.getSimpleName();

    public static final Property entry_id        = property("entry_id");
    public static final Property pdbx_descriptor = property("pdbx_descriptor");
    public static final Property title           = property("title");

    private static final Property property(String item) {
      return PDBO.property(className+"."+item);
    }
    public static final Resource resource() {
      return PDBO.resource(className);
    }
    /** https://rdf.wwpdb.org/pdb/{PDB_id}/struct/{struct.entry_id} */
    public static final String getURI(String PDB_id, String id ) {
      return String.format("%s%s/%s/%s", PDBR_PREFIX, PDB_id, className, id);
    }
  }

  // For citation category
  public static class citation {
    private static final String className = citation.class.getSimpleName();

    public static final Property id             = property("id");
    public static final Property title          = property("title");
    public static final Property journal_abbrev = property("journal_abbrev");
    public static final Property pdbx_database_id_DOI
                     = property("pdbx_database_id_DOI");
    public static final Property pdbx_database_id_PubMed
                     = property("pdbx_database_id_PubMed");
    public static final Property year           = property("year");

    /** https://doi.org/{citation.pdbx_database_id_DOI} */
    public static final Property link_to_doi    = property("link_to_doi");
    /** https://www.ncbi.nlm.nih.gov/pubmed/{citation.pdbx_database_id_PubMed} */
    public static final Property link_to_pubmed = property("link_to_pubmed");

    private static final Property property(String item) {
      return PDBO.property(className+"."+item);
    }
    public static final Resource resource() {
      return PDBO.resource(className);
    }
    /** https://rdf.wwpdb.org/pdb/{PDB_id}/citation/{citation.id} */
    public static final String getURI(String PDB_id, String id ) {
      return String.format("%s%s/%s/%s", PDBR_PREFIX, PDB_id, className, id);
    }
  }

  // For struct_ref category
  public static class struct_ref {
    private static final String className = struct_ref.class.getSimpleName();

    public static final Property id              = property("id");
    public static final Property db_code         = property("db_code");
    public static final Property db_name         = property("db_name");
    public static final Property pdbx_db_isoform = property("pdbx_db_isoform");

    private static final Property property(String item) {
      return PDBO.property(className+"."+item);
    }
    public static final Resource resource() {
      return PDBO.resource(className);
    }
    /** https://rdf.wwpdb.org/pdb/{PDB_id}/struct_ref/{struct_ref.id} */
    public static final String getURI(String PDB_id, String id ) {
      return String.format("%s%s/%s/%s", PDBR_PREFIX, PDB_id, className, id);
    }
  }

  // For exptl category
  public static class exptl {
    private static final String className = "exptl";

    public static final Property entry_id = property("entry_id");
    public static final Property method   = property("method");

    private static final Property property(String item) {
      return PDBO.property(className+"."+item);
    }
    public static final Resource resource() {
      return PDBO.resource(className);
    }
    /** https://rdf.wwpdb.org/pdb/{PDB_id}/exptl/{exptl.entry_id},{exptl.method} */
    public static final String getURI(String PDB_id, String entry_id, String method ) {
      // Spaces " " in "method" must be replaced with "_"
      method = method.replaceAll(" ", "_");
      return String.format("%s%s/%s/%s,%s", PDBR_PREFIX, PDB_id, className, entry_id, method);
    }
  }

  // For entity_src_nat category
  public static class entity_src_nat {
    private static final String className = entity_src_nat.class.getSimpleName();

    public static final Property common_name    = property("common_name");
    public static final Property genus          = property("genus");
    public static final Property pdbx_cell_line = property("pdbx_cell_line");
    public static final Property pdbx_ncbi_taxonomy_id
                     = property("pdbx_ncbi_taxonomy_id");
    public static final Property pdbx_organism_scientific
                     = property("pdbx_organism_scientific");
    public static final Property species = property("species");
    public static final Property strain  = property("strain");
    public static final Property tissue  = property("tissue");

    private static final Property property(String item) {
      return PDBO.property(className+"."+item);
    }
    public static final Resource resource() {
      return PDBO.resource(className);
    }
    /** https://rdf.wwpdb.org/pdb/{PDB_id}/entity_src_nat/{entity_src_nat.entity_id},{entity_src_nat.pdbx_src_id} */
    public static final String getURI(String PDB_id, String entity_id, String pdbx_src_id ) {
      return String.format("%s%s/%s/%s,%s", PDBR_PREFIX, PDB_id, className, entity_id, pdbx_src_id);
    }
  }

  // For pdbx_struct_mod_residue category
  public static class pdbx_struct_mod_residue {
    private static final String className = pdbx_struct_mod_residue.class.getSimpleName();

    public static final Property auth_asym_id   = property("auth_asym_id");
    public static final Property auth_comp_id   = property("auth_comp_id");
    public static final Property auth_seq_id    = property("auth_seq_id");
    public static final Property details        = property("details");
    public static final Property id             = property("id");
    public static final Property label_asym_id  = property("label_asym_id");
    public static final Property label_comp_id  = property("label_comp_id");
    public static final Property label_seq_id   = property("label_seq_id");
    public static final Property parent_comp_id = property("parent_comp_id");

    private static final Property property(String item) {
      return PDBO.property(className+"."+item);
    }
    public static final Resource resource() {
      return PDBO.resource(className);
    }
    /** https://rdf.wwpdb.org/pdb/{PDB_id}/pdbx_struct_mod_residue/{pdbx_struct_mod_residue.id} */
    public static final String getURI(String PDB_id, String id ) {
      return String.format("%s%s/%s/%s", PDBR_PREFIX, PDB_id, className, id);
    }
  }
}

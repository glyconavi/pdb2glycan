package org.glycoinfo.PDB2Glycan.rdf.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class SIO {
  public static final String uri = "http://semanticscience.org/resource/";

  public static final Property has_component_part = SIO.property("SIO_000369");
  public static final Property has_value = SIO.property("SIO_000300");
  public static final Property has_unit = SIO.property("SIO_000221");
  public static final Property has_start_time = SIO.property("SIO_000680");

  public static String getURI() {
    return uri;
  }

  public static final Resource resource(String local) {
    return ResourceFactory.createResource(getURI() + local);
  }

  protected static final Property property(String local) {
    return ResourceFactory.createProperty(getURI(), local);
  }
}

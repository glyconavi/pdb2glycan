package org.glycoinfo.PDB2Glycan.rdf;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import org.apache.commons.codec.net.URLCodec;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.SKOS;
import org.glycoinfo.PDB2Glycan.io.model.Glycan;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchLink;
import org.glycoinfo.PDB2Glycan.io.model.branch.PdbxEntityBranchList;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.Citation;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.EntitySrcNat;
import org.glycoinfo.PDB2Glycan.io.model.exinfo.Exptl;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConf;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructConn;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructRef;
import org.glycoinfo.PDB2Glycan.io.model.protein.StructSheetRange;
import org.glycoinfo.PDB2Glycan.rdf.vocabulary.GLYCONAVI;
import org.glycoinfo.PDB2Glycan.rdf.vocabulary.GLYCORDF;
import org.glycoinfo.PDB2Glycan.rdf.vocabulary.PDBO;
import org.glycoinfo.PDB2Glycan.rdf.vocabulary.SIO;
import org.glycoinfo.PDB2Glycan.rdf.vocabulary.UO;
import org.glycoinfo.PDB2Glycan.structure.Atom;
import org.glycoinfo.PDB2Glycan.structure.AtomAtomDistance;
import org.glycoinfo.PDB2Glycan.structure.EntityPolyType;
import org.glycoinfo.PDB2Glycan.structure.ModelCassette;
import org.glycoinfo.PDB2Glycan.structure.NearbyComponent;
import org.glycoinfo.PDB2Glycan.structure.Proteindb;
import org.glycoinfo.PDB2Glycan.structure.WURCSType;
import org.glycoinfo.PDB2Glycan.structure.glyco.BranchEntry;
import org.glycoinfo.PDB2Glycan.structure.glyco.GlycoEntry;
import org.glycoinfo.PDB2Glycan.structure.glyco.GlycosylationSite;
import org.glycoinfo.PDB2Glycan.structure.glyco.ValidationReport;
import org.glycoinfo.PDB2Glycan.util.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlycanModelBuilder {

  private static final Logger logger = LoggerFactory.getLogger(GlycanModelBuilder.class);

  private static final String IDENTIFIERS_PREFIX = "https://identifiers.org/";
  private static final String MODEL_3D_PREFIX = "http://glyconavi.org/Proteins/tool/3d/LitMol.php?id=";

  private Glycan entry;

  public GlycanModelBuilder entry(Glycan entry) {
    this.entry = entry;

    return this;
  }

  public Model build() {
    Model model = namespacedModel();

    if (entry == null) {
      return model;
    }

    String PDB_id = entry.getPDB_id();

    Resource uri = model.createResource(
        GLYCONAVI.TCarpEntry.getURI(PDB_id),
        GLYCONAVI.TCarpEntry.resource()
      );

    uri.addProperty(DCTerms.identifier, PDB_id);

    DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
            .parseDateTime(entry.getDate());
    uri.addProperty(DCTerms.created,
        model.createTypedLiteral(dateTime.toCalendar(Locale.getDefault())));

    // For software info
    Resource nodeSoftInfo = model.createResource(
        GLYCONAVI.Software.getURI(PDB_id),
        GLYCONAVI.Software.resource()
      );
    uri.addProperty(GLYCONAVI.TCarpEntry.has_software_info, nodeSoftInfo);
    nodeSoftInfo.addProperty(GLYCONAVI.Software.has_name, entry.getSoftwareInfo().getName());
    nodeSoftInfo.addProperty(GLYCONAVI.Software.has_version, entry.getSoftwareInfo().getVersion());
    nodeSoftInfo.addProperty(SIO.has_start_time,
        model.createTypedLiteral(dateTime.toCalendar(Locale.getDefault())));

    Resource bn_process_time = model.createResource();
    nodeSoftInfo.addProperty(GLYCONAVI.Software.has_process_time, bn_process_time);
    bn_process_time.addProperty(RDFS.label, StringUtils.formatTime(entry.getSoftwareInfo().getProcess_time()));
    bn_process_time.addProperty(SIO.has_value,
        model.createTypedLiteral(entry.getSoftwareInfo().getProcess_time()));
    bn_process_time.addProperty(SIO.has_unit, UO.millisecond);


    // seeAlso
    uri.addProperty(RDFS.seeAlso, model.createResource(PDBO.getPDBURI(PDB_id)));
    uri.addProperty(RDFS.seeAlso, model.createResource(
        String.format("%spdb:%s", IDENTIFIERS_PREFIX, PDB_id)
      ));

    // ISK
    uri.addProperty(GLYCONAVI.TCarpEntry.has_link_to_3d_model,
        model.createResource(MODEL_3D_PREFIX + PDB_id));

    if ( entry.getAtom_site_label_asym_id() != null )
      entry.getAtom_site_label_asym_id().forEach(x ->
        uri.addProperty(GLYCONAVI.TCarpEntry.has_atom_site_label_asym_id, x)
      );
    addExInfo(model, uri);
    addProtein_db(model, uri);
//    addAr_pdbx_struct_mod_residue(model, uri);
    addGlycan(model, uri);

    // For branch
    addBranchEntries(model, uri);

    return model;
  }

  private Model namespacedModel() {
    Model model = ModelFactory.createDefaultModel();

    PrefixMapping namespaces = PrefixMapping.Factory.create()
        .setNsPrefixes(PrefixMapping.Standard)
        .setNsPrefix("dcterms", DCTerms.getURI())
        .setNsPrefix("skos", SKOS.getURI())
        .setNsPrefix("sio", SIO.getURI())
        .setNsPrefix("glycan", GLYCORDF.getURI())
        .setNsPrefix("glyconavi", GLYCONAVI.getURI())
        .setNsPrefix("pdbo", PDBO.getURI())
        .setNsPrefix("ccrdf", PDBO.PDBCC_PREFIX)
        .setNsPrefix("pdbrdf", PDBO.PDBR_PREFIX);

    model.setNsPrefixes(namespaces);

    return model;
  }

  /**
   * Adds extra info for Struct, Citation, StructRef, Exptl and EntitySrcNat, as well as ValidationReport.
   * @param model
   * @param uri Resource for TCarpEntry
   */
  private void addExInfo(Model model, Resource uri) {
    String PDB_id = this.entry.getPDB_id();

    if ( entry.getStruct() != null ) {
      Resource node = model.createResource(
          PDBO.struct.getURI(PDB_id, entry.getStruct().getEntry_id()),
          PDBO.struct.resource()
        );
      uri.addProperty(GLYCONAVI.TCarpEntry.has_struct, node);

      node.addProperty(PDBO.struct.entry_id, entry.getStruct().getEntry_id());
      node.addProperty(PDBO.struct.title, entry.getStruct().getTitle());
      node.addProperty(PDBO.struct.pdbx_descriptor, entry.getStruct().getPdbx_descriptor());
    }

    if ( entry.getCitations() != null )
      for (Citation ctt : entry.getCitations()) {
        Resource node = model.createResource(
            PDBO.citation.getURI(PDB_id, ctt.getId()),
            PDBO.citation.resource()
          );
        uri.addProperty(GLYCONAVI.TCarpEntry.has_citation, node);

        node.addProperty(PDBO.citation.id, ctt.getId());
        node.addProperty(PDBO.citation.title, ctt.getTitle());
        node.addProperty(PDBO.citation.journal_abbrev, ctt.getJournal_abbrev());
        node.addProperty(PDBO.citation.pdbx_database_id_PubMed, ctt.getPdbx_database_id_PubMed());
        node.addProperty(PDBO.citation.pdbx_database_id_DOI, ctt.getPdbx_database_id_DOI());
        node.addProperty(PDBO.citation.year, ctt.getYear());

        if ( !ctt.getPdbx_database_id_PubMed().isEmpty() )
          node.addProperty(PDBO.citation.link_to_pubmed,
              model.createResource(String.format(
                  "https://www.ncbi.nlm.nih.gov/pubmed/%s", ctt.getPdbx_database_id_PubMed()
                ))
            );
        if ( !ctt.getPdbx_database_id_DOI().isEmpty() ) {
        	URLCodec codec = new URLCodec("UTF-8");
        	String encodedResult = "";
			try {
				encodedResult = codec.encode(ctt.getPdbx_database_id_DOI(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
        	node.addProperty(PDBO.citation.link_to_doi,
              model.createResource(String.format(
                  "https://doi.org/%s", encodedResult
                ))
            );
        }
      }

    if ( entry.getStruct_refs() != null )
      for (StructRef srr : entry.getStruct_refs()) {
        Resource node = model.createResource(
            PDBO.struct_ref.getURI(PDB_id, srr.getId()),
            PDBO.struct_ref.resource()
          );
        uri.addProperty(GLYCONAVI.TCarpEntry.has_struct_ref, node);
        node.addProperty(PDBO.struct_ref.id, srr.getId());
        node.addProperty(PDBO.struct_ref.db_name, srr.getDb_name());
        node.addProperty(PDBO.struct_ref.db_code, srr.getDb_code());
        node.addProperty(PDBO.struct_ref.pdbx_db_isoform, srr.getPdbx_db_isoform());
      }

    if ( entry.getExptls() != null )
      for (Exptl expp : entry.getExptls()) {
        Resource node = model.createResource(
            PDBO.exptl.getURI(PDB_id, expp.getEntry_id(), expp.getMethod()),
            PDBO.exptl.resource()
          );
        uri.addProperty(GLYCONAVI.TCarpEntry.has_exptl, node);
        node.addProperty(PDBO.exptl.entry_id, expp.getEntry_id());
        node.addProperty(PDBO.exptl.method, expp.getMethod());
      }

    if ( entry.getEntity_src_nats() != null )
      for (EntitySrcNat entt : entry.getEntity_src_nats()) {
        Resource node = model.createResource(
            PDBO.entity_src_nat.getURI(PDB_id, entt.getEntity_id(), entt.getPdbx_src_id()),
            PDBO.entity_src_nat.resource()
          );
        uri.addProperty(GLYCONAVI.TCarpEntry.has_entity_src_nat, node);

        if (entt.getCommon_name().length() > 1) {
        	node.addProperty(PDBO.entity_src_nat.common_name, entt.getCommon_name().substring(0, 1).toUpperCase() + entt.getCommon_name().substring(1).toLowerCase());
        }
        else {
        	node.addProperty(PDBO.entity_src_nat.common_name, entt.getCommon_name());
        }


        if (entt.getPdbx_organism_scientific().length() > 1) {
        	node.addProperty(PDBO.entity_src_nat.pdbx_organism_scientific, entt.getPdbx_organism_scientific().substring(0, 1).toUpperCase() + entt.getPdbx_organism_scientific().substring(1).toLowerCase());
        }
        else {
        	node.addProperty(PDBO.entity_src_nat.pdbx_organism_scientific, entt.getPdbx_organism_scientific());
        }

        node.addProperty(PDBO.entity_src_nat.pdbx_ncbi_taxonomy_id, entt.getPdbx_ncbi_taxonomy_id());
        node.addProperty(PDBO.entity_src_nat.genus, entt.getGenus());
        node.addProperty(PDBO.entity_src_nat.species, entt.getSpecies());

        if (entt.getTissue().length() > 1) {
            node.addProperty(PDBO.entity_src_nat.tissue, entt.getTissue().substring(0, 1).toUpperCase() + entt.getTissue().substring(1).toLowerCase());
        }
        else {
        	node.addProperty(PDBO.entity_src_nat.tissue, entt.getTissue());
        }

        node.addProperty(PDBO.entity_src_nat.strain, entt.getStrain());
        node.addProperty(PDBO.entity_src_nat.pdbx_cell_line, entt.getPdbx_cell_line());
      }

    if ( entry.getValidationReport() != null ) {
      ValidationReport vall = entry.getValidationReport();
      Resource node = model.createResource(
          GLYCONAVI.AtomSiteValidation.getURI(PDB_id),
          GLYCONAVI.AtomSiteValidation.resource()
        );
      uri.addProperty(GLYCONAVI.TCarpEntry.has_validation, node);
      vall.getAtomRepo().forEach(x -> {
        node.addProperty(GLYCONAVI.AtomSiteValidation.has_atom_report, x);
      });
      vall.getBondRepo().forEach(x -> {
        node.addProperty(GLYCONAVI.AtomSiteValidation.has_bond_report, x);
      });
      vall.getSugarRepo().forEach(x -> {
        node.addProperty(GLYCONAVI.AtomSiteValidation.has_sugar_report, x);
      });

    }

  }

  /**
   * Adds protein db info
   * @param model
   * @param uri Resource for TCarpEntry
   */
  private void addProtein_db(Model model, Resource uri) {
    if ( entry.getProtein_db() == null)
      return;

    for (Proteindb db : entry.getProtein_db()) {
      Resource res = model.createResource(
          GLYCONAVI.Strand.getURI(entry.getPDB_id(), db.getStrand_id())
        );

      uri.addProperty(GLYCONAVI.TCarpEntry.has_protein_db, res);
      uri.addProperty(GLYCONAVI.TCarpEntry.has_strand, res);

      EntityPolyType ept = EntityPolyType.lookUp(db.getType());
      if (ept != null) {
        res.addProperty(RDF.type, GLYCONAVI.Strand.forEntityPolyType(ept));
      }

      // TODO: add dbname to Protein_db
      if ( ept != null && ept.isPeptide() ) {
        res.addProperty(DCTerms.identifier, db.getId());
        if ( db.getDb_name() != null )
          res.addProperty(GLYCONAVI.Strand.has_db_name, db.getDb_name());
      }

      res.addProperty(GLYCONAVI.Strand.has_strand_id, db.getStrand_id());
      res.addProperty(GLYCONAVI.Strand.has_entity_id, db.getEntity_id());
      res.addProperty(GLYCONAVI.Strand.has_sequence,
          db.getPdbx_seq_one_letter_code_can().replace("\n", ""));
    }
  }

  private void addGlycan(Model model, Resource uri) {
    if ( entry.getGlyco_entries() == null )
      return;

    String PDB_id = entry.getPDB_id();

    for (GlycoEntry ge : entry.getGlyco_entries()) {
      String modelId = ge.getModelId();
      if (modelId == null) {
        continue;
      }

      Resource node = model.createResource(
          GLYCONAVI.PDBModelGlycan.getURI(PDB_id, modelId),
          GLYCONAVI.PDBModelGlycan.resource()
        );

      uri.addProperty(GLYCONAVI.TCarpEntry.has_PDB_model_glycan, node);

      node.addProperty(DCTerms.identifier, modelId);

      ge.getEntity_ids().forEach(x ->
          node.addProperty(GLYCONAVI.PDBModelGlycan.has_entity_id, x)
      );
      ge.getAsym_ids().forEach(x ->
          node.addProperty(GLYCONAVI.PDBModelGlycan.has_asym_id, x)
      );

      // It will be overwritten when the same monosaccharide is contained
      ge.getChem_comp_ids().forEach(x ->
          node.addProperty(SIO.has_component_part, model.createResource(PDBO.getCCURI(x)))
      );

      // For GlycosylationSite
      if ( ge.getGlycosylationSite() != null ) {
        GlycosylationSite gSite = ge.getGlycosylationSite();
        addGlycosylationSite(model, node, PDB_id, modelId, gSite);
      }

      // For models
      ge.getModels().forEach(x -> {
        Resource nodeModel = populatePDBModel(model, modelId, x);
        node.addProperty(GLYCONAVI.PDBModelGlycan.has_model, nodeModel);
      });
    }
  }

  private void addGlycosylationSite(Model model, Resource node, String PDB_id, String modelId, GlycosylationSite gSite) {
    Resource nodeGSite = model.createResource(
        GLYCONAVI.GlycosylationSite.getURI(PDB_id, modelId),
        GLYCONAVI.GlycosylationSite.resource()
      );
    node.addProperty(GLYCONAVI.PDBModelGlycan.has_glycosylation_site, nodeGSite);

    // GlycanType
    String gType = gSite.getGlycanTypeSymbol();
    Resource nodeGType = model.createResource(
        GLYCONAVI.GlycanType.getURI(gType),
        GLYCONAVI.GlycanType.resource()
      );
    nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.has_glycan_type, nodeGType);

    nodeGType.addProperty(RDFS.label, gType+"-Glycan");

    nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.has_asym_id, gSite.getAsymId());
    nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.has_entity_id, gSite.getEntityId());
    nodeGSite.addProperty(
        GLYCONAVI.GlycosylationSite.has_seq_id,
        model.createTypedLiteral(gSite.getSeqId())
      );
    nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.has_comp_id, gSite.getCompId());
    nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.has_atom_id, gSite.getAtomId());

    // Strand
    Resource nodeStrand = model.createResource(
        GLYCONAVI.Strand.getURI(entry.getPDB_id(), gSite.getStrandId())
      );
    nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.is_on_strand, nodeStrand);

    // residue info
    if ( gSite.getModResidueId() != null ) {
      Resource nodeModRes = model.createResource(
          PDBO.pdbx_struct_mod_residue.getURI(entry.getPDB_id(), gSite.getModResidueId())
        );
      nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.is_mod_residue, nodeModRes);
    }

    // alpha-helix info: struct_conf
    if ( gSite.getHelixInfo() != null ) {
      StructConf sc = gSite.getHelixInfo();
      nodeGSite.addProperty(
          GLYCONAVI.GlycosylationSite.has_helix_info,
          PDBOResourceConverter.toRDFResource(model, PDB_id, sc, sc.getId())
       );
    }
    // beta-sheet info: struct_sheet_range
    if ( gSite.getSheetInfo() != null ) {
      StructSheetRange sr = gSite.getSheetInfo();
      nodeGSite.addProperty(
          GLYCONAVI.GlycosylationSite.has_sheet_info,
          PDBOResourceConverter.toRDFResource(model, PDB_id, sr, sr.getId()+","+sr.getSheet_id())
        );
    }

    // sequence
    nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.has_one_letter_code, gSite.getOneLetterCode());
    nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.has_sequence_before, gSite.getSeqBefore());
    nodeGSite.addProperty(GLYCONAVI.GlycosylationSite.has_sequence_after, gSite.getSeqAfter());
  }

  private Resource populatePDBModel(Model model, String modelId, ModelCassette x) {
    String PDB_id = entry.getPDB_id();
    String model_id = x.pdbx_PDB_model_num;
    if ( x.label_alt_id != null )
      model_id += ","+x.label_alt_id;

    Resource nodeModel = model.createResource(
        GLYCONAVI.PDBModel.getURI(PDB_id, modelId, model_id),
        GLYCONAVI.PDBModel.resource()
      );

    nodeModel.addProperty(GLYCONAVI.PDBModel.has_pdbx_PDB_model_num, x.pdbx_PDB_model_num);
    if ( x.label_alt_id != null )
      nodeModel.addProperty(GLYCONAVI.PDBModel.has_label_alt_id, x.label_alt_id);
    nodeModel.addProperty(GLYCONAVI.PDBModel.has_ctfile, x.CTfile);
    nodeModel.addProperty(GLYCONAVI.PDBModel.has_mmCIF, x.mmCIF.replaceAll("\n", "\\\n")); // added. 6.Jan.2023
    nodeModel.addProperty(GLYCONAVI.PDBModel.has_mmJSON, x.mmJSON); // added. 6.Jan.2023

    x.saccharides.forEach( y -> {
      if ( y.WURCS == null || y.WURCS.isEmpty() )
        return;
      addSaccharide(model, nodeModel, y);
    });

    // PDBAtomList
    Resource nodeAtoms = model.createResource(
        GLYCONAVI.PDBAtomList.getURI(PDB_id, modelId, x.pdbx_PDB_model_num),
        GLYCONAVI.PDBAtomList.resource()
      );
    nodeModel.addProperty(GLYCONAVI.PDBModel.has_atom_list, nodeAtoms);
    for ( Atom atom : x.atoms ) {
      // Ignore dummy atom

      if ( atom.getAtom_site_id() == null )
        continue;

      Resource nodeAtomSite = model.createResource(
          PDBO.atom_site.getURI(PDB_id, atom.getAtom_site_id()),
          PDBO.atom_site.resource()
        );
      nodeAtoms.addProperty(GLYCONAVI.PDBAtomList.has_atom, nodeAtomSite);

      nodeAtomSite.addProperty(PDBO.atom_site.id, atom.getAtom_site_id());

      if ( atom.getAtom_site_pdbx_formal_charge() != null )
        nodeAtomSite.addProperty(
            PDBO.atom_site.pdbx_formal_charge,
            model.createTypedLiteral(
                Double.valueOf( atom.getAtom_site_pdbx_formal_charge() )
              )
          );
      if ( atom.getAtom_site_occupancy() != null )
        nodeAtomSite.addProperty(
            PDBO.atom_site.occupancy,
            model.createTypedLiteral(atom.getAtom_site_occupancy())
          );
      if ( atom.getAtom_site_B_iso_or_equiv() != null )
        nodeAtomSite.addProperty(
            PDBO.atom_site.B_iso_or_equiv,
            model.createTypedLiteral(atom.getAtom_site_occupancy())
          );
    }

    for (NearbyComponent ncc : x.interacting_components) {
      AtomAtomDistance a = ncc.getNearestEntry();

      Resource nodeInteraction = model.createResource(
          GLYCONAVI.Interaction.getURI(PDB_id, modelId, x.pdbx_PDB_model_num,
              a.getSource().getAtom_site_id(), a.getTarget().getAtom_site_id()),
          GLYCONAVI.Interaction.resource()
        );
      nodeModel.addProperty(GLYCONAVI.PDBModel.has_interaction, nodeInteraction);

      // Distance
      nodeInteraction.addProperty(
          GLYCONAVI.Interaction.has_distance,
          model.createTypedLiteral((Double)a.distance())
        );

      // Source atom
      Resource node = populateAtom(model, PDB_id, a.getSource());
      nodeInteraction.addProperty(GLYCONAVI.Interaction.has_source_atom, node);

      // Nearest target atom
      node = populateAtom(model, PDB_id, a.getTarget());
      nodeInteraction.addProperty(GLYCONAVI.Interaction.has_nearest_target_atom, node);

      // alpha-helix info: struct_conf
      if ( ncc.getResidueInfo().getStruct_conf() != null ) {
        StructConf sc = ncc.getResidueInfo().getStruct_conf();
        nodeInteraction.addProperty(
            GLYCONAVI.Interaction.has_helix_info,
            // http://rdf.wwpdb.org/pdb/{PDB_id}/stuct_conf/{struct_conf.id}
            PDBOResourceConverter.toRDFResource(model, PDB_id, sc, sc.getId())
          );
      }
      // beta-sheet info: struct_sheet_range
      if ( ncc.getResidueInfo().getStruct_sheet_range() != null ) {
        StructSheetRange sr = ncc.getResidueInfo().getStruct_sheet_range();
        nodeInteraction.addProperty(
            GLYCONAVI.Interaction.has_sheet_info,
            // http://rdf.wwpdb.org/pdb/{PDB_id}/stuct_sheet_range/{struct_sheet_range.id},{struct_sheet_range.sheet_id}
            PDBOResourceConverter.toRDFResource(model, PDB_id, sr, sr.getId()+","+sr.getSheet_id())
          );
      }

      // TODO: consider structure info for target residue
/*
      {
        ComponentInfo cii = ncc.getResidueInfo();
        Resource bn = model.createResource();
        nodeInteraction.addProperty(GLYCONAVI.target_component, bn);

        bn.addProperty(GLYCONAVI.atom_site_pdbx_PDB_model_num,
            cii.getAtom_site_pdbx_PDB_model_num());
        bn.addProperty(GLYCONAVI.atom_site_label_comp_id, cii.getAtom_site_label_comp_id());
        bn.addProperty(GLYCONAVI.atom_site_label_asym_id, cii.getAtom_site_label_asym_id());
        bn.addProperty(GLYCONAVI.atom_site_label_entity_id, cii.getAtom_site_label_entity_id());
        bn.addProperty(GLYCONAVI.atom_site_label_seq_id, cii.getAtom_site_label_seq_id());
        bn.addProperty(GLYCONAVI.atom_site_pdbx_PDB_ins_code,
          cii.getAtom_site_pdbx_PDB_ins_code());

        cii.getStructureInfo().forEach(y -> {
          bn.addProperty(GLYCONAVI.structure_info, y);
        });
      }
*/
    }

    return nodeModel;
  }

  private Resource populateAtom(Model model, String PDB_id, Atom atom) {
    Resource nodeAtomSite = model.createResource(
        PDBO.atom_site.getURI(PDB_id, atom.getAtom_site_id()),
        PDBO.atom_site.resource()
      );

    if (atom.getAtom_site_label_comp_id() != null)
      nodeAtomSite.addProperty(
          PDBO.atom_site.label_comp_id,
          atom.getAtom_site_label_comp_id()
        );

    if (atom.getAtom_site_label_alt_id() != null)
      nodeAtomSite.addProperty(
          PDBO.atom_site.label_alt_id,
          atom.getAtom_site_label_alt_id()
        );

    if (atom.getAtom_site_pdbx_PDB_ins_code() != null)
      nodeAtomSite.addProperty(
          PDBO.atom_site.pdbx_PDB_ins_code,
          atom.getAtom_site_pdbx_PDB_ins_code()
        );

    if (atom.getAtom_site_pdbx_PDB_model_num() != null)
      nodeAtomSite.addProperty(
          PDBO.atom_site.pdbx_PDB_model_num,
          atom.getAtom_site_pdbx_PDB_model_num()
        );

    if (atom.getAtom_site_id() != null)
      nodeAtomSite.addProperty(PDBO.atom_site.id, atom.getAtom_site_id());

    if (atom.getAtom_site_label_atom_id() != null)
      nodeAtomSite.addProperty(
          PDBO.atom_site.label_atom_id,
          atom.getAtom_site_label_atom_id()
        );

    if (atom.getAtom_site_label_asym_id() != null)
      nodeAtomSite.addProperty(
          PDBO.atom_site.label_asym_id,
          atom.getAtom_site_label_asym_id()
        );

    if (atom.getAtom_site_label_seq_id() != null)
      nodeAtomSite.addProperty(
          PDBO.atom_site.label_seq_id,
          atom.getAtom_site_label_seq_id()
        );

    if (atom.getAtom_site_label_entity_id() != null)
      nodeAtomSite.addProperty(
          PDBO.atom_site.label_entity_id,
          atom.getAtom_site_label_entity_id()
        );

    return nodeAtomSite;
  }

  private void addSaccharide(Model model, Resource uri, ModelCassette.Saccharide saccharide ) {
    String encodedWURCS;
    try {
      encodedWURCS = StringUtils.urlEncode(saccharide.WURCS);
    } catch (UnsupportedEncodingException e) {
      logger.error("Error in WURCS encoding: "+saccharide.WURCS, e);
      logger.error("Saccharide class for WURCS cannot be created.");
      return;
    }

    Resource node = model.createResource(
        GLYCONAVI.Saccharide.getURI(encodedWURCS),
        GLYCORDF.Saccharide
      );

    uri.addProperty(GLYCORDF.has_glycan, node);

    if ( saccharide.GTC != null )
      node.addProperty(GLYCONAVI.Saccharide.has_glytoucan_id, saccharide.GTC);

    node.addProperty(RDF.type, GLYCONAVI.Saccharide.getWURCSType(saccharide.type));

    if ( saccharide.aglycones != null )
      for ( String aglycone : saccharide.aglycones )
        node.addProperty(GLYCONAVI.Saccharide.has_aglycone, aglycone);

    // Skip add properties if node for WURCS has been created already
    if ( node.getProperty(GLYCORDF.has_glycosequence) != null )
      return;

    Resource nodeGseq = model.createResource(
        GLYCONAVI.Glycosequence.getURI(encodedWURCS),
        GLYCORDF.Glycosequence
      );
    node.addProperty(GLYCORDF.has_glycosequence, nodeGseq);

    nodeGseq.addProperty(GLYCORDF.has_sequence, saccharide.WURCS);
    nodeGseq.addProperty(GLYCORDF.in_carbohydrate_format, GLYCORDF.carbohydrate_format_wurcs);

    nodeGseq.addProperty(GLYCONAVI.Glycosequence.has_SHA512, StringUtils.getSHA512(saccharide.WURCS));
  }

  private void addBranchEntries(Model model, Resource uri) {
    if ( this.entry.getBranchEntries() == null )
      return;

    String PDB_id = entry.getPDB_id();
    for (BranchEntry branch : this.entry.getBranchEntries() ) {
      String entryId = branch.getEntity_id();
      if ( branch.getModelId() != null )
        entryId += ","+branch.getModelId();

      if (entryId == null) {
        continue;
      }

      Resource nodeBranch = model.createResource(
          GLYCONAVI.BranchEntry.getURI(PDB_id, entryId),
          GLYCONAVI.BranchEntry.resource()
        );
      uri.addProperty(GLYCONAVI.TCarpEntry.has_branch_entry, nodeBranch);

      nodeBranch.addProperty(GLYCONAVI.BranchEntry.has_entity_id, branch.getEntity_id());
      if ( branch.getModelId() != null )
        nodeBranch.addProperty(GLYCONAVI.BranchEntry.has_model_id, branch.getModelId());

      if ( branch.getLinkages() != null )
        for ( PdbxEntityBranchLink linkage : branch.getLinkages() ) {
          Resource linkNode = PDBOResourceConverter.toRDFResource(
                  model, this.entry.getPDB_id(), linkage, linkage.getLink_id()
                );
          nodeBranch.addProperty(GLYCONAVI.BranchEntry.has_linkage, linkNode);
        }

      if ( branch.getResidues() != null )
        for ( PdbxEntityBranchList residue : branch.getResidues() ) {
          String id = residue.getNum()+","+residue.getComp_id()+","+residue.getEntity_id();
          Resource resNode = PDBOResourceConverter.toRDFResource(
                  model, this.entry.getPDB_id(), residue, id
                );
            nodeBranch.addProperty(GLYCONAVI.BranchEntry.has_residue, resNode);
         }

      if ( branch.getGlycosylationConn() != null ) {
        StructConn conn = branch.getGlycosylationConn();
        Resource connNode = PDBOResourceConverter.toRDFResource(
                model, this.entry.getPDB_id(), conn, conn.getId()
              );
        nodeBranch.addProperty(GLYCONAVI.BranchEntry.has_glycosylation_struct_conn, connNode);
      }

      if ( branch.getCtFile() != null )
        nodeBranch.addProperty(GLYCONAVI.BranchEntry.has_ctfile, branch.getCtFile());
      // added. 25.Nov.2022
      if ( branch.getMMCIF() != null )
          nodeBranch.addProperty(GLYCONAVI.BranchEntry.has_mmCIF, branch.getMMCIF());
      // added. 25.Nov.2022
      if ( branch.getMMJSON() != null )
    	  nodeBranch.addProperty(GLYCONAVI.BranchEntry.has_mmJSON, branch.getMMJSON());

      if ( branch.getWurcs() != null && !branch.getWurcs().isEmpty() ) {
        ModelCassette.Saccharide saccharide = new ModelCassette.Saccharide();
        saccharide.WURCS = branch.getWurcs();
        saccharide.type = WURCSType.STD;
        addSaccharide(model, nodeBranch, saccharide);
//        branchNode.addProperty(GLYCONAVI.branch_entry.has_wurcs, branch.getWurcs());
      }

      {
        Resource nodeValidation = model.createResource(
            GLYCONAVI.BranchEntry.Validation.getURI(PDB_id, entryId),
            GLYCONAVI.BranchEntry.Validation.resource()
          );

        nodeBranch.addProperty(GLYCONAVI.BranchEntry.has_validation, nodeValidation);

        branch.getValidation().get("errors").forEach(x -> {
            nodeValidation.addProperty(GLYCONAVI.BranchEntry.Validation.has_error, x);
        });
        branch.getValidation().get("warnings").forEach(x -> {
            nodeValidation.addProperty(GLYCONAVI.BranchEntry.Validation.has_warning, x);
        });
      }

    }
  }
}

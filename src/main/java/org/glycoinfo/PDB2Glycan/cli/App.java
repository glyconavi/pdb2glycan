package org.glycoinfo.PDB2Glycan.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.glycoinfo.PDB2Glycan.util.WURCS2GlyTouCan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.regex.Pattern;

public class App {

  static final String APP_NAME = "PDB2Glycan";
  static final String APP_VERSION = "1.6.0";

  private static final Logger logger = LoggerFactory.getLogger(App.class);

  private static final String REGEX_PDB_CODE = "[0-9a-zA-Z]{4}";

  private static final int STATUS_PARSE_ERROR = 1;
  private static final int STATUS_ARGUMENT_ERROR = 2;
  private static final int STATUS_RUNTIME_ERROR = 3;

  public static final int NETWORK_ERROR_RETRY = 5;

  private static final String OPTION_NO_NETWORK_LONG = "no-network";

  // used if no-network specified
  private static final String OPTION_PDB_PATH_LONG = "pdb";
  private static final String OPTION_CC_PATH_LONG = "cc";
  private static final String OPTION_LOCAL_GLYTOUCAN_ID_LONG = "local-glytoucan";

  private static final String OPTION_FORCE_LONG = "force";
  private static final String OPTION_FORCE_SHORT = "f";

  private static final String OPTION_INPUT_FORMAT_LONG = "in";
  private static final String OPTION_INPUT_FORMAT_SHORT = "i";

  private static final String OPTION_OUTPUT_FORMAT_LONG = "out";
  private static final String OPTION_OUTPUT_FORMAT_SHORT = "o";

  private static final String OPTION_OUTPUT_PATH_LONG = "dir";
  private static final String OPTION_OUTPUT_PATH_SHORT = "d";

  private static final String OPTION_NOZIP_LONG = "nozip";

  private static final String[] ARGUMENTS_INPUT_FORMAT = {"mmcif", "mmjson"};
  private static final String[] ARGUMENTS_OUTPUT_FORMAT = {"json", "jsonld", "turtle"};


  private static final String OPTION_FOR_ONEDEP_LONG = "for-onedep";

  // used if for-onedep specified
  private static final String OPTION_INPUT_PATH_DIRECT_LONG = "input";
  private static final String OPTION_OUTPUT_PATH_DIRECT_LONG = "output";



  public static void main(String[] args) {
    new App().run(args);
  }

  void run(String[] args) {
    CommandLineParser parser = new DefaultParser();
    Converter converter = getConverter();

    try {
      CommandLine cmd = parser.parse(options(), args);

      if (cmd.hasOption("help")) {
        printUsage();
        return;
      }

      // For for-onedep option
      if ( cmd.hasOption(OPTION_FOR_ONEDEP_LONG) ) {
        if (!cmd.hasOption(OPTION_INPUT_PATH_DIRECT_LONG) || !cmd.hasOption(OPTION_OUTPUT_PATH_DIRECT_LONG) || !cmd.hasOption(OPTION_CC_PATH_LONG)) {
          throw new IllegalArgumentException(
              String.format("all of --%s <FILE>, --%s <FILE> and --%s <DIRECTORY> are required",
                    OPTION_INPUT_PATH_DIRECT_LONG, OPTION_OUTPUT_PATH_DIRECT_LONG, OPTION_CC_PATH_LONG));
        }

        converter
          .forOneDep(true)
          .inputFormat(InputFormat.lookup(ARGUMENTS_INPUT_FORMAT[0])) // force mmcif format
          .outputFormat(OutputFormat.lookup(ARGUMENTS_OUTPUT_FORMAT[0])) // force json format
          .inputFilePathDirect(cmd.getOptionValue(OPTION_INPUT_PATH_DIRECT_LONG))
          .outputFilePathDirect(cmd.getOptionValue(OPTION_OUTPUT_PATH_DIRECT_LONG))
          .ccPath(cmd.getOptionValue(OPTION_CC_PATH_LONG))
          .noZip(true); // force no-zip

        WURCS2GlyTouCan.setUseNetwork(false);
      } else {

        converter
          .inputFormat(InputFormat.lookup(cmd.getOptionValue(OPTION_INPUT_FORMAT_LONG)))
          .outputFormat(OutputFormat.lookup(cmd.getOptionValue(OPTION_OUTPUT_FORMAT_LONG)))
          .outputFilePath(cmd.getOptionValue(OPTION_OUTPUT_PATH_LONG))
          .pdbCode(cmd.getArgList().get(0))
          .noZip(cmd.hasOption(OPTION_NOZIP_LONG));

        // For no-network option
        if (cmd.hasOption(OPTION_NO_NETWORK_LONG)) {
          if (!cmd.hasOption(OPTION_PDB_PATH_LONG) || !cmd.hasOption(OPTION_CC_PATH_LONG)) {
            throw new IllegalArgumentException(
                String.format("all of --%s <DIRECTORY> and --%s <DIRECTORY> are required",
                    OPTION_PDB_PATH_LONG, OPTION_CC_PATH_LONG));
          }

          converter
            .noNetwork(true)
            .pdbPath(cmd.getOptionValue(OPTION_PDB_PATH_LONG))
            .ccPath(cmd.getOptionValue(OPTION_CC_PATH_LONG));

          WURCS2GlyTouCan.setUseNetwork(false);
          if ( cmd.hasOption(OPTION_LOCAL_GLYTOUCAN_ID_LONG) )
            WURCS2GlyTouCan.load(Paths.get(cmd.getOptionValue(OPTION_LOCAL_GLYTOUCAN_ID_LONG)));
        }

        if (cmd.getArgList().size() < 1) {
          if (!Pattern.matches(REGEX_PDB_CODE, cmd.getArgList().get(0))) {
            throw new IllegalArgumentException("Invalid PDB code.");
          }

          throw new IllegalArgumentException("No PDB code given.");
        }

      }

      converter
          .force(cmd.hasOption(OPTION_FORCE_LONG))
          .run();
    } catch (NumberFormatException e) {
      // To catch errors in parsing numbers not as IllegalArgumentException
      logger.error(e.getMessage(), e);
      System.exit(STATUS_RUNTIME_ERROR);
    } catch (ParseException e) {
      System.err.println("Parse Error: " + e.getMessage() + "\n");
      printUsage();
      System.exit(STATUS_PARSE_ERROR);
    } catch (IllegalArgumentException e) {
      System.err.println("Argument Error: " + e.getMessage() + "\n");
      printUsage();
      System.exit(STATUS_ARGUMENT_ERROR);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_RUNTIME_ERROR);
    }
  }

  Converter getConverter() {
    return new Converter();
  }

  private void printUsage() {
    String syntax = String
        .format("%s "
                + "[--%s --%s <DIRECTORY> --%s <DIRECTORY> [--%s <FILE>]]"
                + " [--%s] [--%s <DIRECTORY>]"
                + " --%s <FORMAT> --%s <FORMAT> <PDB CODE>"
                + "\nor\n"
                + "%s --%s --%s <FILE> --%s <FILE> --%s <DIRECTORY>",
            APP_NAME,
            OPTION_NO_NETWORK_LONG,
            OPTION_PDB_PATH_LONG,
            OPTION_CC_PATH_LONG,
            OPTION_LOCAL_GLYTOUCAN_ID_LONG,
            OPTION_FORCE_LONG,
            OPTION_OUTPUT_PATH_LONG,
            OPTION_INPUT_FORMAT_LONG,
            OPTION_OUTPUT_FORMAT_LONG,
            APP_NAME,
            OPTION_FOR_ONEDEP_LONG,
            OPTION_INPUT_PATH_DIRECT_LONG,
            OPTION_OUTPUT_PATH_DIRECT_LONG,
            OPTION_CC_PATH_LONG
        );
    String header = "\nOptions:";
    String footer = String.format("\nVersion: %s", APP_VERSION);

    HelpFormatter hf = new HelpFormatter();

    hf.printHelp(new PrintWriter(System.err, true), HelpFormatter.DEFAULT_WIDTH, syntax,
        header, options(), HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, footer);
  }

  private Options options() {
    Options options = new Options();

    options.addOption(Option.builder()
        .longOpt(OPTION_NO_NETWORK_LONG)
        .desc(String.format(
            "Use local data, all of --%s <DIRECTORY> and --%s <DIRECTORY> --%s <FILE> are required",
            OPTION_PDB_PATH_LONG, OPTION_CC_PATH_LONG, OPTION_LOCAL_GLYTOUCAN_ID_LONG))
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_CC_PATH_LONG)
        .desc("Set path to the directory where PDB Chemical Component Dictionary is placed")
        .hasArg()
        .argName("DIRECTORY")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_PDB_PATH_LONG)
        .desc("Set path to the directory where PDB entry is placed")
        .hasArg()
        .argName("DIRECTORY")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_LOCAL_GLYTOUCAN_ID_LONG)
        .desc("Set path to the file of local mappings for WURCS to GlyTouCan ID")
        .hasArg()
        .argName("FILE")
        .build()
    );

    options.addOption(Option.builder(OPTION_FORCE_SHORT)
        .longOpt(OPTION_FORCE_LONG)
        .desc("Overwrite existing file")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_NOZIP_LONG)
        .desc("Do not zip output file")
        .build()
    );

    options.addOption(Option.builder(OPTION_INPUT_FORMAT_SHORT)
        .longOpt(OPTION_INPUT_FORMAT_LONG)
        .desc("Set input format, required")
        .hasArg()
        .argName("FORMAT")
        .argName("FORMAT=[" + String.join("|", ARGUMENTS_INPUT_FORMAT) + "]")
//        .required()
        .build()
    );

    options.addOption(Option.builder(OPTION_OUTPUT_FORMAT_SHORT)
        .longOpt(OPTION_OUTPUT_FORMAT_LONG)
        .desc("Set output format, required")
        .hasArg()
        .argName("FORMAT")
        .argName("FORMAT=[" + String.join("|", ARGUMENTS_OUTPUT_FORMAT) + "]")
//        .required()
        .build()
    );

    options.addOption(Option.builder(OPTION_OUTPUT_PATH_SHORT)
        .longOpt(OPTION_OUTPUT_PATH_LONG)
        .desc("Set path to output directory")
        .hasArg()
        .argName("DIRECTORY")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_FOR_ONEDEP_LONG)
        .desc(String.format(
            "Output simple results for OneDep system,"
            + " all of --%s <FILE>, --%s <FILE> and --%s <DIRECTORY> are required,"
            + " the other options are ignored even if specified",
            OPTION_INPUT_PATH_DIRECT_LONG, OPTION_OUTPUT_PATH_DIRECT_LONG, OPTION_CC_PATH_LONG))
            .build()
        );

    options.addOption(Option.builder()
        .longOpt(OPTION_INPUT_PATH_DIRECT_LONG)
        .desc("Set path to input PDB file, allowed only mmCIF format")
        .hasArg()
        .argName("FILE")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_OUTPUT_PATH_DIRECT_LONG)
        .desc("Set path to output JSON file")
        .hasArg()
        .argName("FILE")
        .build()
    );

    options.addOption(Option.builder("h")
        .longOpt("help")
        .desc("Show usage help")
        .build()
    );

    return options;
  }
}

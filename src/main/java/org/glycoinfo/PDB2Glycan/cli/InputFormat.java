package org.glycoinfo.PDB2Glycan.cli;

public enum InputFormat {
  MMJSON("mmjson", "json", "/pdbjplus/data/pdb/mmjson/"),
  MMCIF("mmcif", "cif", "/mmcif/");

  private String name;
  private String ext;
  private String path;

  private InputFormat(String name, String ext, String path) {
    this.name = name;
    this.ext = ext;
    this.path = path;
  }

  public String getName() {
    return this.name;
  }

  public String getExtension() {
    return this.ext;
  }

  public String getNetworkPath() {
    return this.path;
  }

  public static InputFormat lookup(String extension) {
    for (InputFormat fmt : InputFormat.values()) {
      if (fmt.name.equals(extension))
        return fmt;
      if (fmt.ext.equals(extension))
        return fmt;
    }
    throw new IllegalArgumentException("Unknown format");
  }

  /*
  public String fileName(String code) {
    switch (this) {
      case JSON:
        return String.format("%s.json.gz", code.toLowerCase());
      case JSONLD:
        return String.format("%s.jsonld.gz", code.toLowerCase());
      case RDF_TURTLE:
          return String.format("%s.ttl.gz", code.toLowerCase());
      default:
        return code.toLowerCase();
    }
  }*/
  
}

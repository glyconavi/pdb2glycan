package org.glycoinfo.PDB2Glycan.cli;

import static java.lang.Thread.sleep;
import static org.glycoinfo.PDB2Glycan.cli.App.NETWORK_ERROR_RETRY;
import static org.glycoinfo.PDB2Glycan.io.file.Files.newBufferedWriter;
import static org.glycoinfo.PDB2Glycan.util.StringUtils.formatTime;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.net.ConnectException;
import java.net.URL;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.PdbJsonParser;
import org.glycoinfo.PDB2Glycan.io.PdbMMcifParser;
import org.glycoinfo.PDB2Glycan.io.file.ChemicalComponentLoader;
import org.glycoinfo.PDB2Glycan.io.file.Files;
import org.glycoinfo.PDB2Glycan.io.file.LocalChemicalComponentLoader;
import org.glycoinfo.PDB2Glycan.io.file.RemoteChemicalComponentLoader;
import org.glycoinfo.PDB2Glycan.io.model.Glycan;
import org.glycoinfo.PDB2Glycan.io.model.Protein;
import org.glycoinfo.PDB2Glycan.io.model.info.SoftwareInfo;
import org.glycoinfo.PDB2Glycan.structure.glyco.BranchEntryBuilder;
import org.glycoinfo.PDB2Glycan.structure.glyco.GlycoProtein;
import org.glycoinfo.PDB2Glycan.structure.glyco.ModelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Converter {

  private static final Logger logger = LoggerFactory.getLogger(Converter.class);

  private boolean noNetwork;

  private String ccPath;

  private String pdbPath;

  private boolean force;

  private OutputFormat outputFormat;

  private InputFormat inputFormat;

  private String outputFilePath;

  private String pdbCode;

  private boolean noZip;

  private boolean forOneDep;

  private String inputFilePath;

  private boolean outputToDir;

  public Converter() {
    this.outputFormat = OutputFormat.JSON;
    this.noNetwork = false;
    this.force = false;
    this.noZip = false;
    this.forOneDep = false;
    this.outputToDir = true;
  }

  public Converter noNetwork(boolean noNetwork) {
    this.noNetwork = noNetwork;
    return this;
  }

  public Converter ccPath(String ccPath) {
    this.ccPath = ccPath;
    return this;
  }

  public Converter pdbPath(String pdbPath) {
    this.pdbPath = pdbPath;
    return this;
  }

  public Converter force(boolean force) {
    this.force = force;
    return this;
  }

  public Converter outputFormat(OutputFormat format) {
    this.outputFormat = format;
    return this;
  }

  public Converter inputFormat(InputFormat format) {
    this.inputFormat = format;
    return this;
  }

  public Converter inputFilePathDirect(String inputFilePath) {
    this.inputFilePath = inputFilePath;
    return this;
  }

  public Converter outputFilePathDirect(String outputFilePath) {
    this.outputFilePath = outputFilePath;
    this.outputToDir = false;
    return this;
  }

  public Converter outputFilePath(String outputFilePath) {
    this.outputFilePath = outputFilePath;
    this.outputToDir = true;
    return this;
  }

  public Converter pdbCode(String pdbCode) {
    this.pdbCode = pdbCode;
    return this;
  }

  public Converter noZip(boolean noZip) {
    this.noZip = noZip;
    return this;
  }

  public Converter forOneDep(boolean forOneDep) {
    this.forOneDep = forOneDep;
    return this;
  }

  public void run() throws IOException, MMcifParseException {
    if (forOneDep) {
      if (this.ccPath == null) {
        throw new RuntimeException(
            "--cc <DIRECTORY> is required with for-OneDep mode.");
      }
    } else if (noNetwork) {
      if (this.pdbPath == null || this.ccPath == null) {
        throw new RuntimeException(
            "both of --pdb <DIRECTORY> and --cc <DIRECTORY> is required with no network mode.");
      }
    }

    if (noNetwork || forOneDep) {
      try (Reader reader = Files.newBufferedReader(getInputFile())) {
        process(reader, new LocalChemicalComponentLoader(ccPath, inputFormat));
      }
    } else {
      String inputFile = getInputFile().toString();
      // Replace wrong path separator for win
      if (inputFile.contains("\\"))
        inputFile = inputFile.replace("\\", "/");
      URL url = new URL("ftp://data.pdbj.org" + inputFile);

      logger.info("Load " + url.getFile() + " from " + url.getHost());

      for (int i = 1; ; i++) {
        try (Reader reader = Files.newBufferedReader(url)) {
          process(reader, new RemoteChemicalComponentLoader());
        } catch (ConnectException e) {
          if (i >= NETWORK_ERROR_RETRY) {
            throw e;
          }

          int wait = (int) Math.pow(2, i);

          logger.warn(e.getMessage());
          logger.warn(String.format("Retry after %d [s]", wait));

          try {
            sleep(wait * 1000);
          } catch (InterruptedException e2) {
            break;
          }
        }
        break;
      }
    }
  }

  private void process(Reader reader, ChemicalComponentLoader ccLoader)
      throws IOException, MMcifParseException {
    long start = System.currentTimeMillis();

    if ( this.inputFilePath != null )
        this.pdbCode = getInputFile().getName();
    logger.info("Start processing {}", this.pdbCode);

    Protein protein;
    if (this.inputFormat == InputFormat.MMCIF) {
//      protein = new PdbJsonParser(MMcifEncoder.getMMjsonString(reader)).parse();
      protein = new PdbMMcifParser().parse((BufferedReader)reader);
    } else {
      protein = new PdbJsonParser(reader).parse();
    }

    if ( this.pdbCode == null && protein.getPdbCode() != null )
      this.pdbCode = protein.getPdbCode().toLowerCase();

    Glycan jsonGlycan;
    if ( this.forOneDep ) {
      jsonGlycan = new Glycan();
      jsonGlycan.setPDB_id(protein.getPdbCode());
    } else {
      ModelBuilder builder = new ModelBuilder(protein, ccLoader);

      if ( ccLoader.saccharideIds().isEmpty() ) {
        logger.info(pdbCode + " has no glyco entries");
        return;
      }

      GlycoProtein gp = builder.build();
      jsonGlycan = gp.toJson();

      if (jsonGlycan.getGlyco_entries().isEmpty()) {
        logger.info(pdbCode + " has no glyco entries");
        return;
      }
    }

    // For Carbohydrate Remediation
    if ( !protein.getPdbxEntityBranch().isEmpty() ) {
      BranchEntryBuilder entryBuilder = new BranchEntryBuilder(protein, ccLoader);
      entryBuilder.setForOneDep(this.forOneDep);
      entryBuilder.build();
      jsonGlycan.setBranchEntries(entryBuilder.getBranchEntries());
    } else {
      String message = pdbCode + " has no pdbx_entity_branch category";
      if ( this.forOneDep )
        logger.warn(message+" requred for OneDep mode.");
      else
        logger.info(message);
    }

    long time = System.currentTimeMillis() - start;
    String timeFormated = formatTime(time);
    jsonGlycan.setTime(timeFormated);
    // modified(jodatime to DateTiemFormatter). 7.Dec.2022
    LocalDateTime dt = LocalDateTime.now();
    DateTimeFormatter dt_format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    jsonGlycan.setDate(dt.format(dt_format));

    SoftwareInfo info = new SoftwareInfo();
    info.setName(App.APP_NAME);
    info.setVersion(App.APP_VERSION);
    info.setProcess_time(time);
    jsonGlycan.setSoftwareInfo(info);

    try (BufferedWriter bw = newBufferedWriter(getOutputFile())) {
      outputFormat.output(bw, jsonGlycan, this.forOneDep);
    }

    logger.info(pdbCode + " finished in " + timeFormated);
  }

  private File getInputFile() {
    if ( this.inputFilePath != null )
      return new File(this.inputFilePath);

    String pdbPath = noNetwork ? this.pdbPath : this.inputFormat.getNetworkPath();
    String format = "%s."+this.inputFormat.getExtension();
    String path = Paths.get(pdbPath, String.format(format, pdbCode.toLowerCase())).toString();
    // Try to find .gz file if file not exists
    if (!new File(path).exists())
      path += ".gz";
    return new File(path);
  }

  private File getOutputFile() {
    File path;

    if (outputFilePath == null) {
      path = new File(".");
    } else {
      path = new File(this.outputFilePath);

      // mkdir if no parent directory
      createParentDir(path.getParentFile());

      if (!outputToDir) {
        if ( path.exists() && path.isDirectory() )
          throw new RuntimeException(path.getPath() + " is directory");
      }

      if (outputToDir && !path.exists())
        path.mkdir();
    }

    if (path.isDirectory()) {
      path = Paths.get(path.toString(), outputFormat.fileName(pdbCode.toLowerCase(), this.noZip)).toFile();
    }

    if (!force && path.exists()) {
      throw new RuntimeException(path.getPath() + " already exists");
    }

    return path;
  }

  private void createParentDir(File path) {
    if ( path == null || path.exists() )
      return;
    if ( path.getParentFile() != null ) {
      if ( !path.getParentFile().exists() || !path.getParentFile().isDirectory() )
        createParentDir(path.getParentFile());
    }

    path.mkdir();
  }
}

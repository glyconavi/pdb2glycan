package org.glycoinfo.PDB2Glycan.cli;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.jena.query.ARQ;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.glycoinfo.PDB2Glycan.io.model.Glycan;
import org.glycoinfo.PDB2Glycan.rdf.GlycanModelBuilder;
import org.glycoinfo.PDB2Glycan.rdf.vocabulary.GLYCONAVI;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public enum OutputFormat {

  JSON("json"),
  JSONLD("jsonld"),
  RDF_TURTLE("ttl,turtle");

  private List<String> ext;

  private OutputFormat(String ext) {
    this.ext = Arrays.asList(ext.split(","));
  }

  public static OutputFormat lookup(String extension) {
    for ( OutputFormat fmt : OutputFormat.values() ) {
      if (fmt.ext.contains(extension))
        return fmt;
    }
    throw new IllegalArgumentException("Unknown format");
  }

  public String fileName(String code, boolean noZip) {
    String file = String.format("%s", code.toLowerCase());
    file += "."+this.ext.get(0);
    file += (noZip)? "" : ".gz";
    return file;
  }

  public void output(BufferedWriter out, Glycan json, boolean nonNull) throws IOException {
    ObjectMapper mapper;
    ObjectWriter writer;
    GlycanModelBuilder rdfBuilder;
    Model model;

    switch (this) {
      case JSON:
        mapper = new ObjectMapper();
        if ( nonNull )
          mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        writer = mapper.writer(new DefaultPrettyPrinter());
        writer.writeValue(out, json);
        break;
      case JSONLD:
        rdfBuilder = new GlycanModelBuilder();
        model = rdfBuilder.entry(json).build();

        ARQ.init();
        RDFDataMgr.write(out, model, RDFFormat.JSONLD_PRETTY);
        break;
      case RDF_TURTLE:
        rdfBuilder = new GlycanModelBuilder();
        model = rdfBuilder.entry(json).build();
        model.write(out, "TURTLE", GLYCONAVI.TCarpEntry.getURI());
        break;
    }
  }
}

package org.glycoinfo.PDB2Glycan.cli;

//import java.io.IOException;
//import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
//import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyBoolean;
//import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.spy;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;

class AppTest {

  //private App testClass = spy(new App());

  @Mock
  private Converter converter = mock(Converter.class);

  /*
  @BeforeEach
  void setUp() {
    // return mocked converter
    when(testClass.getConverter()).thenReturn(converter);

    // return self object when called builder method
    when(converter.outputFilePath(anyString())).thenReturn(converter);
    when(converter.noNetwork(anyBoolean())).thenReturn(converter);
    when(converter.ccPath(anyString())).thenReturn(converter);
    when(converter.pdbPath(anyString())).thenReturn(converter);
    when(converter.outputFormat(any(OutputFormat.class))).thenReturn(converter);
    when(converter.pdbCode(anyString())).thenReturn(converter);
  }
  */
  /*
  @Test
  void runWithShortOptions() throws Exception {
    String[] args = {
        "-o", "/path/to/output",
        "-f", "json",
        "1234"
    };

    testClass.run(args);

    verify(converter).outputFilePath("/path/to/output");
    verify(converter).outputFormat(OutputFormat.JSON);
    verify(converter).pdbCode("1234");
    verify(converter).run();
  }
  */

  @Test
  void runWithLongOptions() throws Exception {
    /*
    String[] args = {
        "--output", "/path/to/output",
        "--format", "turtle",
        "abcd"
    }; 
    testClass.run(args);
   */

    converter.inputFormat(InputFormat.MMJSON);
    //verify(converter).outputFilePath("/path/to/output");
    converter.outputFormat(OutputFormat.RDF_TURTLE);
    converter.pdbCode("1BWU");
    converter.run();
  }

  /*
  @Test
  void runWithNoNetworkOptions() throws Exception {
    String[] args = {
        "--no-network",
        "--cc", "/path/to/cc",
        "--pdb", "/path/to/pdb",
        "--output", "/path/to/output",
        "--format", "ttl",
        "wxyz"
    };

    testClass.run(args);

    verify(converter).noNetwork(true);
    verify(converter).ccPath("/path/to/cc");
    verify(converter).pdbPath("/path/to/pdb");
    verify(converter).outputFilePath("/path/to/output");
    verify(converter).outputFormat(OutputFormat.RDF_TURTLE);
    verify(converter).pdbCode("wxyz");
    verify(converter).run();
  }
   */
}

package org.glycoinfo.PDB2Glycan.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
//import java.util.List;

//import org.biojava.nbio.structure.io.mmcif.model.ChemComp;
//import org.biojava.nbio.structure.io.mmcif.model.ChemCompAtom;
//import org.biojava.nbio.structure.io.mmcif.model.ChemCompBond;
import org.glycoinfo.PDB2Glycan.exception.MMcifParseException;
import org.glycoinfo.PDB2Glycan.io.ChemicalComponentMMcifParser;
//import org.glycoinfo.PDB2Glycan.io.MMcifParser;
import org.glycoinfo.PDB2Glycan.io.PdbMMcifParser;
import org.glycoinfo.PDB2Glycan.io.model.ChemicalComponent;
import org.glycoinfo.PDB2Glycan.io.model.Protein;
//import org.glycoinfo.PDB2Glycan.io.model.protein.AtomSite;
//import org.glycoinfo.PDB2Glycan.io.model.protein.Entry;
//import org.glycoinfo.PDB2Glycan.io.model.protein.Struct;

public class TestMMcifParser {

	public static void main(String[] args) throws Exception{
		if (args.length < 1) {
			System.out.println("no cif files");
			return;
		}
		for (String file : args) {
			System.out.println("parsing " + file);
			String id = new File(file).getName().replaceAll("\\..*$", "");
			if ( id.length() == 4 )
				loadProtein(file);
			if ( id.length() == 3 )
				loadChemicalComponent(file);
			
		}

	}

	private static void loadProtein(String file) {
		PdbMMcifParser parser = new PdbMMcifParser();
		try {
			Protein protein = parser.parse(new FileInputStream(new File(file)));
			System.out.println(protein.getEntry().get(0).getId());
		} catch (IOException | MMcifParseException e) {
			e.printStackTrace();
		}
	}

	private static void loadChemicalComponent(String file) {
		ChemicalComponentMMcifParser parser = new ChemicalComponentMMcifParser();
		try {
			ChemicalComponent cc = parser.parse(new FileInputStream(new File(file)));
			System.out.println(cc.getChemComps().get(0).getId());
		} catch (IOException | MMcifParseException e) {
			e.printStackTrace();
		}
	}

	/*
	private static void parseCCDFile(String file) {
		MMcifParser parser = new MMcifParser();
		parser.addCategoryClassToLoad(ChemComp.class);
		parser.addCategoryClassToLoad(ChemCompAtom.class);
		parser.addCategoryClassToLoad(ChemCompBond.class);
		try {
			parser.parse(new FileInputStream(new File(file)));
			List<ChemComp> lStruct = parser.getCategoryClassObjects(ChemComp.class);
			for ( ChemComp cc : lStruct ) {
				System.out.println(cc.getId());
				System.out.println(cc.getType());
			}
			List<ChemCompAtom> lAtoms = parser.getCategoryClassObjects(ChemCompAtom.class);
			for ( ChemCompAtom a : lAtoms ) {
				System.out.println(String.format("%s\t%s",
						a.getComp_id(),
						a.getAtom_id()
					));
			}
			List<ChemCompBond> lBonds = parser.getCategoryClassObjects(ChemCompBond.class);
			for ( ChemCompBond b : lBonds ) {
				System.out.println(String.format("%s\t%s\t%s",
						b.getComp_id(),
						b.getAtom_id_1(),
						b.getAtom_id_2()
					));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MMcifParseException e) {
			e.printStackTrace();
		}
	}
	 */
}

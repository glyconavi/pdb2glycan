# PDB2Glycan

* This software outputs the result of analysis of glycan information about PDB's mmJSON and mmCIF format data.
* The results can be output as JSON, JSONLD, or Turtle format, and each file is gzip-compressed by default.

## Requirements
* Software development language: Java 1.8 or more
* Build tool: maven 3.5 or higher


## Build

    mvn clean compile assembly:single


## Usage

    $ bin/PDB2Glycan [--no-network --pdb <DIRECTORY> --cc <DIRECTORY> [--local-glytoucan <FILE>]]
                     [--force] [--nozip] [--dir <DIRECTORY>] --in <FORMAT> --out <FORMAT> <PDB_4_LETTER_CODE>

    $ bin/PDB2Glycan --for-onedep --input <FILE> --output <FILE> --cc <DIRECTORY>

### Options

| option | argument | description |
| ------ | -------- | ----------- |
| --cc              | DIRECTORY                     | Set path to the directory where PDB Chemical Component Dictionary is placed |
| -d, --dir         | DIRECTORY                     | Set path to output directory |
| -f, --force       |                               | Overwrite existing file |
| -h, --help        |                               | Show usage help |
| -i, --in          | FORMAT=[mmcif\|mmjson]        | Set input format |
| --local-glytoucan | FILE                          | Set path to the file of local mappings for WURCS to GlyTouCan ID |
| --no-network      |                               | Use local data, all of --cc <DIRECTORY>, --pdb <DIRECTORY> and --local-glytoucan <FILE> are required |
| --nozip           |                               | Do not zip output file |
| -o, --out         | FORMAT=[json\|jsonld\|turtle] | Set output format |
| --pdb             | DIRECTORY                     | Set path to the directory where PDB entry is placed |
| --for-onedep      |                               | Output simple results for OneDep system, all of --input <FILE>, --output <FILE> and --cc <DIRECTORY> are required, the other options are ignored even if specified |
| --input           | FILE                          | Set path to input PDB file, allowed only mmCIF format |
| --output          | FILE                          | Set path to output JSON file |


### ReleaseNote

* 2023/03/10: released ver 1.6.0

    * Changed MolWURCS version to 0.10.0
    * add mmcif for branch entities

    
#### Note

If you would like to use this program in an environment disconnected from the internet,
please generate ID mappings manually by using `bin/LocalGlyTouCanID` and pass the file to argument of `--local-glytoucan`.

    bin/LocalGlyTouCanID > glytoucan.tsv


### Example

Fetch data from remote

    bin/PDB2Glycan --in mmjson --out turtle 1BWU


Use local files

    bin/PDB2Glycan --no-network --pdb /path/to/mmjson/direcotry --cc /path/to/cc/directory --local-glytoucan /path/to/glytoucan.tsv --in mmjson --out jsonld 1BWU


Run for OneDep system

    bin/PDB2Glycan --for-onedep --input /path/to/input/mmcif/file.cif --output /path/to/output/json/file.json --cc /path/to/cc/directory


## Messages in data loading
### INFO

* ```Load <PDB file name> from <Host URL>```  
The PDB file is loaded from the remote host.

* ```<PDB code> has no glyco entries```  
There is no glyco entries in the PDB data.

* ```<PDB code> finished in <time>```  
Processing the PDB data is finished in the time.

* ```Load <CCD file name> from <Host URL>```  
The CCD file is loaded from the remote host.

* ```GET <URL of WURCS2GlyTouCan api>```  
The GET process which run WURCS2GlyTouCan api is started.

* ```Successfully loaded <file path of GlyTouCanID.tsv>```  
The GlyTouCanID.tsv file is successfully loaded.

### WARN

* ```<connection error message>```  
```Retry after <time> [s]```  
A connection error is occured during remote access for finding CCD or PDB file from PDBj remote service.
The connection will be re-tried until data is found or the process is stopped due to the other error.

* ```CCD file with format "<used format>" different from the input format "<input format>" is used for "<shaccaride id in CCD>"```  
The CCD file format different from the input format is used.
It is happen when the CCD file for the given shaccaride ID with the input format could not find, but the other file with the different format is found.

* ```Failed to optain remote resource, use default data```  
An error is occured when CCD files are not optained from remote resource. Default CCD data is used when it is occured.

### ERROR

* ```Error validating the structure - <error message>```  
An error is occured in the validation of glycan structure using GlycomeDB validation is found.

* ```<error message>```  
An error in loading or parsing input files.

## VALIDATION REPORT

* ```atom_id duplication: <既に見つかっていた原子の UniqueCode>#<新しく見つかった原子の UniqueCode>```  
一つの asym 内に同じ atom_site.label_atom_id を持つ原子が存在する場合に書き込まれる。MMCIF でなく PDB フォーマットのファイルを読み込ませた、コーディングのミス、想定されていない分子が与えられた可能性がある。


* ```Dummy atom: in model:<atom_site.pdbx_PDB_model_num>;asym:<atom_site.label_asym_id>+";comp:<atom_site.label_comp_id> as <追加された原子の UniqueCode>;```  
ある糖において、同一分子中にあるべき原子が無く、別分子にもそれらしき原子がないため、リファレンス分子との重ね合わせによって存在しない原子が追加された場合に書き込まれる。追加された原子の atom_site.label_asym_id は dummyXX （XX は追加先の atom_site.label_asym_id）、atom_site.label_comp_id, atom_site.label_entity_id, atom_site.auth_comp_id, atom_site.auth_asym_id, <atom_site.auth_seq_id は null になっている。


* ```Possibly missing atom: (Because multiple atoms are missing, I coundn't assign atoms correctly.) <atom_site.label_atom_id> in model:<atom_site.pdbx_PDB_model_num>;asym:<atom_site.label_asym_id>;comp:<atom_site.label_comp_id>```  
ある糖において、同一分子中にあるべき原子が無く、別分子に存在するそれらしき原子との結合が登録されているが、その原子は別の糖にも使われている場合に書き込まれる。


* ```Missing atom: <atom_site.label_atom_id> in model:<atom_site.pdbx_PDB_model_num>;asym:<atom_site.label_asym_id>+";comp:<atom_site.label_comp_id>```  
ある糖において、同一分子中にあるべき原子が無く、別分子にもそれらしき原子がない場合に書き込まれる。


* ```Atom construction error: <atom_site.label_atom_id> in model:<atom_site.pdbx_PDB_model_num>;asym:<atom_site.label_asym_id>+";comp:<atom_site.label_comp_id>```  
ある糖において、同一分子中にあるべき原子が無く、別分子にもそれらしき原子がない、かつ同一分子中に存在する原子の数が少なく、リファレンス分子との重ね合わせによる原子の追加ができない場合書き込まれる。


* ```Atoms too close( < 1.0): distance:<原子間の距離>#<結合の一方にある Atom の UniqueCode>#<結合の他方にある Atom の UniqueCode>```  
多糖内のある原子とある原子の距離が近すぎる場合に書き込まれる。実験機器の性能により原子の位置が異常な状況になっている場合、重ね合わせルーチンでの計算間違い、特殊な処理により別の種類の原子が結合しているため（たとえば酸素の代わりに窒素など）「存在しない」とみなされて原子が追加された可能性がある。


* ```Possibly different anomer: <O 原子と結合している C 原子の UniqueCode>```  
Chemical Component Dictionary に登録されているアノマーと異なると思われる場合に書き込まれる。


* ```Bond from pdbx_validate_close_contact: <結合の一方にある Atom の UniqueCode>#<結合の他方にある Atom の UniqueCode>```  
mmcif の struct_conn セクションではなく pdbx_validate_close_contact セクションの情報から結合が作られた場合に書き込まれる。


* ```Dummy bond: <結合の一方にある Atom の UniqueCode>#<結合の他方にある Atom の UniqueCode>#<Bond Order>```  
存在しない原子が追加された際にその原子に対する結合も（Chemical Component Dictionary に基づいて）追加された場合に書き込まれる。


* ```Long bond:  <結合の一方にある Atom の UniqueCode>#<結合の他方にある Atom の UniqueCode>#<Bond Order>##entry:結合長#threshold:<長いとみなす閾値（現在のところ二つの原子の VDW 合計）>```  
結合している原子間の距離が異常に長い場合に書き込まれる。


### UniqueCode とは
atom_site.label_atom_id;atom_site.label_comp_id;atom_site.label_asym_id;atom_site.label_entity_id;atom_site.label_seq_id;atom_site.auth_comp_id;atom_site.auth_asym_id;atom_site.auth_seq_id;atom_site.pdbx_PDB_model_num;atom_site.pdbx_PDB_ins_code;
である。  
atom_site.pdbx_PDB_ins_code は null である場合は存在しない。  

